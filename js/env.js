(function (window) {
  window.__env = window.__env || {};

  window.__env.defaultLanguage = 'en';
  // to enable placementTest module
  // set moduleNumber to placementTest
  window.__env.moduleNumber = '1';

  // setting this to false will disable
  // set to true for local environment
  window.__env.enableDebug = true;
}(this));
