app.filter("localization", ['localizationService', function (localizationService) {
    return function (key, lang, args, collection) {
        if (key) {
            if (!collection) {
                var resources = localizationService.getResources(lang);
                key = key.toUpperCase();
                var resource = resources[key];

                if (args) {
                    for (var i = 0; i < args.length; i++) {
                        var arg = args[i];
                        if(resource)
                        resource = resource.replace('{' + i + '}', arg);
                    }
                }

                return resource;
            } else {
                if (lang == 'en')
                    return key;
                else
                    return collection;
            }
        }
    }
}]);





