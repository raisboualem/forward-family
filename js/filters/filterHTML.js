app.filter("sanitize", ['$sce', function ($sce) {
    return function (htmlCode) {
        if (angular.isString(htmlCode))
            return $sce.trustAsHtml(htmlCode);
    }
}]);

app.filter('debug', function () {
    return function (input) {
        if (input === '') return 'empty string';
        return input ? input : ('' + input);
    };
});

app.filter('capitalize', function() {
  return function(input, scope) {
    if (input!=null)
    input = input.toLowerCase();
    return input.substring(0,1).toUpperCase()+input.substring(1);
  }
});