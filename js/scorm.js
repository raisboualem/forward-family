//Include the standard ADL-supplied API discovery algorithms
var findAPITries = 0;

//Constants
var SCORM_NO_ERROR = "0";

//Since the Unload handler will be called twice, from both the onunload
//and onbeforeunload events, ensure that we only call Terminate once.
var terminateCalled = false;

//Track whether or not we successfully initialized.
var initialized = false;

//TODO
var interactionCnt = 0;

function findAPI(win) {
    // Check to see if the window (win) contains the API
    // if the window (win) does not contain the API and
    // the window (win) has a parent window and the parent window
    // is not the same as the window (win)
    while ((win.API == null) &&
    (win.parent != null) &&
    (win.parent != win)) {
        // increment the number of findAPITries
        findAPITries++;

        // Note: 7 is an arbitrary number, but should be more than sufficient
        if (findAPITries > 7) {
            console.log("Error finding API -- too deeply nested.");
            return null;
        }

        // set the variable that represents the window being
        // being searched to be the parent of the current window
        // then search for the API again
        win = win.parent;
    }
    return win.API;
}

function getAPI() {
    // start by looking for the API in the current window
    var theAPI = findAPI(window);

    // if the API is null (could not be found in the current window)
    // and the current window has an opener window
    if ((theAPI == null) &&
        (window.opener != null) &&
        (typeof(window.opener) != "undefined")) {
        // try to find the API in the current window�s opener
        theAPI = findAPI(window.opener);
    }
    // if the API has not been found
    if (theAPI == null) {
        // Alert the user that the API Adapter could not be found
         console.log("Unable to find an API adapter");
    }
    return theAPI;
}

//Create function handlers for the loading and unloading of the page

//Constants
var SCORM_TRUE = "true";
var SCORM_FALSE = "false";

//Since the Unload handler will be called twice, from both the onunload
//and onbeforeunload events, ensure that we only call LMSFinish once.
var finishCalled = false;

//Track whether or not we successfully initialized.
var initialized = false;

var API = null;

var ISpeakLangFromDB = null;
var SystemLangFromDB = null;
var FontSizeFromDB = null;
var AudioCaptionFromDB = null;
var VideoCaptionFromDB = null;
var ImageCaptionFromDB = null;


function ScormProcessInitialize() {
    var result;

    API = getAPI();

    if (API == null) {
         console.log("ERROR - Could not establish a connection with the LMS.\n\nYour results may not be recorded.");
        return;
    }

    result = API.LMSInitialize("");

    if (result == SCORM_FALSE) {
        var errorNumber = API.LMSGetLastError();
        var errorString = API.LMSGetErrorString(errorNumber);
        var diagnostic = API.LMSGetDiagnostic(errorNumber);

        var errorDescription = "Number: " + errorNumber + "\nDescription: " + errorString + "\nDiagnostic: " + diagnostic;

         console.log("Error - Could not initialize communication with the LMS.\n\nYour results may not be recorded.\n\n" + errorDescription);
        return;
    }
    //Getting Settings Menu Option's value from DB
    var sysLangOpt = result.systemLang;
        if(sysLangOpt)
            SystemLangFromDB = sysLangOpt;

	var iSpeakLangOpt= result.maternalLang;
	    if(iSpeakLangOpt)
	    	ISpeakLangFromDB = iSpeakLangOpt;

    var fontOpt = result.fontSize;
        if(fontOpt != null)
            FontSizeFromDB = fontOpt;

    var audioCapOpt = result.audioCaption;
        if(audioCapOpt)
            AudioCaptionFromDB = audioCapOpt;

    var videoCapOpt = result.videoCaption;
        if(videoCapOpt)
            VideoCaptionFromDB = videoCapOpt;

    var imageCapOpt = result.imageCaption;
        if(imageCapOpt)
            ImageCaptionFromDB = imageCapOpt;

	initialized = true;

	var isAccessAllowed = ScormProcessGetValue("checkModuleAccessPermission",true);
        if(!isAccessAllowed)
        {
            alert("You are not allowed to access this module. Please contact your administrator!");
            window.top.close();
        }

}
function ScormProcessResetTimeOut () {
        var result = API.LMSResetTimeOut('RefreshSession',1);

        if(result == 'ok') {
            console.log('success');
        } else {
            console.log('failed');
        }
}

function ScormProcessCommit() {

    var result;

    //Don't terminate if we haven't initialized or if we've already terminated
    if (initialized == false || finishCalled == true) {
        return;
    }

    result = API.LMSCommit("");

    if (result == SCORM_FALSE) {
        var errorNumber = API.LMSGetLastError();
        var errorString = API.LMSGetErrorString(errorNumber);
        var diagnostic = API.LMSGetDiagnostic(errorNumber);

        var errorDescription = "Number: " + errorNumber + "\nDescription: " + errorString + "\nDiagnostic: " + diagnostic;

       // alert("Error - Could not commit communication with the LMS.\n\nYour results may not be recorded.\n\n" + errorDescription);
        return;
    }
    var code = result.statusCode;

        if (code == "-40") {
            document.cookie="statusValue=" + code;
    }
}

function ScormProcessFinish() {

    var result;

    //Don't terminate if we haven't initialized or if we've already terminated
    if (initialized == false || finishCalled == true) {
        return;
    }

    result = API.LMSFinish("");

    finishCalled = true;

    if (result == SCORM_FALSE) {
        var errorNumber = API.LMSGetLastError();
        var errorString = API.LMSGetErrorString(errorNumber);
        var diagnostic = API.LMSGetDiagnostic(errorNumber);

        var errorDescription = "Number: " + errorNumber + "\nDescription: " + errorString + "\nDiagnostic: " + diagnostic;

        alert("Error - Could not terminate communication with the LMS.\n\nYour results may not be recorded.\n\n" + errorDescription);
        return;
    }
}

function getUser() {
    var userName, userId;
    if (initialized) {
        userId = API.LMSGetValue('cmi.core.student_id');
        userName = API.LMSGetValue('cmi.core.student_name');
    }

    return {userId: userId, userName: userName};
}

//There are situations where a GetValue call is expected to have an error
//and should not alert the user.
function ScormProcessGetValue(element, checkError) {

    var result;

    if (initialized == false || finishCalled == true) {
        return;
    }

    result = API.LMSGetValue(element);

    if (checkError == true && result == "") {

        var errorNumber = API.LMSGetLastError();

        if (errorNumber != SCORM_NO_ERROR) {
            var errorString = API.LMSGetErrorString(errorNumber);
            var diagnostic = API.LMSGetDiagnostic(errorNumber);

            var errorDescription = "Number: " + errorNumber + "\nDescription: " + errorString + "\nDiagnostic: " + diagnostic;

            alert("Error - Could not retrieve a value from the LMS.\n\n" + errorDescription);
            return "";
        }
    }

    return result;
}

function ScormProcessSetValue(element, value) {

    var result;

    if (initialized == false || finishCalled == true) {
        return;
    }

    var result = API.LMSSetValue(element, value);
    var code = result.statusCode;

    if (code == "-40") {
        document.cookie="statusValue=" + code;
   }
}

function ScormProcessGetStatus(objectiveId, checkError) {
    var result;

    var objIndex = objectiveId;//FindObjectiveIndex(objectiveId);
    if (objIndex) {
        result = ScormProcessGetValue("cmi.objectives." + objIndex + ".status", checkError);
    }

    return result;
}

function ScormProcessSetStatus(objectiveId, status) {
    var result;
    var objIndex = objectiveId;//FindObjectiveIndex(objectiveId);

    if (status && objIndex) {
        result = ScormProcessSetValue("cmi.objectives." + objIndex + ".status", status);
    }

    return result;
}

function setLocation(location) {
    var result;

    if (location) {
        result = ScormProcessSetValue("cmi.core.lesson_location", location);
    }

    return result;
}

function FindObjectiveIndex(objectiveId) {
    var objCount = ScormProcessGetValue("cmi.objectives._count", true);

    for (var i = 0; i < objCount; i++) {
        var objId = ScormProcessGetValue("cmi.objectives." + i + ".id", true);

        if (objId == objectiveId) {
            return i;
        }
    }

    alert("ERROR - could not find objective: " + objectiveId);
}

function ScormProcessSetScore(objectiveId, rawScore) {
    var objIndex = objectiveId;//FindObjectiveIndex(objectiveId);
    if (rawScore && objIndex) {
        var min = 0, max = 100, raw = rawScore;
        ScormProcessSetValue("cmi.objectives." + objIndex + ".score.raw", rawScore);
        ScormProcessSetValue("cmi.objectives." + objIndex + ".score.min", min);
        ScormProcessSetValue("cmi.objectives." + objIndex + ".score.max", max);
    }
}

function ScormProcessGetSuspendState() {
    var suspendDataVar = 'cmi.suspend_data';

    var suspendData = ScormProcessGetValue(suspendDataVar);
    //TODO:: REMOVE
    //var suspendData = localStorage.getItem(suspendDataVar);

    return suspendData;
};

function ScormProcessSetSuspendState(suspendData) {
    var suspendDataVar = 'cmi.suspend_data';

    ScormProcessSetValue(suspendDataVar, suspendData);
    //TODO:: REMOVE
    //localStorage.setItem(suspendDataVar, suspendData);
};

var questionTypes = ['true-false', 'choice', 'fill-in', 'matching', 'performance', 'sequencing', 'likert', 'numeric'];

function getQuestionType(appQuestionType) {
    return 'fill-in';
}

function savePdf(data, fileName, wn){
    if(data){
        data = data.toString();
        API.LMSSavePdf(data, fileName, wn);
    }
}

function recordInteraction(data) {

    var id = data.id + 1,
        questionType = data.questionType,
        learnerResponse = data.learnerResponse,
        correctAnswer = data.correctAnswer,
        wasCorrect = data.wasCorrect,
        objectiveId = data.objectiveId;

    //Interactions are stored in a collection. We need to find the number of items currently in
    //the collection so we can store this question in the next bucket.
    //Since the collection indicies are 0-based, the count will be the identifier for the next
    //available bucket.
    nextIndex = interactionCnt; //ScormProcessGetValue("cmi.interactions._count", true);

    //question id must be the first item set
    ScormProcessSetValue("cmi.interactions." + nextIndex + ".id", id);

    //Associate this question with one of our four defined learning ojectives to enable
    //the LMS to provide detailed reporting
    ScormProcessSetValue("cmi.interactions." + nextIndex + ".objectives.0.id", objectiveId);

    //The question type should be set before detailed response information
    var scormQuestionType = getQuestionType(questionType);
    if (scormQuestionType)
        ScormProcessSetValue("cmi.interactions." + nextIndex + ".type", scormQuestionType);

    if (learnerResponse && learnerResponse != "") {
        //Record the response the learner gave (if one was provided)
        ScormProcessSetValue("cmi.interactions." + nextIndex + ".student_response", learnerResponse);
    }

    //Record the correct answer, even if it is the same as the learner response
    ScormProcessSetValue("cmi.interactions." + nextIndex + ".correct_responses.0.pattern", correctAnswer);

    //Finally record whether or not the learner's response was correct
    if (wasCorrect == true) {
        ScormProcessSetValue("cmi.interactions." + nextIndex + ".result", "correct");
    }
    else {
        ScormProcessSetValue("cmi.interactions." + nextIndex + ".result", "wrong");
    }

    interactionCnt = interactionCnt + 1;
}


/*
 Assign the processing functions to the page's load and unload
 events. The onbeforeunload event is included because it can be
 more reliable than the onunload event and we want to make sure
 that Terminate is ALWAYS called.
 */
//window.onload = ScormProcessInitialize;
window.onunload = ScormProcessFinish;
//window.onbeforeunload = ScormProcessFinish;
