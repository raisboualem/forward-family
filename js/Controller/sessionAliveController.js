app.controller('sessionAliveController', function ($scope, ngDialog, dataService) {
   $(document).keyup(function(e) {
     if (e.keyCode === 27) // esc
          window.top.close();
   });
   $scope.ok = function () {
          window.top.close();
   };
   $scope.init = function () {
       $scope.ds = dataService.getDataSource();
   }();
});