app.controller("whatsNewController", ['$rootScope', '$scope', '$state', '$stateParams', 'groupService', 'dataService',
    'localStorageService', 'xmlDataService', 'localStorageService', '$window', 'utilityService', '$timeout', '$compile','$filter', '$cookies', 'ngDialog',
    function (rootScope, scope, state, stateParams, groupService, dataService,
              localStorageService, xmlDataService, localStorageService, window,
              utilityService, $timeout, compile, filter, cookies, ngDialog) {


        scope.goToPage = function (pageNo) {
            if (scope.currentPageNo != pageNo)
                scope.currentPageNo = pageNo;

            scope.scrollToTop();

            scope.currentSection = scope.ds.whatsNewSections[pageNo - 1];

            state.go('selectedWhatsNew.content', {
                'pageNo': pageNo
            });
        };

        var menuUpdateHandler = scope.$on('menu-updated', function () {
            scope.storeMenu();
        });

        scope.replaceLI = function (ul) {
            var liList = ul.children;
            var divs = document.createElement('DIV');
            for (var i = 0; i < liList.length; i++) {
                var li = liList[i];
                var div = document.createElement('DIV');
                var span = document.createElement('SPAN');
                span.innerHTML = '•';
                span.className = 'liSpan'
                div.className = 'liContent';
                div.innerHTML = li.innerHTML;
                divs.appendChild(span);
                divs.appendChild(div);
            }

            ul.innerHTML = divs.innerHTML;
        };

        scope.manageLI = function (child) {
            var lists = child.getElementsByTagName('UL');

            for (var i = 0; i < lists.length; i++) {
                scope.replaceLI(lists[i]);
            }
        };

        scope.getNewHeight = function (element) {
            var newHeight = 0;
            var rect = element.getBoundingClientRect();

            if (element && rect.top) {
                var contentTop = scope.content.getBoundingClientRect().top;
                var relativeTop = rect.top - contentTop;
                newHeight = relativeTop + rect.height;
            }

            return newHeight;
        };

        scope.parseChild = function (parent, contentChildren) {
            scope.manageLI(parent);

            var mainContainer = parent.getElementsByClassName('mainContainer')[0];
            var snapShots = [];

            var contentChildren = mainContainer.children;
            var startHeight = 0;
            var pageSize = 1000;
            var lastSnapshot = 0;

            for (var i = 0; i < contentChildren.length; i++) {
                var contentChild = contentChildren[i];

                if (contentChild.className.indexOf('FixedHeader') != -1) {
                    var newHeight = scope.getNewHeight(contentChild, lastSnapshot);

                    if (newHeight)
                        startHeight = newHeight;
                }
                else {
                    var divChildren = contentChild.children[0] && contentChild.children[0].children;

                    if (divChildren) {
                        for (var j = 0; j < divChildren.length; j++) {
                            var divChild = divChildren[j];

                            if (divChild.className.indexOf('contentSpan') != -1) {
                                var spanChildren = divChild.children;

                                for (var k = 0; k < spanChildren.length; k++) {
                                    var spanChild = spanChildren[k];

                                    if (spanChild.className.indexOf('rules-block') != -1) {
                                        var newHeight = scope.getNewHeight(spanChild);

                                        if (newHeight) {
                                            if (newHeight - lastSnapshot < pageSize) {
                                                startHeight = newHeight;
                                            }else{
                                                //var firstRuleBlock = (parseInt(newHeight/pageSize))*pageSize;
                                                var firstRuleBlock = startHeight + 1;
                                                lastSnapshot = firstRuleBlock;
                                                snapShots.push(lastSnapshot);
                                                startHeight = newHeight; //Add second half to next page.
                                            }
                                        }
                                    }
                                    else if (spanChild.tagName.toLowerCase() == 'table') {//Handle tables
                                        var newHeight = scope.getNewHeight(spanChild);

                                        if (newHeight) {
                                            if (newHeight - lastSnapshot < pageSize) {
                                                startHeight = newHeight;
                                            } else {
                                                //Add snapshot so that it does not split any row
                                                var tableChildren = spanChild.children;
                                                for (var l = 0; l < tableChildren.length; l++) {
                                                    var tableChild = tableChildren[l];
                                                    if (tableChild.tagName.toLowerCase() == 'tbody') {
                                                        var tbodyChildren = tableChild.children;
                                                        for (var m = 0; m < tbodyChildren.length; m++) {
                                                            var tbodyChild = tbodyChildren[m];
                                                            var tcNewHeight = scope.getNewHeight(tbodyChild);

                                                            if (tcNewHeight) {
                                                                if (tcNewHeight - lastSnapshot < pageSize) {
                                                                    startHeight = tcNewHeight;
                                                                } else {
                                                                    lastSnapshot = startHeight + 1;//Add table row border
                                                                    snapShots.push(lastSnapshot);
                                                                    //Add remaining half table to new page
                                                                    startHeight = newHeight;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        var tcNewHeight = scope.getNewHeight(tableChild);

                                                        if (tcNewHeight) {
                                                            if (tcNewHeight - lastSnapshot < pageSize) {
                                                                startHeight = tcNewHeight;
                                                            } else {
                                                                lastSnapshot = startHeight;
                                                                snapShots.push(lastSnapshot);
                                                                //Add remaining half table to new page
                                                                startHeight = newHeight;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        var newHeight = scope.getNewHeight(spanChild);

                                        if (newHeight) {
                                            if (newHeight - lastSnapshot < pageSize) {
                                                startHeight = newHeight;
                                            } else {
                                                //Add snapshot until this block
                                                lastSnapshot = startHeight;
                                                snapShots.push(startHeight);

                                                if (spanChild.innerText)//Skip <br> tags to avoid empty blank page
                                                    startHeight = newHeight;
                                            }

                                        }
                                    }
                                }
                            } else {
                                var newHeight = scope.getNewHeight(divChild, lastSnapshot);

                                if (newHeight) {
                                    if (newHeight - lastSnapshot < pageSize) {
                                        startHeight = newHeight;
                                    } else {
                                        //Add snapshot until this block
                                        lastSnapshot = startHeight;
                                        snapShots.push(startHeight);

                                        if (divChild.innerText)//Skip <br> tags to avoid empty blank page
                                            startHeight = newHeight;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (lastSnapshot != startHeight)
                snapShots.push(startHeight);

            parent.snapshots = snapShots;
        };

        var menuSelectedHandler = scope.$on('menu-item-selected', function (e, data) {
            if (scope.currentPageNo != data.section)
                scope.currentPageNo = data.section;

            scope.ds.menuSelectionCompleted = false;

            if (scope.ds.whatsNewSections && Array.isArray(scope.ds.whatsNewSections)) {
                scope.currentSection = scope.ds.whatsNewSections[scope.currentPageNo - 1];
            } else {
                scope.currentSection = scope.ds.whatsNewSections;
            }

            if(scope.moduleId == '14' && scope.ds.mainMenuLinkLang == 'fr'){
                if(scope.currentSection.TITLE2.ID == 'usefulexpressions'){
                    if(scope.currentSection.SUBSECTIONS.SUBSECTION[3].TITLE4.ID =='expressingfeelingemotions'){
                        $("table:eq(4)> tbody> tr> td").addClass("customTableWidth");
                    }

                }
            }

            scope.nav = data.nav;

            if(scope.nav === undefined){
                scope.scrollToTop();
            }

            state.go('selectedWhatsNew.content', {
                'pageNo': data.section,
                'nav': data.nav
            });

             scope.menuSelectedTimeout = $timeout(function () {
                scope.$broadcast('focusSelectedElement', data.nav);
            },100);

        });

        scope.goToStudySection = function () {
            var studySectionId = angular.copy(scope.ds.studySectionId);
            scope.ds.studySectionId = null;

            if (rootScope.inProgress) {
                // utilityService.refreshSuspendData(scope.moduleId);
                var suspendData = JSON.stringify(scope.ds.suspendData);
                ScormProcessSetSuspendState(suspendData);
                ScormProcessCommit();
                rootScope.inProgress = false;
            }

            state.go('selectedStudySection', {
                'moduleId': scope.moduleId,
                'studySectionId': studySectionId
            });
        };

        scope.pdfPageRendered = function () {
            scope.pagesRendered = scope.pagesRendered + 1;
            if (scope.pagesRendered < scope.totalPages) {
                scope.doc.addPage();
                scope.scanPdfSection();
            } else if (scope.pagesRendered == scope.totalPages) {
                scope.content.innerHTML = "";

                if (rootScope.isSafari) {
                    var pdfBlob = scope.doc.output('datauristring');
                    savePdf(pdfBlob, scope.filename, scope.wn);
                    //scope.doc.output('dataurlnewwindow');
                } else {
                    scope.doc.save(scope.filename, 'dataurlnewwindow');
                }
            }
        };


        var onCanvasImgLoad = function () {
            var canvas1 = document.createElement('CANVAS');
            canvas1.width = this.width;
            canvas1.height = 900;
            var pageWidth = this.width;
            var pageHeight = 1000;
            var snapshots = this.snapshots;
            var ctx = canvas1.getContext("2d");

            for (var j = 0; j < snapshots.length; j++) {
                //Clear canvas before redraw
                scope.pdfPageNo += 1;

                ctx.clearRect(0, 0, canvas1.width, canvas1.height);
                var clipY = j > 0 ? parseInt(snapshots[j - 1]) : 0;
                var clipHeight = j > 0 ? snapshots[j] - snapshots[j - 1] : snapshots[j];
                clipHeight = clipHeight < 1000 ? clipHeight : 1000;
                pageHeight = clipHeight;
                canvas1.height = clipHeight;

                if (clipHeight) {
                    ctx.drawImage(
                        this,
                        0, clipY, pageWidth, clipHeight,
                        0, 0, pageWidth, pageHeight
                    );

                    scope.doc.addImage(canvas1, 'jpeg', 27, 20);

                    //Add footer
                    scope.doc.setFontSize(10);
                    scope.doc.text(195, 600, '© 2016 LRDG Inc.');
                    scope.doc.text(417 - scope.pdfPageNo.toString().length, 600, scope.pdfPageNo.toString());
                    scope.doc.text(157, 615, 'All Rights Reserved/Tous droits réservés');

                    if (j < snapshots.length - 1)
                        scope.doc.addPage();
                }
            }

            scope.pdfPageRendered();
        };

        var preLoadPdf = function (printElement) {
            var html2obj = html2canvas(printElement, {'logging': false, 'useCORS': true});

            html2obj.preload({
                'htmlObj': html2obj, 'printElement': printElement, 'complete': function (data) {
                    var html2obj = data.options.htmlObj;
                    var printElement = data.options.printElement;
                    var queue = html2obj.parse(data);
                    var canvas = html2obj.render(queue);
                    var img = document.createElement('IMG');
                    img.snapshots = printElement.snapshots;
                    var imgSrc = canvas.toDataURL();
                    img.src = imgSrc;
                    img.onload = onCanvasImgLoad;
                    printElement.style.display = 'none';
                }
            });
        }

        scope.printFile = function (printFile) {
            scope.ds.printWhatsNewSections = [];
            var content = scope.content = document.getElementById('newContent');
            var footer = document.getElementById('footer');

            if (rootScope.isSafari) {
                scope.wn = window.open('', '_blank');
                scope.wn.document.writeln('Please wait...');
            }

            scope.pagesRendered = 0;
            scope.pdfPageNo = 0;

            scope.scanForSections();
            scope.totalPages = Array.isArray(scope.ds.printWhatsNewSections) ? scope.ds.printWhatsNewSections.length : 1;
            var doc = scope.doc = new jsPDF({unit: 'px', format: 'a4'});
            scope.scanPdfSection();
        };

        scope.scanForSections = function () {
            if (Array.isArray(scope.ds.whatsNewSections)) {
                var sections = scope.ds.whatsNewSections;
                var copySections = sections.slice(0);
                var newSectionsCnt = 0;
                var maxSubsections = 10;

                for (var i = 0; i < sections.length; i++) {
                    var subSections = sections[i].SUBSECTIONS.SUBSECTION;
                    var result = scope.scanSubsections(subSections, maxSubsections, copySections, newSectionsCnt, i);
                    copySections = result.copySections;
                    newSectionsCnt = result.newSectionsCnt;
                }

                scope.ds.printWhatsNewSections = copySections;
            } else {
                var newSectionsCnt = 0;
                var maxSubsections = 20;
                var copySections = [angular.copy(scope.ds.whatsNewSections)];
                var subSections = copySections[0].SUBSECTIONS.SUBSECTION;
                var result = scope.scanSubsections(subSections, maxSubsections, copySections, newSectionsCnt, 0);
                copySections = result.copySections;
                scope.ds.printWhatsNewSections = copySections;
            }
        };


        scope.scanSubsections = function (subSections, maxSubsections, copySections, newSectionsCnt, sectionIndex) {

            if (Array.isArray(subSections) && subSections.length > maxSubsections) {
                var newSections = [];
                for (var j = 0; j <= subSections.length / maxSubsections; j++) {
                    var start = maxSubsections * j;
                    var end = maxSubsections * (j + 1) < subSections.length ? maxSubsections * (j + 1) : subSections.length;
                    var newSubSections = subSections.slice(start, end);

                    var section = {SUBSECTIONS: {SUBSECTION: newSubSections}};
                    newSections.push(section);
                }

                var replaceSectionIndex = sectionIndex + newSectionsCnt;
                var replaceSection = copySections[replaceSectionIndex];

                angular.forEach(replaceSection, function (value, key) {
                    if (key != 'SUBSECTIONS') {
                        newSections[0][key] = value;
                    }
                });

                copySections.splice.apply(copySections, [replaceSectionIndex, 1].concat(newSections));
                newSectionsCnt = newSectionsCnt + newSections.length - 1;
            }

            return {'copySections': copySections, 'newSectionsCnt': newSectionsCnt};
        };

        scope.scanPdfSection = function () {
            var pageNo = scope.pagesRendered + 1;
            scope.content.innerHTML = "";
            var contentTemplate = "<div whats-new-content page-no='" + pageNo +
                "' module-id='" + scope.moduleId + "' whats-new-id='" + scope.selectedWhatsNew.id + "'></div>";
            var element = compile(contentTemplate)(scope)[0];
            var content = scope.content;
            content.appendChild(element);

            scope.scanPdfTimeout = $timeout(function () {
                var whatsNewContent = document.getElementsByClassName('whatsNewContentContainer')[1];

                content.style.width = "700px";

                if (rootScope.isSafari)
                    whatsNewContent.style.height = parseInt(whatsNewContent.clientHeight) + 1 + 'px';

                var printElement = content.children[0];
                var filenamePrefix = scope.moduleId == 0 || scope.moduleId == 99 ?
                    (rootScope.isFrenchModule ? scope.currentSection.TITLE1.FR : scope.currentSection.TITLE1.EN) :
                    scope.selectedWhatsNew.title;

                scope.filename = rootScope.isFrenchModule ? "FSL-" + "Module-" + stateParams.moduleId + (filenamePrefix != '' ? "-" + filenamePrefix + '.pdf' : '') :
                "ESL-" + "Module-" + scope.moduleId + (filenamePrefix != '' ? "-" + filenamePrefix + '.pdf' : '');

                scope.filename = scope.filename.replace('/', '-');

                scope.parseChild(printElement);
                preLoadPdf(printElement);
            }, 1000);
        };

        scope.storeMenu = function () {
            var jsonToStringWhatsNew = JSON.stringify(scope.menu);
            localStorageService.setItem('whats-new-menu_' + scope.selectedWhatsNew.id, jsonToStringWhatsNew);
        };

        scope.whatsNewFetched = function (whatsNew) {
            scope.whatsNew = whatsNew;
            scope.pdfPrintFile = scope.getPrintFile();
            scope.sections = scope.whatsNew.SECTIONS.SECTION;
            scope.ds.whatsNewSections = scope.sections;
            scope.currentPageNo = "1";

            var menu = localStorageService.getItem('whats-new-menu_' + scope.selectedWhatsNew.id);
            if (menu) {
                scope.menu = JSON.parse(menu);
                scope.menu.isInit = true;
            } else {
                scope.addNodeLevels(scope.whatsNew.MENU.ITEMS.ITEM, 1);
                scope.menu = scope.whatsNew.MENU;

                if (!Array.isArray(scope.menu.ITEMS.ITEM)) {
                    scope.menu.ITEMS.ITEM = [scope.menu.ITEMS.ITEM];
                }

                scope.menu.isPanelOpened = true;
                scope.menu.isInit = true;
            }

            if (Array.isArray(scope.sections)) {
                scope.totalPageNo = scope.sections.length;
            }
            else {
                scope.totalPageNo = 1;
            }

            var terms = [];
            var termGroup = scope.whatsNew.DICTIONARY ? scope.whatsNew.DICTIONARY.TERMGROUP : null;

            if (termGroup) {
                if (Array.isArray(scope.whatsNew.DICTIONARY.TERMGROUP)) {
                    for (var i = 0; i < termGroup.length; i++) {
                        terms = terms.concat(termGroup[i].TERM);
                    }
                } else {
                    terms = scope.whatsNew.DICTIONARY.TERMGROUP.TERM;
                }
            }

            scope.ds.terms = terms;

            if (scope.ds.whatsNewSections && Array.isArray(scope.ds.whatsNewSections)) {
                scope.currentSection = scope.ds.whatsNewSections[0];
            } else {
                scope.currentSection = scope.ds.whatsNewSections;

            }


            var pageNo = stateParams.page ? stateParams.page : 1;
            state.go('selectedWhatsNew.content', {
                'pageNo': pageNo
            });
        };

        scope.addNodeLevels = function (items, level) {
            if (items) {
                if (Array.isArray(items)) {
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.level = level;

                        if (item.ITEMS) {
                            var newLevel = level == 4 ? level : level + 1;
                            scope.addNodeLevels(item.ITEMS.ITEM, newLevel);
                        }
                    }
                } else {
                    items.level = level;
                    if (items.ITEMS) {
                        var newLevel = level == 4 ? level : level + 1;
                        scope.addNodeLevels(items.ITEMS.ITEM, newLevel);
                    }
                }
            }
        };

        scope.scrollToTop = function () {
            var content = document.getElementById('content');
            content.scrollTop = 0;
        };

        scope.fetchWhatsNewFromXML = function (selectedWhatsNew) {
            xmlDataService.getXMLData(selectedWhatsNew.filename).then(function (data) {
                var json = xmlDataService.parseXmlToJSON(data);
                var whatsNew = json.WHATSNEW;
                var jsonToStringWhatsNew = JSON.stringify(whatsNew);

                localStorageService.setItem('whatsNew_' + scope.selectedWhatsNew.id, jsonToStringWhatsNew);


                scope.whatsNewFetched(whatsNew);
            }, function () {
                console.log('Cant load dialogue xml' + scope.selectedWhatsNew.filename);
            });
        };

        scope.getPrintFile = function () {
            if (scope.whatsNew.MENU) {
                var pdfFile = utilityService.getAssetsCommonPath(scope.selectedWhatsNew.path);
                pdfFile = pdfFile + '/' + scope.whatsNew.MENU.PRINT;
                return pdfFile;
            }
        };

        scope.togglePanel = function () {
            scope.menu.isPanelOpened = !scope.menu.isPanelOpened;
            scope.menu.isInit = false;
            scope.storeMenu();
        };

        scope.setSize = function () {
            var height = window.innerHeight;
            scope.contentHeight = height;
        };

        scope.isArray = function (obj) {
            return Array.isArray(obj);
        };

        window.onresize = function () {
                    scope.$apply(function () {
                        scope.panelHeight = scope.getPanelHeight();
                    });
                };

         var accessChangeHandler = scope.$on('accessibilityChanged', function () {
             $timeout(function () {
                 scope.panelHeight = scope.getPanelHeight();
             }, 1000);
         });

          scope.getPanelHeight = function () {
            var mainCon = document.getElementById('mainContent');
            var mainConOffset = mainCon ? mainCon.offsetTop : 0;

             return window.innerHeight - mainConOffset;

          };

          scope.$on('$destroy', function(){
            $timeout.cancel( scope.panelHeightTimeout );
            $timeout.cancel( scope.changeLangTimeout );
            $timeout.cancel( scope.scanPdfTimeout );
            $timeout.cancel( scope.menuSelectedTimeout );
            menuUpdateHandler();
            menuSelectedHandler();
            accessChangeHandler();
          });

        scope.init = function () {
            var whatsNewId = stateParams.whatsNewId;
            var sectionNo = stateParams.section;
            var moduleId = stateParams.moduleId;
            scope.ds.mainTitle = 'WHATS_NEW';

            scope.selectedWhatsNew = groupService.getItem(whatsNewId, moduleId);
            var title = filter('localization')('WHATS_NEW',scope.ds.language);
            utilityService.setPageTitle(title+' - '+scope.selectedWhatsNew.title);

            scope.moduleId = parseInt(stateParams.moduleId);
            scope.nav = null;
            var whatsNewStatus = utilityService.getSuspendDataStatus(whatsNewId);
            ScormProcessSetStatus(whatsNewId, "completed");

            if (whatsNewStatus != "success") {
                utilityService.setSuspendDataStatus(scope.selectedWhatsNew.id, 'success');
            }
            utilityService.checkForSessionAlive();

            scope.ds = dataService.getDataSource();
            scope.ds.communicativeSkill = 'whatsNew';
            scope.sectionsCompleted = 1;

            angular.element(window).bind('resize', function () {
                scope.$apply(function () {
                    scope.setSize();
                });
            });

            scope.setSize();

            window.scroll(0, 0);

            if (scope.selectedWhatsNew) {
                rootScope.inProgress = true;

                //Fetch dialogue
                var whatsNew = localStorageService.getItem('whatsNew_' + scope.selectedWhatsNew.id);

                if (whatsNew) {
                    var jsonWhatsNew = JSON.parse(whatsNew);
                    scope.whatsNewFetched(jsonWhatsNew);
                } else {
                    scope.fetchWhatsNewFromXML(scope.selectedWhatsNew);
                }

                 scope.panelHeightTimeout = $timeout(function () {
                     scope.panelHeight = scope.getPanelHeight();
                 }, 500);

            }
            //else {
            //    location.path('/');
            //}
        }();
    }]);