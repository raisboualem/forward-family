app.controller('mainController', ['$rootScope', '$scope', '$state', 'ngDialog', 'utilityService',
    'dataService', '$location', '$cookies', '$window', 'Idle','$timeout','$window', 'env', 'Keepalive',
    function (rootScope, scope, state, ngDialog, utilityService, dataService, location, cookies, window, Idle, timeout, $window, env, Keepalive) {

        scope.showResumeModal = function (resumeLocation) {
            var model = ngDialog.open({
                template: 'templates/modal/multipleTabs.html',
                 controller: 'multipleTabsController'
            });

            model.closePromise.then(function (event) {
                if (event.value == true) {
                    scope.ds.isResumedFromModal = true;
                    var indexOfPage = resumeLocation.indexOf("page");
                    var resumeLocationDescriptionPage = resumeLocation.substring(0,indexOfPage - 1); //Setting Resume Location to the Activity Description Page to avoid loss of answers
                    var model = ngDialog.open({
                        template: '<img src="images/modules_spinner.gif" alt="loading"></img>',
                        plain: true,
                        showClose: false,
                        closeByEscape: false
                    });
                    location.path(resumeLocationDescriptionPage);
                } else {
                    utilityService.removeLastAccessLocation();
                }
            });
        };

        scope.showMultipleTabText = function() {
                 var model = ngDialog.open({
                 template: 'templates/modal/multipleTabs.html',
                 controller: 'multipleTabsController'
             });
        };

       function IsNewTab() {
         return cookies.get('TabOpen');
       }

       $(function() {
         if (!IsNewTab()) {
           cookies.put('TabOpen', "YES", {path: '/'});
           $(window).unload(function() {
             cookies.remove('TabOpen', {path: '/'});
           });
         } else {
          scope.showMultipleTabText();
         }
       });


        scope.closeModals = function() {
           if (scope.warning) {
             ngDialog.close(null, false);
           }

           if (scope.timedout) {
            ngDialog.close(null, false);
             scope.timedout = null;
           }
         };

         scope.$on('IdleStart', function() {
           scope.closeModals();

           scope.warning = ngDialog.open({
             templateUrl: 'templates/modal/warning-dialogue.html',
             controller: 'timeoutWarningController'
           });
         });

         scope.$on('IdleEnd', function() {
           scope.closeModals();
           ScormProcessResetTimeOut();
        });

         scope.$on('IdleTimeout', function() {
           scope.closeModals();
           scope.timedout = ngDialog.open({
             templateUrl: 'templates/modal/timedout-dialogue.html',
             controller: 'timeoutWarningController',
             showClose: false,
             ariaAuto: true,
             closeByEscape: false
           });
           cookies.remove('html5_open',{'path': '/'});
           //console.log(cookies.get('html5_open'));
         });

        scope.init = function () {
            scope.ds = dataService.getDataSource();

            if (env.enableDebug) {
                cookies.put('languageCookie', env.defaultLanguage);
                cookies.put('moduleCookie', env.moduleNumber);
            };

           var lang = cookies.get('languageCookie');
            rootScope.isLearnerRole = cookies.get('userRoleCookie') == 1;

            rootScope.isFrenchModule = lang.toLowerCase() == 'fr';
            rootScope.isSpanishModule = lang.toLowerCase() == 'es';
            rootScope.isChineseModule = lang.toLowerCase() == 'zh';

            scope.ds.mainMenuLinkLang = rootScope.isFrenchModule ? 'fr' : 'en';
            scope.ds.oppLang = rootScope.isFrenchModule ? 'en' : 'fr';

            if (!scope.ds.language) {
                scope.ds.language = lang ? lang.toLowerCase() : 'en';
            }

            if (scope.ds.mainMenuLinkLang == 'en'){
                rootScope.sysLang = 0;
            } else if (scope.ds.mainMenuLinkLang == 'fr'){
                rootScope.sysLang = 1;
            }

            rootScope.language = 'module-' + scope.ds.language;

            var user = getUser();
            scope.user = user;

            var module = cookies.get('moduleCookie');
            scope.moduleId = module;

            if (module == 99) {
                scope.menuText = rootScope.isFrenchModule ? "Offre active de services+" : "Active Offer of Service+";
            } else if (module == 199)  {
                 scope.menuText = "TEFAQ";
            } else{
                scope.menuText =  module;
            }

            rootScope.isUnits = (scope.moduleId == 99 || scope.moduleId == 0 || scope.moduleId == 199);

            if (rootScope.isUnits) {
                if (!scope.ds.communicativeSkill)
                    scope.ds.communicativeSkill = 'unit1';
            } else {
                if (!scope.ds.communicativeSkill)
                    scope.ds.communicativeSkill = 'dialogues';
            }

            state.go('groups', {
                'moduleId': scope.moduleId,
				'selectedUnit': 'activities'
            });

            angular.element(window).bind('beforeunload', function () {
                if (scope.ds.suspendData) {
                    var path = location.path();
                    utilityService.saveData(path, scope.ds);
                }

                ScormProcessFinish();
                cookies.remove('html5_open',{'path': '/'});
                //console.log(cookies.get('html5_open'));
            });

            angular.element(window).bind('load', function () {
                if (!initialized) {
                    ScormProcessInitialize();
                    var suspendStateData = ScormProcessGetSuspendState();
                    if (suspendStateData) {
                        var jsonData = JSON.parse(suspendStateData);
                        scope.ds.suspendData = jsonData;

                        //rootScope.selectedBodyClass = cookies.get('theme'] ? cookies.get('theme'] : 'silver-blue';
                        rootScope.fontSize =  parseInt(FontSizeFromDB) ?  parseInt(FontSizeFromDB) : 2;
                        rootScope.audioCaptionControl =  parseInt(AudioCaptionFromDB) ?  parseInt(AudioCaptionFromDB) : 0;
                        rootScope.videoCaptionControl =  parseInt(VideoCaptionFromDB) ?  parseInt(VideoCaptionFromDB) : 0;
                        rootScope.imageCaptionControl =  parseInt(ImageCaptionFromDB) ?  parseInt(ImageCaptionFromDB) : 0;
                        //rootScope.accessibilityModeControl = cookies.get('accessibilityMode') ? cookies.get('accessibilityMode') : 0;
                        rootScope.sysLang =  parseInt(SystemLangFromDB) ?  parseInt(SystemLangFromDB) : 0;
                        cookies.remove('statusValue');

						if(rootScope.sysLang == 0){
                            scope.ds.language = 'en';
                        }
                        if(rootScope.sysLang == 1){
                            scope.ds.language = 'fr';
                        }
                        if(rootScope.sysLang == 2){
                            scope.ds.language = 'es';
                        }
                        if(rootScope.sysLang == 3){
                            scope.ds.language = 'zh';
                        }

						if(ISpeakLangFromDB != null)
						{
							rootScope.translateLang = parseInt(ISpeakLangFromDB);
						}
						else{
							rootScope.translateLang = 0;
							var modalInstance = ngDialog.open({
							templateUrl: 'templates/modal/popup.html',
							controller: 'popupController',
							});
						}
						if(rootScope.isInternetExplorer){
						 var isIEPopup = cookies.get('IEPopup')?cookies.get('IEPopup'):'';
                         var isIEPopupDiff = 'IEPopup-' + scope.moduleId +'-' + lang;
                         var popupValue = isIEPopup+'_'+isIEPopupDiff;
                         var resultCookie = isIEPopup.split("_");
                         if(resultCookie.indexOf(isIEPopupDiff) == -1){
                             cookies.put('IEPopup',popupValue);
                             var modalInstance = ngDialog.open({
                                 templateUrl: 'templates/modal/internetExplorerPopup.html',
                                 controller: 'internetExplorerPopupController',
                                 closeByEscape: false,
                                 showClose: false
                             });
                         }
						}

                        //Set accessibility settings from cookie to make sure those settings are unified for all the modules.
                        utilityService.setAccessibilityData();

                        //Check for last location and popup to resume
                        var lastAccessLocation = scope.ds.suspendData.lastAccessLocation;
                        if (scope.ds.suspendData &&
                            lastAccessLocation) {
                            scope.showResumeModal(lastAccessLocation);
                        }
                    }
                }
                cookies.put('html5_open', '1', {'path': '/'});
                //console.log(cookies.get('html5_open'));
            });

            scope.$on('recordInteraction', function (event, data) {
                recordInteraction(data);
            });

            Idle.watch();
        }();

        scope.goToMainContent = function () {
            utilityService.goToMainContent();
        };

        scope.stopQuizAudio = function () {
            if(scope.ds.sound){
               scope.ds.sound.stop();
            }
        };

        scope.open = function () {
            ngDialog.open({
                template: 'templates/modal/accessibility.html',
                controller: 'accessibilityController',
                className: 'ngdialog-theme-default ngdialog-theme-custom'
            });
        };

        scope.setLanguage = function (lang) {
            timeout(function () {
            scope.ds.language = lang;
            var mainHTML = window.parent.document.getElementsByTagName('html')[0];
            if(mainHTML) {
                mainHTML.setAttribute('lang',lang);
                mainHTML.setAttribute('xml:lang',lang);
            }
            }, 100);
        };

        scope.placementTestEnabled = true;

        if (scope.moduleId === 'placementTest') {
            scope.placementTestEnabled = false;
        }

    }]);
