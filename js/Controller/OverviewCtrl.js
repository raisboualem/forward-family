app.controller('OverviewCtrl',
    ['$rootScope', '$scope', 'dataService', '$timeout', 'xmlDataService','interactionFactory',
        '$sce', '$stateParams', '$state', 'localStorageService', 'utilityService', '$location', 'groupService', '$window', '$compile', '$filter','$cookies', 'ngDialog',
        function (rootScope, scope, dataService, $timeout, xmlDataService, interactionFactory,
                  $sce, $stateParams, state, localStorageService, utilityService, location, groupService, $window, compile, $filter, cookies, ngDialog) {
        	//all what we need to be integrated
        	//console.log(scope.activity);
        	//console.log(scope.testStarted);
			//console.log(localStorageService.getItem('RESULTS'));
			//console.log(localStorageService.getItem('totalScore_' + scope.selectedActivity.id));
			//console.log(localStorageService.getItem('answeredQuestions_' + scope.selectedActivity.id));
			//console.log(localStorageService.getItem('activity_' + scope.selectedActivity.id));
			//console.log(localStorageService.getItem('attendedTotalQuestions_' + scope.selectedActivity.id));

			//INITIALISATION (this initialisation is if the drag 
			//and drop activity is on the first position after shuffling)
			scope.initRoute()

			if(!localStorageService.getItem('activity_' + scope.selectedActivity.id)){
				//console.log('activity_ none');
				var jsonToStringActivity = JSON.stringify(scope.activity);
                localStorageService.setItem('activity_' + scope.selectedActivity.id, jsonToStringActivity);
			}

			if(!scope.ds.dragDrop){
				scope.ds.dragDrop = [];
			}


			$('.btn-drag-drop-question').on('click', function(){
			 	var answer_choice = $(this).parent().children();
			 	//answer_choice_score = answer_choice[0].id;
			 	answer_choice_data = answer_choice[0].dataset.score;
			 	$(this)[0].disabled = true;
			 	
			 	//console.log($(this)[0].id);			 	
			 	scope.ds.dragDrop['question_drag_num'+scope.currentSection.QUESTIONS.QUESTION.ANSWERSET['@num']] = true;
			 	//console.log(scope.ds.dragDrop);
			 	
			 	var totalScore_ = Number(localStorageService.getItem('totalScore_' + scope.selectedActivity.id)) + 1;
				localStorageService.setItem('totalScore_' + scope.selectedActivity.id, totalScore_)
				scope.ds.totalScore = totalScore_;

				var attendedTotalQuestions_ = Number(localStorageService.getItem('attendedTotalQuestions_' + scope.selectedActivity.id)) + 1;
				localStorageService.setItem('attendedTotalQuestions_' + scope.selectedActivity.id, attendedTotalQuestions_)
				scope.ds.attendedTotalQuestions =attendedTotalQuestions_;

				var answeredQuestions_ = Number(localStorageService.getItem('answeredQuestions_' + scope.selectedActivity.id)) + 1;
				localStorageService.setItem('answeredQuestions_' + scope.selectedActivity.id, answeredQuestions_);
				scope.ds.answeredQuestions = answeredQuestions_;
				existing_results = localStorageService.getItem('RESULTS');

				//console.log('the score is =:' + answer_choice_score);
				//console.log('the data is =:' + answer_choice_data);
				//console.log(scope.currentSection.QUESTIONS.QUESTION.ANSWERSET['@CATEGORIE']);

				//THIS DATA IS TO PREAPARE THE SCORE
				//{"category":"CAT1","index":3,"score":"-2","issue_number":"1","choice_number":"4"}
				console.log(answer_choice[1].dataset.score);
				json_result = JSON.parse(answer_choice_data);

				var pageNo = $stateParams.pageNo;
				scope.recordAnswer('input_'+pageNo+'_'+json_result.choice_number,'DRAGDROP');
				console.log(answer_choice_data);

				var data = existing_results ? existing_results + ',' +  answer_choice_data :  answer_choice_data;
				localStorageService.setItem('RESULTS', data);
            	scope.ds.results = localStorageService.getItem('RESULTS');

    	        console.log(scope.ds.results);
                    utilityService.setActivityResumeData(scope.selectedActivity.id, location.path(),
                scope.ds.answeredQuestions, scope.ds.attendedTotalQuestions, scope.ds.totalScore, scope.ds.activity, localStorageService.getItem('RESULTS'));
		 	
			})


			//flage disabled button
			if(scope.ds.dragDrop['question_drag_num'+scope.currentSection.QUESTIONS.QUESTION.ANSWERSET['@num']]){
				$('#btn_drag_drop_question')[0].disabled = true ;
				//console.log('yes'+scope.ds.dragDrop['question_drag_num'+scope.currentSection.QUESTIONS.QUESTION.ANSWERSET['@num']]);
			}


				//THIS AREA IS TO ACTIVATE HE RESUME
				var answeredQuestions = localStorageService.getItem('answeredQuestions_' + scope.selectedActivity.id);
                var totalScore = localStorageService.getItem('totalScore_' + scope.selectedActivity.id);
                var attendedTotalQuestions = localStorageService.getItem('attendedTotalQuestions_' + scope.selectedActivity.id);

        scope.recordAnswer = function (id, type, value, source) {
            if(!scope.ds.isResuming &&  scope.ds.communicativeSkill == "activities") {
                var pageNo = $stateParams.pageNo;
                var interaction = interactionFactory.getInteraction(id, pageNo, type, value, source);
                utilityService.addInteraction(scope.selectedActivity.id, interaction);
				console.log(interaction);
				console.log(scope.selectedActivity);
				xml = xmlDataService.getXMLData(scope.selectedActivity.filename);
				console.log(xml);
            }
            var statusCode = cookies.get('statusValue');
            if (statusCode == '-40'){
                var model = ngDialog.open({
                    template: 'templates/modal/sessionAlive.html',
                    controller: 'sessionAliveController'
                });
            }
            return true;
        };
        //console.log(scope.ds.activity);

        var updateData = scope.$on('updateData', function (event, data) {
                if (scope.ds.resumeDataLoaded){
                    if (scope.ds.answeredQuestions != undefined)
                        localStorageService.setItem('answeredQuestions_' + scope.selectedActivity.id, scope.ds.answeredQuestions);

                    if (scope.ds.attendedTotalQuestions != undefined)
                        localStorageService.setItem('attendedTotalQuestions_' + scope.selectedActivity.id, scope.ds.attendedTotalQuestions);

                    if (scope.ds.totalScore != undefined) {utilityService.checkForSessionAlive();
                        localStorageService.setItem('totalScore_' + scope.selectedActivity.id, scope.ds.totalScore);
                        utilityService.setTotalScore(scope.selectedActivity.id, scope.ds.totalScore);
                    }

                    var jsonToStringActivity = JSON.stringify(scope.activity);
                    localStorageService.setItem('activity_' + scope.selectedActivity.id, jsonToStringActivity);

                    console.log('local storage on updatedata');
                    console.log(localStorageService.getItem('RESULTS'));

                    utilityService.setActivityResumeData(scope.selectedActivity.id, location.path(),
                        scope.ds.answeredQuestions, scope.ds.attendedTotalQuestions, scope.ds.totalScore, scope.ds.activity, localStorageService.getItem('RESULTS'));

                    //Add interaction
                    var objectiveId = scope.selectedActivity.skillType + "_" + scope.selectedActivity.id;

                    data.objectiveId = objectiveId;


                    //Set status based on percentage
                    var percentage = scope.calculatePercentage();
                    scope.ds.activityScore = percentage;
                    if (percentage >= 90 && scope.ds.activityScormStatus != "passed") {
                        ScormProcessSetStatus(scope.selectedActivity.id, 'passed');

                        //Set suspend data status
                        utilityService.setSuspendDataStatus(scope.selectedActivity.id, 'success');
                    }

                    //console.log(scope.selectedActivity);

                    utilityService.playFeedbackAudio(data.wasCorrect);

                    scope.$emit('recordInteraction', data);
                }
            });


}]);



