app.controller('internetExplorerPopupController', function ($scope, ngDialog, dataService) {

    $scope.init = function () {
        $scope.ds = dataService.getDataSource();
    }();

    $scope.ok = function () {
        ngDialog.close(null, false);
    };
});