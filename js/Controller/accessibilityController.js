app.controller('accessibilityController', ['$scope', '$rootScope', 'ngDialog','dataService', 'utilityService','$stateParams','$timeout', '$cookies', 'env', function ($scope, $rootScope, ngDialog, dataService, utilityService, $stateParams, timeout, cookies, env) {

    $scope.fontTranslate = function (value) {
        $scope.fontSize = value;
        return $scope.availableFontSize[value];
    };

    var selectedBodyClassWatcher = $scope.$watch('selectedBodyClass', function (value) {
        $scope.selectedBodyClass = value;
    });

    var audioCaptionControlWatcher = $scope.$watch('audioCaptionControl', function (value) {
        $scope.audioCaptionControl = value;
    });

    $scope.sysLangWatcher = function () {
        if($scope.selectedLang == 0){
            $scope.ds.language = 'en';
        }
        if($scope.selectedLang == 1){
            $scope.ds.language = 'fr';
        }
        if($scope.selectedLang == 2){
            $scope.ds.language = 'es';
        }
        if($scope.selectedLang == 3){
            $scope.ds.language = 'zh';
        }
    };

    $scope.iSpeakLangWatcher = function () {
         if($scope.ds.mainMenuLinkLang =='en'){
            if($rootScope.translateLang == '0'){
              $scope.ds.ispeaklanguage = 'fr';
            }
            else if($rootScope.translateLang == '1'){
             $scope.ds.ispeaklanguage = 'es';
            }
            else if($rootScope.translateLang == '2'){
              $scope.ds.ispeaklanguage = 'zh';
            }
         }
         if($scope.ds.mainMenuLinkLang =='fr' ){
            if($rootScope.translateLang == '0'){
             $scope.ds.ispeaklanguage = 'en';
            }else if($rootScope.translateLang == '1'){
              $scope.ds.ispeaklanguage = 'es';
            }
            else if($rootScope.translateLang == '2'){
              $scope.ds.ispeaklanguage = 'zh';
            }

         }
    };

    $scope.cancelChanges = function () {
        ngDialog.close();
    };

    $scope.selectFont = function(value){
        $scope.selectedFontSize = value;
    };

    $scope.selectLang = function(value){
       $scope.selectedLang = value;
    };

    $scope.selectAudioCaption = function(value){
      $scope.audioCaptionControl = value;
    };

    $scope.selectVideoCaption = function(value){
        $scope.videoCaptionControl = value;
        utilityService.videoCaptionControl = value;
    };

    $scope.selectImageCaption = function(value){
        $scope.imageCaptionControl = value;
    };

    $scope.selectAccessibilityMode  = function(value){
        $scope.accessibilityModeControl = value;
    };

    $scope.selectiSpeakLang  = function(value){
       $scope.selectiSpeakLangControl = value;
    };

    $scope.$on('$destroy', function(){
        selectedBodyClassWatcher();
        audioCaptionControlWatcher();
    });

    $scope.applyChanges = function () {
        ngDialog.close();
        //$rootScope.selectedBodyClass = $scope.selectedBodyClass;
        $rootScope.accessibilityModeControl = $scope.accessibilityModeControl;
        $rootScope.langModeControl = $scope.langModeControl;
        $rootScope.audioCaptionControl = $scope.audioCaptionControl;
        $scope.ds.audioCaption = $scope.audioCaptionControl;
        $rootScope.videoCaptionControl = $scope.videoCaptionControl;
        $rootScope.imageCaptionControl = $scope.imageCaptionControl;
        $rootScope.fontSize = $scope.selectedFontSize;
        $rootScope.sysLang = $scope.selectedLang;
        $rootScope.translateLang = $scope.selectiSpeakLangControl;
        $rootScope.volumePercentage = $scope.volumePercentage.max;
        utilityService.setAccessibilityData();
        $scope.sysLangWatcher();
        $scope.iSpeakLangWatcher();

        $rootScope.$broadcast('accessibilityChanged');
        var modulePreferences= JSON.stringify(
            {"ISpeakLangDB":$scope.selectiSpeakLangControl,
            "SystemLangFromDB":$scope.selectedLang ,
            "FontSizeFromDB":$scope.selectedFontSize ,
            "AudioCaptionFromDB":$scope.audioCaptionControl ,
            "VideoCaptionFromDB":$scope.videoCaptionControl ,
            "ImageCaptionFromDB":$scope.imageCaptionControl }
        );
		ScormProcessSetValue('LMSModulePreferences',modulePreferences);
		utilityService.checkForSessionAlive();
    };

    $scope.init = function () {
        $scope.ds = dataService.getDataSource();
        $scope.moduleId = $stateParams.moduleId;
        $scope.fontSizeClass = ['AAAAA', 'AAAA', 'AAA', 'AA', 'A'];
        if(($rootScope.isFrenchModule && $scope.ds.language == 'fr') || $scope.ds.language == 'fr') {
            $scope.availableFontSize = ['AAAAA', 'AAAA', 'AAA', 'AA', 'A'];
        }
        else if(($rootScope.isFrenchModule && $scope.ds.language == 'en') || $scope.ds.language == 'en') {
            $scope.availableFontSize = ['AAAAA', 'AAAA', 'AAA', 'AA', 'A'];
        }
        else if(($rootScope.isSpanishModule && $scope.ds.language == 'es') || $scope.ds.language == 'es'){
            $scope.availableFontSize = ['AAAAA', 'AAAA ', 'AAA', 'AA', 'A'];
        }
        else if(($rootScope.isChineseModule && $scope.ds.language == 'zh') || $scope.ds.language == 'zh'){
            $scope.availableFontSize = ['很小', '小 ', '中', '大', '很大'];
        }

        if ($rootScope.volumePercentage === undefined) {
            $rootScope.volumePercentage = 75;
        }
        $scope.captionsButtonLabel =[{
            name_en: 'Inactive',
            name_fr: 'Désactivé',
            name_es: 'Desactivado',
            name_zh: '待用',
            class: false
        },{
            name_en: 'Active',
            name_fr: 'Activé',
            name_es: 'Activado',
            name_zh: '活性',
            class: true
        }];

        $scope.sysLangClass = ['English', 'French', 'Spanish', 'Chinese'];
        if($scope.ds.language == 'fr') {
             $scope.availableLang = ['Anglais', 'Français', 'Espagnol','Mandarin'];
        }
         else if($scope.ds.language == 'en') {
             $scope.availableLang = ['English', 'French', 'Spanish','Mandarin'];
         }
         else if($scope.ds.language == 'es'){
             $scope.availableLang = ['Inglés', 'Francés', 'Español','Mandarina'];
         }
         else if( $scope.ds.language == 'zh'){
             $scope.availableLang = ['英语', '法语', '西班牙语','中文'];
         }

         if($scope.ds.mainMenuLinkLang == 'en'){
            if($scope.ds.language == 'fr') {
                 $scope.langButtonLabel = ['Français', 'Espagnol','Mandarin'];
            }
             else if($scope.ds.language == 'en') {
                 $scope.langButtonLabel = ['French', 'Spanish', 'Mandarin'];
            }
             else if($scope.ds.language == 'es'){
                 $scope.langButtonLabel = ['Francés','Español','Mandarina'];
            }
            else if( $scope.ds.language == 'zh'){
                 $scope.langButtonLabel = ['法语','西班牙语','中文'];
            }
         }

        if($scope.ds.mainMenuLinkLang == 'fr'){
           if($scope.ds.language == 'fr') {
               $scope.langButtonLabel = ['Anglais', 'Espagnol','Mandarin'];
           }
           else if( $scope.ds.language == 'en') {
               $scope.langButtonLabel = ['English', 'Spanish', 'Mandarin'];
           }
           else if( $scope.ds.language == 'es'){
               $scope.langButtonLabel = ['Inglés','Español','Mandarina'];
           }
           else if( $scope.ds.language == 'zh'){
               $scope.langButtonLabel = ['英语','西班牙语','中文'];
           }
        }
        this.audioCaptionControl = $scope.audioCaptionControl;
        this.videoCaptionControl = $scope.videoCaptionControl;
        this.imageCaptionControl = $scope.imageCaptionControl;

        $scope.audioCaptionControl = $rootScope.audioCaptionControl?this.audioCaptionControl:
            $scope.captionsButtonLabel[0].class;

        $scope.videoCaptionControl = $rootScope.videoCaptionControl?this.videoCaptionControl:
            $scope.captionsButtonLabel[0].class;

        $scope.imageCaptionControl = $rootScope.imageCaptionControl?this.imageCaptionControl:
            $scope.captionsButtonLabel[0].class;

//        $scope.accessibilityModeControl = $rootScope.accessibilityModeControl?$rootScope.accessibilityModeControl:
//            $scope.captionsButtonLabel[0].class;

        $scope.ds.audioCaption = $scope.audioCaptionControl;

        $scope.selectedFontSize = $rootScope.fontSize;
        $scope.selectedLang = $rootScope.sysLang;
         $scope.selectiSpeakLangControl = $rootScope.translateLang;

        $scope.volumePercentage = {
            max: $rootScope.volumePercentage
        };

        if (env.enableDebug) {
            cookies.put('languageCookie', env.defaultLanguage);
            cookies.put('moduleCookie', env.moduleNumber);
        };

        var module = cookies.get('moduleCookie');
        $scope.moduleId = module;

        $scope.placementTestEnabled = true;
        if ($scope.moduleId === 'placementTest') {
            $scope.placementTestEnabled = false;
        };

    }();
}]);
