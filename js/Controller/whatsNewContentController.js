app.controller("whatsNewContentController", ['$rootScope', '$scope', 'dataService', '$stateParams',
    'utilityService', 'groupService', '$timeout','$window',
    function (rootScope, scope, dataService, stateParams, utilityService, groupService, $timeout,$window) {

        scope.parseSection = function (whatNewSections) {
            if (whatNewSections) {

                if (Array.isArray(whatNewSections)) {
                    for (var i = 0; i < whatNewSections.length; i++) {
                        var content = whatNewSections[i].CONTENT;
                        whatNewSections[i].CONTENT = scope.setParsedSpeech(content);
                    }
                } else {
                    whatNewSections.CONTENT = scope.setParsedSpeech(whatNewSections.CONTENT);
                }
            }
        };

        scope.formatTerm = function (term) {
            if (term) {
                term = term.replace("'", "\\'");
                return term;
            }
        };

        scope.getFormattedTerm = function (term) {
            if (term) {
                var regex = /<c>(.*)<\/c>/;
                // var regex = /<c>(\w*)<\/c>/;
                var highlightedWord = term.match(regex);

                if (highlightedWord && highlightedWord.length > 1)
                    term = term.replace(highlightedWord[0], '<span style="color: #03B0F0;">' + highlightedWord[1] + '</span>')

                return term;
            }
        };


        scope.setParsedSpeech = function (content) {

            var term = scope.getAllTerms(content);

            if (term && term.length > 1) {
                for (var j = 0; j < term.length; j = j + 2) {
                    var key = scope.formatTerm(term[j + 1]);
                    var formattedTerm = scope.getFormattedTerm(term[j + 1]);
                    var newStr = "<button class=\"btn btn-link audio-term\" alt=\"" + key + "\" data-ng-click=\"termSelected(section, \'" + key +
                        "\')\">" + formattedTerm + "<\/button>";

                    content = content.replace(term[j], newStr);
                }
            }

            content = scope.replaceAllLangTerms(content);

            return content;
        };

        scope.replaceAllLangTerms = function (str) {
            if (str) {
                var regEx = rootScope.isFrenchModule ? /(language='en')/ : /(language='fr')/;
                var replaceTerm = rootScope.isFrenchModule ? "ng-show='currentSection.titleFR == false'" : "ng-show='currentSection.titleFR'";
                var terms = str.match(regEx);
                var finalStr = str;

                if (terms) {
                    finalStr = str.replace(terms[0], replaceTerm);
                    finalStr = scope.replaceAllLangTerms(finalStr);
                }

                var langAttr = rootScope.isFrenchModule ? "<em lang='en'>" : "<em lang='fr'>";
                var regexLangExist = /<em(?![^>]*\b)[^>]*?>/g;
                var matchTerms = finalStr.match(regexLangExist);
                var replacedTerm = finalStr;

                if (matchTerms) {
                    replacedTerm = finalStr.replace(matchTerms[0], langAttr);
                    replacedTerm = scope.replaceAllLangTerms(replacedTerm);
                }

                return replacedTerm;
            }
        };


        scope.getAllTerms = function (str) {
            if (str) {
                var regEx = /\{([^}]*)\}/;
                var terms = [];
                terms = str.match(regEx);
                if (terms) {
                    var newStr = str.substring(terms.index + terms[0].length);
                    var newTerms = scope.getAllTerms(newStr);

                    if (newTerms)
                        terms = terms.concat(newTerms);
                }

                return terms;
            }
        };

        scope.playAudio = function (src) {
            var whatsNew = groupService.getItem(scope.whatsNewId, scope.moduleId);

            angular.element('#loadAudio').attr("src", utilityService.getAudioPath(whatsNew.path, src));
            audio = angular.element('#loadAudio');
            audio[0].play();
        };

        scope.termSelected = function (section, term) {
            var selectedTerm;
            for (var i = 0; i < scope.ds.terms.length; i++) {
                var dictionaryTerm = scope.ds.terms[i];

                if (dictionaryTerm.KEY == term) {
                    selectedTerm = dictionaryTerm;

                    if (selectedTerm.AUDIO)
                        scope.playAudio(selectedTerm.AUDIO);

                    break;
                }
            }
        };

        scope.langClicked = function (isDown) {
            scope.isLangClicked = isDown;
        };

        scope.toggleFocus = function (isFocused) {
            if (!scope.isLangClicked)
                scope.isLangFocused = isFocused;
        };

         var selectedFocusHandler = scope.$on('focusSelectedElement', function(e, data) {
            if(data) {
                var element = document.getElementById(data);
                if(element) {
                        element.focus();
                }
            } else {
                var element = document.getElementsByTagName('h2');
                 if(element) {
                         element[0].focus();
                 }
            }
          });

          scope.$on('$destroy', function(){
            $timeout.cancel( scope.getContentTimeout );
            $timeout.cancel( scope.selectionCompleteTimeout );
            selectedFocusHandler();
          });

        scope.init = function () {
            scope.ds = dataService.getDataSource();
            var pageNo = stateParams.pageNo;
            scope.moduleId = stateParams.moduleId;
            scope.whatsNewId = stateParams.whatsNewId;
            scope.isLangFocused = false;
            scope.isLangClicked = false;

            if(stateParams.nav == undefined) {
                scope.getContentTimeout = $timeout(function () {
                    var whatsNewContent = document.querySelector('#content');
                    whatsNewContent.scrollTop = 0;
                }, 100);
            }

            if (scope.ds.whatsNewSections && Array.isArray(scope.ds.whatsNewSections)) {
                scope.currentSection = scope.ds.whatsNewSections[pageNo - 1];
            } else {
                scope.currentSection = scope.ds.whatsNewSections;
            }

            scope.parseSection(scope.currentSection.SUBSECTIONS.SUBSECTION);

            if (scope.ds.menuSelectionCompleted == false) {
                scope.selectionCompleteTimeout = $timeout(function () {
                    scope.ds.menuSelectionCompleted = true;
                }, 100);
            }
        }();
    }]);