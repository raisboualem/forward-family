app.controller('answerSetController', ['$rootScope', '$scope', 'xmlDataService', 'dataService', 'localStorageService',
    '$stateParams', 'utilityService', '$timeout', '$sce', '$filter', 'interactionFactory', '$stateParams','$cookies','ngDialog',
    function (rootScope, scope, xmlDataService, dataService, localStorageService, stateParams, utilityService, timeout, $sce, filter, interactionFactory, $stateParams, cookies,ngDialog) {
        var checkFilled = [];
        scope.lst = [];

        scope.ds = dataService.getDataSource();
        scope.ds.totalScoreCheckedBox = 1;

        scope.chk_is_array = function (arr) {
            return Array.isArray(arr);
        };

        scope.getAnswerText = function (answers) {
            if (answers != null) {
                var answerTexts = [];
                if (Array.isArray(answers)) {
                    for (var i = 0; i < answers.length; i++) {
                        var answer = answers[i];
                        if (answer['@CORRECT'] == "TRUE" || answer['@correct'] == "TRUE") {
                            var answerText = answer.TEXT ? answer.TEXT : answer.IMAGE || answer.AUDIO;
                            answerTexts.push(answerText);
                            //console.log(answer['@SCORE']);
                        }
                    }
                }
                else {
                    if (answers['@CORRECT'] == "TRUE" || answers['@correct'] == "TRUE") {
                        var answerText = answers.TEXT ? answers.TEXT : answers.IMAGE || answers.AUDIO;
                        answerTexts.push(answerText);
                    }
                }
            }
            //console.log(answerTexts);
            return answerTexts;
        };

        scope.compareAnswers = function (selected, values) {
            /*we don't have to compare answers because all answers are correct*/
            //return false;
            //we must return true because all answer['@CORRECT'] == "TRUE"
            return true;
        };

        scope.getCorrectAnswersCount = function (answerSet) {
            var cnt = 0;
            if (answerSet && Array.isArray(answerSet)) {
                for (var i = 0; i < answerSet.length; i++) {
                    var answer = answerSet[i].ANSWER;
                    cnt += scope.getAnswerText(answer).length;
                }
            }
            if(scope.selectedActivity.hasDoubleAnswer)
                cnt = cnt-1;

            return cnt;
        };

        scope.addColorText = function (quesText) {
            if (quesText) {
                var regex = /<b>(.*?)<\/b>|<i>(.*?)<\/i>|<font\b[^>]*>(.*?)<\/font>|<u>(.*?)<\/u>|<span\b[^>]*>(.*?)<\/span>/g;
                var highlightedWord = quesText.match(regex);
                var result = [];
                var mainQues = quesText;

                if (highlightedWord && highlightedWord.length > 0) {

                    var quesText = [];

                    for (var i = 0; i < highlightedWord.length; i++) {

                        var matched = mainQues.replace(highlightedWord[i], '{1}');
                        mainQues = matched;
                        quesText = matched.split(' ');

                    }

                    for (var k = 0; k < highlightedWord.length; k++) {
                        for (var j = 0; j < quesText.length; j++) {
                            if (quesText[j].indexOf('{1}') != -1) {
                                quesText[j] = quesText[j].replace('{1}', highlightedWord[k]);
                                break;
                            }
                        }
                    }


                }
                else {
                    quesText = quesText.split(' ');
                }

                for (var i = 0; i < quesText.length; i++) {
                    if (quesText[i] != '') {
                        if (quesText[i] != '<br/>' && quesText[i] != '</br>') {
                            result.push(quesText[i] + '&nbsp;');
                        } else {
                            result.push(quesText[i]);
                        }
                    }
                }

                return result;
            }
        }

        scope.updateScore = function (answerSet, result, prevResult) {
            var answerSetScore = answerSet.score ? answerSet.score : 0;
            var answerScore = result ? 1 : 0;
            var prevAnswerScore = prevResult ? 1 : 0;

           if (scope.ds.communicativeSkill != 'dialogues' && !scope.selectedActivity.hasFeedback){
                if (Array.isArray(answerSet)) {
                    var correctAnswersCnt = scope.getCorrectAnswersCount(answerSet);
                    answerScore = answerScore / correctAnswersCnt;
                    prevAnswerScore = prevAnswerScore / correctAnswersCnt;
                } else {
                    var answers = answerSet.ANSWER;

                    //console.log(answers);
                    if (answers) {
                        var correctAnswersCnt = scope.getAnswerText(answers).length;
                        var isMultiCorrect = scope.selectedActivity.isMultiCorrect;

                        if (isMultiCorrect) {
                            answerScore = answerScore / correctAnswersCnt;
                            prevAnswerScore = prevAnswerScore / correctAnswersCnt;
                        }
                    }
                }
           }
           if(scope.selectedActivity.hasFeedback){
                if (Array.isArray(answerSet)) {
                    var correctAnswersCnt = scope.getCorrectAnswersCount(answerSet);
                    answerScore = answerScore / (correctAnswersCnt / 2);
                    prevAnswerScore = prevAnswerScore / correctAnswersCnt;
                }
           }

            answerSetScore = answerSetScore - prevAnswerScore + answerScore;
            answerSet.score = answerSetScore;

            scope.ds.totalScore = scope.ds.totalScore - prevAnswerScore + answerScore;
        };

        scope.showFeedbackEOM = function () {
            var quizId = parseInt(stateParams.quizId);
            var moduleId = parseInt(stateParams.moduleId);
            var attempts = utilityService.getSuspendDataEOMAttempts(quizId);
            attempts = attempts ? parseInt(attempts) : 1;

            var result = attempts;
            return result;
        };

        scope.checkAllAnswered = function (answers, maxAttempt) {
            if (answers) {
                var correctAnswers = scope.getAnswerText(answers).length;
                var allAnswered = false;
                var cnt = 0;

                for (var i = 0; i < answers.length; i++) {
                    cnt = answers[i].attempts == parseInt(maxAttempt) ? cnt + 1 : cnt;
                }

                allAnswered = cnt == correctAnswers;
                return allAnswered;
            }
        };

        scope.getTooltip = function (answer) {
            answer = String(answer);
            if (answer) {
                var regEx = /<.*>(.*)<\/.*>|(.*)/;
                var tooltip = answer;

                tooltip = answer.match(regEx);

                if (tooltip.length > 1)
                    tooltip = tooltip[1] ? tooltip[1] : tooltip[0];

                var txt = scope.htmlDecode(tooltip);
                return txt;
            }
        };

        scope.htmlDecode = function (input) {
            var e = document.createElement('div');
            e.innerHTML = input;
            return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
        }

        scope.answerText = {
            'value' : undefined
        };

         $('html').keyup(function(event) {
               if(scope.selectedActivity.hasFeedback === true){
                timeout(function () {
                var elem = $(event.target).hasClass('FIBVIPointerBlock');
                  if(elem){
                      $( ".form-control" ).prop( "disabled", true );
                  }
                }, 10);
               }
               else{
                 return;
               }
          });

           scope.setModelValue = function(id,answerSet) {
               if (Array.isArray(answerSet)) {

                   if(scope.selectedActivity.hasFeedback === true){
                    var stringId = id + "";
                                var splittedId = stringId.split("_");
                                if (splittedId.length > 0) {
                                    $("#inputAnswerset_"+ splittedId[1] +"_0").val(answerSet[1]['@FEEDBACK']);
                                    if(answerSet.length > 3){
                                        $("#inputAnswerset_"+ splittedId[1] +"_2").val(answerSet[3]['@FEEDBACK']);
                                    }
                                    $("#inputAnswerset_"+ splittedId[1] +"_0").prop('disabled', true);

                                }
                    }
               }
           };

        scope.validate = function (selectedValue, answer, maxAttempts, answers, correctValues, answerSet, index, id) {
            answers = answers ? answers : answer;


            var results_repenses = new Array();
            var results_repenses_categories = new Array();
            //console.log(answer['@CATEGORIE']);
            //console.log(Number(selectedValue['@SCORE']));
            //console.log(index);
            var key_category = answer['@CATEGORIE'];
            var key_issue_number = answer['@num'];
            var key_respense = index;
            //results_repenses[index] =  new Array();
            //results_repenses[index] = selectedValue['@SCORE'];
            //console.log(results_repenses);
            //results_repenses_categories[key_category] = selectedValue['@SCORE'];
            //console.log(results_repenses);

            myJString = JSON.stringify({"category": key_category , "index" :  key_respense, "score" : selectedValue['@SCORE'], "issue_number" : key_issue_number, "choice_number" : selectedValue['@num_choice']});
            //console.log(myJString);


            // Get the existing data
            var existing = localStorageService.getItem('RESULTS');

            // If no existing data, use the value by itself
            // Otherwise, add the new value to it
            var data = existing ? existing + ',' +  myJString :  myJString;

            // Save back to localStorage
            localStorageService.setItem('RESULTS', data);
            //console.log(localStorageService.getItem('RESULTS'));
            scope.setModelValue(id,answerSet);
            //set input value
            var feedback = scope.selectedActivity.feedback;
            var showAlert = false;
            scope.selectedValue = selectedValue;
            maxAttempts = maxAttempts ? maxAttempts : 1;

            //Check for max attempts for final quiz
            if (scope.ds.communicativeSkill == 'quiz') {
                maxAttempts = 1;
            }
            if(scope.ds.sound){
               scope.ds.sound.stop();
            }

            if (answer && !answer.showResult) {
                if (selectedValue) {
                    var correctAnswers = correctValues ? correctValues : scope.getAnswerText(answer.ANSWER ? answer.ANSWER : answerSet.ANSWER);
                    var result = scope.compareAnswers(selectedValue, correctAnswers);
                    var prevResult = answer.result;
                    answer.result = result;

                    scope.updateScore(answerSet, result, prevResult);

                    answer.index = index;

                    answer.attempts = answer.attempts ? answer.attempts + 1 : 1;

                    if (feedback && (result || answer.attempts >= parseInt(maxAttempts))) {
                        if (answers != answerSet)
                            answers.attempted = true;

                        if ((!scope.question.attempted && scope.selectedActivity.type != 'GroupHeading') ||
                            (!scope.question.attempted && scope.selectedActivity.type == 'GroupHeading' && result)) {
                            scope.ds.answeredQuestions = scope.ds.answeredQuestions + 1;
                            scope.question.attempted = true;
                        }

                        scope.checkForAttendedAnswers(answerSet, result, maxAttempts);

                        //Remove drag drop answer
                        if (scope.selectedActivity.type == "DragAndDrop" ||
                            scope.selectedActivity.type == "DragAndDropVideo" ||
                            scope.selectedActivity.type == "DragDropColumn" ||
                            scope.selectedActivity.type == "GroupHeading" ||
                            scope.selectedActivity.type == "WordScramble") {
                            answer.ANSWER.isRemoved = result;
                        }

                        //Show feedback
                        if (result == false)
                            answer.toolTip = Array.isArray(correctAnswers) ? scope.getTooltip(correctAnswers[0]) : scope.getTooltip(correctAnswers);
                        else
                            answer.toolTip = null;

                        if (scope.ds.communicativeSkill == "quiz")
                            answer.showResult = scope.showFeedbackEOM();
                        else
                            answer.showResult = true;

                        if (answer['@controlType'] == "BUTTON" && Array.isArray(answerSet)) {
                            for (var i = 0; i < answer.ANSWER.length; i++) {
                                var _answer = answer.ANSWER[i];

                                if (!result) {
                                    if (scope.getAnswerText(_answer).length == 1) {
                                        _answer.index = i;
                                        _answer.result = true;
                                        _answer.showResult = true;
                                    }
                                    var answerText = _answer.TEXT ? _answer.TEXT : _answer.IMAGE || _answer.AUDIO;
                                    if (answerText == selectedValue) {
                                        _answer.index = i;
                                        _answer.result = false;
                                        _answer.showResult = true;
                                    }
                                } else {
                                    var answerText = _answer.TEXT ? _answer.TEXT : _answer.IMAGE || _answer.AUDIO;
                                    if (answerText == selectedValue) {
                                        _answer.index = i;
                                        _answer.result = true;
                                        _answer.showResult = true;
                                    }
                                }
                            }
                        }

                        if (Array.isArray(answers)) {
                            var isMultiCorrect = scope.selectedActivity.isMultiCorrect;
                            var isAllAnswered = isMultiCorrect && scope.checkAllAnswered(answers, maxAttempts);

                            if (!isMultiCorrect || isAllAnswered) {
                                for (var i = 0; i < answers.length; i++) {
                                    var answer = answers[i];
                                    answer.disabled = true;
                                    answer.tabIndex = -1;
                                }
                            }
                            else {
                                answer.disabled = true;
                                answer.tabIndex = -1;
                            }


                        } else {
                            answers.disabled = true;
                            answers.tabIndex = -1;
                        }
                    } else {
                        showAlert = feedback;
                    }

                    var questionInteractionInfo = {
                        'id': scope.questionNo,
                        'questionText': scope.displayAsText(scope.question),
                        'questionType': scope.controlType,
                        'learnerResponse': selectedValue,
                        'correctAnswer': correctAnswers.toString(),
                        'wasCorrect': result,
                        'showAlert': showAlert,
                        'dragDropAnswerRemoved': answer.ANSWER ? answer.ANSWER.isRemoved : answer.isRemoved,
                    };

                    scope.$emit('updateData', questionInteractionInfo);

                    var statusCode = cookies.get('statusValue');
                    if (statusCode == '-40'){
                        var model = ngDialog.open({
                            template: 'templates/modal/sessionAlive.html',
                            controller: 'sessionAliveController'
                        });

                    }
                }
            }
        }

        scope.getFileName = function (filePath) {
            if (filePath) {
                var temp = filePath.split('/');
                filePath = temp[temp.length - 1];
            }

            return filePath;
        };

        scope.checkMultiAnswerResult = function (answers) {
            var result;
            for (var i = 0; i < answers.length; i++) {
                var answer = answers[i];

                if (answer.result != undefined) {
                    result = result ? result && answer.result : answer.result;
                }
            }

            return result;
        };

        scope.recordAnswer = function (id, type, value, source) {
            if(!scope.ds.isResuming &&  scope.ds.communicativeSkill == "activities") {
                var pageNo = $stateParams.pageNo;
                var interaction = interactionFactory.getInteraction(id, pageNo, type, value, source);
                utilityService.addInteraction(scope.selectedActivity.id, interaction);
				//console.log(interaction);
				//console.log(scope.selectedActivity);
				//xml = xmlDataService.getXMLData(scope.selectedActivity.filename);
				//console.log(xml);
            }
            var statusCode = cookies.get('statusValue');
            if (statusCode == '-40'){
                var model = ngDialog.open({
                    template: 'templates/modal/sessionAlive.html',
                    controller: 'sessionAliveController'
                });
            }
            return true;
        };

        scope.checkForAttendedAnswers = function (answerSet, result, maxAttempts) {
            if (answerSet && Array.isArray(answerSet)) {
                var attended = false;
                var attendedCnt = 0;
                var correctAnswerSet = 0;
                for (var i = 0; i < answerSet.length; i++) {
                    if (!scope.selectedActivity.hasFeedback){
                        var currentAnswerSet = answerSet[i];
                        correctAnswerSet += scope.getAnswerText(currentAnswerSet.ANSWER).length > 0;
                        if (currentAnswerSet && currentAnswerSet.attempted) {
                            result = result && currentAnswerSet.result;
                            attendedCnt += 1;
                            if (i == answerSet.length - 1)
                                attended = true;
                        } else {
                            break;
                        }
                    } else if(scope.selectedActivity.hasFeedback && i > 0){
                        if (i % 2 != 0){
                            var currentAnswerSet = answerSet[i];
                             correctAnswerSet += scope.getAnswerText(currentAnswerSet.ANSWER).length > 0;
                             if (currentAnswerSet && currentAnswerSet.attempted) {
                                 result = result && currentAnswerSet.result;
                                 attendedCnt += 1;
                                 if (i == answerSet.length - 1)
                                     attended = true;
                             } else {
                                  attended = false;
                             }
                         }
                    }
                }
                attended = correctAnswerSet == attendedCnt;

                if (scope.selectedActivity.type != 'GroupHeading' ||
                    (scope.selectedActivity.type == 'GroupHeading' && result)) {
                    scope.ds.attendedTotalQuestions = attended ? scope.ds.attendedTotalQuestions + 1 :
                        scope.ds.attendedTotalQuestions;
                }

                scope.question.disabled = attended;

                if (attended) {
                    scope.question.result = result;

                    if (scope.ds.communicativeSkill == "quiz")
                        scope.question.showResult = scope.showFeedbackEOM();
                    else
                        scope.question.showResult = true;
                }

                scope.question.tabIndex = -1;
            }
            else if (answerSet) {
                var answerTexts = scope.getAnswerText(answerSet.ANSWER);
                var isMultiCorrect = scope.selectedActivity.isMultiCorrect;
                var isAllAnswered = isMultiCorrect && scope.checkAllAnswered(answerSet.ANSWER, maxAttempts);

                if (scope.selectedActivity.type != 'GroupHeading' ||
                    (scope.selectedActivity.type == 'GroupHeading' && result)) {
                    scope.ds.attendedTotalQuestions += isMultiCorrect ? (isAllAnswered ? 1 : 0) : 1;
                }

                scope.question.disabled = isMultiCorrect ? isAllAnswered : true;
                scope.question.result = isMultiCorrect ? scope.checkMultiAnswerResult(answerSet.ANSWER) : result;

                if (scope.ds.communicativeSkill == "quiz")
                    scope.question.showResult = scope.showFeedbackEOM();
                else
                    scope.question.showResult = isMultiCorrect ? isAllAnswered : true;

                if (answerSet['@controlType'] == "BUTTON" && isAllAnswered) {
                    for (var i = 0; i < answerSet.ANSWER.length; i++) {
                        var answer = answerSet.ANSWER[i];

                        if (scope.getAnswerText(answer).length == 1) {
                            answer.index = i;
                            answer.result = true;
                            answer.showResult = true;
                        }
                    }
                }
                else {
                    if (answerSet['@controlType'] == "BUTTON" && scope.selectedActivity.type != 'FinalTest') {
                        var isAnswered = scope.checkAllAnswered(answerSet.ANSWER, maxAttempts);

                        if (isAnswered) {
                            for (var i = 0; i < answerSet.ANSWER.length; i++) {
                                var answer = answerSet.ANSWER[i];

                                if (scope.getAnswerText(answer).length == 1) {
                                    answer.index = i;
                                    answer.result = true;
                                    answer.showResult = true;
                                }
                            }
                        }
                    }else if(scope.question.showResult){
                        var isAnswered = scope.checkAllAnswered(answerSet.ANSWER, maxAttempts);

                            if (isAnswered) {
                                for (var i = 0; i < answerSet.ANSWER.length; i++) {
                                    var answer = answerSet.ANSWER[i];

                                    if (scope.getAnswerText(answer).length == 1) {
                                        answer.index = i;
                                        answer.result = true;
                                        answer.showResult = true;
                                    }
                                }
                            }
                    }
                }

                scope.question.tabIndex = -1;
            }
        };

        scope.getAssetsPath = function (audioName) {
            if (audioName['#text'] !== undefined) {
                audioName = audioName['#text'];
            }

            return utilityService.getAudioPath(scope.selectedActivity.path, audioName);
        };

        scope.getImageAssetsPath = function (imageName) {
            return utilityService.getImagePath(scope.selectedActivity.path, imageName);
        };

        scope.displayAsText = function (text) {

            if (text === undefined)
                return false;

            if (typeof text !== 'string') {
                txt = '';

                angular.forEach(text.TEXT, function (v, k) {
                    txt += v;
                });

                text.TEXT = txt;
            }

            if ((/\.(gif|jpg|jpeg|tiff|png)$/i).test(text.TEXT) === true) {
                return '<img src="' + utilityService.getImagePath(scope.selectedActivity.path, text.TEXT) + '" class="q-img" alt="Learning Image" >';
            } else if ((/\.(mp3)$/i).test(text.TEXT) === true) {
                return '<audio src="' + utilityService.getAudioPath(scope.selectedActivity.path, text.TEXT) + '" controls="controls">';
            } else if (text.TEXT === '{0}') {
                return '';
            } else {
                return text.TEXT;
            }
        };

        scope.get_drop_down_text_array = function (txt) {
            if (txt === undefined || txt == false)
                return {};

            var data = txt.split(/\{\d+}/g);
            return data;
        };

        scope.get_drop_down_text_array_count = function (questData) {
            return Object.keys(questData).length;
        };

        scope.play_audio = function (src, tooltipIndex) {
            timeout.cancel(scope.ds.cancelTimeout);
            timeout(function () {
                scope.ds.showTooltip = true;
                scope.ds.focuss = true;
                scope.tooltipIndex = tooltipIndex;
                scope.ds.toolTipIndex = tooltipIndex;
                angular.element('#load_audio').attr("src", scope.getAssetsPath(src));
                angular.element('#load_audio').attr("alt", scope.getFileName(src));
                audio = angular.element('#load_audio');
                audio[0].play();
            }, 10);
        };


        scope.hidetip = function () {
            scope.ds.focuss = false;
        }

        scope.dropped = function (data, answer, answerSet, fromPopup) {
            scope.ds.isPopoverOpen = false;

            if (answer.disabled)
                return false;

            if (answer.ANSWER) {
                answerSet = answerSet ? answerSet : answer;
                data = data.replace('\n', '').trim();

                answer.value = data;
                scope.validate(data, answer, answerSet['@maxAttempts'], null, null, answerSet);
            }
            else if (answer.ANSWER.AUDIO) {
                var correctAnswer = answer.ANSWER.AUDIO;
                scope.validate(data, answer, 1, null, correctAnswer, answer);
            }
            else if (answer.ANSWER.VIDEO) {
                var correctAnswer = answer.ANSWER.VIDEO;
                scope.validate(data, answer, 1, null, correctAnswer, answer);
            }
            if (fromPopup)
                rootScope.$broadcast('reset-focus');
        };

        scope.groupHeaderDropped = function (data, answer, answerSet, itemId) {
            if (scope.question.ANSWERSET.result || scope.currentSection['@maxAttempts'] == 0)
                return false;

            answerSet = answerSet ? answerSet : answer;

            var answerText = data.TEXT ? data.TEXT : data.AUDIO;
            answerText = answerText.replace('\n', '').trim();

            if (answer.showResult)
                answer.showResult = false;

            scope.validate(answerText, answer, answerSet['@maxAttempts'], null, null, answerSet);

            if (scope.question.ANSWERSET.result) {
                answer.value = answerText;
                scope.ds.isPopoverOpen = false;
                rootScope.$broadcast('reset-focus');
            } else {
                scope.currentSection['@maxAttempts'] = parseInt(scope.currentSection['@maxAttempts']) - 1;
                scope.$emit('wrongAnswer', itemId);
            }

            if (scope.question.result) {
                var cnt = 0;
                var len = scope.currentSection.QUESTIONS.QUESTION.length;

                for (var i = 0; i < len; i++) {
                    var question = scope.currentSection.QUESTIONS.QUESTION[i];
                    if (question.result == true) {
                        cnt++;
                    } else {
                        break;
                    }
                }

                if (cnt == len) {
                    scope.currentSection.result = true;
                    scope.currentSection.showResult = true;
                }
            }

            scope.$emit('updateActivity');
        };

        scope.checkedBox = function(data, answer, active){
            var correctAnswers = [];
             if (active)
                 scope.lst.push(data);
             else
                 scope.lst.splice(scope.lst.indexOf(data), 1);
        };

        scope.calculateCheckBoxScore = function(question,answerSet,itemId,data){
           for (var i = 0; i < scope.lst.length; i++) {
             var scoreAchieved = parseInt(scope.lst[i]['@SCORE']);
             scope.ds.totalScoreCheckedBox = scope.ds.totalScoreCheckedBox * scoreAchieved;
             scope.ds.totalScoreCheckedBox = scope.ds.totalScoreCheckedBox ? scope.ds.totalScoreCheckedBox : 0;
           }

          question.disabled = true;

           var totalScore_ = Number(localStorageService.getItem('totalScore_' + scope.selectedActivity.id)) + scope.ds.totalScoreCheckedBox;
           localStorageService.setItem('totalScore_' + scope.selectedActivity.id, totalScore_)
           scope.ds.totalScore = totalScore_;
           var attendedTotalQuestions_ = Number(localStorageService.getItem('attendedTotalQuestions_' + scope.selectedActivity.id)) + 1;
           localStorageService.setItem('attendedTotalQuestions_' + scope.selectedActivity.id, attendedTotalQuestions_)
           scope.ds.attendedTotalQuestions =attendedTotalQuestions_;
           var answeredQuestions_ = Number(localStorageService.getItem('answeredQuestions_' + scope.selectedActivity.id)) + 1;
           localStorageService.setItem('answeredQuestions_' + scope.selectedActivity.id, answeredQuestions_);
           scope.ds.answeredQuestions = answeredQuestions_;

          var results_repenses = new Array();
          var results_repenses_categories = new Array();
          var key_category = answerSet['@CATEGORIE'];
          var key_issue_number = answerSet['@num'];
          var key_respense = itemId;
          var existingCheckBoxIndex = localStorageService.getItem('checkBoxIndex');



          myJString = JSON.stringify({"category": key_category , "index" :  key_respense, "score" :  scope.ds.totalScoreCheckedBox, "issue_number" : key_issue_number, "choice_number" : data['@num_choice']});

          // Get the existing data
          var existing = localStorageService.getItem('RESULTS');

          // If no existing data, use the value by itself
          // Otherwise, add the new value to it
          if(itemId != parseInt(existingCheckBoxIndex))
             var resultData = existing ? existing + ',' +  myJString :  myJString;
          else
             var resultData = myJString;

          // Save back to localStorage
          localStorageService.setItem('RESULTS', resultData);
          localStorageService.setItem('checkBoxIndex', itemId);


              var questionInteractionInfo = {
                          'id': scope.questionNo,
                          'questionText': scope.displayAsText(scope.question),
                          'questionType': scope.controlType,
                          'learnerResponse': data,
                          'wasCorrect': true,
                          'showAlert': false,
                          'dragDropAnswerRemoved': true,
                      };

                      scope.$emit('updateData', questionInteractionInfo);



        };

        scope.groupListDropped = function (data, answer, answerSet, itemId) {
            if (scope.question.ANSWERSET.result || scope.currentSection['@maxAttempts'] == 0)
                return false;

            answerSet = answerSet ? answerSet : answer;
            var answerText = data.TEXT ? data.TEXT : data.AUDIO;
            answerText = answerText.replace('\n', '').trim();

            //Get correct answers
            var correctAnswers = [];

            if (Array.isArray(answer)) {
                for (var i = 0; i < answer.length; i++) {
                    correctAnswers.push(scope.getAnswerText(answer[i].ANSWER)[0]);
                }
            }
            else {
                correctAnswers.push(scope.getAnswerText(answer.ANSWER)[0]);
            }

            var result = scope.compareAnswers(answerText, correctAnswers);

            if (!answerSet.attempted) {
                scope.ds.answeredQuestions = parseInt(scope.ds.answeredQuestions) + 1;
                answerSet.attempted = true;
            }

             var results_repenses = new Array();
             var results_repenses_categories = new Array();
             var key_category = answerSet['@CATEGORIE'];
             var key_issue_number = answerSet['@num'];
             var key_respense = itemId;
             var scoreAchieved = parseInt(data['@SCORE']);

             scope.ds.totalScoreCheckedBox = scope.ds.totalScoreCheckedBox * scoreAchieved;

             myJString = JSON.stringify({"category": key_category , "index" :  key_respense, "score" :  scope.ds.totalScoreCheckedBox, "issue_number" : key_issue_number, "choice_number" : data['@num_choice']});

             // Get the existing data
             var existing = localStorageService.getItem('RESULTS');

             // If no existing data, use the value by itself
             // Otherwise, add the new value to it
             var resultData = existing ? existing + ',' +  myJString :  myJString;

             // Save back to localStorage
             localStorageService.setItem('RESULTS', resultData);



            if (result) {
                scope.updateScore(answerSet, result, false);

                if (!scope.question.items)
                    scope.question.items = [{data: data, result: true}];
                else
                    scope.question.items.push({data: data, result: true});
                var len = answer.length ? answer.length : 1;
                if (scope.question.items.length == len) {
                    scope.question.result = true;
                    scope.ds.isPopoverOpen = false;
                    scope.ds.attendedTotalQuestions = parseInt(scope.ds.attendedTotalQuestions) + 1;

                    var cnt = 0;
                    var length = scope.currentSection.QUESTIONS.QUESTION.length;
                    for (var i = 0; i < length; i++) {
                        var question = scope.currentSection.QUESTIONS.QUESTION[i];
                        if (question.result) {
                            cnt += 1;
                        }
                    }

                    if (cnt == length) {
                        scope.currentSection.result = true;
                        scope.currentSection.showResult = true;
                    }
                }

                //check for removed answer
                if (answer.length != undefined) {
                    for (var i = 0; i < answer.length; i++) {
                        var correctAnswer = scope.getAnswerText(answer[i].ANSWER)[0];

                        if (scope.compareGroupListAnswers(answerText, correctAnswer)) {
                            answer[i].ANSWER.isRemoved = true;
                        }
                    }
                } else {
                    var correctAnswer = scope.getAnswerText(answer.ANSWER)[0];
                    if (scope.compareGroupListAnswers(answerText, correctAnswer)) {
                        answer.ANSWER.isRemoved = true;
                    }
                }

                //Set focus to first item in popover on correct answer
                timeout(function () {
                    if (scope.ds.isPopoverOpen && itemId) {
                        var nextItem = document.getElementById(itemId);

                        if (nextItem) {
                            nextItem.focus();
                        } else {
                            var popoverContainer = document.getElementById('grpListPopover');

                            if (popoverContainer) {
                                var firstItem = popoverContainer.firstElementChild;
                                firstItem.focus();
                            }
                        }
                    }
                }, 100);
            } else {
                scope.currentSection['@maxAttempts'] = parseInt(scope.currentSection['@maxAttempts']) - 1;
                scope.$emit('wrongAnswer', itemId);
            }

            var questionInteractionInfo = {
                'id': scope.questionNo,
                'questionText': scope.displayAsText(scope.question),
                'questionType': scope.controlType,
                'learnerResponse': data,
                'correctAnswer': correctAnswers.toString(),
                'wasCorrect': result,
                'showAlert': false,
                'dragDropAnswerRemoved': result,
            };

            scope.$emit('updateData', questionInteractionInfo);
        }
        scope.compareGroupListAnswers = function (selected, values) {
            if (values) {
                if (Array.isArray(values)) {
                    for (var i = 0; i < values.length; i++) {
                        if (values[i] && selected.trim() == values[i].trim()) {
                            return true;
                        }
                    }
                } else {
                    if (selected.trim() == values)
                        return true;
                }
            }

            return false;
        };

        scope.wordScrambleDropped = function (data, answer, answerSet) {
            if (scope.question.ANSWERSET.result || scope.currentSection['@maxAttempts'] == 0)
                return false;

            answerSet = answerSet ? answerSet : answer;
            data = data.replace('\n', '').trim();

            if (answer.showResult)
                answer.showResult = false;

            scope.validate(data, answer, answerSet['@maxAttempts'], null, null, answerSet);

            if (answer.result) {
                answer.value = data;
            } else {
                scope.currentSection['@maxAttempts'] = parseInt(scope.currentSection['@maxAttempts']) - 1;
            }

            scope.$emit('updateActivity');
        };

        scope.getGroupListIncorrectAnswers = function (question, answer) {
            var correctAnswer = scope.getAnswerText(answer)[0];

            if (question.items) {
                var matched = false;
                for (var k = 0; k < question.items.length; k++) {
                    var text = question.items[k].data.TEXT ? question.items[k].data.TEXT : question.items[k].data.AUDIO;
                    if (text == correctAnswer) {
                        matched = true;
                        break;
                    }
                }
                if (!matched) {
                    question.items.push({data: answer, result: false});
                }
            } else {
                question.items = [{data: answer, result: false}];
            }
        };

        var maxAttemptsWatcher = scope.$watch('currentSection["@maxAttempts"]', function (newVal) {
            if (newVal == 0 && !scope.currentSection.showResult) {
                if (scope.selectedActivity.type == 'GroupHeading') {
                    for (var i = 0; i < scope.currentSection.QUESTIONS.QUESTION.length; i++) {
                        var question = scope.currentSection.QUESTIONS.QUESTION[i];
                        var result = false;
                        if (question.result != true) {
                            scope.ds.answeredQuestions = parseInt(scope.ds.answeredQuestions) + 1;
                            scope.ds.attendedTotalQuestions = parseInt(scope.ds.attendedTotalQuestions) + 1;
                        } else {
                            result = true;
                        }

                        scope.currentSection.result = result;
                        scope.currentSection.showResult = true;
                        scope.ds.isPopoverOpen = false;
                    }

                    scope.$emit('updateActivity');
                }
                else if (scope.selectedActivity.type == 'GroupList') {
                    for (var i = 0; i < scope.currentSection.QUESTIONS.QUESTION.length; i++) {
                        var question = scope.currentSection.QUESTIONS.QUESTION[i];

                        if (Array.isArray(question.ANSWERSET)) {
                            for (var j = 0; j < question.ANSWERSET.length; j++) {
                                var answer = question.ANSWERSET[j].ANSWER;
                                scope.getGroupListIncorrectAnswers(question, answer);
                            }
                        } else {
                            var answer = question.ANSWERSET.ANSWER;
                            scope.getGroupListIncorrectAnswers(question, answer);
                        }

                        if (question.result == undefined) {
                            question.result = false;
                            scope.ds.attendedTotalQuestions = parseInt(scope.ds.attendedTotalQuestions) + 1;
                        }

                        if (!question.ANSWERSET.attempted) {
                            scope.ds.answeredQuestions = parseInt(scope.ds.answeredQuestions) + 1;
                        }
                    }

                    scope.currentSection.result = false;
                    scope.currentSection.showResult = true;
                    scope.ds.isPopoverOpen = false;

                    scope.$emit('updateActivity');
                }
                else if (scope.selectedActivity.type == 'WordScramble') {
                    scope.question.disabled = true;
                    scope.question.result = false;
                    scope.question.showResult = true;
                    scope.ds.attendedTotalQuestions = parseInt(scope.ds.attendedTotalQuestions) + 1;
                    scope.$emit('updateActivity');
                }
            }
        });

        //Max width for button
        scope.getMaxAnswer = function (answerSet) {
            var result = '';
            var answers = answerSet.ANSWER;

            if (answers) {
                if (Array.isArray(answers)) {
                    for (var i = 0; i < answers.length; i++) {
                        var answer = answers[i];
                        result = answer.TEXT.length > result.length ? answer.TEXT : result;
                    }

                    /*result = answers.filter(function(current) {
                                  if(result.length > current.TEXT.length){
                                     return result;
                                  } else {
                                     result = current.TEXT;
                                     return current.TEXT;
                                  }
                             });*/
                    /*result = answers.reduce(function(prev, current) {
                                 if(prev.length > current.TEXT.length){
                                    return prev;
                                 } else {
                                    prev = current.TEXT;
                                    return current.TEXT;
                                 }
                            },'')*/
                } else {
                    result = answers.TEXT.length > result.length ? answers.TEXT : result;
                }
            }

            return result;
        };

        //Max width for button
        scope.getMaxCharsFromQuestion = function (answerSet) {
            var maxWidth;
            var maxChar = scope.getMaxAnswer(answerSet);
            var input = document.getElementById('maxInput');

            if (input) {
                input.innerHTML = maxChar;
                maxWidth = input.clientWidth + 4;
            }

            return maxWidth;
        };

        scope.getImagePath = function (imageName) {
            return utilityService.getImagePath(scope.selectedActivity.path, imageName)
        };

        scope.checkSpecialActivityAnswer = function(data,region,id,reg){
            if(region == reg){
                $('#'+id).hide();
                var contents =$('#'+id);
                $('#'+reg).prepend($('<img>',{id:id, src:contents[0].src}))
            }
        }

        scope.getImageCaptionPath = function (imgCaptions) {
           if(imgCaptions){
                var captionText = '';
                var icap = imgCaptions.IMAGECAPTION;

                if(Array.isArray(icap)) {
                    var text = icap.filter(function(value){
                        return value['@lang'] == scope.ds.ispeaklanguage;
                    });
                    return text[0].TEXT;
                } else {
                     return icap.TEXT;
                 }
           }
        };

        scope.$on('$destroy', function(){
             maxAttemptsWatcher();
        });
    }
])
;