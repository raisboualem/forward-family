app.controller('resumeModalController', function ($scope, ngDialog, dataService) {
    $scope.ok = function () {
        ngDialog.close(null, true);
    };

    $scope.cancel = function () {
        ngDialog.close(null, false);
    };

    $scope.init = function () {
        $scope.ds = dataService.getDataSource();
    }();
});