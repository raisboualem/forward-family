app.controller("finalQuizController", ['$rootScope', '$scope', '$state', '$stateParams', 'groupService',
    'localStorageService', 'xmlDataService', 'dataService', 'utilityService', '$timeout', '$window', '$compile','$filter','ngAudio','$cookies', 'ngDialog',
    function (rootScope, scope, state, stateParams, groupService, localStorageService,
              xmlDataService, dataService, utilityService, timeout, $window, compile, $filter, ngAudio, cookies, ngDialog) {
        scope.showQ  = false;
        scope.shuffleDragDropAnswers = function (question, questions) {

            if (question && question.ANSWERSET) {
                if (Array.isArray(question.ANSWERSET)) {
                    for (var j = 0; j < question.ANSWERSET.length; j++) {
                        var answer = question.ANSWERSET[j].ANSWER;

                        if (!answer.order) {
                            var order = utilityService.getRandomNumber(questions.length);
                            answer.order = order;
                        }

                        answer.isRemoved = answer.isRemoved ? answer.isRemoved : false;
                        scope.ds.shuffledAnswers.push({
                            ANSWER: answer,
                            questionAudio: {audio: question.AUDIO, audioCaption: question.AUDIOCAPTION}
                        });

                        scope.totalDragDropAnswers = scope.totalDragDropAnswers + 1;
                    }
                } else {
                    var answer = question.ANSWERSET.ANSWER;

                    if (!answer.order) {
                        var order = utilityService.getRandomNumber(questions.length);
                        answer.order = order;
                    }

                    scope.ds.shuffledAnswers.push({
                        ANSWER: answer,
                        questionAudio: {audio: question.AUDIO, audioCaption: question.AUDIOCAPTION}
                    });

                    answer.isRemoved = answer.isRemoved ? answer.isRemoved : false;
                    scope.totalDragDropAnswers = scope.totalDragDropAnswers + 1;
                }
            }
        }

        scope.setMaxInput = function () {
            var container = document.getElementById('finalQuiz-container');

            var element = compile('<span class=\"form-control\" ' +
                'style=\"visibility: hidden;position: absolute;min-width:0;padding:0 18px\" id="maxInput" type="text"></span>')(scope);

            var maxInput = document.getElementById('maxInput');
            if (!maxInput) {
                container.appendChild(element[0]);
            }
        };

        scope.hideTooltip = function () {
            scope.ds.cancelTimeout = timeout(function () {
                scope.ds.showTooltip = false;
            }, 2000);
        }


        scope.adjustTotalQuestions = function(){
            if(scope.totalQuestions){
                scope.totalQuestions--;
            }
        };

        scope.setDragScroll = function(){
            var dragContainer = document.getElementById('dragContainer');
            if(dragContainer) {
                dragContainer.onscroll = function () {
                    scope.$apply(function () {
                        if (dragContainer.scrollTop <= dragContainer.scrollHeight) {
                            scope.dragDropTop = dragContainer.scrollTop;
                        }
                        else {
                            scope.dragDropTop = 0;
                        }
                    });
                };
            }
        };

        scope.quizFetched = function (quiz) {
            scope.quiz = quiz;
            var sectionNo = parseInt(stateParams.section);
            var questionNo = parseInt(stateParams.question);
            scope.currentSectionNo = sectionNo;
            scope.currentQuestionNo = questionNo;
            scope.sections = quiz.SECTIONS.SECTION;
            scope.totalSections = scope.sections.length;

            if (sectionNo) {
                var sections = scope.sections;

                if (sections && Array.isArray(sections)) {
                    var section = sections[sectionNo - 1];

                    if (section) {
                        scope.currentSection = section;

                        var questions = section.QUESTIONS.QUESTION;
                        //var quesCount = 0;
                        if (Array.isArray(questions)) {

                           /* for (var i = 0; i<questions.length; i++) {
                                if(questions[i].ANSWERSET)
                                quesCount++;

                            }
                            scope.totalQuestions = quesCount;*/
                            scope.totalQuestions = questions.length;
                        }
                        else {
                            scope.totalQuestions = 1;
                        }


                        if (questionNo) {
                            if (questions && Array.isArray(questions)) {
                                scope.currentQuestion = questions[questionNo - 1];
                            }
                            else {
                                scope.currentQuestion = questions;
                            }
                             if(scope.currentQuestion.IMAGECAPTIONS){
                                  scope.getImageCaptionsFromLang();
                             }

                        }

                        scope.quizStarted = true;

                        if ((section['@TYPE'] == 'DRAGDROP' || section['@TYPE'] == 'MULTIANSWERDRAGDROP')) {

                            var activityName = 'quizStarted_dragDrop';
                            scope.dragDropRemovedAnswers = 0;
                            scope.totalDragDropAnswers = 0;

                            if (state.current.name != activityName) {
                                localStorageService.removeItem('question_' + scope.selectedQuiz.id);

                                state.go(activityName,
                                    {
                                        'moduleId': scope.moduleId,
                                        'communicativeSkill': scope.communicativeSkill,
                                        'quizId': scope.selectedQuiz.id,
                                        'section': sectionNo,
                                    });
                            }

                            scope.ds.shuffledAnswers = [];

                            if (!Array.isArray(questions)) {
                                scope.currentSection.QUESTIONS.QUESTION = questions = [scope.currentSection.QUESTIONS.QUESTION];
                            }

                            for (var i = 0; i < questions.length; i++) {
                                var question = questions[i];
                                scope.shuffleDragDropAnswers(question, questions);
                            }

                            scope.updateQuiz();

                        }
                        if (section['@TYPE'] == 'FILLTHEBLANKS') {
                            scope.setMaxInput();
                        }

                        if(scope.currentSection.INSTRUCTIONS){
                             scope.getInstructionsFromLang();
                        }
                    }
                }
            }
            scope.getDescriptionFromLang();
        }

        scope.fetchQuizFromXML = function (selectedQuiz) {
            xmlDataService.getXMLData(selectedQuiz.filename).then(function (data) {
                var json = xmlDataService.parseXmlToJSON(data);
                var quiz = json.ACTIVITY;
                var jsonToStringQuiz = JSON.stringify(quiz);

                localStorageService.setItem('quiz_' + scope.selectedQuiz.id, jsonToStringQuiz);

                scope.quizFetched(quiz);
            }, function () {
                console.log('Cant load dialogue xml' + selectedQuiz.filename);
            });
        };

        scope.startQuiz = function (quizStarted) {
            if (quizStarted) {
                localStorageService.clearStorage();

                localStorageService.setItem('section_' + scope.selectedQuiz.id, 1);
                localStorageService.setItem('question_' + scope.selectedQuiz.id, 1);

                state.go('quizStarted',
                    {
                        'moduleId': scope.moduleId,
                        'quizId': scope.selectedQuiz.id, 'section': 1, 'question': 1
                    });
            } else {
                scope.closeQuiz();
            }
        };


        scope.showNextQuestion = function () {
            var nextQuestionNo = 0;
            var nextSectionNo = 0;
            var showScore = false;

            if (scope.currentQuestionNo < scope.totalQuestions) {
                nextQuestionNo = scope.currentQuestionNo + 1;
                nextSectionNo = scope.currentSectionNo;
            } else if (scope.currentSectionNo < scope.totalSections) {
                nextSectionNo = scope.currentSectionNo + 1;
                nextQuestionNo = 1;
            } else {
                showScore = true;

            }

            if (nextSectionNo || showScore) {
                var nextSection = scope.sections[nextSectionNo - 1];
                var activityName;

                if (showScore) {
                    activityName = 'quiz_showScore';
                    localStorageService.removeItem('section_' + scope.selectedQuiz.id);
                    localStorageService.removeItem('question_' + scope.selectedQuiz.id);
                } else if (nextSection['@TYPE'] == 'DRAGDROP' ||
                    nextSection['@TYPE'] == 'MULTIANSWERDRAGDROP') {
                    activityName = 'quizStarted_dragDrop';
                    localStorageService.setItem('section_' + scope.selectedQuiz.id, nextSectionNo);
                    localStorageService.removeItem('question_' + scope.selectedQuiz.id);
                } else {
                    activityName = 'quizStarted';
                    localStorageService.setItem('section_' + scope.selectedQuiz.id, nextSectionNo);
                    localStorageService.setItem('question_' + scope.selectedQuiz.id, nextQuestionNo);
                }
                if(scope.ds.sound){
                   scope.ds.sound.stop();
                }

                state.go(activityName,
                    {
                        'moduleId': scope.moduleId,
                        'quizId': scope.selectedQuiz.id,
                        'section': nextSectionNo,
                        'question': nextQuestionNo,
                    });
            }
        };

        scope.updateQuiz = function () {
            var jsonToStringQuiz = JSON.stringify(scope.quiz);
            localStorageService.setItem('quiz_' + scope.selectedQuiz.id, jsonToStringQuiz);
        };


        var updateDataHandler = scope.$on('updateData', function (event, data) {
            var jsonToStringQuiz = JSON.stringify(scope.quiz);

            localStorageService.setItem('totalScore_' + scope.selectedQuiz.id + '_' +
                scope.currentSectionNo, scope.ds.totalScore);
            localStorageService.setItem('answeredQuestions_' + scope.selectedQuiz.id + '_' +
                scope.currentSectionNo, scope.ds.answeredQuestions);
            localStorageService.setItem('attendedTotalQuestions_' + scope.selectedQuiz.id + '_' +
                scope.currentSectionNo, scope.ds.attendedTotalQuestions);

            localStorageService.setItem('quiz_' + scope.selectedQuiz.id, jsonToStringQuiz);

            //Add interaction
            var objectiveId = scope.selectedQuiz.id;

            data.objectiveId = objectiveId;

            if (data.dragDropAnswerRemoved) {
                scope.dragDropRemovedAnswers = scope.dragDropRemovedAnswers + 1;

                if (scope.totalDragDropAnswers == scope.dragDropRemovedAnswers) {
                    scope.dragDropAnswersRemoved = true;
                }
            }

            var percentage = scope.calculateTotalScore();
            scope.ds.quizScore = percentage;


            if (percentage >= 90) {
                ScormProcessSetStatus(scope.selectedQuiz.id, 'passed');

                //Set suspend data status
                utilityService.setSuspendDataStatus(scope.selectedQuiz.id, 'success');
                utilityService.setSuspendDataEOMAttempts(scope.selectedQuiz.id, 0, true);
            } else if (scope.currentSectionNo == scope.totalSections &&
                scope.ds.attendedTotalQuestions == scope.totalQuestions) {
                scope.attempts = scope.attempts + 1;
                utilityService.setSuspendDataEOMAttempts(scope.selectedQuiz.id, scope.attempts);
            }

            if(scope.ds.sound){
                scope.ds.sound.stop();
            }

            utilityService.playFeedbackAudio(data.wasCorrect, 'loadAudio');


            scope.$emit('recordInteraction', data);
        });

        scope.getAssetsPath = function (audioName) {
            if (audioName['#text'] !== undefined) {
                audioName = audioName['#text'];
            }

            return utilityService.getAudioPath(scope.selectedQuiz.path, audioName);
        };

        scope.playAudio = function (src, tooltipIndex) {
            timeout.cancel(scope.ds.cancelTimeout);
            scope.ds.showTooltip = true;
            scope.tooltipIndex = tooltipIndex;
            scope.ds.FinalQuizFocus = true;
//            angular.element('#loadAudio').attr("src", scope.getAssetsPath(src));
//            audio = angular.element('#loadAudio');
//            audio[0].play();
            scope.ds.sound = ngAudio.load(scope.getAssetsPath(src));
            scope.ds.sound.play();

        };

        scope.hidetip = function () {
            scope.ds.FinalQuizFocus = false;
        };

        scope.calculateTotalScore = function () {
            var totalQuestions = 0;
            var totalScore = 0;
            var quizId = stateParams.quizId;

            for (var i = 0; i < scope.sections.length; i++) {
                var section = scope.sections[i];
                var sectionScore = 0;
                if (section['@TYPE'] == 'MULTICHOICEMULTIANSWER') {
                    totalQuestions = totalQuestions + (section.QUESTIONS.QUESTION.length);
                    sectionScore = parseFloat(localStorageService.getItem('totalScore_' + quizId + '_' + (i + 1)));
                    sectionScore = sectionScore ? (sectionScore) : 0;
                } else {
                    var questions = section.QUESTIONS.QUESTION;
                    var quesCount = 0;
                    for (var j = 0; j < questions.length; j++) {
                         if(questions[j].ANSWERSET) {
                            quesCount++;
                         }
                    }
                    if(!Array.isArray(questions)){
                       quesCount = 1;
                    }
                    totalQuestions = totalQuestions + quesCount;
                    sectionScore = parseFloat(localStorageService.getItem('totalScore_' + quizId + '_' + (i + 1)));
                    sectionScore = sectionScore ? sectionScore : 0;
                }

                totalScore = totalScore + sectionScore;
            }

            var percentage = 0;

            if (totalQuestions) {
                percentage = (totalScore / totalQuestions) * 100;
            }

            return percentage.toFixed(2);
        };

        scope.calculateScore = function (sectionNo) {
            var section = scope.sections[sectionNo - 1];
            var questions = section.QUESTIONS.QUESTION;
            var quesCount = 0;
            for (var i = 0; i < questions.length; i++) {
                 if(questions[i].ANSWERSET) {
                    quesCount++;
                 }
             }
            var totalQuestions = quesCount;
            var quizId = stateParams.quizId;
            var totalScore = localStorageService.getItem('totalScore_' + quizId + '_' + sectionNo);
            var percentage = 0;

            if (totalQuestions) {
                percentage = (totalScore / totalQuestions) * 100;
            }

            return percentage.toFixed(2);
        };

        scope.closeQuiz = function () {
            localStorageService.clearStorage();

            state.go('groups',
                {
                    'moduleId': scope.moduleId,
                    'selectedUnit': 'quiz',
                });
        };

        var answeredQuestionWatcher = scope.$watch('ds.answeredQuestions', function (newVal) {
            if (newVal == 1) {
                if (scope.suspendDataStatus != 'success') {
                    ScormProcessSetStatus(scope.selectedQuiz.id, "incomplete");
                    utilityService.setSuspendDataStatus(scope.selectedQuiz.id, 'inProgress');
                }
            }
        });

        scope.getFileName = function (filePath) {
            if (filePath) {
                var temp = filePath.split('/');
                filePath = temp[temp.length - 1];
            }

            return filePath;
        };

        scope.play_audio = function (src, tooltipIndex) {
            timeout.cancel(scope.ds.cancelTimeout);
            scope.ds.showTooltip = true;
            scope.tooltipIndex = tooltipIndex;
            scope.ds.FinalQuizFocus = true;
            angular.element('#load_audio').attr("src", utilityService.getAudioPath(scope.selectedQuiz.path, src));

            angular.element('#load_audio').attr("alt", scope.getFileName(src));
            audio = angular.element('#load_audio');
            audio[0].play();
        };

        scope.getImageAssetsPath = function (image) {
            return utilityService.getImagePath(scope.selectedQuiz.path, image);
        };

        scope.getControlType = function (sectionType) {
            if (sectionType && sectionType == 'DRAGDROP') {
                return 'drag_and_drop';
            }
            else if (sectionType && sectionType == 'MULTIANSWERDRAGDROP') {
                return 'multianswerdragdrop';
            }
            else {
                var answerSet = Array.isArray(scope.currentQuestion.ANSWERSET) ?
                    scope.currentQuestion.ANSWERSET[0] : scope.currentQuestion.ANSWERSET;

                var controlType = answerSet['@controlType'] ?
                    answerSet['@controlType'].toLowerCase() :
                    console.log(scope.currentSection);
                    scope.currentSection['@TYPE'].toLowerCase();

                return controlType;
            }
        };


        scope.getScoreHeight = function () {
            var scoreMain = document.getElementById('scoreMain');
            var close = document.getElementById('close');
            var smh = scoreMain ? scoreMain.offsetTop : 0;
            var ch = close ? close.scrollHeight : 0;

            return $window.innerHeight - smh - ch - 40;
        };

        scope.getInstructionHeight = function () {
            var instructionDiv = document.getElementById('quizStarted-instructions-main');
            var questionContainer = document.getElementById('quiz-question-container');
            var questionContainerDragDrop = document.getElementById('dragContainer');
            var smh = instructionDiv ? instructionDiv.offsetTop : 0;
            if (questionContainer) {
                var ch = questionContainer ? questionContainer.scrollHeight : 0;
            }
            else {
                var ch = questionContainerDragDrop ? questionContainerDragDrop.scrollHeight : 0;
            }


            return $window.innerHeight - smh - ch;
        };

        scope.getMainQuestionHeight = function () {
            var answerContainer = document.getElementById('answerSet');
            var nextQuestion = document.getElementById('nextQuestion');
            var smh = answerContainer ? answerContainer.offsetTop : 0;
            var ch = nextQuestion ? nextQuestion.scrollHeight : 0;


            return $window.innerHeight - smh - ch - 40;

        };
        scope.getQuestionDivHeight = function(){
             var questionAnswerContainer = document.getElementById('quiz-question-container');
             var nextQuestiondrag = document.getElementById('nextQuestion');
             var quesAnsPar = document.getElementById('QuestionAnswerContainerParent');
             var imgContainer = document.getElementById('imageContainer');
             var quesCont = document.getElementById('EOMQuestionDiv');

             var dnq = nextQuestiondrag ? nextQuestiondrag.scrollHeight : 0;
             var qnp = quesAnsPar ? quesAnsPar.scrollHeight : 0;
             var ques = questionAnswerContainer ? questionAnswerContainer.offsetTop : 0;
             var sizeOfQuestionContainer = $window.innerHeight - ques;
             var sizeWithOutNext = sizeOfQuestionContainer - dnq;

             if(imgContainer) {
                var splitedDiv = (sizeWithOutNext / 3);
             }
             else if(!quesCont){
                 var splitedDiv = sizeWithOutNext - 50;
             }
             else {
                 var splitedDiv = (sizeWithOutNext / 2 );
             }

             return splitedDiv;
        }

        scope.getEOMquestionPadding = function() {
            var questionAnswerContainerParent = document.getElementById('quiz-question-container');
            var questionAnswerContainer = document.getElementById('QuestionAnswerContainerParent');
             var nextContainer = document.getElementById('nextQuestion');
             var wordScramble = document.getElementsByClassName('WordScrambleClick');
             var moduleId = parseInt(stateParams.moduleId);

             var questioncontainerOffsetHeight = questionAnswerContainerParent ? questionAnswerContainerParent.offsetTop : 0;
             var questionContainerHeight = $window.innerHeight - questioncontainerOffsetHeight;
             var nextHeight = nextContainer ? nextContainer.scrollHeight : 0;
                if (window.screenY != 0) {
                     if(questionAnswerContainer && wordScramble.length == 0){
                            var contentHeight = 250;
                            if(scope.ds.mainMenuLinkLang == 'fr' && moduleId == 11 || scope.ds.mainMenuLinkLang == 'en' && moduleId == 14){
                                var contentHeight = questionContainerHeight;
                            }
                        }

                var pad = (questionContainerHeight - contentHeight - nextHeight) / 4;
               }
                 else {
                    var pad = (questionContainerHeight - contentHeight - nextHeight - 30) / 4;
                 }

             return pad;

        };

        scope.getMainQuestionDragHeight = function () {
            var dragContainer = document.getElementById('dragContainer');
            var nextQuestiondrag = document.getElementById('nextQuestions');
            var dc = dragContainer ? dragContainer.offsetTop : 0;
            var dnq = nextQuestiondrag ? nextQuestiondrag.scrollHeight : 0;

            return $window.innerHeight - dc - dnq;
        };

        scope.getLandingPageHeight = function () {
            var LandingPageHeight = document.getElementById('landingpage-instruction');
            var InstructionButtonHeight = document.getElementById('nextQuestion');
            var lpot = LandingPageHeight ? LandingPageHeight.offsetTop : 0;
            var instbtn = InstructionButtonHeight ? InstructionButtonHeight.scrollHeight : 0;

            return $window.innerHeight - lpot - instbtn - 100;
        };

        scope.getInputImageHeight = function () {
            var imagediv = document.getElementById('QuestionAnswerContainer');
            var imageanddiv = document.getElementById('answerSet');
            var nextbtn = document.getElementById('quizInstructions');
            var imgd = imagediv ? imagediv.offsetTop : 0;
            var ad = imageanddiv ? imageanddiv.scrollHeight : 0;
            var nb = nextbtn ? nextbtn.scrollHeight : 0;

            return $window.innerHeight - imgd - ad - nb - 40;
        };


        scope.addAccent = function ($event, accent) {
            rootScope.$broadcast('reset-focus');
            rootScope.$broadcast('add-french-accent', accent);
        };

        $window.onresize = function () {
            scope.$apply(function () {
                scope.sh = scope.getScoreHeight();
                scope.qh = scope.getMainQuestionHeight();
                scope.dh = scope.getMainQuestionDragHeight();
                scope.ih = scope.getLandingPageHeight();
                scope.iih = scope.getInputImageHeight();
                scope.instHeight = scope.getInstructionHeight();
                scope.padTop = scope.getEOMquestionPadding();
                scope.questionDivHeight = scope.getQuestionDivHeight();

            });
        };

        var accessibilityChangeHandler = scope.$on('accessibilityChanged', function () {
            scope.accessChangeTimeout = timeout(function () {
                scope.sh = scope.getScoreHeight();
                scope.qh = scope.getMainQuestionHeight();
                scope.dh = scope.getMainQuestionDragHeight();
                scope.ih = scope.getLandingPageHeight();
                scope.iih = scope.getInputImageHeight();
                scope.instHeight = scope.getInstructionHeight();
                scope.padTop = scope.getEOMquestionPadding();
                scope.questionDivHeight = scope.getQuestionDivHeight();
                scope.getDescriptionFromLang();
                scope.getInstructionsFromLang();
                if(scope.currentQuestion.IMAGECAPTIONS){
                     scope.getImageCaptionsFromLang();
                }
            }, 1000);
        });

        scope.myPopover = {
            isOpen: false,
            templateUrl: 'myPopoverTemplate.html',
            open: function open() {
                scope.myPopover.isOpen = true;
                scope.myPopover.data = 'hello';
                scope.myPopover.image = utilityService.getImageAssetsPath(currentQuestion.IMAGE);
            },
            close: function close() {
                scope.myPopover.isOpen = false;
            }
        };

        var disableQuestionWatcher = scope.$watch('currentQuestion.disabled || ds.attendedTotalQuestions == totalQuestions', function (value) {
                if(value == true) //&& scope.currentSection['@TYPE'] !='MULTIPLECHOICE'
                {
                  scope.currentQuestionDisableTimeout = timeout(function () {
                        scope.sh = scope.getScoreHeight();
                        scope.qh = scope.getMainQuestionHeight();
                        scope.dh = scope.getMainQuestionDragHeight();
                        scope.ih = scope.getLandingPageHeight();
                        scope.iih = scope.getInputImageHeight();
                        scope.instHeight = scope.getInstructionHeight();
                        scope.questionDivHeight = scope.getQuestionDivHeight();

                    }, 500);
                }
            });

         scope.htmlDecode = function(input){
            var e = document.createElement('div');
            e.innerHTML = input;
            return e.childNodes.length === 0 ? "" : e.innerText;
         }

         scope.makeEomFocusActive = function (e) {
            if(e.keyCode == 9)
             scope.focusActive = true;
         };

         scope.makeEomFocusDeActive = function () {
             scope.focusActive = false;
         };

         scope.replaceBlank = function(questionText) {
            var langAttr = rootScope.isFrenchModule ? "<p class='hiddenValidationDescription'>Entrée vide</p>" : "<p class='hiddenValidationDescription'>blank input</p>";
            var regex = /_/g;
            var matched = questionText.match(regex);//MULTICHOICEMULTIANSWER
            if(matched && scope.currentSection['@TYPE'] != 'MULTICHOICEMULTIANSWER') {
                var strMatched = matched.join("");
                var spanTagged = '<span aria-hidden="true">' + strMatched + '</span>';
                var replaced = questionText.replace(strMatched, spanTagged + langAttr);
                return replaced;
            } else if(scope.currentSection['@TYPE'] == 'MULTICHOICEMULTIANSWER'){
                var strMatched = matched.slice(0,(matched.length/2));
                var strArr = strMatched.join("");
                var spanTagged = '<span aria-hidden="true">' + strArr + '</span>';
                var replaced = questionText.replaceAll(strArr, spanTagged + langAttr);
                return replaced;
            } else {
                return questionText;
            }
         };
         scope.getImageCaptionsFromLang = function() {
          var imgC = scope.currentQuestion.IMAGECAPTIONS.IMAGECAPTION;
          var resultImgC = '';
              if(imgC) {
                  resultImgC = imgC.filter(function(elem,indx){
                      return scope.ds.ispeaklanguage === elem['@lang'];
                  })
                  scope.imageCaptionObj = resultImgC[0].TEXT;
              }
        };

        scope.getDescriptionFromLang = function() {
             var desc = scope.quiz.DESCRIPTIONS.DESCRIPTION;
             var resultDesc = '';
                 if(Array.isArray(desc)) {
                        resultDesc = desc.filter(function(elem,indx){
                            if(parseInt(scope.moduleId) <=8){
                                return scope.ds.ispeaklanguage === elem['@lang'];
                            }else{
                            return scope.ds.mainMenuLinkLang === elem['@lang'];
                            }
                        })
              }
             scope.landRow = resultDesc[0];
        };

        scope.getInstructionsFromLang = function() {
             var inst = scope.currentSection.INSTRUCTIONS.INSTRUCTION;
             var resultInst = '';
                 if(Array.isArray(inst)) {
                        resultInst = inst.filter(function(elem,indx){
                            return true;
                        })
                 }
             scope.instRowObj = resultInst[0].TEXT;
        };

        scope.showTranslationInstruction = function(){
             var inst = scope.currentSection.INSTRUCTIONS.INSTRUCTION;
             var resultInst = '';
                 if(Array.isArray(inst)) {
                        resultInst = inst.filter(function(elem,indx){
                           return true;
                        })
                 }
             scope.instRowObj = resultInst[0].TEXT;

         };

        // Added another function for enabling toggle function in Instruction and Description
        scope.translateDes= function(){
            var click = $(this).data('clicks') || 2;
            if (click%2 == 1){
            		scope.getDescriptionFromLang();
            }else{
            		scope.showTranslationDescription();
             }
             $(this).data('clicks',click+1);
        };

        scope.translateIns= function(){
             var click = $(this).data('clicks') || 2;
             if (click%2 == 1){
             		scope.getInstructionsFromLang();
             }else{
             		scope.showTranslationInstruction();
              }
              $(this).data('clicks',click+1);
        };

        scope.showTranslationDescription = function(){
             if(scope.ds.mainMenuLinkLang =='en'){
                if(rootScope.translateLang == '0'){
                  scope.ds.ispeaklanguage = 'fr';
                }
                else if(rootScope.translateLang == '1'){
                 scope.ds.ispeaklanguage = 'es';
                 }
                 else if(rootScope.translateLang == '2'){
                  scope.ds.ispeaklanguage = 'zh';
                 }
            }
             if(scope.ds.mainMenuLinkLang =='fr' ){
                if(rootScope.translateLang == '0'){
                 scope.ds.ispeaklanguage = 'en';
                }else if(rootScope.translateLang == '1'){
                 scope.ds.ispeaklanguage = 'es';
                }
                else if(rootScope.translateLang == '2'){
                 scope.ds.ispeaklanguage = 'zh';
                }
             }

             var desc = scope.quiz.DESCRIPTIONS.DESCRIPTION;
             var resultDesc = '';
                 if(Array.isArray(desc)) {
                        resultDesc = desc.filter(function(elem,indx){
                            if(parseInt(scope.moduleId) <=8){
                                return scope.ds.mainMenuLinkLang === elem['@lang'];
                            }
                        })

                           if (parseInt(scope.moduleId)>=9 && scope.ds.ispeaklanguage) {
                               resultDesc = desc.filter(function(elem,indx){
                                    return scope.ds.ispeaklanguage === elem['@lang'];
                            })
                        }
                 }

             scope.landRow = resultDesc[0];
        };

        scope.init = function () {
            var quizId = stateParams.quizId;
            var sectionNo = parseInt(stateParams.section);
            var questionNo = parseInt(stateParams.question);
            var moduleId = stateParams.moduleId;
            scope.ds.mainTitle = 'QUIZ';

            scope.accents = [{text: 'à'}, {text: 'À'}, {text: 'â'}, {text: 'Â'}, {text: 'ç'}, {text: 'Ç'}, {text: 'é'}, {text: 'É'},
                {text: 'è'}, {text: 'È'}, {text: 'ê'}, {text: 'Ê'}, {text: 'ë'}, {text: 'Ë'},
                {text: 'î'}, {text: 'Î'}, {text: 'ï'}, {text: 'Ï'}, {text: 'ô'}, {text: 'Ô'}, {text: 'ù'},
                {text: 'Ù'}, {text: 'û'}, {text: 'Û'}, {text: 'ü'}, {text: 'Ü'}, {text: 'ÿ'}, {text: 'Ÿ'}];

            scope.selectedQuiz = groupService.getItem(quizId, moduleId);

            scope.attempts = utilityService.getSuspendDataEOMAttempts(scope.selectedQuiz.id);
            scope.attempts = scope.attempts ? parseInt(scope.attempts) : 0;
            scope.suspendDataStatus = utilityService.getSuspendDataStatus(quizId);
            scope.shuffledAnswers = [];

            var startTime = localStorageService.getItem('quizStartTime_' + quizId);
            if (!startTime) {
                var quizStartTime = new Date();
                quizStartTime = quizStartTime.toString();
                localStorageService.setItem('quizStartTime_' + quizId, quizStartTime);
                scope.startTime = moment(quizStartTime);
            } else {
                var quizStartTime = localStorageService.getItem('quizStartTime_' + quizId);
                scope.startTime = moment(quizStartTime);
            }

            var storedSectionNo = parseInt(localStorageService.getItem('section_' + scope.selectedQuiz.id));
            var storedQuestionNo = parseInt(localStorageService.getItem('question_' + scope.selectedQuiz.id));

            scope.moduleId = stateParams.moduleId;

            ScormProcessSetStatus(quizId, "browsed");
            //
            //if ((storedSectionNo && sectionNo != storedSectionNo)
            //    || (storedQuestionNo && storedQuestionNo != questionNo)) {
            //    var activityName = storedQuestionNo ? 'quizStarted' : 'quizStarted_dragDrop';
            //    state.go(activityName,
            //        {
            //            'moduleId': scope.moduleId,
            //            'quizId': scope.selectedQuiz.id,
            //            'section': storedSectionNo,
            //            'question': storedQuestionNo,
            //        });
            //}

            scope.ds = dataService.getDataSource();
            scope.ds.communicativeSkill = 'quiz';
            scope.ds.quizId = quizId;
            scope.showScore = state.current.name == 'quiz_showScore';
            utilityService.checkForSessionAlive();

            var answeredQuestions = localStorageService.getItem('answeredQuestions_' + quizId + '_' + sectionNo);
            scope.ds.answeredQuestions = answeredQuestions ?
                parseInt(answeredQuestions) : 0;

            var attendedTotalQuestions = localStorageService.getItem('attendedTotalQuestions_' + quizId + '_' + sectionNo);
            scope.ds.attendedTotalQuestions = attendedTotalQuestions ?
                parseInt(attendedTotalQuestions) : 0;


            scope.quizStarted = false;
            scope.timePassed = parseFloat(localStorageService.getItem('timePassed_' + scope.selectedQuiz.id));
            scope.timePassed = scope.timePassed ? scope.timePassed : 0;

            var title = $filter('localization')('QUIZ', scope.ds.language);
            utilityService.setPageTitle(title);

            var totalScore = localStorageService.getItem('totalScore_' + quizId + '_' + sectionNo);
            scope.ds.totalScore = totalScore ? parseFloat(totalScore) : 0;

            if (scope.selectedQuiz) {
                rootScope.inProgress = true;

                //Fetch dialogue
                var quiz = localStorageService.getItem('quiz_' + scope.selectedQuiz.id);

                if (quiz) {
                    var jsonQuiz = JSON.parse(quiz);
                    scope.quizFetched(jsonQuiz);
                } else {
                    scope.fetchQuizFromXML(scope.selectedQuiz);
                }

                scope.eomHeightCalTimeout = timeout(function () {
                    scope.sh = scope.getScoreHeight();
                    scope.qh = scope.getMainQuestionHeight();
                    scope.dh = scope.getMainQuestionDragHeight();
                    scope.ih = scope.getLandingPageHeight();
                    scope.iih = scope.getInputImageHeight();
                    scope.questionDivHeight = scope.getQuestionDivHeight();
                    scope.setDragScroll();
                }, 1000);

                var browserDelays = (storedSectionNo == '1' && storedQuestionNo == '1') ? 500 : 100;

               var browserDelays = (storedSectionNo == '1' && storedQuestionNo == '1') ? 500 : 100;
                   timeout(function () {
                      scope.padTop = scope.getEOMquestionPadding();
                      scope.showQ  = true;
                   }, browserDelays);

            } else {
                location.path('/');
            }
        }();
    }
])
;