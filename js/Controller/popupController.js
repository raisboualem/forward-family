app.controller('popupController', function ($scope, ngDialog, dataService, $rootScope) {

    $scope.cancel = function () {
        ngDialog.close(null, false);
    };

    $scope.iSpeakLangWatcher = function () {
             if($scope.ds.mainMenuLinkLang =='en'){
                if($rootScope.translateLang == '0'){
                  $scope.ds.ispeaklanguage = 'fr';
                }
                else if($rootScope.translateLang == '1'){
                 $scope.ds.ispeaklanguage = 'es';
                 }
                else if($rootScope.translateLang == '2'){
                 $scope.ds.ispeaklanguage = 'zh';
                 }
             }
             if($scope.ds.mainMenuLinkLang =='fr' ){
                if($rootScope.translateLang == '0'){
                 $scope.ds.ispeaklanguage = 'en';
                }else  if($rootScope.translateLang == '1'){
                 $scope.ds.ispeaklanguage = 'es';
                }
                else if($rootScope.translateLang == '2'){
                $scope.ds.ispeaklanguage = 'zh';
                }
             }
        };

      $scope.selectiSpeakLang  = function(value){
            $scope.selectiSpeakLangControl = value;
           };
        $scope.applyChanges = function () {
              ngDialog.close();
              $rootScope.translateLang = $scope.selectiSpeakLangControl;
              $scope.iSpeakLangWatcher();
              $rootScope.$broadcast('accessibilityChanged');
               var modulePreferences= JSON.stringify(
                   {"ISpeakLangDB":$scope.selectiSpeakLangControl,
                   "SystemLangFromDB":$rootScope.sysLang ,
                   "FontSizeFromDB":$rootScope.fontSize  ,
                   "AudioCaptionFromDB":$rootScope.audioCaptionControl ,
                   "VideoCaptionFromDB":$rootScope.videoCaptionControl ,
                   "ImageCaptionFromDB":$rootScope.imageCaptionControl }
               );
			 ScormProcessSetValue('LMSModulePreferences',modulePreferences);
          };


    $scope.init = function () {
        $scope.ds = dataService.getDataSource();

         if($scope.ds.mainMenuLinkLang == 'en'){
              $scope.langButtonLabel = ['Je parle français', 'Yo hablo español','我说西班牙语'];
         }
         if($scope.ds.mainMenuLinkLang == 'fr'){
              $scope.langButtonLabel = ['I speak English', 'Yo hablo español','我说西班牙语'];
         }
    }();
});