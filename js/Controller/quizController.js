﻿app.controller('quizController', ['$scope', '$rootScope', 'ActivityService',
    'dataService', 'localStorageService', 'dialogueService', 'whatsNewService', 'quizService', '$location', '$stateParams'
    , '$state', 'xmlDataService', '$timeout', '$window', '$cookies','ngDialog', 'env',
    function ($scope, $rootScope, ActivityService, dataService, localStorageService,
              dialogueService, whatsNewService, quizService, location, stateParams, state, xmlDataService, $timeout, $window, cookies, ngDialog, env) {

        $scope.start = function () {
            state.go('groups', {'moduleId': $scope.moduleId, 'selectedUnit': 'whats_new'});
        };

        $scope.fetchLandingPageFromXML = function () {
            var module = $rootScope.isFrenchModule ? 'fsl' : 'esl';
            var fileName = 'm' +$scope.moduleId + '-' + module + '-objectives-landing';
            xmlDataService.getXMLData(fileName).then(function (data) {
                var json = xmlDataService.parseXmlToJSON(data);
                $scope.landingPage = json.ACTIVITY;
                $scope.landingPage.SECTIONS.SECTION = Array.isArray($scope.landingPage.SECTIONS.SECTION)?$scope.landingPage.SECTIONS.SECTION:[$scope.landingPage.SECTIONS.SECTION];
                if($scope.ds.sound){
                  $scope.ds.sound.stop();
                }
                $scope.getObjectivesFromLang();
            },function () {
                console.log('Cant load xml');
            });
        };

       $scope.getObjectivesFromLang = function(){
        if($scope.ds.mainMenuLinkLang =='en'){
           if($rootScope.translateLang == '0'){
             $scope.ds.ispeaklanguage = 'fr';
           }
           else if($rootScope.translateLang == '1'){
            $scope.ds.ispeaklanguage = 'es';
           }
           else if($rootScope.translateLang == '2'){
            $scope.ds.ispeaklanguage = 'zh';
           }
        }
        if($scope.ds.mainMenuLinkLang =='fr' ){
           if($rootScope.translateLang == '0'){
            $scope.ds.ispeaklanguage = 'en';
           }else if($rootScope.translateLang == '1'){
            $scope.ds.ispeaklanguage = 'es';
           }
           else if($rootScope.translateLang == '2'){
            $scope.ds.ispeaklanguage = 'zh';
           }
        }

        $scope.title = [];
        $scope.heading = [];
        $scope.objective = [];
        var section = $scope.landingPage.SECTIONS.SECTION;
            for (i = 0; i < section.length; i++) {
                 var title =  $scope.landingPage.SECTIONS.SECTION[i].TITLES.TITLE;
                 var heading = $scope.landingPage.SECTIONS.SECTION[i].HEADINGS.HEADING;
                 var objectives = section[i].OBJECTIVES.OBJECTIVE;
                 var resultTitle = '';
                 var resultHeading = '';
                 var resultObjective = '';

                  if (section){
                          resultTitle = title.filter(function(elem,indx){
                           if(parseInt($scope.moduleId) <=8){
                             return $scope.ds.ispeaklanguage === elem['@lang'];
                             }
                             else{
                              return $scope.ds.mainMenuLinkLang === elem['@lang'];
                             }

                           })

                           resultHeading = heading.filter(function(elem,indx){
                           if(parseInt($scope.moduleId) <=8){
                             return $scope.ds.ispeaklanguage === elem['@lang'];
                           }
                           else{
                             return $scope.ds.mainMenuLinkLang === elem['@lang'];
                           }
                           })

                           resultObjective = objectives.filter(function(elem,indx){
                           if(parseInt($scope.moduleId) <=8){
                             return $scope.ds.ispeaklanguage === elem['@lang'];
                           }
                           else{
                             return $scope.ds.mainMenuLinkLang === elem['@lang'];
                           }
                           })
                            $scope.objective[i] = resultObjective[0].ROW;
                  }

                  $scope.title[i] = resultTitle[0];
                  $scope.heading[i] = resultHeading[0];
            }

       };

      $scope.showTranslationObjectives = function(){
      $scope.title = [];
      $scope.heading = [];
      $scope.objective = [];
      var section = $scope.landingPage.SECTIONS.SECTION;
           for (i = 0; i < section.length; i++) {
           var title =  $scope.landingPage.SECTIONS.SECTION[i].TITLES.TITLE;
           var heading = $scope.landingPage.SECTIONS.SECTION[i].HEADINGS.HEADING;
           var objectives = section[i].OBJECTIVES.OBJECTIVE;
           var resultTitle = '';
           var resultHeading = '';
           var resultObjective = '';

               if (section){
                   resultTitle = title.filter(function(elem,indx){
                    if(parseInt($scope.moduleId) <=8){
                      return $scope.ds.mainMenuLinkLang === elem['@lang'];
                      }
                      else{
                       return $scope.ds.ispeaklanguage === elem['@lang'];
                      }

                    })
                    resultHeading = heading.filter(function(elem,indx){
                    if(parseInt($scope.moduleId) <=8){
                      return $scope.ds.mainMenuLinkLang === elem['@lang'];
                    }
                    else{
                      return $scope.ds.ispeaklanguage === elem['@lang'];
                    }
                    })

                    resultObjective = objectives.filter(function(elem,indx){
                    if(parseInt($scope.moduleId) <=8){
                      return $scope.ds.mainMenuLinkLang === elem['@lang'];
                    }
                    else{
                      return $scope.ds.ispeaklanguage === elem['@lang'];
                    }
                    })
                     $scope.objective[i] = resultObjective[0].ROW;
               }

               $scope.title[i] = resultTitle[0];
               $scope.heading[i] = resultHeading[0];
           }

      };

      $scope.translateObj= function(){
        var click = $(this).data('clicks') || 2;
        if (click%2 == 1){
        		$scope.getObjectivesFromLang();
        }else{
        		$scope.showTranslationObjectives();
        }
        $(this).data('clicks',click+1);
       };

        var accessChangeEvent = $scope.$on('accessibilityChanged', function () {
            $timeout(function () {
                $scope.getHeight();
                $scope.getObjectivesFromLang();
            }, 100);
        });

        $scope.getHeight = function () {
            $timeout(function () {
                var instructions = document.getElementById('instructions');
                var startBtn = document.getElementById('startBtn');
                var translationLabel = document.getElementById('transLabel') ;
                var padBottom;

                 if ($rootScope.fontSize == "4") {
                    padBottom = 90;
                 } else if ($rootScope.fontSize == "3") {
                     padBottom = 55;
                 } else if ($rootScope.fontSize == "2") {
                    padBottom = 30;
                 }

                if (!$scope.instructionHeight && startBtn && startBtn.offsetTop + startBtn.scrollHeight + 140 > $window.innerHeight)
                    var qcHeight = instructions ? instructions.offsetTop : 0;
                    var transHeight = translationLabel ? translationLabel.scrollHeight : 0;
                    var fHeight = startBtn ? startBtn.scrollHeight : 0;
                    fHeight = fHeight ? fHeight : 90;
                    if (qcHeight){
                        $scope.saveqcHeigt = qcHeight;
                        $scope.saveTransHeigt = transHeight;
                    }
                    else{
                        qcHeight = $scope.saveqcHeigt;
                        transHeight = $scope.saveTransHeigt;
                    }
                    $scope.instructionHeight = $window.innerHeight - qcHeight - fHeight - transHeight - padBottom;
                }, 100);
        };

        $window.onresize = function () {
            $scope.$apply(function () {
              $scope.getHeight();
            });
        };

         $scope.$on('$destroy', function(){
             accessChangeEvent();
             $timeout.cancel( $scope.getHeightTimeout );
         });

        $scope.init = function () {
            $scope.ds = dataService.getDataSource();
            $scope.moduleId = stateParams.moduleId;
            $scope.communicativeSkill = stateParams.communicativeSkill;
            $scope.fetchLandingPageFromXML();
            $scope.ds.mainTitle = 'MAIN_TITLE_OBJECTIVE';

            $scope.getHeightTimeout = $timeout(function () {
                $scope.getHeight();
            }, 200);

            if(ISpeakLangFromDB != null)
            {
            	$rootScope.translateLang = ISpeakLangFromDB;
            }
            else
            {
            	if(cookies.get('iSpeakLanguage'))
            	{
            		$rootScope.translateLang = cookies.get('iSpeakLanguage');
            		ScormProcessSetValue('ISpeakLangDB',cookies.get('iSpeakLanguage'));
            	}
            	else
            	{
            		$rootScope.translateLang = 0;
            	}

            }

            if($scope.moduleId == 'placementTest') {
              const results = JSON.parse(localStorageService.getItem('results'));
              const qindex = JSON.parse(localStorageService.getItem("currentQuestionIndex"));
              const sindex = JSON.parse(localStorageService.getItem('currentSectionIndex'));
              const sectionLength = JSON.parse(localStorageService.getItem('sectionLength'));

              if(qindex === null) {
                state.go('placementTest_landing');
              } else if (results[results.length - 1] < 60) {
                state.go('result-page')
              } else if (sindex === sectionLength) {
                state.go('result-page')
              } else {
                state.go('placementTest_quiz');
              }
            }

        }();
    }]);
