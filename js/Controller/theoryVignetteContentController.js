app.controller("theoryVignetteContentController",
    ['$rootScope','$scope', 'dataService', '$stateParams', 'utilityService', 'groupService','$timeout','$state',
        function (rootScope, scope, dataService, stateParams, utilityService, groupService,$timeout,state) {

            scope.getVideoPath = function () {
                var videoFile = scope.currentSection.VIDEO;
                var theoryId = stateParams.tvId;
                var theory = groupService.getItem(theoryId,  scope.moduleId);
                if(scope.moduleId == 0){
                    var vCapText = videoFile.filter(function(elem,indx){
                        return scope.ds.ispeaklanguage === elem['@lang'];
                    });
                   var videoFile = vCapText[0]['#text'];
                }
                var videoFilePath = utilityService.getVideoPath(theory.path, videoFile);
                return videoFilePath;

            };

            scope.getVideoCaptionPath = function () {
                var videoCaption = scope.currentSection.AUDIOCAPTION;
                var theoryId = stateParams.tvId;
                var theory = groupService.getItem(theoryId,  scope.moduleId);
                 if(scope.moduleId == 0){
                    var vCapText = videoCaption.filter(function(elem,indx){
                        return scope.ds.ispeaklanguage === elem['@lang'];
                    });
                    var videoCaptionText = vCapText[0]['#text'];
                    var videoCaptionPath = utilityService.getVideoPath(theory.path, videoCaptionText);
                 }
                 else{
                   var videoCaptionPath = utilityService.getVideoPath(theory.path, videoCaption);
                 }
                return videoCaptionPath;

            };

            var accessChange = scope.$on('accessibilityChanged', function () {
                  if(scope.moduleId == 0){
                    scope.getTranslatedVideoPath();
                    scope.getTranslatedVideoCaptionPath();
                  }
            });

            scope.getTranslatedVideoPath = function () {
                 var videoFile = scope.currentSection.VIDEO;
                 var theoryId = stateParams.tvId;
                 var theory = groupService.getItem(theoryId,  scope.moduleId);
                 var vCapText = videoFile.filter(function(elem,indx){
                     return scope.ds.ispeaklanguage === elem['@lang'];
                 });
                 var videoFile = vCapText[0]['#text'];
                 var videoFilePath = utilityService.getVideoPath(theory.path, videoFile);
                        videojs("my-video").ready(function() {
                            scope.myPlayer.src(videoFilePath);
                            scope.myPlayer.textTracks()[0].mode = 'showing';
                        })
                 return videoFilePath;
            };

            scope.getTranslatedVideoCaptionPath = function () {

                var videoCaption = scope.currentSection.AUDIOCAPTION;
                var theoryId = stateParams.tvId;
                var theory = groupService.getItem(theoryId,  scope.moduleId);
                var vCapText = videoCaption.filter(function(elem,indx){
                    return scope.ds.ispeaklanguage === elem['@lang'];
                });
                var videoCaptionText = vCapText[0]['#text'];
                var videoCaptionPath = utilityService.getVideoPath(theory.path, videoCaptionText);
                var tracks = scope.myPlayer.textTracks();
                for (i = 0; i<tracks.length;i++) {
                  scope.myPlayer.removeRemoteTextTrack(tracks[i]);
                }
                // Add a track
                scope.myPlayer.addRemoteTextTrack({
                  kind: 'captions',
                  src: videoCaptionPath,
                });
                return videoCaptionPath;

            };


            scope.parseSection = function (TheorySections) {
                if (TheorySections) {
                    if (Array.isArray(TheorySections)) {
                        for (var i = 0; i < TheorySections.length; i++) {
                            var content = TheorySections[i].CONTENT;
                            TheorySections[i].CONTENT = scope.setParsedSpeech(content);
                        }
                    } else {
                        TheorySections.CONTENT = scope.setParsedSpeech(TheorySections.CONTENT);
                    }
                }
            };

            scope.formatTerm = function (term) {
                if (term) {
                    term = term.replace("'", "\\'");
                    return term;
                }
            };

            scope.etForTerm = function (term) {
                var regex = /\{(.*?)\}/;
                // var regex = /<c>(.*)<\/c>/;
                // var regex = /<c>(\w*)<\/c>/;
                var highlightedWord = term.match(regex);

                if (highlightedWord && highlightedWord.length > 1){
                    term = term.replace(highlightedWord[0], '<span style="color: #03B0F0;">' + highlightedWord[1] + '</span>')
                   // term =   scope.etForTerm(term);
                }

                return term;
            };


            scope.setParsedSpeech = function (content) {

                var term = scope.getAllTerms(content);

                if (term && term.length > 1) {
                    for (var j = 0; j < term.length; j = j + 2) {
                        var key = scope.formatTerm(term[j + 1]);
                        var formattedTerm = scope.etForTerm(term[j + 1]);
                        var newStr = "<button class=\"btn btn-link audio-term\" alt=\"" + key + "\" data-ng-click=\"termSelectedTheory(section,\'" + key +
                            "\')\">" + formattedTerm + "<\/button>";

                        content = content.replace(term[j], newStr);
                    }
                }

                return content;
            };


            scope.getAllTerms = function (str) {
                var regEx = /\$([^$]*)\$/;
                var terms = [];
                terms = str.match(regEx);
                if (terms) {
                    var newStr = str.substring(terms.index + terms[0].length);
                    var newTerms = scope.getAllTerms(newStr);

                    if (newTerms)
                        terms = terms.concat(newTerms);
                }

                return terms;
            };

            scope.playAudio = function (src) {
                var theoryV = groupService.getItem(scope.theoryId,  scope.moduleId);

                angular.element('#loadAudio').attr("src", utilityService.getAudioPath(theoryV.path, src));
                audio = angular.element('#loadAudio');
                audio[0].play();
            };

            scope.termSelectedTheory = function (section,term) {
                var selectedTerms;
                for (var i = 0; i < scope.ds.terms.length; i++) {
                    var dictionaryTerms = scope.ds.terms[i];

                    if (dictionaryTerms.KEY == term) {
                        selectedTerms = dictionaryTerms;

                        if (selectedTerms.AUDIO)
                            scope.playAudio(selectedTerms.AUDIO);

                        break;
                    }
                }
            };

            scope.ShowHide = function() {
                scope.IsVisible = scope.IsVisible ? false : true;
                scope.CaptionButtonValue = scope.IsVisible ? "Disable Caption" : "Enable Caption";
            };

            scope.$on('$destroy', function() {
                   // Destroy the object if it exists
                if ((scope.myPlayer !== undefined) && (scope.myPlayer !== null)  && (scope.myPlayer.el_ !== null)) {
                     $timeout(function () {
                       scope.myPlayer.dispose();
                     }, 500);
                }

                var tvPageId = window.location.hash;
                var tvPage = tvPageId.substr(tvPageId.length - 7);
                var indexOfTV = window.location.hash.indexOf("theoryVignettes");
                if(indexOfTV == 18 && tvPage != 'content' ){
                     state.go('groups', {moduleId: scope.moduleId});
                }
            });

            scope.init = function () {
                scope.ds = dataService.getDataSource();
                scope.moduleId = stateParams.moduleId;
                scope.theoryId = stateParams.tvId;
                scope.IsVisible = false;
                scope.CaptionButtonValue = "Enable Caption";
                scope.currentSection = scope.ds.tvSection;

                scope.parseSection(scope.currentSection.SUBSECTIONS.SUBSECTION);

                 if( scope.currentSection.VIDEO){
                    $timeout(function () {
                         scope.myPlayer = videojs('my-video');
                         videojs("my-video").ready(function() {
                             scope.myPlayer = this; // Store the object on a variable
                             scope.myPlayer.textTracks()[0].mode = 'showing';
                         })
                    }, 10);
                 }



              //  for(var i =0; i<scope.currentSection.SUBSECTIONS.SUBSECTION.length; i++)
                //    scope.currentSection.SUBSECTIONS.SUBSECTION[i].CONTENT = scope.etForTerm(scope.currentSection.SUBSECTIONS.SUBSECTION[i].CONTENT);


            }();



        }]);