app.controller("theoryVignetteController", ['$rootScope', '$scope', '$state', '$stateParams', 'dataService',
    'localStorageService', 'xmlDataService', 'localStorageService', '$window', 'utilityService','groupService','$filter','$cookies', 'ngDialog',
    function (rootScope, scope, state, stateParams,  dataService,
              localStorageService, xmlDataService, localStorageService, window, utilityService, groupService, $filter, cookies, ngDialog) {

        scope.tvFetched = function (tv) {
            scope.tv = tv;
            scope.sections = scope.tv.SECTIONS.SECTION;
            scope.ds.tvSection = scope.sections;


            var terms = [];
            var termGroup = scope.tv.DICTIONARY ? scope.tv.DICTIONARY.TERMGROUP : null;

            if (termGroup) {
                if (Array.isArray(scope.tv.DICTIONARY.TERMGROUP.TERM)) {

                    terms = scope.tv.DICTIONARY.TERMGROUP.TERM;

                } else {
                    terms = [scope.tv.DICTIONARY.TERMGROUP.TERM];
                }
            }

            scope.ds.terms = terms;
            state.go('selectedTheoryVignette.content');
        };

        scope.isNextItem = function () {
            scope.nextItemId = groupService.getNextItem(scope.selectedTV.id, scope.moduleId);
            return scope.nextItemId;
        };

        scope.goToNextItem = function () {
           groupService.goToNextItem(scope.nextItemId, scope.moduleId);
        };

        scope.changeLanguage = function (section) {
            if (section.titleFR == undefined && rootScope.isFrenchModule) {
                section.titleFR = false;
            } else {
                section.titleFR = !section.titleFR;
            }
        };

        scope.fetchTVFromXML = function (selectedWhatsNew) {
            xmlDataService.getXMLData(scope.selectedTV.filename).then(function (data) {
                var json = xmlDataService.parseXmlToJSON(data);
                var tv = json.THEORYVIGNETTES;
                var jsonToStringTV = JSON.stringify(tv);

                localStorageService.setItem('tv_' + scope.selectedTV.id, jsonToStringTV);


                scope.tvFetched(tv);
            },function () {
                console.log('Cant load theory vignettes xml' + scope.selectedTV.filename);
            });
        };

        scope.setSize = function () {
            var height = window.innerHeight;
            scope.contentHeight = height;
        };

        scope.isArray = function (obj) {
            return Array.isArray(obj);
        };

        scope.makeDivFocus = function (e) {
            if(e.keyCode == 9)
             scope.focusActive = true;
         };

         scope.makeDivblur = function () {
             scope.focusActive = false;
         };

        scope.init = function () {
            var tvId = parseInt(stateParams.tvId);
            var sectionNo = parseInt(stateParams.section);
            var moduleId = parseInt(stateParams.moduleId);

            scope.selectedTV = groupService.getItem(tvId, moduleId);
            scope.moduleId = stateParams.moduleId;

            var tvStatus = utilityService.getSuspendDataStatus(tvId);

            if (tvStatus != "success") {
                ScormProcessSetStatus(tvId, "completed");
                utilityService.setSuspendDataStatus(scope.selectedTV.id, 'success');
            }

            scope.ds = dataService.getDataSource();
            scope.ds.communicativeSkill = 'theoryVignette';
            scope.ds.mainTitle = 'THEORY_VIGNETTE';

            var title = $filter('localization')('THEORY_VIGNETTE', scope.ds.language);
            utilityService.setPageTitle(title + ' - '+  scope.selectedTV.title);

            angular.element(window).bind('resize', function () {
                scope.$apply(function () {
                    scope.setSize();
                });
            });

            scope.setSize();

            if (scope.selectedTV) {
                rootScope.inProgress = true;

                //Fetch dialogue
                var tv = localStorageService.getItem('tv_' + scope.selectedTV.id);

                if (tv) {
                    var jsonTV = JSON.parse(tv);
                    scope.tvFetched(jsonTV);
                } else {
                    scope.fetchTVFromXML(scope.selectedTV);
                }

            } else {
                location.path('/');
            }
             utilityService.checkForSessionAlive();
        }();
    }]);
