app.controller("studySectionController", ['$rootScope', '$scope', '$state', '$stateParams',
    'groupService', 'dataService', 'localStorageService', 'xmlDataService', 'utilityService', '$filter','$cookies', 'ngDialog',
    function (rootScope, scope, state, stateParams, groupService,
              dataService, localStorageService, xmlDataService, utilityService, $filter, cookies, ngDialog) {

        scope.getAssetPath = function (fileName) {
            var commonPath = utilityService.getAssetsCommonPath(scope.selectedStudySection.path);
            return commonPath + '/' + fileName;
        };

        scope.parseLinks = function (studySection) {
            var links = scope.getAllLinks(studySection.CONTENT);

            if (links) {
                for (var i = 0; i < links.length; i = i + 2) {
                    var link = links[i];
                    var regEx = /nav="(.+)"/;

                    var activityId = link.match(regEx)[1];
                    studySection.CONTENT = studySection.CONTENT.replace(link, "ng-click='linkSelected(" + activityId + ")'")
                }
            }
        };

        scope.linkSelected = function (id, pageNo) {
            var item = groupService.getItem(id, scope.moduleId);
            if (item) {
                switch (item.skillType) {
                    case 'activities':
                    {
                        localStorageService.clearStorage();

                        state.go('selectedActivity', {moduleId: scope.moduleId, activityId: id});
                        break;
                    }
                    case 'dialogues':
                    {
                        localStorageService.clearStorage();

                        state.go('selectedDialogue', {moduleId: scope.moduleId, dialogueId: id, tabNo: 1});
                        break;
                    }
                    case 'whatsNew':
                    {
                        pageNo = pageNo ? pageNo : 1;

                        scope.ds.studySectionId = scope.selectedStudySection.id;
                        scope.ds.isWhatsNew = true;
                        localStorageService.clearStorage();
                        state.go('selectedWhatsNew', {moduleId: scope.moduleId, whatsNewId: id, page: pageNo});
                        break;
                    }
                }
            }
        };

        scope.getAllLinks = function (str) {
            var regEx = /nav="(.*?)"/;
            var links = [];
            links = str.match(regEx);

            if (links) {
                var newStr = str.substring(links.index + links[0].length);
                var newLinks = scope.getAllLinks(newStr);

                if (newLinks)
                    links = links.concat(newLinks);
            }

            return links;
        };

        scope.studySectionFetched = function (studySection) {
            scope.studySection = studySection;
            scope.parseLinks(studySection);
        };

        scope.fetchStudySectionFromXML = function (selectedStudySection) {
            xmlDataService.getXMLData(selectedStudySection.filename).then(function (data) {
                var json = xmlDataService.parseXmlToJSON(data);
                var studySection = json.STUDYSECTION;
                var jsonToStringStudySection = JSON.stringify(studySection);

                localStorageService.setItem('studySection_' + selectedStudySection.id, jsonToStringStudySection);

                scope.studySectionFetched(studySection);
            },function () {
                console.log('Cant load study section xml' + selectedStudySection.filename);
            });
        };

        scope.isNextItem = function () {
            utilityService.getNextItem();
        };

        scope.isNextItem = function () {
            scope.nextItemId = groupService.getNextItem(scope.selectedStudySection.id, scope.moduleId);
            return scope.nextItemId;
        };

        scope.goToNextItem = function () {
            var nextUnit = groupService.getNextUnit(scope.selectedStudySection.id, scope.moduleId);
            if (nextUnit) {
                state.go('groups', {'selectedUnit': nextUnit, 'moduleId': scope.moduleId});
            }
        };

        scope.makeDivFocus = function (e) {
            if (e.keyCode == 9)
                scope.focusActive = true;
        };

        scope.makeDivblur = function () {
            scope.focusActive = false;
        };

        scope.init = function () {
            var studySectionId = stateParams.studySectionId;
            var moduleId = parseInt(stateParams.moduleId);
            utilityService.removeLastAccessLocation();

            scope.selectedStudySection = groupService.getItem(studySectionId, moduleId);
            scope.moduleId = stateParams.moduleId;

            scope.ds = dataService.getDataSource();
            scope.ds.mainTitle = 'STUDY_SECTION';

            var title = $filter('localization')('STUDY_SECTION', scope.ds.language);
            utilityService.setPageTitle(title + ' - ' + scope.selectedStudySection.title);
            ScormProcessSetStatus(studySectionId, "completed");

            var studySectionStatus = utilityService.getSuspendDataStatus(studySectionId);

            if (studySectionStatus != "success") {
                utilityService.setSuspendDataStatus(studySectionId, 'success');
            }

            if (scope.selectedStudySection) {

                rootScope.inProgress = true;

                //Fetch study section
                var studySection = localStorageService.getItem('studySection_' + scope.selectedStudySection.id);

                if (studySection) {
                    var jsonStudySection = JSON.parse(studySection);
                    scope.studySectionFetched(jsonStudySection);
                } else {
                    scope.fetchStudySectionFromXML(scope.selectedStudySection);
                }
            } else {
                location.path('/');
            }
            utilityService.checkForSessionAlive();
        }();
    }]);
