app.directive('compileData', function ($compile) {
    return {
        scope: true,
        link: function (scope, element, attrs) {
            var elmnt;
            attrs.$observe('template', function (myTemplate) {
                if (angular.isDefined(myTemplate)) {
                    // compile the provided template against the current scope
                    elmnt = $compile(myTemplate)(scope);
                    element.html(""); // dummy "clear"
                    element.append(elmnt);
                }
            });
        }
    };
});

app.controller("dialogueController", ['$rootScope', '$scope', '$timeout', 'xmlDataService', 'groupService',
    'localStorageService', '$state', '$stateParams', '$location',
    '$filter', '$modal', 'dataService', 'utilityService', '$window', '$compile','ngAudio','$cookies', 'ngDialog',
    function (rootScope, scope, $timeout, xmlDataService, groupService, localStorageService, state,
              stateParams, location, filter, modal, dataService, utilityService, $window, compile, ngAudio, cookies, ngDialog) {

        scope.stopAudio = function () {
            var audio = angular.element('#load_audio');
            if (!audio[0].paused) {
                audio[0].pause();
                audio[0].currentTime = 0;
            }
        };

        //Store all updated information into browser's local storage to
        // prevent data loss while refreshing page
        var updateDataHandler = scope.$on('updateData', function (event, data) {
            scope.stopAudio();
            scope.questionsAttended = scope.questionsAttended + 1;
            if(scope.sectionStatusQuestion != 'inProgress'){
                scope.sectionStatusQuestion = "inProgress";
                localStorageService.setItem('sectionStatusQuestion_' + scope.selectedDialogue.id,
                                scope.sectionStatusQuestion);

            }
            var percentage = 0;

             if (scope.totalQuestions) {
                percentage = (scope.ds.totalScore / scope.totalQuestions) * 100;
                percentage = percentage.toFixed(2);
             }

            localStorageService.setItem('questionsAttended_' + scope.selectedDialogue.id,
                scope.questionsAttended);


            if (!scope.questionsSection.completed &&
                scope.questionsAttended == scope.totalQuestions && percentage >= 50) {
                scope.questionsSection.completed = true;
                scope.sectionStatusQuestion = "success";
                localStorageService.setItem('sectionStatusQuestion_' + scope.selectedDialogue.id,
                                                scope.sectionStatusQuestion);
                scope.sectionsCompleted = scope.sectionsCompleted + 1;
            }

            scope.updateDataToLocalStorage();
            utilityService.playFeedbackAudio(data.wasCorrect);

            //Add interaction
            var objectiveId = scope.selectedDialogue.id;
            data.objectiveId = objectiveId;
            scope.$emit('recordInteraction', data);
        });

        scope.updateDataToLocalStorage = function () {
            localStorageService.setItem('sectionsCompleted_' + scope.selectedDialogue.id, scope.sectionsCompleted);

           if (scope.ds.totalScore != undefined)
                localStorageService.setItem('totalScore_' + scope.selectedDialogue.id, scope.ds.totalScore);

            var jsonToStringDialogue = JSON.stringify(scope.dialogue);
            localStorageService.setItem('dialogue_' + scope.selectedDialogue.id, jsonToStringDialogue);
        };

        scope.getGrammer = function () {
            var lang = rootScope.isFrenchModule ? 'fr' : 'en';
            var grammer = scope.grammer[lang];
            return grammer;
        };

        scope.initSection = function (section) {
            if (section['@type'] == 'TRANSCRIPT' || section['@type'] == 'READ') {
                scope.grammer =
                {
                    'en': [{value: 'Nouns'}, {value: 'Verbs'}, {value: 'Pronouns'},
                        {value: 'Adjectives', text: 'ABC'}, {value: 'Adverbs', text: 'ABC'},
                        {value: 'Preposition', text: 'ABC'},
                        {value: 'Conjuctions', text: 'ABC'}, {value: 'Related Vocabulary', text: 'ABC'},
                        {value: 'Useful Expressions'}, {value: 'Idioms'}],
                    'fr': [{
                        value: 'Noms',
                        items: [{text: 'M', backcolor: '#1B3AF6', color: 'white'}, {
                            text: 'F',
                            backcolor: '#F383E8',
                            color: 'white'
                        }]
                    }, {value: 'Verbes'}, {value: 'Pronoms'},
                        {
                            value: 'Adjectifs',
                            items: [{text: 'M', color: '#1B3AF6'}, {text: 'F', color: '#F383E8'}]
                        }, {value: 'Adverbes', text: 'ABC'},
                        {value: 'Prepositions', text: 'ABC'},
                        {value: 'Conjonctions', text: 'ABC'}, {value: 'Vocabulaire Connexe', text: 'ABC'},
                        {value: 'Expression Utiles'}, {value: 'Idiomes'}]
                };
                scope.parseTranscript(section);
            } else if (section['@type'] == 'REPEAT') {
                scope.totalTracks = section.CONTENT.AC.length;
            } else if (section.selected && section['@type'] == 'QUESTION') {
                scope.initQuestionTab(section);
            }
        };

        scope.getImageCaptionsFromLang = function(imgCaptions) {
           if(imgCaptions){
                var icap = imgCaptions.IMAGECAPTION;

                if(Array.isArray(icap)) {
                    var text = icap.filter(function(value){
                        return value['@lang'] == scope.ds.ispeaklanguage;
                    });
                    return text[0].TEXT;
                } else {
                    return icap.TEXT;
                }
           }
        };

        scope.showInstructions = function () {
              var section,inst;

              if (Array.isArray(scope.dialogue.SECTIONS.SECTION))
                  section = scope.dialogue.SECTIONS.SECTION[scope.currentTabNo - 1];
              else
                  section = scope.dialogue.SECTIONS.SECTION;

              if (section.INSTRUCTIONS) {
                var inst = section.INSTRUCTIONS.INSTRUCTION;
              } else {
                var inst = section.INSTRUCTION;
              }

              var resultInst = '';

                  if(Array.isArray(inst)) {
                      resultInst = inst.filter(function(elem,indx){
                           return scope.ds.language === elem['@lang'];
                          })
                        }
              
              var resInst = resultInst[0];
              if (resInst) {
                scope.inst = resInst.TEXT;
              }
              
              scope.labelHeading = [];

              var sections = scope.dialogue.SECTIONS.SECTION;
                  for(i=0; i<sections.length; i++){

                  var labels = sections[i].LABELS;
                  var labelText = [];

                  if (labels) {
                    labelText = sections[i].LABELS.LABEL;
                  } else {
                    labelText = sections[i].LABEL; 
                  }
                  
                  var resultLabel = '';

                      if(Array.isArray(labelText)) {
                            resultLabel = labelText.filter(function(elem,indx){
                                return scope.ds.language === elem['@lang'];
                           })
                      }
                  
                  var resLabel = resultLabel[0];
                  if (resLabel) {
                    scope.labelHeading[i] = resLabel.TEXT;
                  }
              }
        };

        scope.dialogueFetched = function (dialogue) {
            scope.dialogue = dialogue;
            var terms = [];
            var termGroup = scope.dialogue.DICTIONARY ? scope.dialogue.DICTIONARY.TERMGROUP : null;

            if (termGroup) {
                if (Array.isArray(scope.dialogue.DICTIONARY.TERMGROUP)) {
                    for (var i = 0; i < termGroup.length; i++) {
                        terms = terms.concat(termGroup[i].TERM);
                    }
                } else {
                    terms = scope.dialogue.DICTIONARY.TERMGROUP.TERM;
                }

                scope.terms = terms;
            }


            scope.currentTabNo = stateParams.tabNo;

            scope.isSectionArray = Array.isArray(scope.dialogue.SECTIONS.SECTION);
            if (scope.isSectionArray) {
                scope.totalTabs = scope.dialogue.SECTIONS.SECTION.length;

                for (var i = 0; i < scope.dialogue.SECTIONS.SECTION.length; i++) {
                    var section = scope.dialogue.SECTIONS.SECTION[i];
                    section.selected = (i == (scope.currentTabNo - 1));
                    section.INDEX = i;
                    scope.initSection(section);
                }
            }
            else {
                scope.totalTabs = 1;

                var section = scope.dialogue.SECTIONS.SECTION;
                scope.initSection(section);
                scope.section = section;
            }
            scope.showInstructions ();
        };

        scope.isArray = function (item) {
            return Array.isArray(item);
        };

        scope.initQuestionTab = function (section) {
            scope.currentQuestionTabPageNo = stateParams.pageNo ? stateParams.pageNo : 1;
            scope.questionsPerPage = 2;
            scope.totalQuestions = section.CONTENT.QUESTIONS.QUESTION.length;
            scope.questionsSection = section;
            scope.showPager = true;
            scope.ds.answeredQuestions = 0;

            scope.$watch('questionsPerPage + currentQuestionTabPageNo', function () {
                var begin = ((scope.currentQuestionTabPageNo - 1) * scope.questionsPerPage);
                var end = begin + scope.questionsPerPage;

                scope.filteredQuestions = section.CONTENT.QUESTIONS.QUESTION.slice(begin, end);
            });
        };

        scope.getQuestionNo = function (index) {
            var questionNo = ((scope.currentQuestionTabPageNo - 1) * scope.questionsPerPage) + index;
            return questionNo;
        };

        scope.formatTerm = function (term) {
            if (term) {
                term = term.replace("'", "\\&#39;");

                if (term.indexOf("'") != -1)
                    term = scope.formatTerm(term);
            }

            return term;
        };

        scope.tabSelected = function (tabNo) {
            scope.currentTabNo = tabNo + 1;
            scope.tabChanged();
        };

        scope.parseTranscript = function (transcriptSection) {
            if (transcriptSection) {
                var lines = transcriptSection.CONTENT.LINE;

                if (lines) {
                    if (Array.isArray(lines)) {
                        for (var i = 0; i < lines.length; i++) {
                            var line = lines[i];
                            scope.setParsedSpeech(line);
                        }
                    } else {
                        scope.setParsedSpeech(lines);
                    }

                }
            }
        };

        scope.setParsedSpeech = function (line) {
            var speech = line.SPEECH;

            var term = scope.getAllTerms(speech);

            if (term && term.length > 1) {
                for (var j = 0; j < term.length; j = j + 2) {
                    var _term = term[j + 1];
                    var formatedTerm = scope.formatTerm(_term);
                    var newStr = "<label class=\"transcript-term\" lang=\"{{ds.mainMenuLinkLang}}\" ng-mouseup=\"termUp($event)\" data-ng-click=\"transcriptTermSelected(section, \'"
                    + formatedTerm +"\')\">" + term[j + 1] + "<\/label>";

                    speech = speech.replace(term[j], newStr);
                }
            }

            line.parsedSpeech = "<label>" + speech + "</label>";
        };

        scope.termUp = function ($event) {
            $event.currentTarget.blur();
        };

        scope.getAllTerms = function (str) {
            var regEx = /\{([^}]*)\}/;
            var terms = [];
            terms = str.match(regEx);
            if (terms) {
                var newStr = str.substring(terms.index + terms[0].length);
                var newTerms = scope.getAllTerms(newStr);

                if (newTerms)
                    terms = terms.concat(newTerms);
            }

            return terms;
        };

        scope.transcriptTermSelected = function (section, term) {
            var selectedTerm;
            var colorCode;
            for (var i = 0; i < scope.terms.length; i++) {
                var dictionaryTerm = scope.terms[i];

                if (dictionaryTerm.KEY == term) {
                    selectedTerm = dictionaryTerm;

                    if (selectedTerm.AUDIO)
                        scope.playAudio(selectedTerm.AUDIO);

                    section.termWithColorCode = selectedTerm.COLORCODING ? selectedTerm.COLORCODING.span : '';
                    section.selectedTerm = term;
                    section.selectedTermDescription = selectedTerm.VALUE;
                    break;
                }
            }
        };

        scope.playTermAudio = function (section, term) {
            if (!term.attended) {
                scope.termsAttended = scope.termsAttended + 1;
                term.attended = true;

                if (!section.completed && scope.termsAttended == scope.terms.length) {
                    scope.sectionsCompleted = scope.sectionsCompleted + 1;
                    section.completed = true;
                }

                scope.updateDataToLocalStorage();
                localStorageService.setItem('termsAttended_' + scope.selectedDialogue.id, scope.termsAttended);
            }

            scope.checkAndSetInprogress();
            scope.playAudio(term.AUDIO);
        };

        scope.playRepeatAudio = function (src, totalAudios, tooltipIndex) {

            scope.ds.showTooltip = true;
            scope.ds.focuss = true;
            scope.tooltipIndex = tooltipIndex;
            scope.ds.toolTipIndex = tooltipIndex;
            if (scope.currentTrack == totalAudios)
                scope.sectionsCompleted = scope.sectionsCompleted + 1;

            scope.checkAndSetInprogress();
            scope.playAudio(src);
        };

        scope.hideTooltip = function () {
            scope.ds.cancelTimeout = $timeout(function () {
                scope.ds.showTooltip = false;
                scope.ds.focuss = false;
                scope.ds.toolTipIndex = null;
                scope.ds.Grpfocuss = false;
            }, 2000);

        };

        scope.playAudio = function (src) {
            $timeout.cancel(scope.ds.cancelTimeout);
            scope.playAudioTimeout = $timeout(function () {
                scope.ds.showTooltip = true;
                scope.ds.focuss = true;
//                angular.element('#loadAudio').attr("src", scope.getAssetsPath(src));
//                audio = angular.element('#loadAudio');
//                audio[0].play();
                scope.ds.sound = ngAudio.load(scope.getAssetsPath(src));
                scope.ds.sound.play();
            }, 10);
        };

        scope.hidetip = function () {
            scope.ds.focuss = false;
            scope.ds.showAudioTooltip = false;
        }

         var accessChange = scope.$on('accessibilityChanged', function () {
                  scope.showInstructions();

                  var section = scope.dialogue.SECTIONS.SECTION[scope.currentTabNo - 1];
                  scope.ds.playerNameWidth = scope.getMaxPlayerNameFromSpeech(section);
         });


        scope.stopTranscriptAudio = function () {
            audio = angular.element('#transcript_audio');
            audio[0].pause();
            audio[0].currentTime = 0;
        };

        scope.fetchDialogueFromXML = function (selectedDialogue) {
            xmlDataService.getXMLData(selectedDialogue.filename).then(function (data) {
                var json = xmlDataService.parseXmlToJSON(data);
                var dialogue = json.DIALOGUE;
                var jsonToStringDialogue = JSON.stringify(dialogue);

                localStorageService.setItem('dialogue_' + scope.selectedDialogue.id, jsonToStringDialogue);

                scope.dialogueFetched(dialogue);
            },function () {
                console.log('Cant load dialogue xml' + selectedDialogue.filename);
            });
        };

        scope.videoEnded = function (selectedSection) {
            if (selectedSection && !selectedSection.completed) {
                scope.sectionsCompleted = scope.sectionsCompleted + 1;
                selectedSection.completed = true;
                scope.updateDataToLocalStorage();
            }
             scope.sectionStatusVideo = "success";
             scope.ds.showAudioTooltip = false;
             rootScope.$broadcast('sectionStatusVideo', scope.sectionStatusVideo);
        };

        scope.getFileName = function (filePath) {
            if (filePath) {
                var temp = filePath.split('/');
                filePath = temp[temp.length - 1];
            }

            return filePath;
        };

        var currentTrackWatcher = scope.$watch('currentTrack', function (newVal) {
            if (newVal == 1) {
                scope.isPrevEnabled = false;
                scope.isNextEnabled = true;
            } else if (newVal == scope.totalTracks) {
                scope.isNextEnabled = false;
                scope.isPrevEnabled = true;
            } else {
                scope.isNextEnabled = true;
                scope.isPrevEnabled = true;
            }
        });

        scope.changeAudioTrack = function (isPrev) {
            scope.changeAudioTimeout = $timeout(function () {
                if (isPrev && scope.currentTrack > 1) {
                    scope.currentTrack = scope.currentTrack - 1;
                } else if (!isPrev && scope.currentTrack < scope.totalTracks) {
                    scope.currentTrack = scope.currentTrack + 1;
                }
            }, 300);

        };

        scope.tabChanged = function () {
            var activityName = 'selectedDialogue';

            if(scope.ds.sound)
              scope.ds.sound.stop();

            if (scope.dialogue && scope.dialogue.SECTIONS && scope.currentTabNo >= 1) {
                var section = scope.dialogue.SECTIONS.SECTION[scope.currentTabNo - 1];

                if (section && section['@type'] == 'QUESTION') {
                    activityName = 'selectedDialogueQuestions';
                }
                else if (section && (section['@type'] == 'TRANSCRIPT' || section['@type'] == 'READ' || section['@type'] == 'TERM' || section['@type'] == 'REPEAT')) {
                    if (!section.completed) {
                        scope.sectionsCompleted = localStorageService.getItem('sectionsCompleted_' + scope.selectedDialogue.id);
                        scope.sectionsCompleted = scope.sectionsCompleted ? parseInt(scope.sectionsCompleted) : 0;
                        scope.sectionsCompleted = scope.sectionsCompleted + 1;
                        section.completed = true;
                        scope.updateDataToLocalStorage();
                    }
                    if(section['@type'] == 'TRANSCRIPT'){
                        scope.sectionStatusTranscript = "success";
                        localStorageService.setItem('sectionStatusTranscript_' + scope.selectedDialogue.id,
                            scope.sectionStatusTranscript);
                    }
                    if(section['@type'] == 'TERM'){
                        scope.sectionStatusTerm = "success";
                        localStorageService.setItem('sectionStatusTerm_' + scope.selectedDialogue.id,
                            scope.sectionStatusTerm);
                    }
                    if(section['@type'] == 'REPEAT'){
                        scope.sectionStatusRepeat = "success";
                        localStorageService.setItem('sectionStatusRepeat_' + scope.selectedDialogue.id,
                            scope.sectionStatusRepeat);
                    }


                    scope.ds.playerNameWidth = scope.getMaxPlayerNameFromSpeech(section);

                    if(scope.ds.playerNameWidth){
                        $timeout(function () {
                             if(scope.myPlayerAudio){
                                 scope.myPlayerAudio = videojs('transcript_audio');
                                 videojs("transcript_audio").ready(function() {
                                     scope.myPlayerAudio = this; // Store the object on a variable
                                 })
                             }
                        }, 500);
                    }

                }

                var title = filter('localization')('',scope.ds.language, section.LABEL, section.LABEL2);
                utilityService.setPageTitle('Dialogue - ' + scope.selectedDialogue.title+' - '+ title);
            }



            state.go(activityName, {
                'moduleId': scope.moduleId,
                'dialogueId': scope.selectedDialogue.id,
                'tabNo': scope.currentTabNo,
                'pageNo': scope.currentQuestionTabPageNo
            });

            //scope.updateDataToLocalStorage();

        };

        scope.setFullDialgouePageTitle = function(section){
            var title = filter('localization')('',scope.ds.language, section.LABEL, section.LABEL2);
            utilityService.setPageTitle('Dialogues - ' + scope.selectedDialogue.title+' - '+ title);
        }

        scope.getMaxPlayerNameFromSpeech = function (section) {
            var maxWidth;
            var maxChar = scope.getMaxAnswer(section);
            var element = compile('<span class=\"form-controls\" ' +
                'style=\"visibility: hidden;position: absolute;min-width:0;\" id="maxInput" type="text"></span>')(scope);
            element[0].className += (" fontScaleX" + rootScope.fontSize);

            var container = document.getElementById('mainContent');

            if (container)
                container.appendChild(element[0]);

            var e = element[0];

            if (e) {

                e.innerHTML = maxChar;
                maxWidth = e.clientWidth + 15;
                if (rootScope.fontSize == "4") {
                     maxWidth = e.clientWidth + 40;
                } else if (rootScope.fontSize == "3") {
                    maxWidth = e.clientWidth + 20;
                }
            }

            return maxWidth;
        };

        scope.getMaxAnswer = function (section) {
            var result = '';
            if (section.CONTENT){
            var answers = section.CONTENT.LINE;

                if (answers) {
                    if (Array.isArray(answers)) {
                        for (var i = 0; i < answers.length; i++) {
                            var answer = answers[i];
                            if (answer.PLAYER)
                                result = answer.PLAYER.length > result.length ? answer.PLAYER : result;
                        }
                    } else {
                        result = answers.PLAYER.length > result.length ? answers.PLAYER : result;
                    }
                }
            }

            return result;
        };

        scope.getSpeechContentWidth = function () {
            var speechCont = document.getElementById('line');

            if (speechCont)
                var speechWidth = speechCont.clientWidth - scope.ds.playerNameWidth - 35;

            return speechWidth;
        };

        scope.getAudioAssetsPath = function (path) {
            return utilityService.getAudioPath(scope.selectedDialogue.path, path);
        };

        scope.getAssetsPath = function (path) {
            return utilityService.getAssetsCommonPath(scope.selectedDialogue.path) + '/' + path;
        };

        var sectionCompletedWatcher = scope.$watch('sectionsCompleted', function (newVal, oldVal) {
            if (newVal) {
                localStorageService.setItem('sectionsCompleted_' + scope.selectedDialogue.id, newVal);
            }

            if (scope.totalTabs && newVal == scope.totalTabs) {
                ScormProcessSetStatus(scope.selectedDialogue.id, 'completed');
                //Set suspend data status
                utilityService.setSuspendDataStatus(scope.selectedDialogue.id, 'success');
                scope.status = 'completed';
            }
        });

        scope.checkAndSetInprogress = function () {
            if (scope.suspendDataStatus != 'inProgress' &&
                scope.suspendDataStatus != 'success') {
                utilityService.setSuspendDataStatus(scope.selectedDialogue.id, 'inProgress');
                scope.suspendDataStatus = 'inProgress';
            }

            if (scope.status != 'incomplete' && scope.status != 'completed') {

            	var sectionCount = localStorageService.getItem('sectionsCompleted_' + scope.selectedDialogue.id);
            	if (scope.totalTabs && sectionCount == scope.totalTabs) {
            		ScormProcessSetStatus(scope.selectedDialogue.id, 'completed');
            		//Set suspend data status
            		utilityService.setSuspendDataStatus(scope.selectedDialogue.id, 'success');
            		scope.status = 'completed';
            	}
            	else
            	{
            		ScormProcessSetStatus(scope.selectedDialogue.id, 'incomplete');
            		scope.status = 'incomplete';
            	}
            }
        };

        scope.ShowHide = function () {
            scope.IsVisible = scope.IsVisible ? false : true;
            scope.CaptionButtonValue = scope.IsVisible ? "Disable Caption" : "Enable Caption";
        };

        scope.getSectionStatusVideo = function() {
            scope.$on('sectionStatusVideo', function(events, value){
                $timeout(function () {
               scope.sectionStatusVideo = value;
                    localStorageService.setItem('sectionStatusVideo_' + scope.selectedDialogue.id,
                                          scope.sectionStatusVideo);
               }, 10);
            })
        };


        scope.videoStarted = function (selectedSection) {
            scope.checkAndSetInprogress();
            utilityService.checkForSessionAlive();
            scope.ds.showAudioTooltip = true;
            scope.sectionStatusVideo = "inProgress";
            rootScope.$broadcast('sectionStatusVideo', scope.sectionStatusVideo);
        };

        $("video").on("timeupdate", function() {
            var curTime = $("video").prop("currentTime");
            var endTime = $("video").prop("duration");
            var halfTime= (curTime / endTime) * 100;

            var section = scope.dialogue.SECTIONS.SECTION;
                if ((halfTime.toFixed() == 50)){
                    scope.videoEnded(section);
                    var sectionCount = localStorageService.getItem('sectionsCompleted_' + scope.selectedDialogue.id);
                    if (scope.totalTabs && sectionCount == scope.totalTabs) {
                    	ScormProcessSetStatus(scope.selectedDialogue.id, 'completed');
                    	//Set suspend data status
                    	utilityService.setSuspendDataStatus(scope.selectedDialogue.id, 'success');
                    	scope.status = 'completed';
                    }
                }
        });



        var answeredQuestionWatcher = scope.$watch('ds.answeredQuestions', function (newVal) {
            if (newVal == 1) {
                scope.checkAndSetInprogress();
            }
        });

        scope.isNextItem = function () {
            scope.nextItemId = groupService.getNextItem(scope.selectedDialogue.id, scope.moduleId);
            return scope.nextItemId;
        };

        scope.goToNextItem = function () {
            groupService.goToNextItem(scope.nextItemId, scope.moduleId);
        };


        scope.calculateTranscriptHeight = function () {
            var speech = document.getElementsByClassName('speech')[0];
            var next = document.getElementById('next');
            var sh = speech ? speech.offsetTop : 0;
            var nh = next ? next.scrollHeight : 0;

            return $window.innerHeight - sh - nh;
        };

        scope.calculateHeight = function () {
            var instructionContainer = document.getElementById('instructionsContainer');
            var tabContainer = document.getElementById('tabContainer');
            var navTabs = document.getElementsByClassName('nav-tabs')[0];
            var footer = document.getElementById('footer');
            var fHeight = footer && footer.scrollHeight ? footer.scrollHeight : 0;
            var tabContainerOffset = tabContainer ? tabContainer.offsetTop : 0;
            var navTabsHeight = navTabs ? navTabs.clientHeight : 0;
            var fullDialogueContainer = document.getElementsByClassName('fullDialogueVideo')[0];
            var fullDialogueOffset = fullDialogueContainer ? fullDialogueContainer.offsetTop : 0;

            scope.height = $window.innerHeight - tabContainerOffset - fHeight - navTabsHeight - fullDialogueOffset;
            scope.footerWidth = tabContainer ? tabContainer.clientWidth - 60 : 0;
            scope.th = scope.calculateTranscriptHeight();
        };

        rootScope.$watch('fontSize', function (newVal) {
            if (newVal) {
               scope.fontSizeTimeout = $timeout(function () {
                    scope.calculateHeight();
                }, 100);
            }
        });

        $window.onresize = function () {
            scope.$apply(function () {
                scope.calculateHeight();
            });
        };

        scope.makeFocusActive = function (e) {
            if (e.keyCode == 9)
                scope.focusActive = true;
        };

        scope.makeFocusDeActive = function () {
            scope.focusActive = false;
        };

         scope.$on('$destroy', function() {
                // Destroy the object if it exists
                 if ((scope.myPlayerAudio !== undefined) && (scope.myPlayerAudio !== null) && (scope.myPlayerAudio.el_ !== null)) {
                     scope.myPlayerAudio.dispose();
                 }
                 if ((scope.myPlayer !== undefined) && (scope.myPlayer !== null)  && (scope.myPlayer.el_ !== null)) {
                        scope.myPlayer.dispose();
                 }

                  $timeout.cancel( scope.changeAudioTimeout );
                  $timeout.cancel( scope.loadAuidoTimeout );
                  $timeout.cancel( scope.fontSizeTimeout );
                  updateDataHandler();
                  answeredQuestionWatcher();
                  sectionCompletedWatcher();
                  currentTrackWatcher();
         });


        scope.init = function () {
            var dialogueId = stateParams.dialogueId, videoEnded;
            scope.moduleId = stateParams.moduleId;
            scope.selectedDialogue = groupService.getItem(dialogueId, scope.moduleId);
            scope.isPrevEnabled = false;
            scope.isNextEnabled = false;
            scope.currentTrack = 1;
            scope.currentTabNo = 1;
            scope.currentQuestionTabPageNo = 1;
            scope.showPager = false;
            scope.IsVisible = false;
            scope.ds.showAudioTooltip = false;
            scope.CaptionButtonValue = "Enable Caption";
            scope.ds.mainTitle = 'DIALOGUE';
            var termsAttended = localStorageService.getItem('termsAttended_' + scope.selectedDialogue.id);
            scope.termsAttended = termsAttended ? parseInt(termsAttended) : 0;
            var questionsAttended = localStorageService.getItem('questionsAttended_' + scope.selectedDialogue.id);
            scope.questionsAttended = questionsAttended ? parseInt(questionsAttended) : 0;
            var playedRepeatTracks = localStorageService.getItem('playedRepeatTracks_' + scope.selectedDialogue.id);
            scope.playedRepeatTracks = playedRepeatTracks ? playedRepeatTracks.split(',') : [];
            var totalScore = localStorageService.getItem('totalScore_' + scope.selectedDialogue.id);
            scope.ds.totalScore = totalScore ? parseFloat(totalScore) : 0;
            scope.sectionStatusVideo = localStorageService.getItem('sectionStatusVideo_' + scope.selectedDialogue.id);
            scope.sectionStatusTerm = localStorageService.getItem('sectionStatusTerm_' + scope.selectedDialogue.id);
            scope.sectionStatusRepeat = localStorageService.getItem('sectionStatusRepeat_' + scope.selectedDialogue.id);
            scope.sectionStatusQuestion = localStorageService.getItem('sectionStatusQuestion_' + scope.selectedDialogue.id);
            scope.sectionStatusTranscript = localStorageService.getItem('sectionStatusTranscript_' + scope.selectedDialogue.id);

            scope.ds = dataService.getDataSource();
            scope.ds.communicativeSkill = 'dialogues';

            scope.ds.transcriptDescLang = rootScope.isFrenchModule ? 'en' : 'fr';

            scope.suspendDataStatus = utilityService.getSuspendDataStatus(dialogueId);

            if (scope.selectedDialogue) {
                rootScope.inProgress = true;

                ScormProcessSetStatus(scope.selectedDialogue.id, 'browsed');
                scope.status = 'browsed';
                scope.sectionsCompleted = localStorageService.getItem('sectionsCompleted_' + scope.selectedDialogue.id);
                scope.sectionsCompleted = scope.sectionsCompleted ? parseInt(scope.sectionsCompleted) : 0;

                //Fetch dialogue
                var dialogue = localStorageService.getItem('dialogue_' + scope.selectedDialogue.id);

                if (dialogue) {
                    var jsonDialogue = JSON.parse(dialogue);
                    scope.dialogueFetched(jsonDialogue);
                } else {
                    scope.fetchDialogueFromXML(scope.selectedDialogue);
                }

                scope.heightCalculatorTimeout = $timeout(function () {
                    scope.calculateHeight();
                    scope.th = scope.calculateTranscriptHeight();
                }, 500);
                scope.contentWidthTimeout = $timeout(function () {
                    scope.ds.speechTextContent = scope.getSpeechContentWidth();
                }, 50);

                if (scope.selectedDialogue.title) {
                    var str = scope.selectedDialogue.title.split("/");
                    scope.dialogueTitle1 = str[0];
                    scope.dialogueTitle2 = str[1];
                }

            } else {
                location.path('/');
            }
            if( scope.sectionStatusVideo == undefined)
                 scope.sectionStatusVideo = "tryIt";

            if( scope.sectionStatusTerm == undefined)
                scope.sectionStatusTerm = "tryIt";

            if( scope.sectionStatusRepeat == undefined)
                scope.sectionStatusRepeat = "tryIt";

            if( scope.sectionStatusQuestion == undefined)
                scope.sectionStatusQuestion = "tryIt";

            if( scope.sectionStatusTranscript == undefined)
                scope.sectionStatusTranscript = "tryIt";

            utilityService.checkForSessionAlive();
        }();
    }
])
