app.controller('questionsController',
    ['$rootScope', '$scope', 'dataService', '$timeout', 'xmlDataService',
        '$sce', '$stateParams', '$state', 'localStorageService', 'utilityService', '$location', 'groupService', '$window', '$compile', '$filter','$cookies', 'ngDialog',
        function (rootScope, scope, dataService, $timeout, xmlDataService,
                  $sce, $stateParams, state, localStorageService, utilityService, location, groupService, $window, compile, $filter, cookies, ngDialog) {

            scope.models = {
                selected: null,
                lists: {"A": [], "B": []}
            };

            // Generate initial model
            for (var i = 1; i <= 3; ++i) {
                scope.models.lists.A.push({label: "Item A" + i});
                scope.models.lists.B.push({label: "Item B" + i});
            }

            // Model to JSON for demo purpose
            scope.$watch('models', function(model) {
                scope.modelAsJson = angular.toJson(model, true);
            }, true);
            scope.hideTooltip = true;

            var updateActivity = scope.$on('updateActivity', function () {
                var jsonToStringActivity = JSON.stringify(scope.activity);
                localStorageService.setItem('activity_' + scope.selectedActivity.id, jsonToStringActivity);

                if (scope.ds.answeredQuestions != undefined)
                    localStorageService.setItem('answeredQuestions_' + scope.selectedActivity.id, scope.ds.answeredQuestions);

                if (scope.ds.attendedTotalQuestions != undefined)
                    localStorageService.setItem('attendedTotalQuestions_' + scope.selectedActivity.id, scope.ds.attendedTotalQuestions);

                if (scope.ds.totalScore != undefined)
                    localStorageService.setItem('totalScore_' + scope.selectedActivity.id, scope.ds.totalScore);
                
            });

            //Store all updated information into browser's local storage to
            // prevent data loss while refreshing page
            var updateData = scope.$on('updateData', function (event, data) {
                if (scope.ds.resumeDataLoaded){
                    if (scope.ds.answeredQuestions != undefined)
                        localStorageService.setItem('answeredQuestions_' + scope.selectedActivity.id, scope.ds.answeredQuestions);

                    if (scope.ds.attendedTotalQuestions != undefined)
                        localStorageService.setItem('attendedTotalQuestions_' + scope.selectedActivity.id, scope.ds.attendedTotalQuestions);

                    if (scope.ds.totalScore != undefined) {utilityService.checkForSessionAlive();
                        localStorageService.setItem('totalScore_' + scope.selectedActivity.id, scope.ds.totalScore);
                        utilityService.setTotalScore(scope.selectedActivity.id, scope.ds.totalScore);
                    }

                    var jsonToStringActivity = JSON.stringify(scope.activity);
                    localStorageService.setItem('activity_' + scope.selectedActivity.id, jsonToStringActivity);


                    utilityService.setActivityResumeData(scope.selectedActivity.id, location.path(),
                        scope.ds.answeredQuestions, scope.ds.attendedTotalQuestions, scope.ds.totalScore, scope.ds.activity, localStorageService.getItem('RESULTS'));

                    //Add interaction
                    var objectiveId = scope.selectedActivity.skillType + "_" + scope.selectedActivity.id;

                    data.objectiveId = objectiveId;


                    //Set status based on percentage
                    var percentage = scope.calculatePercentage();
                    scope.ds.activityScore = percentage;
                    if (percentage >= 90 && scope.ds.activityScormStatus != "passed") {
                        ScormProcessSetStatus(scope.selectedActivity.id, 'passed');

                        //Set suspend data status
                        utilityService.setSuspendDataStatus(scope.selectedActivity.id, 'success');
                    }

                    //console.log(scope.selectedActivity);

                    utilityService.playFeedbackAudio(data.wasCorrect);

                    scope.$emit('recordInteraction', data);
                }
            });

            scope.initRoute = function () {
                //Initialize parameters from routes
                var pageNo = $stateParams.pageNo;

                //Fetch status of activity
                rootScope.inProgress = true;

                if (!scope.ds.activityScormStatus)
                    scope.ds.activityScormStatus = ScormProcessGetStatus(scope.selectedActivity.id, true);

                //Check for whether user is manipulating with url for showing score
                if (scope.showScore &&
                    (scope.ds.attendedTotalQuestions == 0 || scope.totalQuestions != scope.ds.attendedTotalQuestions)) {
                    scope.descriptionTest();
                }

                if (scope.showScore) {
                    scope.landPanel = false;
                    scope.instPanel = false;
                }
                else if (pageNo - 1 >= 0) {
                    scope.currentPageNo = pageNo;
                    scope.landPanel = false;
                    scope.instPanel = false;
                    scope.testStarted = true;

                    //Set resume data
                    if (!scope.showScore && scope.testStarted)
                        utilityService.setActivityResumeData(scope.selectedActivity.id, location.path(),
                            scope.ds.answeredQuestions, scope.ds.attendedTotalQuestions, scope.ds.totalScore, scope.ds.activity, localStorageService.getItem('RESULTS'));

                    if (scope.sectionObj) {
                        if (Array.isArray(scope.sectionObj) && pageNo <= scope.sectionObj.length)
                            scope.currentSection = scope.sectionObj[pageNo - 1];
                        else if (pageNo == 1)
                            scope.currentSection = scope.sectionObj;
                        else
                            state.go('selectedActivity', {
                                'moduleId': scope.moduleId,
                                'activityId': scope.selectedActivity.id
                            });
                    }
                } else {
                    if (!scope.ds.isResumed)
                        scope.ds.answeredQuestions = 0;
                }

                if ((scope.selectedActivity.type == 'WordScramble' || scope.selectedActivity.type == 'WordScrambleClick' || scope.selectedActivity.type == 'GroupHeading' ||
                    scope.selectedActivity.type == 'GroupList') && scope.currentSection['@maxAttempts'] === undefined) {
                    scope.currentSection['@maxAttempts'] = 5;
                }

                /*if (scope.selectedActivity.type == 'FillInTheBlank' ||
                    scope.selectedActivity.type == 'FillInTheBlank_VariableInput' ||
                    scope.selectedActivity.type == 'DialogQuiz');
                    scope.setMaxInput();*/
                if (pageNo - 1 >= 0) {
                     var  controlTypeInput;
                     var controlAnswer;
                     if(Array.isArray(scope.currentSection.QUESTIONS.QUESTION)) {
                          controlTypeInput = scope.currentSection.QUESTIONS.QUESTION[0];
                      } else {
                          controlTypeInput = scope.currentSection.QUESTIONS.QUESTION;
                      }

                     controlAnswer = scope.getControlType(controlTypeInput);

                     if ((scope.selectedActivity.type == 'FillInTheBlank' &&  controlAnswer == 'input')){
                         var maxWidth = scope.getMaxInputWidth(scope.activity);
                         scope.selectedActivity.maxWidth = maxWidth;
                     }
                }

            };

            scope.getImageAssetsPath = function (imageName) {
                return utilityService.getImagePath(scope.selectedActivity.path, imageName);
            };

            scope.isImage = function (path) {
                var patt = new RegExp(".(png|jpg|gif|bmp|jpeg|PNG|JPG|GIF|BMP)$");
                var result = patt.test(path);
                return result;
            };

            scope.getMaxCharsFromSection = function (section, maxCharAnswer) {
                if (Array.isArray(section.QUESTIONS.QUESTION)) {
                    for (var j = 0; j < section.QUESTIONS.QUESTION.length; j++) {
                        var question = section.QUESTIONS.QUESTION[j];
                        maxCharAnswer = scope.getMaxCharsFromQuestion(question, maxCharAnswer);
                    }
                } else {
                    maxCharAnswer = scope.getMaxCharsFromQuestion(section.QUESTIONS.QUESTION, maxCharAnswer);
                }

                return maxCharAnswer;
            };

            scope.getMaxCharsFromQuestion = function (question, maxCharAnswer) {
                if (Array.isArray(question.ANSWERSET)) {
                    for (var k = 0; k < question.ANSWERSET.length; k++) {
                        var answerSet = question.ANSWERSET[k];

                        if (answerSet['@controlType'].toLowerCase() == 'input') {
                            if (answerSet.ANSWER['@correct'] == 'TRUE' || answerSet.ANSWER['@CORRECT'] == 'TRUE') {
                                if (answerSet.ANSWER.TEXT.length >= maxCharAnswer.length) {
                                    maxCharAnswer = answerSet.ANSWER.TEXT;
                                }
                            }
                        }
                    }
                }
                else {
                    var answerSet = question.ANSWERSET;

                    if (answerSet.ANSWER['@correct'] == 'TRUE' || answerSet.ANSWER['@CORRECT'] == 'TRUE') {
                        if (answerSet.ANSWER.TEXT.length >= maxCharAnswer.length) {
                            maxCharAnswer = answerSet.ANSWER.TEXT;
                        }
                    }
                    if(scope.selectedActivity.type == 'Match' && Array.isArray(answerSet.ANSWER)) {
                        maxCharAnswer = answerSet.ANSWER[0].TEXT;
                    }
                }

                return maxCharAnswer;
            };

            scope.setMaxInput = function () {
                var maxCharAnswer = '';

                if (Array.isArray(scope.activity.SECTIONS.SECTION)) {
                    for (var i = 0; i < scope.activity.SECTIONS.SECTION.length; i++) {
                        var section = scope.activity.SECTIONS.SECTION[i];
                        maxCharAnswer = scope.getMaxCharsFromSection(section, maxCharAnswer);
                    }
                } else {
                    maxCharAnswer = scope.getMaxCharsFromSection(scope.activity.SECTIONS.SECTION, maxCharAnswer);
                }
                /*
                var containerFill = document.getElementById('container');

                var container = document.getElementById('activityContainer');

                var elementFill = compile('<span class=\"form-control fontScaleX{{fontSize}} compiledInput\" ' +
                    'style=\"visibility: hidden;position: absolute;min-width:0;padding:0 20px;font-size: 1.1em !important;\" id="maxInput_Fill" type="text">' + maxCharAnswer + '</span>')(scope);

                var element = compile('<span class=\"form-control fontScaleX{{fontSize}} compiledInput\" ' +
                    'style=\"visibility: hidden;position: absolute;min-width:0;padding:0 20px;font-size: 1.1em !important;\" id="maxInput" type="text">' + maxCharAnswer + '</span>')(scope);

                var maxInput = document.getElementById('maxInput');
                var maxInputFill = document.getElementById('maxInput_Fill');
                if (!maxInput) {
                    container.appendChild(element[0]);
                }

                if (!maxInputFill) {
                    containerFill.appendChild(elementFill[0]);
                }*/

                return maxCharAnswer;
            };

            scope.getMaxInputWidth = function () {
                var maxInput = document.getElementById('maxInput');
                 var maxCharSec = scope.setMaxInput();
                if (maxInput) {
                   //maxInput.innerHTML = maxCharSec;
                   //maxInput.setAttribute("size", maxCharSec.length);
                   //var maxWidth = maxInput.clientWidth;
                   return maxCharSec.length;
                }
            };

            scope.getControlType = function (question) {

                var controlType = undefined;

                if (scope.selectedActivity.meta == 'DialogQuiz')
                    return 'button';

                if (question !== undefined && scope.currentSection['@controlType'] == undefined) {
                    if (question.ANSWERSET !== undefined && question.ANSWERSET.length > 1) {
                        controlType = question.ANSWERSET[0]['@controlType'];
                    } else if (question.ANSWERSET !== undefined && question.ANSWERSET['@controlType'] !== undefined) {
                        controlType = question.ANSWERSET['@controlType'];
                    }
                }

                if (controlType == undefined) {
                    controlType = scope.selectedActivity.type;
                }

                if (scope.currentSection &&
                    scope.currentSection['@controlType'] !== undefined) {
                    controlType = scope.currentSection['@controlType'];
                }

                return controlType.toLowerCase();
            };

            scope.openPolicy = function(){
                var model = ngDialog.open({
                    template: 'templates/modal/privacyPolicy.html',
                    controller: 'questionsController'
                });
            }

            scope.getInstructionsFromLang = function() {
                var inst = scope.activity.INSTRUCTIONS.INSTRUCTION;
				//console.log(inst[0])
                var resultInst = '';

                    if(Array.isArray(inst)) {
                        resultInst = inst
                    }

                scope.instructionObj = resultInst[0]; //scope.activity.INSTRUCTIONS;
                scope.instRowObj = scope.instructionObj.ROW;
            };

            scope.getDescriptionFromLang = function() {
                var desc = scope.activity.DESCRIPTIONS.DESCRIPTION;
				//console.log(desc[0]);
                var resultDesc = '';

                    if(Array.isArray(desc)) {
                        resultDesc = desc;
                    }
                scope.descriptionObj = resultDesc[0];
				//console.log(resultDesc[0]);
                scope.landRow = scope.descriptionObj.ROW;
            };

             scope.getImageCaptionsFromLang = function(imgCaptions) {
                var captionText = '';
                var icap = imgCaptions.IMAGECAPTION;

                if(Array.isArray(icap)) {
                    var text = icap.filter(function(value){
                        return value['@lang'] == scope.ds.ispeaklanguage;
                    });
                    return text[0].TEXT;
                } else {
                    return icap.TEXT;
                }
             };


            scope.activityFetched = function (activity) {
                scope.activity = activity;
                scope.getDescriptionFromLang();
                scope.getInstructionsFromLang();
                scope.landPanel = true;
                scope.instPanel = true;
                scope.sectionsObj = scope.activity.SECTIONS;
                scope.sectionObj = scope.activity.SECTIONS.SECTION;
                scope.currentSection = scope.sectionObj;

                var pageNum = [];
                var totalQuestions = 0;

                //Get total number of questions in the section
                if (Array.isArray(scope.sectionObj)) {
                    for (i = 0; i < scope.sectionObj.length; i++) {
                        var questions = scope.sectionObj[i].QUESTIONS.QUESTION;

                        if (Array.isArray(questions)) {
                            for (var j = 0; j < questions.length; j++) {
                                var question = questions[j];

                                if (question.ANSWERSET) {
                                    totalQuestions += 1;
                                }
                            }
                        } else if (questions.ANSWERSET) {
                            totalQuestions++;
                        }
                    }
                }
                else {
                    if (Array.isArray(scope.sectionObj.QUESTIONS.QUESTION)) {
                        totalQuestions += scope.sectionObj.QUESTIONS.QUESTION.length;
                    } else {
                        totalQuestions++;
                    }
                }

                scope.totalPageNo = scope.sectionObj.length;
                scope.answers = [];

                for (i = 0; i < scope.totalPageNo; i++) {
                    pageNum.push(i + 1);
                }

                scope.pageNum = pageNum;
                scope.totalQuestions = totalQuestions;

                if(scope.sectionObj[0]['@type'] ==  'GROUPLIST'){
                    scope.totalQuestions = totalQuestions/2;
                }

                if (scope.ds.isResumed) {
                    scope.ds.activityScore = scope.calculatePercentage();
                }


                if (scope.selectedActivity.type == 'FillInTheBlank_VariableInput' ||
                    (scope.selectedActivity.type == 'Match' && !scope.selectedActivity.shuffleAnswers)) {
                    var maxWidth = scope.getMaxInputWidth(activity);
                    scope.selectedActivity.maxWidth = maxWidth;
                }

                scope.initRoute();
            };

            scope.fetchActivityFromXML = function () {
                xmlDataService.getXMLData(scope.selectedActivity.filename).then(function (data) {
                    var json = xmlDataService.parseXmlToJSON(data);
                    //console.log(json);
                    var activity = json.ACTIVITY;
                    var resumeData = utilityService.getResumeData(scope.selectedActivity.id);
                    if (scope.ds.isResumed && scope.ds.resumeDataLoaded) {
                        activity = resumeData.shuffle_acivity;//utilityService.modifyActivityWithResumeData(scope, scope.selectedActivity, activity, resumeData);
                        scope.ds.activity = resumeData.shuffle_acivity;
                        //console.log(scope.ds.activity);
                        scope.activityFetched(activity);
                    } else if (!scope.ds.isResumed) {                        
                        scope.ds.activity = activity;
                        scope.activityFetched(activity);
                        //console.log(scope.ds.activity);
                        
                    }
                },function () {
                    console.log('Cant load xml');
                });
            };

            var resumeStateLoad = scope.$on('resumeStateLoaded', function (event, activity) {
                scope.ds.resumeDataLoaded = true;
                scope.ds.resumeActivity = activity;
                scope.ds.activity = activity;
                var resumeData = utilityService.getResumeData(scope.selectedActivity.id);
                location.path(resumeData.location);
                ngDialog.close(null, false);
            });

            scope.descriptionTest = function () {
                var path = location.path();
                utilityService.saveData(path, scope.ds);

                scope.ds.isResumed = false;
                scope.ds.answeredQuestions = 0;
                scope.ds.attendedTotalQuestions = 0;
                scope.ds.isWhatsNew = false;
                scope.ds.isTheory = false;
                scope.ds.resumeActivity = angular.copy(scope.activity);
                scope.ds.resumeDataLoaded = true;
                scope.ds.menuSelectionCompleted = undefined;
                rootScope.inProgress = false;

                state.go('selectedActivity', {
                    'moduleId': scope.moduleId,
                    'activityId': scope.selectedActivity.id
                });
            };

            scope.hidetip = function () {
                scope.ds.focuss = false;
            }

            scope.hideTooltip = function () {
                scope.ds.cancelTimeout = $timeout(function () {
                    scope.ds.showTooltip = false;
                    scope.ds.focuss = false;
                    scope.ds.toolTipIndex = null;
                    scope.ds.Grpfocuss = false;
                }, 2000);

            };

            scope.hidetipinverse = function () {
                scope.ds.focuss = false;
            };

            scope.calculatePercentage = function () {
                
                chart_array = scope.get_all_results_local_storage();
                chart_array.unshift(['Categorie', 'Score', { role: 'style' }]);
                console.log(chart_array);
                google.charts.load('current', {packages: ['corechart']});
                //google.charts.setOnLoadCallback(drawChart);
                //document.getElementById("activity-triage-container").addEventListener("load", scope.changePage(scope.current_page));
                
                function drawChart() {
                      var data = google.visualization.arrayToDataTable(chart_array);

                      var view = new google.visualization.DataView(data);
                      view.setColumns([0, 1,
                                       { calc: "stringify",
                                         sourceColumn: 1,
                                         type: "string",
                                         role: "annotation" },
                                       2]);

                      var options = { title: "Family Learning Assessment Scorecard"};
                  
                      //document.getElementById("activity-triage-container").addEventListener("load", scope.changePage(scope.current_page));
                      var chart = new google.visualization.ColumnChart(document.getElementById("activity-score-container-graph"));
                          chart.draw(view, options);
                          document.getElementById("activity-triage-container").addEventListener("load", scope.changePage(scope.current_page));
                }

                /*function drawChart() {
                    // Define the chart to be drawn.

                    var data = google.visualization.arrayToDataTable(chart_array);

                    var options = {title: 'Evaluation'}; 

                    // Instantiate and draw the chart.
                    var chart = new google.visualization.ColumnChart(document.getElementById('activity-score-container-graph'));
                    chart.draw(data, options);
                    document.getElementById("activity-triage-container").addEventListener("load", scope.changePage(scope.current_page));

                }*/

             google.charts.setOnLoadCallback(drawChart);
             
             
            };

            scope.get_all_results_local_storage = function (){
                //FUNCTION TO GET ALL DATA STOCKED ON LOCAL STORAGE 'TEMPORARY FUNCTION DLETE IT ON THE END'
                //get the json from the local storage to be displayed on google chart
                string_json_results = '[' + localStorageService.getItem('RESULTS') + ']';
                json_results = JSON.parse(string_json_results);

                //creat array cat/score
                var array_cat_total_score = [];
                var uniqueArray_cat_total_score = [];
                for (var i = 0; i < json_results.length; i++) {
                    array_cat_total_score.push([json_results[i]['category'],json_results[i]['score']]);
                    uniqueArray_cat_total_score.push([json_results[i]['category'],0]);
                } 
                //mettre uniqueArray_cat_total_score on unique array
                function multiDimensionalUnique(arr) {
                    var uniques = [];
                    var itemsFound = {};
                    for(var i = 0, l = arr.length; i < l; i++) {
                        var stringified = JSON.stringify(arr[i]);
                        if(itemsFound[stringified]) { continue; }
                        uniques.push(arr[i]);
                        itemsFound[stringified] = true;
                    }
                    return uniques;
                }



                array_calc_results = multiDimensionalUnique(uniqueArray_cat_total_score);
                for (var i = 0; i < array_calc_results.length; i++) {
                    //console.log(array_calc_results[i]);
                    array_color = ['#9e53a1','#78bdd6','#00505c','#fcb32e','#f05937','#b9d24f']
                    for (var j = 0; j < array_cat_total_score.length; j++) {
                        if (array_cat_total_score[j][0] == array_calc_results[i][0]) {
                            //if answer have a score 
                            
                            if (array_cat_total_score[j][1] != "NULL") {
                                array_calc_results[i][1] += Number(array_cat_total_score[j][1]);
                                array_calc_results[i][2] = 'color: '+array_color[i];
                            }
                            
                        }
                    }
                }
                //console.log(array_calc_results);
                return array_calc_results;
            }

            scope.checkForResume = function () {
                var isResumable = utilityService.checkForResume(scope.selectedActivity.id);
                return isResumable;
            };

            scope.resumeActivity = function () {
                //console.log(scope.selectedActivity);
                var resumeData = utilityService.getResumeData(scope.selectedActivity.id);
                rootScope.insDivVisible = true;
                if (resumeData && resumeData.data) {
                    scope.ds.activity = resumeData.shuffle_acivity;
                    scope.ds.resumeData = resumeData.data;
                    scope.ds.isResumed = true;
                    scope.masked = true;
                    scope.ds.activity = null;
                    ScormProcessSetStatus(scope.selectedActivity.id, 'browsed');
                    //console.log('is resumed acitivity');
                    if (!scope.ds.resumeActivity) {
                         var model = ngDialog.open({
                             template:'templates/modal/resumeState.html',
                             controller: 'resumeModalController',
                             showClose: false,
                             closeByEscape: false
                         });
                        scope.ds.resumeActivity = resumeData.shuffle_acivity;

                        //start update local storage after resuming
                        localStorageService.setItem('answeredQuestions_' + scope.selectedActivity.id, resumeData.answeredQuestions);               
                        localStorageService.setItem('attendedTotalQuestions_' + scope.selectedActivity.id, resumeData.attendedTotalQuestions);                
                        localStorageService.setItem('totalScore_' + scope.selectedActivity.id, resumeData.totalScore);
                        localStorageService.setItem('RESULTS', resumeData.results);
                        var jsonToStringActivity = JSON.stringify(scope.activity);
                        localStorageService.setItem('activity_' + scope.selectedActivity.id, jsonToStringActivity);
                        //end update local storage after resuming

                        utilityService.modifyActivityWithResumeData(scope, scope.selectedActivity, scope.ds.resumeActivity, resumeData);
                        scope.ds.resumeDataLoaded = true;
                    }
                    else{
                        scope.ds.answeredQuestions = resumeData.answeredQuestions ? resumeData.answeredQuestions : 0;
                        scope.ds.totalScore = resumeData.totalScore ? resumeData.totalScore : 0;
                        scope.ds.attendedTotalQuestions = resumeData.attendedTotalQuestions ? resumeData.attendedTotalQuestions : 0;
                        location.path(resumeData.location);
                    }
                }else {
                     scope.resumeWithoutData(0);
                }
            };

             scope.resumeActivityBtn = function () {
                 var resumeData = utilityService.getResumeData(scope.selectedActivity.id);
                 //console.log(resumeData);
                 if (resumeData && resumeData.data) {
                    scope.resumeActivity();
                    //console.log('is resumed activity');
                 } else {
                      scope.resumeWithoutData(0);
                      //console.log('is resumed activity');
                 }
             };

            scope.resumeWithoutData = function (sectionNo) {
                scope.landPanel = false;
                scope.instPanel = false;

                if (scope.sectionObj) {
                    if (Array.isArray(scope.sectionObj))
                        scope.currentSection = scope.sectionObj[sectionNo];
                    else
                        scope.currentSection = scope.sectionObj;
                }

                scope.testStarted = true;
                var pageNo = parseInt(sectionNo) + 1;
                localStorageService.clearStorage();

                //Remove resume data if any
                scope.ds.isResumed = false;
                scope.ds.isResuming = false;
                scope.ds.resumeDataLoaded = true;
                scope.ds.activity = null;
                scope.ds.resumeActivity = null;
                scope.ds.answeredQuestions = 0;
                utilityService.removeResumeData(scope.selectedActivity.id);

                ScormProcessSetStatus(scope.selectedActivity.id, 'browsed');
                ngDialog.close(null, false);

                state.go('activityTestStarted', {
                    'moduleId': scope.moduleId,
                    'activityId': scope.selectedActivity.id,
                    'pageNo': pageNo
                });
            };

            scope.startTest = function (sectionNo) {
                var showDiv = true;
                scope.toggleDiv(showDiv);
                scope.landPanel = false;
                scope.instPanel = false;
                //shuffle(scope.sectionObj);
                //console.log(scope.sectionObj);
                if (scope.sectionObj) {
                    if (Array.isArray(scope.sectionObj)){
                        scope.currentSection = scope.sectionObj[sectionNo];
                        //console.log(scope.currentSection);
                    }else{
                        scope.currentSection = scope.sectionObj;
                        //console.log(scope.currentSection);
                    }
                }

                scope.testStarted = true;
                var pageNo = parseInt(sectionNo) + 1;
                //scope.shuffleActivity();
                localStorageService.clearStorage();
                scope.ds.dragDrop = [];
                //Remove resume data if any
                scope.ds.isResumed = false;
                scope.ds.isResuming = false;
                scope.ds.resumeDataLoaded = true;
                scope.ds.resumeData = utilityService.getResumeData(scope.selectedActivity.id);
                scope.ds.activity = null;
                scope.ds.resumeActivity = null;
                scope.ds.answeredQuestions = 0;
                utilityService.removeResumeData(scope.selectedActivity.id);
                var suspendData = JSON.stringify(scope.ds.suspendData);
                ScormProcessSetSuspendState(suspendData);
                //console.log(suspendData);
                ScormProcessSetStatus(scope.selectedActivity.id, 'browsed');

                state.go('activityTestStarted', {
                    'moduleId': scope.moduleId,
                    'activityId': scope.selectedActivity.id,
                    'pageNo': pageNo
                });

            };

            scope.pageChanged = function () {
                scope.ds.isPageChanged = true;
                state.go('activityTestStarted', {
                    'moduleId': scope.moduleId,
                    'activityId': scope.selectedActivity.id, 'pageNo': scope.currentPageNo
                });
            };

            scope.returnAsHtml = function (text) {
                if(text)
                return $sce.trustAsHtml(text.toString());
            };

            scope.isArray = function (obj) {
                return Array.isArray(obj);
            };

            scope.goToScore = function () {
                ////Remove resume data if any
                utilityService.removeResumeData(scope.selectedActivity.id);

                state.go('activity_showScore', {
                    'moduleId': scope.moduleId,
                    'activityId': scope.selectedActivity.id
                });
            };

            var attendedTotQues = scope.$watch('!(ds.attendedTotalQuestions != 0 && totalQuestions == ds.attendedTotalQuestions)' +
                '|| showScore', function (newVal, oldVal) {
                scope.scoreDisabled = newVal;
            });

            scope.goToNextItem = function () {
                if (scope.selectedActivity.type == 'DialogQuiz') {
                    state.go('groups', {'selectedUnit': 'activities', 'moduleId': scope.moduleId});
                } else {
                    groupService.goToNextItem(scope.nextActivityId, scope.moduleId);
                }
            };

            scope.showTranslationDescription = function(){
            if(scope.ds.mainMenuLinkLang =='en'){
                if(rootScope.translateLang == '0'){
                  scope.ds.ispeaklanguage = 'fr';
                }
                else if(rootScope.translateLang == '1'){
                 scope.ds.ispeaklanguage = 'es';
                 }
                 else if(rootScope.translateLang == '2'){
                 scope.ds.ispeaklanguage = 'zh';
                 }
            }
             if(scope.ds.mainMenuLinkLang =='fr' ){
                if(rootScope.translateLang == '0'){
                  scope.ds.ispeaklanguage = 'en';
                }else if(rootScope.translateLang == '1'){
                  scope.ds.ispeaklanguage = 'es';
                }
                else if(rootScope.translateLang == '2'){
                 scope.ds.ispeaklanguage = 'zh';
                }
             }

             var desc = scope.activity.DESCRIPTIONS.DESCRIPTION;
                 var resultDesc = '';

                     if(Array.isArray(desc)) {
                         resultDesc = desc.filter(function(elem,indx){
                             if(parseInt(scope.moduleId) <=8){
                                 return scope.ds.mainMenuLinkLang === elem['@lang'];
                             }
                         })
                     }

                     if (parseInt(scope.moduleId) >=9 && scope.ds.ispeaklanguage) {
                         resultDesc = desc.filter(function(elem,indx){
                           return scope.ds.ispeaklanguage === elem['@lang'];
                         })
                     }

                 scope.descriptionObj = resultDesc[0];
                 scope.landRow = scope.descriptionObj.ROW;

             };

             scope.showTranslationInstruction = function(){
                 if(scope.ds.mainMenuLinkLang =='en'){
                     if(rootScope.translateLang == '0'){
                       scope.ds.ispeaklanguage = 'fr';
                     }
                     else if(rootScope.translateLang == '1'){
                      scope.ds.ispeaklanguage = 'es';
                      }
                     else if(rootScope.translateLang == '2'){
                      scope.ds.ispeaklanguage = 'zh';
                     }
                 }
                  if(scope.ds.mainMenuLinkLang =='fr' ){
                     if(rootScope.translateLang == '0'){
                      scope.ds.ispeaklanguage = 'en';
                     }else if(rootScope.translateLang == '1'){
                      scope.ds.ispeaklanguage = 'es';
                     }
                     else if(rootScope.translateLang == '2'){
                      scope.ds.ispeaklanguage = 'zh';
                     }
                  }

                  var inst = scope.activity.INSTRUCTIONS.INSTRUCTION;
                      var resultInst = '';

                          if(Array.isArray(inst)) {
                              resultInst = inst.filter(function(elem,indx){
                                   return scope.ds.mainMenuLinkLang === elem['@lang'];
                                  }
                              )
                          }
                          if (parseInt(scope.moduleId) >=9 && scope.ds.ispeaklanguage) {
                              resultInst = inst.filter(function(elem,indx){
                                return scope.ds.ispeaklanguage === elem['@lang'];
                              })
                          }

                      scope.instructionObj = resultInst[0]; //scope.activity.INSTRUCTIONS;
                      scope.instRowObj = scope.instructionObj.ROW;
             };

             // Added another function for enabling toggle function in Instruction and Description
             scope.translateDes= function(){
                 var click = $(this).data('clicks') || 2;
                 if (click%2 == 1){
                 		scope.getDescriptionFromLang();
                 }else{
                 		scope.showTranslationDescription();
                  }
                  $(this).data('clicks',click+1);
             };

             scope.translateIns= function(){
                  var click = $(this).data('clicks') || 2;
                  if (click%2 == 1){
                  		scope.getInstructionsFromLang();
                  }else{
                  		scope.showTranslationInstruction();
                   }
                   $(this).data('clicks',click+1);
             };

            scope.isNextActivity = function () {
                var result;
                var id = scope.selectedActivity.id;
                if (scope.showScore) {
                    var forcedPath = groupService.getForcedPath(scope.moduleId);

                    if (forcedPath && rootScope.isLearnerRole) {
                        var fpIndex = groupService.getItemFPIndex(id, forcedPath);

                        if (fpIndex != undefined) {
                            var status = utilityService.getSuspendDataStatus(id);
                            if (status == 'success') {
                                scope.nextActivityId = groupService.getNextItem(id, scope.moduleId);
                            }
                        } else {
                            scope.nextActivityId = groupService.getNextItem($stateParams.activityId, scope.moduleId);
                        }
                    } else {
                        scope.nextActivityId = groupService.getNextItem($stateParams.activityId, scope.moduleId);
                    }

                    result = scope.nextActivityId != undefined;
                    return result
                }

            };

            var answeredQues = scope.$watch('ds.answeredQuestions', function (newVal) {
                if (newVal == 1) {
                    if (scope.suspendDataStatus != 'inProgress' && scope.suspendDataStatus != 'success') {
                        ScormProcessSetStatus(scope.selectedActivity.id, 'incomplete');
                        utilityService.setSuspendDataStatus(scope.selectedActivity.id, 'inProgress');
                    }
                }

                if (newVal)
                    utilityService.setAnsweredQuestions(scope.selectedActivity.id, newVal);
            });

            var attendedQues = scope.$watch('ds.attendedTotalQuestions', function (newVal) {
                if (newVal)
                    utilityService.setAttendedTotalQuestions(scope.selectedActivity.id, newVal);
            });

            scope.accentMouseDown = function () {
                rootScope.$broadcast('accent-mouse-down');
            };

            scope.addAccent = function ($event, accent) {
                rootScope.$broadcast('reset-focus');
                rootScope.$broadcast('add-french-accent', accent);
            };

            scope.getVideoPath = function (videoFile) {
                return utilityService.getVideoPath(scope.selectedActivity.path, videoFile);
            };

            scope.getFileName = function (filePath) {
                if (filePath) {
                    var temp = filePath.split('/');
                    filePath = temp[temp.length - 1];
                }

                return filePath;
            };

            scope.stopAudio = function (audioElement) {
                 angular.element('#load_audio').attr("alt", scope.getFileName(audioElement));
                 angular.element('#load_audio').attr("src", utilityService.getAudioPath(scope.selectedActivity.path, audioElement));
                 audio = angular.element('#load_audio');
                 audio[0].currentTime = 0;;

            };

            scope.play_audio = function (src, tooltipIndex) {
                $timeout.cancel(scope.ds.cancelTimeout);
                scope.ds.showTooltip = true;
                scope.ds.focuss = true;
                scope.tooltipIndex = tooltipIndex;
                scope.ds.toolTipIndex = tooltipIndex;
                angular.element('#load_audio').attr("alt", scope.getFileName(src));
                angular.element('#load_audio').attr("src", utilityService.getAudioPath(scope.selectedActivity.path, src));

                audio = angular.element('#load_audio');
                audio[0].play();
            };

            scope.showAccentBar = function () {
                var showAccentBar = false;

                if (rootScope.isFrenchModule && !scope.showScore && scope.testStarted) {
                    if (scope.selectedActivity.type == 'FillInTheBlank_VariableInput' ||
                        scope.selectedActivity.type == 'FillInTheBlank') {
                        if (Array.isArray(scope.currentSection.QUESTIONS.QUESTION)) {
                            for (var i = 0; i < scope.currentSection.QUESTIONS.QUESTION.length; i++) {
                                var question = scope.currentSection.QUESTIONS.QUESTION[i];

                                if (Array.isArray(question.ANSWERSET)) {
                                    for (var j = 0; j < question.ANSWERSET.length; j++) {
                                        var answerSet = question.ANSWERSET[j];
                                        if (answerSet['@controlType'].toLowerCase() == 'input') {
                                            showAccentBar = true;
                                        }
                                    }
                                } else {
                                    var answerSet = question.ANSWERSET;
                                    if (answerSet['@controlType'].toLowerCase() == 'input') {
                                        showAccentBar = true;
                                    }
                                }
                            }
                        }
                        else {
                            var question = scope.currentSection.QUESTIONS.QUESTION;
                            if (Array.isArray(question.ANSWERSET)) {
                                for (var j = 0; j < question.ANSWERSET.length; j++) {
                                    var answerSet = question.ANSWERSET[j];
                                    if (answerSet['@controlType'].toLowerCase() == 'input') {
                                        showAccentBar = true;
                                    }
                                }
                            } else {
                                var answerSet = question.ANSWERSET;
                                if (answerSet['@controlType'].toLowerCase() == 'input') {
                                    showAccentBar = true;
                                }
                            }
                        }
                    }
                }

                return showAccentBar;
            };

            $window.onresize = function () {
                scope.$apply(function () {
                    scope.getHeight();
                    scope.getScoreContentHeight();
                    scope.Descriptionheight = scope.getDescriptionHeight();
                });
            };

            scope.getHeight = function () {
                var instructionTbl = document.getElementById('instruction');

                if (instructionTbl != null)
                    scope.ds.pageWidth = instructionTbl.clientWidth + 5;
                else
                    scope.ds.pageWidth = $window.innerWidth - 170;


                scope.getHeightTimeout = $timeout(function () {
                    var qc = document.getElementById('questionContainer');
                    var footer = document.getElementById('footer');
                    var qcHeight = qc ? qc.offsetTop : 0;
                    var fHeight = footer && footer.scrollHeight ? footer.scrollHeight : 77;
                    scope.ds.height = $window.innerHeight - qcHeight - fHeight;

                    //calculate height if section header
                    var sectionHeader = document.getElementById('sectionHeader');
                    if (sectionHeader && scope.selectedActivity.type != 'FillInTheBlank') {
                        scope.ds.containerHeight = scope.ds.height - sectionHeader.clientHeight - 35;
                    }
                }, 1000);
            }

            scope.getScoreContentHeight = function () {
                var content = document.getElementById('scoreItems');

                if (content) {
                    var scorecontainer = document.getElementById('activity-score-container');
                    var scorecontainerHeight = scorecontainer ? scorecontainer.offsetTop : 0;
                    var scoreHeight = $window.innerHeight - scorecontainerHeight;
                    var contentHeight = content.scrollHeight;
                    //content.style.paddingTop = (scoreHeight - contentHeight) / 2 + "px";
                    var pad = (scoreHeight - contentHeight) / 2;

                    return pad;
                }
            }

            scope.getDescriptionHeight = function () {
                var tableBody = document.getElementById('DescriptionTableBody');
                var descriptionButtons = document.getElementById('DescriptionTableButtons');
                var qcHeight = tableBody ? tableBody.offsetTop : 0;
                var fHeight = descriptionButtons ? descriptionButtons.scrollHeight : 0;
                fHeight = fHeight ? fHeight : 77;
                return $window.innerHeight - qcHeight - fHeight;
            }

            scope.makeDivFocusActive = function (e) {
                if (e.keyCode == 9)
                    scope.focusActive = true;
            };

            scope.makeDivFocusDeActive = function () {
                scope.focusActive = false;
            };

            angular.element(document).ready(function () {
                scope.Descriptionheight = scope.getDescriptionHeight();
            });


            var directiveRender = scope.$on('directive-rendered', function () {
                if (!scope.ds.isPageChanged) {
                        scope.getHeight();
                        scope.scorePadding = scope.getScoreContentHeight();
                }
                else {
                    scope.ds.isPageChanged = false;
                }
            });

            scope.setScoreMargin = function () {
                if (rootScope.fontSize == "4") {
                    scope.scoreMargin = 150;
                } else if (rootScope.fontSize == "3") {
                    scope.scoreMargin = 100;
                } else if (rootScope.fontSize == "2") {
                    scope.scoreMargin = 210;
                }
            }

            var accessChange = scope.$on('accessibilityChanged', function () {
               scope.accessChangeTimeout =  $timeout(function () {
                    scope.getHeight();
                    scope.setScoreMargin();
                    scope.getInstructionsFromLang();
                    scope.getDescriptionFromLang();
                    scope.scorePadding = scope.getScoreContentHeight();
                    scope.selectedActivity.maxWidth = scope.getMaxInputWidth();
                }, 500);
            });

            scope.destroyVideoJS = function() {
                //destroy istance of VideoJS
               if ((scope.myPlayer !== undefined) && (scope.myPlayer !== null)  && (scope.myPlayer.el_ !== null)) {
                     scope.myPlayer.dispose();
               }
            };

            scope.$on('$destroy', function(){
                scope.destroyVideoJS();
            });

            scope.toggleDiv = function(showDiv) {
               rootScope.insDivVisible  = showDiv ? showDiv : !rootScope.insDivVisible;
            };

            //display score after finishin the triage
            scope.display_score = function() {
                $('#thank_you').show();
                $("#activity-score-container-graph").show();
                $("#activity-triage-container").hide();
            };

            //PAGINATION INSIDE THE TRIAGE
            scope.current_page = 1;
            scope.records_per_page = 6;
            scope.objJson = [
                { adName: "AdName 1"},
                { adName: "AdName 2"},
                { adName: "AdName 3"},
                { adName: "AdName 4"},
                { adName: "AdName 5"},
                { adName: "AdName 6"},
                { adName: "AdName 7"},
                { adName: "AdName 8"},
                { adName: "AdName 9"},
                { adName: "AdName 10"},
                { adName: "AdName 11"},
                { adName: "AdName 12"}
            ]; // Can be obtained from another source, such as your objJson variable

            scope.prevPage = function()
            {
                if (scope.current_page > 1) {
                    scope.current_page--;
                    scope.changePage(scope.current_page);
                }
            }

            scope.nextPage = function()
            {
                if (scope.current_page < scope.numPages()) {
                    scope.current_page++;
                    scope.changePage(scope.current_page);
                }else{
                    scope.changePage(scope.current_page);
                }
            }


            var array_triage = [];
        
            scope.changePage = function(page)
            {
                //console.log(scope.activity.TRIAGE);
                //scope.fetchActivityFromXML();
                //console.log(scope);
                var activityId = $stateParams.activityId;
                scope.selectedActivity = groupService.getItem(activityId, scope.moduleId);
                //console.log(scope.selectedActivity.id);

                var activity_triage = localStorageService.getItem('activity_' + scope.selectedActivity.id);
                activity_triage = JSON.parse(activity_triage);
                var triage = activity_triage.TRIAGE;




                array_triage.length = triage.QUESTIONS.QUESTION.length;
                
                var btn_next = document.getElementById("btn_next");
                var btn_prev = document.getElementById("btn_prev");
                var listing_table = document.getElementById("listingTable");
                var page_span = document.getElementById("page");
                $('#btn_next').hide();
                $('#btn_prev').hide();
                $('#display_graph').hide();
                
             
                // Validate page


                if (page < 1) page = 1;
                if (page > scope.numPages()) page = scope.numPages();

                if(listing_table){

                
                    listing_table.innerHTML = "";
                    for (var i = (page-1) * scope.records_per_page; i < (page * scope.records_per_page); i++) {
                        answers_triage = "";
                        //in case if the numpage return val bigger than what we attend
                        if(triage.QUESTIONS.QUESTION[i]){
                            for (var j = 0; j < triage.QUESTIONS.QUESTION[i].ANSWERSET.ANSWER.length; j++) {
                                answers_triage += "<div><label><input class='option_triage triage_"+i+"' type='radio' name='optradio"+ i +"' id='"+i+"_"+j+"'>"+ triage.QUESTIONS.QUESTION[i].ANSWERSET.ANSWER[j].TEXT +"<label></div>";
                            }
                         listing_table.innerHTML += "<div class='col-sm-4'><h5>" + triage.QUESTIONS.QUESTION[i].TEXT + "</h5>" + answers_triage + "</div>";
                        }                        
                       
                    }


                    var listing_table_ele = $("#listingTable .option_triage");
                    listing_table_ele.click(function() {
                        
                        var val_triage = this.id.split("_");
                        //console.log(val_triage[0]);
                        //console.log(val_triage[1]);
                        array_triage[val_triage[0]] = val_triage[1];
                        jQuery(".triage_"+val_triage[0]).attr("disabled", 'disabled');
                        var empties = array_triage.length - array_triage.filter(String).length;

                        //display next button to navigate to the next page
                        if(array_triage.filter(String).length == scope.records_per_page){
                           $('#btn_next').show('fade');
                        }

                        //display statistique button to navigate to the graph

                        if(empties == 0){
                            $('#display_graph').show('fade');
                        }
                        // ADD TRIAGE RESULTS TO LOCAL STORAGE
                        localStorageService.setItem('TRIAGE' , array_triage);
                    
                    });
                }
                

                //page_span.innerHTML = page;

                /*if (page == 1) {
                    btn_prev.style.visibility = "hidden";
                } else {
                    btn_prev.style.visibility = "visible";
                }

                if (page == scope.numPages()) {
                    btn_next.style.visibility = "hidden";
                } else {
                    btn_next.style.visibility = "visible";
                }*/
                //console.log('change page :')
                //onsole.log(scope.objJson);
            }


            scope.numPages = function()
            {
                return Math.ceil(scope.objJson.length / scope.records_per_page);
            }

            // END PAGINATION INSIDE TGE TRIAGE

            scope.init = function () {

                scope.ds = dataService.getDataSource();
                scope.testStarted = false;
                scope.masked = false;
                var activityId = $stateParams.activityId;
                scope.ds.activityId = activityId;
                scope.ds.communicativeSkill = "activities";

                scope.moduleId = $stateParams.moduleId;
                scope.suspendDataStatus = utilityService.getSuspendDataStatus(activityId);

                scope.showScore = state.current.name == 'activity_showScore';
                scope.setScoreMargin();

                //document.getElementById('questionContainer').offsetTop
                scope.accents = [{text: 'à'}, {text: 'À'}, {text: 'â'}, {text: 'Â'}, {text: 'ç'}, {text: 'Ç'}, {text: 'é'}, {text: 'É'},
                    {text: 'è'}, {text: 'È'}, {text: 'ê'}, {text: 'Ê'}, {text: 'ë'}, {text: 'Ë'},
                    {text: 'î'}, {text: 'Î'}, {text: 'ï'}, {text: 'Ï'}, {text: 'ô'}, {text: 'Ô'}, {text: 'ù'},
                    {text: 'Ù'}, {text: 'û'}, {text: 'Û'}, {text: 'ü'}, {text: 'Ü'}, {text: 'ÿ'}, {text: 'Ÿ'}];


                var answeredQuestions = localStorageService.getItem('answeredQuestions_' + activityId);
                var totalScore = localStorageService.getItem('totalScore_' + activityId);
                var attendedTotalQuestions = localStorageService.getItem('attendedTotalQuestions_' + activityId);

                if (!scope.ds.isResumed) {
                     if(scope.ds.isResumedFromModal == true){
                           scope.ds.answeredQuestions = 0;
                            scope.ds.totalScore = 0;

                     } else {
                           scope.ds.answeredQuestions = answeredQuestions ? parseInt(answeredQuestions) : 0;
                           scope.ds.totalScore = totalScore ? parseFloat(totalScore) : 0;
                           //console.log('answerd questions');
                           console.log(answeredQuestions);
                     }
                }else{
                           scope.ds.answeredQuestions = answeredQuestions ? parseInt(answeredQuestions) : 0;
                           scope.ds.totalScore = totalScore ? parseFloat(totalScore) : 0;
                           scope.ds.attendedTotalQuestions = attendedTotalQuestions ? parseFloat(attendedTotalQuestions) : 0;
                           console.log('answerd questions');
                           console.log(answeredQuestions);
                           scope.changePage(1);
                }

                scope.ds.selectedText = scope.ds.selectedImage = '';
                scope.startingQuestionIndex = 1;

                scope.clickable = 0;

                // current Page
                scope.currentPageNo = 0;
                scope.selectedActivity = groupService.getItem(activityId, scope.moduleId);

                var title = $filter('localization')('ACTIVITY', scope.ds.language);
                utilityService.setPageTitle(title + ' - ' + scope.selectedActivity.title);

                if (scope.ds.isResumedFromModal == true) {
                   scope.ds.isResumedFromModal = false;
                   scope.attendedQuestionLastAccess = true;
                   scope.resumeActivityTimeout = $timeout(function(){
                     scope.resumeActivity();
                     scope.ds.communicativeSkill = "activities";
                   },2000);

                }
                utilityService.checkForSessionAlive();

                //Initialize selected activity
                var attendedTotalQuestions;
                if (scope.selectedActivity) {
                    scope.activityType = scope.selectedActivity.type;
                    attendedTotalQuestions = localStorageService.getItem('attendedTotalQuestions_' + scope.selectedActivity.id);

                    if (!scope.ds.isResumed) {
                        if(scope.attendedQuestionLastAccess == true) {
                            scope.ds.attendedTotalQuestions = 0;
                        } else {
                            scope.ds.attendedTotalQuestions = attendedTotalQuestions ? parseInt(attendedTotalQuestions) : 0;
                        }
                    }


                    scope.totalQuestions = 0;
                    scope.lastest_number = 0;

                    var skillType = scope.selectedActivity.skillType;
                    scope.selectedActivity.skillType = skillType ? skillType : 'activities';

                    //Fetch Activity
                    if (!scope.ds.activity)
                        scope.fetchActivityFromXML();
                    else {
                        scope.activityFetched(scope.ds.activity);
                    }

                    scope.templateLoaded = true;

                    var delay = scope.isFireFox ? 2500 : 1500;

                    scope.descHeightInitTimeout = $timeout(function () {
                        scope.Descriptionheight = scope.getDescriptionHeight();
                    }, delay);

                    scope.heightInitTimeout = $timeout(function () {
                        scope.getHeight();
                    }, 200);

                    scope.scorePadInitTimeout = $timeout(function () {
                        scope.scorePadding = scope.getScoreContentHeight();
                    }, 1);

                     if(scope.selectedActivity.type == 'DialogQuiz') {
                          scope.ds.mainTitle = 'DIALOGUES';
                          var str = scope.selectedActivity.title.split("/");
                          scope.dialogueTitle1 = str[0];
                          scope.dialogueTitle2 = str[1];
                     }
                     else {
                         scope.ds.mainTitle = 'ACTIVITY';
                     }

                }
                else {
                    state.go('instructions');
                }
            }();
        }]);
