app.controller('groupingController', ['$scope', '$http', 'helperService', '$rootScope',
    'dataService', 'localStorageService','utilityService',
    '$location', '$stateParams', '$state', 'groupService', '$timeout', '$window','$filter',
    function ($scope, $http, helper, $rootScope, dataService, localStorageService,utilityService,
              location, stateParams, state, groupService, $timeout, $window, $filter) {

        $scope.tabSelected = function (unit) {
            $scope.ds.selectedUnit = unit.title;
            var title=  $filter('localization')($filter('uppercase')(unit.title), $scope.ds.language)
            utilityService.setPageTitle(title);

            if($scope.ds.sound){
               $scope.ds.sound.stop();
            }
        };

        $scope.setFPStatus = function (units) {
            if (units) {
                for (var i = 0; i < units.length; i++) {
                    var unit = units[i];

                    if (unit.contents) {
                        for (var j = 0; j < unit.contents.length; j++) {
                            var item = unit.contents[j];

                            var forcedPath = groupService.getForcedPath($scope.moduleId);
                            var isDisabled = groupService.checkForStatus(item, forcedPath);
                            item.isDisabled = isDisabled;
                        }
                    }
                }
            }
        };


        $scope.status = [
            {
                className: 'success',
                statusName: 'SUCCESS'
            },
            {
                className: 'inProgress',
                statusName: 'IN_PROGRESS'
            },
            {
                className: 'tryIt',
                statusName: 'TRY_IT'
            },
        ];

        $scope.getStatusName = function(status) {
            var statuses = $scope.status;
            for(var i = 0; i < statuses.length; i++)
            {
              if(statuses[i].className == status)
              {
                return statuses[i].statusName;
              }
            }
        };

        $scope.getContentStatus = function (id) {
            var suspendData = $scope.ds.suspendData;
            var status = "tryIt";

            if (suspendData && suspendData != "null") {
                var contentData = suspendData[id];
                if (contentData && contentData.status)
                    status = contentData.status;
            }

            return status;
        };

        $scope.setActivity = function (activity) {
            $scope.ds.selectedActivity = activity;
        };

        $scope.itemSelected = function (item) {

            var type = item.sectionType ? item.sectionType : item.skillType;
            var ds = $scope.ds;

            groupService.selectedType(type, item, ds);
        };

        $scope.organizeUnits = function (units) {
            if (units) {
                if($scope.moduleId == "3H" || $scope.moduleId == "4H" || $scope.moduleId == "5H"){
                    units.splice(1, 1);
                }
                if($scope.moduleId == "15"){
                    units.splice(3, 1);
                }
                for (var i = 0; i < units.length; i++) {
                    var unit = units[i];
                    var maxItems = $scope.moduleId == 1000 ? unit.contents.length : 10;
                    var allContents = $filter('orderBy')(unit.contents, 'order');
                    var contents = [];

                    //Remove hidden items
                    for (var j = 0; j < allContents.length; j++) {
                        var currentContent = allContents[j];
                        if (!currentContent.isHidden) {
                            contents.push(currentContent);
                        }
                    }

                    var grps = contents.length / maxItems;
                    unit.grpContents = [];

                    for (var j = 0; j < grps; j++) {
                        unit.grpContents[j] = [];

                        for (var k = 0; k < maxItems; k++) {
                            if (contents[j * maxItems + k]) {
                                unit.grpContents[j].push(contents[j * maxItems + k]);
                            }
                        }
                    }
                }
            }
        };

        $scope.calculateContainerHeight = function () {
            var ic = document.getElementById('itemContainer');
            var legend = document.getElementById('legend');

                var ch = $window.innerHeight - ic.offsetTop - legend.scrollHeight;

            return ch;
        };

        $scope.calculateMenuHeight = function () {
            var largeAccessFont = document.getElementById('fontScaleX4');

            if(largeAccessFont)
            var mh = 70;
            else
            var mh = 0;

            return mh;
        };

        $window.onresize = function () {
            $scope.$apply(function () {
                $scope.ch = $scope.calculateContainerHeight();
                $scope.mh = $scope.calculateMenuHeight();
            });
        };

        var accessChangedHandler = $scope.$on('accessibilityChanged', function () {
            $scope.accessChangeTimeout = $timeout(function () {
                $scope.ch = $scope.calculateContainerHeight();
                $scope.mh = $scope.calculateMenuHeight();
            }, 200);
        });

        $scope.$on('$destroy', function(){
            accessChangedHandler();
            $timeout.cancel( $scope.groupingContHeightTimeout );
            $timeout.cancel( $scope.accessChangeTimeout );
        });

        $scope.init = function () {
            $scope.ds = dataService.getDataSource();
            $rootScope.version = '0.2';
            $scope.ds.isWhatsNew = false;
            $scope.ds.mainTitle = 'MAIN_MENU_TITLE';

            $scope.moduleId = stateParams.moduleId;
            $scope.units = groupService.getUnitContents($scope.moduleId);
            $scope.organizeUnits($scope.units);

            //Wait until suspend state is loaded
            $scope.groupingContHeightTimeout = $timeout(function () {
                if ($rootScope.isLearnerRole) {
                    $scope.setFPStatus($scope.units);
                }

                $scope.ch = $scope.calculateContainerHeight();
                $scope.mh = $scope.calculateMenuHeight();
            }, 500);

            $scope.selectedUnit = stateParams.selectedUnit;

            if ($scope.units && $scope.selectedUnit) {
                for (var i = 0; i < $scope.units.length; i++) {
                    var unit = $scope.units[i];

                    if (unit.title == $scope.selectedUnit)
                        unit.selected = true;
                }
            }
        }();
    }]);