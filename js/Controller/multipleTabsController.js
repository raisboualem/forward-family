app.controller('multipleTabsController', ['$scope','ngDialog','dataService','$rootScope','$cookies',function (scope, ngDialog, dataService,rootScope,cookies) {


     $(document).keyup(function(e) {
       if (e.keyCode === 27) // esc
            window.close();
     });


    scope.init = function () {
        scope.ds = dataService.getDataSource();

         rootScope.sysLang = cookies.get('systemLanguage') ? cookies.get('systemLanguage') : false;

        	if(rootScope.sysLang == 0){
                scope.ds.language = 'en';
            }
            if(rootScope.sysLang == 1){
                scope.ds.language = 'fr';
            }
            if(rootScope.sysLang == 2){
                scope.ds.language = 'es';
            }
            if(rootScope.sysLang == 3){
                scope.ds.language = 'zh';
            }
    }();

}]);


