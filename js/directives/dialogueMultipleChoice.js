app.directive('dialogueMultipleChoice', function () {
    return {
        restrict: 'E',
        templateUrl: "templates/directives/dialogueMultipleChoice.html",
        scope: {
            question: '=',
            questionNo: '=',
            selectedActivity: '='
        },
        controller: "answerSetController",
        link: function (scope, element, attr) {
            scope.isCorrect = function (answer) {
                var result = false;
                if (answer && answer['@CORRECT'] == "TRUE" ||
                    answer['@correct'] == "TRUE")
                    result = true;

                return result;
            }

            scope.getCorrectAnswer = function () {
                for (var i = 0; i < scope.question.ANSWERS.ANSWER.length; i++) {
                    var answer = scope.question.ANSWERS.ANSWER[i];

                    if (scope.isCorrect(answer)) {
                        return answer['#text'];
                    }
                }
            }
        }
    };
});
