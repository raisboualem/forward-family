app.directive('inputAnswerSet', ['$timeout', '$rootScope', 'utilityService', function ($timeout, $rootScope, utilityService) {
    return {
        restrict: 'E',
        controller: 'answerSetController',
        templateUrl: "templates/directives/inputAnswerSet.html",
        link: function (scope, element, attr) {
            scope.cnt = 0;

            var questionWatcherInput = scope.$watch('question', function () {
                scope.drop_down_texts = scope.get_drop_down_text_array(scope.displayAsText(scope.question));
                scope.dropDownTextsCount = scope.get_drop_down_text_array_count(scope.drop_down_texts);
                scope.questionAnswerSet = scope.question.ANSWERSET;
            });

            scope.isSingleCharacterAnswer = function (answerSet) {
                var correctAnswer = scope.getAnswerText(answerSet.ANSWER);
                if (correctAnswer && correctAnswer[0] &&
                    correctAnswer[0].length == 1)
                    return true;
            };

            scope.hidetipblanks = function () {
                scope.ds.focuss = false;
                scope.ds.inputFocus = false;
            };

            scope.hidetipinverse = function () {
                scope.ds.focuss = false;
            };



            scope.play_audio = function (src, tooltipIndex, event) {
                $timeout.cancel(scope.ds.cancelTimeout);

                if (event) {
                    var element = angular.element(event.currentTarget);
                    element.focus();
                }

                scope.inputAudioTimeout = $timeout(function () {
                    scope.ds.showTooltip = true;
                    scope.ds.focuss = true;
                    scope.tooltipIndex = tooltipIndex;
                    scope.ds.toolTipIndex = tooltipIndex;
                    angular.element('#load_audio').attr("src", utilityService.getAudioPath(scope.selectedActivity.path, src));
                    angular.element('#load_audio').attr("alt", scope.getFileName(src));
                    audio = angular.element('#load_audio');
                    audio[0].play();
                    $rootScope.$broadcast('reset-audio-focus', src);
                }, 10);
            };

            scope.$on('$destroy', function(){
                element.remove();
                questionWatcherInput();
                $timeout.cancel( scope.inputAudioTimeout );
            });
        }
    };
}]);
