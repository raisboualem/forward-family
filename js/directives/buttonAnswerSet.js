app.directive('buttonAnswerSet', function () {
    return {
        restrict: 'E',
        templateUrl: "templates/directives/buttonAnswerSet.html",
        controller: "answerSetController",
        link: function (scope, element, attr) {
            var questionHandlerButton = scope.$watch('question', function () {
                scope.drop_down_texts = scope.get_drop_down_text_array(scope.displayAsText(scope.question));
                scope.dropDownTextsCount = scope.get_drop_down_text_array_count(scope.drop_down_texts);
                scope.questionAnswerSet = scope.question.ANSWERSET;
            });

            scope.checkForDisable = function (answer) {
                if (answer.cssDisabled) {
                    return false;
                }

                return true;
            };

            scope.$on('$destroy', function(){
                if(scope.ds.sound){
                   scope.ds.sound.stop();
                }
                element.remove();
                questionHandlerButton();
            });
        }
    }
})
