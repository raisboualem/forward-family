app.directive('wordScramble', ['utilityService','dataService', function (utilityService, dataService) {
    return {
        restrict: 'E',
        templateUrl: "templates/directives/wordScramble.html",
        controller:"answerSetController",
        link: function(scope, element, attr) {

            scope.ds = dataService.getDataSource();

            if (Array.isArray(scope.question.ANSWERSET)) {
                var length = scope.question.ANSWERSET.length;
                for (var i = 0; i < length; i++) {
                    var answerSet = scope.question.ANSWERSET[i];

                    if (!answerSet.ANSWER.order) {
                        var order = utilityService.getRandomNumber(length);
                        answerSet.ANSWER.order = order;
                    }
                }

                scope.$emit('updateActivity');
            }

            scope.ds.dragStarted = false;
            var dragMoveHandler = scope.$on('draggable:move', function () {
                if (scope.ds.dragStarted == false) {
                    scope.$apply(function () {
                        scope.ds.dragStarted = true;
                    });
                }
            });

            var dragEndHandler = scope.$on('draggable:end', function (e) {
                if (scope.ds.dragStarted == true) {

                    scope.$apply(function () {
                        scope.ds.dragStarted = false;
                    });
                }
            });

            scope.$on('$destroy', function(){
               dragMoveHandler();
               dragEndHandler();
            });
        }
    };
}]);
