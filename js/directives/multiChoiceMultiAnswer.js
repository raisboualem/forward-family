app.directive('multiChoiceMultiAnswer',['utilityService',function(utilityService){
    return {
        restrict: 'E',
        controller:'answerSetController',
        templateUrl: "templates/directives/multiChoiceMultiAnswer.html",
        link: function(scope, element, attr) {
            var questionHandlerMultichoice = scope.$watch('question', function () {
                scope.drop_down_texts    = scope.get_drop_down_text_array(scope.displayAsText(scope.question));
            });

            scope.isCorrect = function (answer) {
                var result = false;
                if (answer && answer['@CORRECT'] == "TRUE" ||
                    answer['@correct'] == "TRUE")
                    result = true;

                return result;
            };

            scope.$on('$destroy', function(){
                element.remove();
                questionHandlerMultichoice();
            });

        }
    };
}]);

