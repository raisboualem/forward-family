app.directive('dropDownAnswerSet', ['$rootScope', '$timeout', function (rootScope, timeout) {
    return {
        restrict: 'E',
        templateUrl: "templates/directives/dropDownAnswerSet.html",
        controller: "answerSetController",
        link: function (scope, element, attr) {

            var questionHandlerDropdown = scope.$watch('question', function () {
                scope.drop_down_texts = scope.get_drop_down_text_array(scope.displayAsText(scope.question));
                scope.dropDownTextsCount = scope.get_drop_down_text_array_count(scope.drop_down_texts);
                scope.questionAnswerSet = scope.question.ANSWERSET;
            });

            scope.showTooltip = function (showTooltip, index) {
                scope.ds.showTooltip = showTooltip;
                scope.ds.index == index;
            };

            scope.setModel = function (answerSet, txt) {
                answerSet.value = txt;
            };

            scope.addColorText = function (quesText) {

                var regex = /<span\b[^>]*>(.*?)<\/span>/;

                var highlightedWord = quesText.match(regex);

                if (highlightedWord && highlightedWord.length > 1) {
                    quesText = quesText.replace(highlightedWord[0], '<span style="color: #0000D5;" class="innerspan">&nbsp;' + highlightedWord[1] + '</span>')

                    quesText = quesText.split(/ (?=[^>]*(?:< |$))/);
                }
                else {
                    quesText = quesText.split(' ');
                }

                return quesText;
            };

            scope.$on('$destroy', function(){
                questionHandlerDropdown();
            });

        }
    };
}]);