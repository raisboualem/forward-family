app.directive('menu', function () {
    return {
        restrict: "E",
        replace: true,
        scope: {
            collection: '='
        },
        template: "<div><ul class='menu' ng-show='isArray(collection)'>" +
        "<member ng-repeat='member in collection' member='member' is-root='true' section='member.SECTION'></member>" +
        "</ul>" +
        "<ul class='menu' ng-show='!isArray(collection)'>" +
        "<member member='collection' is-root='true' section='collection.SECTION'></member>" +
        "</ul></div>",
        link: function (scope, element, attrs) {
            scope.isArray = function (collection) {
                var result = Array.isArray(collection);
                return result;
            }
        }
    }
});

app.directive('collection', function () {
    return {
        restrict: "E",
        replace: true,
        scope: {
            collection: '=',
            section: '='
        },
        template: "<div><ul class='level-{{collection[0].level}}' ng-if='isArray(collection)'>" +
        "<member ng-repeat='member in collection' member='member' section='section'></member>" +
        "</ul>" +
        "<ul ng-if='!isArray(collection)' class='level-{{collection.level}}'>" +
        "<member member='collection' section='section'></member>" +
        "</ul></div>",
        link: function (scope, element, attrs) {
            scope.isArray = function (collection) {
                var result = Array.isArray(collection);
                return result;
            }
        }
    }
});

app.directive('member', ['$compile', 'dataService', function ($compile, dataService) {
    return {
        restrict: "E",
        replace: true,
        scope: {
            member: '=',
            isRoot: '=',
            section: '=',
        },
        template: "<li class='level-{{member.level}}'><a tabindex='-1' ng-href='' >" +
        "<div class='btn-link' tabindex='-1' ng-class='{\"rootBtn\":isRoot, \"linkTxtOnClick\":ds.makeLinkActive}' ng-keyup=\"makeActive()\" ng-mousedown=\"makeDeActive()\" ng-show='ds.language == \"en\"' style='user-select: none' >" +
        "<span class='linkTxt col-md-10 col-sm-10' ng-class='{\"linkTxtOnClick\":ds.makeLinkActive}' ng-keyup=\"makeActive()\" ng-mousedown=\"makeDeActive()\" tabindex='{{ds.language == \"en\"?0:-1}}' ng-click='itemSelected(section,member.NAV)'>{{member.TEXT.EN}}</span>"+
            "</div>" +
        "<div class='btn-link' tabindex='-1' ng-class='{\"rootBtn\":isRoot, \"linkTxtOnClick\": ds.makeLinkActive}' ng-keyup=\"makeActive()\" ng-mousedown=\"makeDeActive()\" ng-show='ds.language == \"fr\"' style='user-select: none' >" +
        "<span class='linkTxt col-md-10 col-sm-10' ng-class='{\"linkTxtOnClick\":ds.makeLinkActive}' ng-keyup=\"makeActive()\" ng-mousedown=\"makeDeActive()\" tabindex='{{ds.language == \"fr\"?0:-1}}' ng-click='itemSelected(section,member.NAV)' ng-bind-html='member.TEXT.FR | sanitize'></span>"+
            "</div>" +
        "<div class='btn-link' tabindex='-1' ng-class='{\"rootBtn\":isRoot, \"linkTxtOnClick\": ds.makeLinkActive}' ng-keyup=\"makeActive()\" ng-mousedown=\"makeDeActive()\" ng-show='ds.language == \"es\"' style='user-select: none' >" +
        "<span class='linkTxt col-md-10 col-sm-10' ng-class='{\"linkTxtOnClick\":ds.makeLinkActive}' ng-keyup=\"makeActive()\" ng-mousedown=\"makeDeActive()\" tabindex='{{ds.language == \"es\"?0:-1}}' ng-click='itemSelected(section,member.NAV)'>{{member.TEXT.ES}}</span>"+
            "</div>" +
        "<div class='btn-link' tabindex='-1' ng-class='{\"rootBtn\":isRoot, \"linkTxtOnClick\": ds.makeLinkActive}' ng-keyup=\"makeActive()\" ng-mousedown=\"makeDeActive()\" ng-show='ds.language == \"zh\"' style='user-select: none' >" +
        "<span class='linkTxt col-md-10 col-sm-10' ng-class='{\"linkTxtOnClick\":ds.makeLinkActive}' ng-keyup=\"makeActive()\" ng-mousedown=\"makeDeActive()\" tabindex='{{ds.language == \"zh\"?0:-1}}' ng-click='itemSelected(section,member.NAV)'>{{member.TEXT.ZH}}</span>"+
            "</div>" +
        "</a></li>",
        link: function (scope, element, attrs) {
            scope.ds = dataService.getDataSource();
            scope.ds.makeLinkActive = false;
            if (scope.member && scope.member.isCollapsed == undefined)
                scope.member.isCollapsed = true;

            scope.toggleCollapse = function () {
                scope.ds.makeLinkActive = false;
                scope.member.isCollapsed = !scope.member.isCollapsed;
                scope.$emit('menu-updated');
            };

            scope.makeActive = function () {
                scope.ds.makeLinkActive = true;
            };

            scope.makeDeActive = function () {
                scope.ds.makeLinkActive = false;
            };

            scope.itemSelected = function (section, nav) {
                scope.$emit('menu-item-selected', {section: section, nav: nav});
            };

            scope.isCollapsed = true;
            if (scope.member && scope.member.ITEMS && scope.member.ITEMS.ITEM) {
                element.append("<collection collapse='isRoot && member.isCollapsed' section='section' collection='member.ITEMS.ITEM'></collection>");
                $compile(element.contents())(scope)
            }
        }
    }
}]);

