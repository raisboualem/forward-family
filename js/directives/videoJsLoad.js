app.directive('videoJsLoad', ['$rootScope', '$timeout', function(rootScope, $timeout) {
	return {
		restrict: 'A',
		controller: "dialogueController",
		link: function(scope, element, attr) {
            element.ready(function(){
                  scope.myPlayer = videojs('my-video', { controls: true });
                  var sectionVideo = Array.isArray(scope.dialogue.SECTIONS.SECTION) ? scope.dialogue.SECTIONS.SECTION[0] : scope.dialogue.SECTIONS.SECTION;
                  videojs("my-video", { controls: true }).ready(function() {
                      scope.myPlayer = this; // Store the object on a variable
                      scope.myPlayer.src({type: 'video/mp4', src: scope.getAssetsPath(sectionVideo.CONTENT.VIDEO)});
                  })
            })

		}
	}
}]);

app.directive('videoJsLoadActivity', ['$rootScope', '$timeout', function(rootScope, $timeout) {
	return {
		restrict: 'A',
		controller: "questionsController",
		link: function(scope, element, attr) {
            element.ready(function(){
                 $timeout(function () {
                    scope.myPlayer = videojs('my-video', { controls: true });
                    videojs("my-video", { controls: true }).ready(function() {
                        scope.myPlayer = this;
                    })
                  }, 10);
            })

		}
	}
}]);