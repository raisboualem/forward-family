app.directive('dragDropVideo', ['dataService', 'utilityService', '$rootScope',
    function (dataService, utilityService, $rootScope) {
        return {
            restrict: 'E',
            templateUrl: "templates/directives/dragDropVideo.html",
            controller: 'answerSetController',
            link: function (scope, element, attr) {
                scope.ds = dataService.getDataSource();
                scope.$watch('question', function () {
                    scope.drop_down_texts = scope.get_drop_down_text_array(scope.displayAsText(scope.question));
                    scope.dropDownTextsCount = scope.get_drop_down_text_array_count(scope.drop_down_texts);
                    scope.questionAnswerSet = scope.question.ANSWERSET;
                });

                scope.dynamicPopover = {
                    templateUrl: 'videoDragDropPopover.html',
                };

                scope.ds.isPopoverOpen = false;
                scope.ds.dragStarted = false;

                var dragMoveHandler = scope.$on('draggable:move', function () {
                    if (scope.ds.dragStarted == false) {
                        scope.$apply(function () {
                            scope.ds.dragStarted = true;
                        });
                    }
                });

                var dragEndHandler = scope.$on('draggable:end', function (e) {
                    if (scope.ds.dragStarted == true) {
                            scope.$apply(function () {
                            scope.ds.dragStarted = false;
                        });
                    }
                });

                scope.setFocus = function (isFocus, index, showTooltip) {
                    if (index) {
                        scope.ds.index = index;
                        scope.ds.lastFocusedTxt = index;
                    }

                    scope.ds.showTooltip  = showTooltip;
                    scope.ds.isPopoverOpen = isFocus;
                };

                scope.getVideoPath = function (videoFile) {
                    return utilityService.getVideoPath(scope.selectedActivity.path, videoFile);
                };

                scope.hidetip = function () {
                    scope.ds.focuss = false;

                };

                scope.$on('$destroy', function(){
                   dragMoveHandler();
                   dragEndHandler();
                });
            }
        };
    }]);
