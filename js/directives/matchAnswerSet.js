app.directive('matchAnswerSet', [function () {
    return {
        restrict: 'E',
        controller: 'answerSetController',
        templateUrl: "templates/directives/matchAnswerSet.html",
        link: function (scope, element, attr) {
            var questionHandlerMatch = scope.$watch('question', function () {
                scope.drop_down_texts = scope.get_drop_down_text_array(scope.displayAsText(scope.question));
                scope.dropDownTextsCount = scope.get_drop_down_text_array_count(scope.drop_down_texts);
                scope.questionAnswerSet = scope.question.ANSWERSET;
            });

            scope.$on('$destroy', function(){
                element.remove();
                questionHandlerMatch();
            });
        }
    };
}]);