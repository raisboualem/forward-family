app.directive("question", ['$rootScope', '$sce', '$timeout', '$compile', 'dataService', 'localStorageService', 'utilityService', '$stateParams','ngAudio',
    function ($rootScope, $sce, $timeout, $compile, dataService, localStorageService, utilityService, $stateParams, ngAudio) {
        return {
            restrict: "E",
            scope: {
                selectedActivity: '=',
                activity: '=',
                currentSection: '=',
                currentPageNo: '=',
                controlType: '='
            },
            controller: function ($scope, $element, $attrs) {
            },
            templateUrl: "templates/directives/questions.html",
            link: function (scope, element, attr) {
                scope.moduleId = $stateParams.moduleId;
                scope.drop_down_single = "drop_down_single";
                scope.drag_drop_single = "drag_drop_single";
                scope.drag_and_drop = "drag_and_drop";
                scope.dragDropVideo = 'dragDropVideo';
                scope.showResult = false;
                scope.ds = dataService.getDataSource();
                scope.ds.showTooltip = false;
                scope.dragDropTop = 0;
                var questions = [];
                var dragDropAnswers;
                var mainContent;

                scope.matchClass =
                    (scope.selectedActivity.type == 'Match' && scope.currentSection.IMAGE) ? 'ParentImgDivMatch' : '';

                if (scope.selectedActivity.type == 'multichoicemultianswer') {
                    scope.MultiAnswerMultiC = "MultiAnswerMultiChoiceDiv";
                }

                var questionContainer = document.getElementById('questionContainer');
                if (questionContainer) {
                    questionContainer.onscroll = function () {
                        scope.$apply(function () {
                            if (dragDropAnswers) {
                                var height = dragDropAnswers.scrollHeight;
                                if (questionContainer.scrollTop <= questionContainer.scrollHeight) {
                                    scope.dragDropTop = questionContainer.scrollTop;

                                }
                            }
                            else {
                                scope.dragDropTop = 0;
                            }
                        });
                    };
                }
                ;

                if (scope.selectedActivity.type == 'DragAndDropVideo') {
                    var activity = scope.activity || scope.ds.resumeActivity;
                    var size = (activity.SECTIONS.SECTION.length / 3);
                    var start, end;

                    if (scope.currentPageNo <= size) {
                        start = 0;
                        end = size;
                    } else if (scope.currentPageNo <= 2 * size) {
                        start = size;
                        end = 2 * size;
                    } else {
                        start = 2 * size;
                        end = 3 * size;
                    }


                    for (var i = start; i < end; i++) {
                        var section = activity.SECTIONS.SECTION[i];
                        questions = questions.concat(section.QUESTIONS.QUESTION);
                    }
                }
                else if (scope.selectedActivity.shuffleAllAnswers) {
                    for (var i = 0; i < scope.activity.SECTIONS.SECTION.length; i++) {
                        var section = scope.activity.SECTIONS.SECTION[i];
                        questions = questions.concat(section.QUESTIONS.QUESTION);
                    }
                }
                else if (scope.currentSection) {
                    questions = scope.currentSection.QUESTIONS.QUESTION;
                }

                scope.shuffleDragDropAnswers = function (question, questionNo) {
                    if (question && question.ANSWERSET) {
                        if (Array.isArray(question.ANSWERSET) && !scope.selectedActivity.isMultiAnswer) {
                            for (var j = 0; j < question.ANSWERSET.length; j++) {
                                var answer = question.ANSWERSET[j].ANSWER;

                                if (!answer.order) {
                                    var order = utilityService.getRandomNumber(question.ANSWERSET.length);
                                    answer.order = order;
                                }

                                answer.isRemoved = answer.isRemoved ? answer.isRemoved : false;
                                scope.ds.shuffledAnswers.push({
                                    ANSWER: answer,
                                    INDEX: questionNo ? questionNo + '_' + j : j,
                                    questionAudio: {audio: question.AUDIO, audioCaption: question.AUDIOCAPTION}
                                });

                                scope.totalDragDropAnswers = scope.totalDragDropAnswers + 1;
                            }
                        } else if (Array.isArray(question.ANSWERSET) && scope.selectedActivity.isMultiAnswer) {
                                var length = question.ANSWERSET.length;
                                for (var i = 0; i < length; i++) {
                                    var answerSet = question.ANSWERSET[i].ANSWER;

                                    if (!answerSet.order) {
                                        var order = utilityService.getRandomNumber(length);
                                        answerSet.order = order;
                                        answerSet.INDEX = i;
                                    }
                                }
                        }
                        else {
                            var answer = question.ANSWERSET.ANSWER;

                            if (!answer.order) {
                                var order = utilityService.getRandomNumber(questions.length);
                                answer.order = order;
                            }

                            scope.ds.shuffledAnswers.push({
                                ANSWER: answer,
                                INDEX: questionNo,
                                questionAudio: {audio: question.AUDIO, audioCaption: question.AUDIOCAPTION}
                            });

                            answer.isRemoved = answer.isRemoved ? answer.isRemoved : false;
                            scope.totalDragDropAnswers = scope.totalDragDropAnswers + 1;
                        }

                        scope.$emit('updateActivity');
                    }
                }


                if (scope.selectedActivity.type == 'Match' ||
                    scope.selectedActivity.type == 'DragAndDrop' ||
                    scope.selectedActivity.type == 'DragAndDropVideo' ||
                    scope.selectedActivity.type == 'DragDropColumn' ||
                    scope.selectedActivity.type == 'GroupHeading' ||
                    scope.selectedActivity.type == 'GroupList' ||
                    scope.selectedActivity.shuffleAllAnswers) {

                    scope.dragDropAnswersRemoved = false;
                    scope.dragDropRemovedAnswers = 0;
                    scope.totalDragDropAnswers = 0;
                    scope.ds.shuffledAnswers = [];

                    if (Array.isArray(questions)) {
                        for (var i = 0; i < questions.length; i++) {
                            var question = questions[i];
                            scope.shuffleDragDropAnswers(question, i);
                        }
                    }
                    else {
                        scope.shuffleDragDropAnswers(questions);
                    }
                }


                scope.displayAsText = function (text) {

                    if (text === undefined)
                        return false;

                    if (typeof text !== 'string') {
                        txt = '';

                        angular.forEach(text.TEXT, function (v, k) {
                            txt += v;
                        });

                        text.TEXT = txt;
                    }

                    if ((/\.(gif|jpg|jpeg|tiff|png)$/i).test(text.TEXT) === true) {
                        return '<img src="' + "data/assets/" + scope.$parent.selectedActivity.path + '/image/' + text.TEXT + '" class="q-img" alt="Learning Image">';
                    } else if ((/\.(mp3)$/i).test(text.TEXT) === true) {
                        return '<audio src="' + scope.$parent.selectedActivity.path + '/audio/' + text.TEXT + '" controls="controls">';
                    } else if (text.TEXT === '{0}') {
                        return '';
                    } else {
                        return text.TEXT;
                    }
                };

                scope.checkForArray = function (answerSet) {
                    return Array.isArray(answerSet);
                }

                scope.htmlDecode = function (input) {
                    var e = document.createElement('div');
                    e.innerHTML = input;
                    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
                }

                scope.getControlType = function (question) {

                    var controlType = undefined;

                    if (scope.selectedActivity.meta == 'DialogQuiz')
                        return 'button';

                    if (question !== undefined && scope.currentSection['@controlType'] == undefined) {
                        if (question.ANSWERSET !== undefined && question.ANSWERSET.length > 1) {
                            controlType = question.ANSWERSET[0]['@controlType'];
                        } else if (question.ANSWERSET !== undefined && question.ANSWERSET['@controlType'] !== undefined) {
                            controlType = question.ANSWERSET['@controlType'];
                        }
                    }

                    if (controlType == undefined) {
                        controlType = scope.selectedActivity.type;
                    }

                    if (scope.currentSection &&
                        scope.currentSection['@controlType'] !== undefined) {
                        controlType = scope.currentSection['@controlType'];
                    }

                    return controlType.toLowerCase();
                };


                scope.tooltip = function (objCtrl, correctAnswer) {

                    scope.questionTooltipTimeout = setTimeout(function () {
                        var unique_id = $.gritter.add({
                            text: correctAnswer,
                            // (bool | optional) if you want it to fade out on its own or just sit there
                            sticky: true,
                            // (int | optional) the time you want it to be alive for before fading out
                            time: '',
                            // (string | optional) the class name you want to apply to that specific message
                            class_name: 'my-sticky-class',
                            // HTML Control id
                            id: objCtrl
                        });

                        // You can have it return a unique id, this can be used to manually remove it later using
                        scope.questionTooltipFadeTimeout = setTimeout(function () {
                            $.gritter.remove(unique_id, {
                                fade: true,
                                speed: 'medium',
                                id: objCtrl
                            });
                        }, 5000);
                    }, 100);
                };

                scope.getAssetsPath = function (audioName) {
                    if (audioName['#text'] !== undefined) {
                        audioName = audioName['#text'];
                    }

                    utilityService.getAudioPath(scope.selectedActivity.path, audioName);
                };

                var updateDataHandlerQuestion = scope.$on('updateData', function (event, data) {
                    //Stop playing audio
                    if (scope.selectedActivity.stopAudio != false)
                        scope.stopAudio();

                    if (data.dragDropAnswerRemoved) {
                        scope.dragDropRemovedAnswers = scope.dragDropRemovedAnswers + 1;

                        if (scope.totalDragDropAnswers == scope.dragDropRemovedAnswers) {
                            scope.dragDropAnswersRemoved = true;
                        }
                    }

                });

                scope.getVideoPath = function (videoFile) {
                    return utilityService.getVideoPath(scope.selectedActivity.path, videoFile);
                };


                scope.getFileName = function (filePath) {
                    if (filePath) {
                        var temp = filePath.split('/');
                        filePath = temp[temp.length - 1];
                    }

                    return filePath;
                };

                scope.play_audio = function (src, tooltipIndex, event) {
                    $timeout.cancel(scope.ds.cancelTimeout);

                    if (event) {
                        var element = angular.element(event.currentTarget);
                        element.focus();
                    }

                    scope.questionAudioTimeout = $timeout(function () {
                        scope.ds.showTooltip = true;
                        scope.ds.showTooltipBtn = true;
                        scope.ds.focuss = true;
                        scope.tooltipIndex = tooltipIndex;
                        scope.ds.toolTipIndex = tooltipIndex;
                        scope.ds.sound = ngAudio.load(utilityService.getAudioPath(scope.selectedActivity.path, src));
                        scope.ds.sound.play();
                    }, 10);
                };

                scope.hidetip = function () {
                    if (scope.selectedActivity.type != 'FillInTheBlank' && scope.selectedActivity.type != 'Match') {
                        scope.ds.focuss = false;
                    }
                    var question = Array.isArray(scope.currentSection.QUESTIONS.QUESTION) ? scope.currentSection.QUESTIONS.QUESTION[0] : scope.currentSection.QUESTIONS.QUESTION;
                    if (question.ANSWERSET['@controlType'] == 'DROPDOWN' ||
                        question.ANSWERSET['@controlType'] == 'BUTTON' ||
                        question.ANSWERSET[0] && question.ANSWERSET[0]['@controlType'] == 'DROPDOWN' ||
                        question.ANSWERSET[0] && question.ANSWERSET[0]['@controlType'] == 'BUTTON') {
                        scope.ds.focuss = false;
                    }
                };
                scope.hidetipinverse = function () {
                    scope.ds.focuss = false;
                };

                scope.stopAudio = function () {
                    var audio = angular.element('#load_audio');

                    if (!audio[0].paused) {
                        audio[0].pause();
                        audio[0].currentTime = 0;
                    }
                };

                scope.getImageAssetsPath = function (imageName) {
                    return utilityService.getImagePath(scope.selectedActivity.path, imageName);
                };

                scope.chk_is_array = function (arr) {
                    return Array.isArray(arr);
                };

                scope.chk_is_array_Answerset = function () {
                    var sectionLength = scope.activity && scope.activity.SECTIONS ?scope.activity.SECTIONS.SECTION.length:0;
                    var isarray = [];
                    for (var i = 0; i < sectionLength; i++) {
                        for (var j = 0; j < scope.currentSection.QUESTIONS.QUESTION.length; j++) {
                           isarray.push(Array.isArray(scope.currentSection.QUESTIONS.QUESTION[j].ANSWERSET));
                        }
                        if(isarray.length != 0){
                            if (isarray.indexOf(true) > -1)
                                return false;
                            else
                                return true;
                        }else if (Array.isArray(scope.currentSection.QUESTIONS.QUESTION.ANSWERSET)){
                            return false;
                        }
                    }
                };

                scope.getImageCaptionsFromLang = function(imgCaptions) {
                    if(imgCaptions){
                        var captionText = '';
                        var icap = imgCaptions.IMAGECAPTION;

                        if(Array.isArray(icap)) {
                            var text = icap.filter(function(value){
                                return value['@lang'] == scope.ds.ispeaklanguage;
                            });
                            return text[0].TEXT;
                        } else {
                             return icap.TEXT;
                         }
                    }
                };

                scope.getQuestionNumber = function (index) {
                    dragDropAnswers = document.getElementById('dragDropAnswers');
                    mainContent = document.getElementById('mainContent');

                    index = index ? index : 0;
                    var questionNo = 0;
                    var pageNo = parseInt(scope.currentPageNo);
                    var activity = scope.activity ? scope.activity : scope.ds.resumeActivity;

                    if (activity) {
                        var sections = activity.SECTIONS.SECTION;

                        for (var i = 0; i < pageNo - 1; i++) {
                            var section = sections[i];

                            if (Array.isArray(section.QUESTIONS.QUESTION))
                                questionNo = questionNo + section.QUESTIONS.QUESTION.length;
                            else
                                questionNo = questionNo + 1;
                        }

                        questionNo = questionNo + index + 1;
                    }

                    return questionNo;
                };

                scope.get_correct_answer = function (answer) {
                    return answer['@CORRECT'] != undefined ? answer['@CORRECT'] : answer['@correct'];
                };

                scope.isArray = function (obj) {
                    return Array.isArray(obj);
                };


                scope.$emit('directive-rendered');

                scope.$on('$destroy',function(){
                    updateDataHandlerQuestion();
                    $timeout.cancel( scope.questionAudioTimeout );
                    $timeout.cancel( scope.questionTooltipTimeout );
                    $timeout.cancel( scope.questionTooltipFadeTimeout );
                });
            }
        }
    }]);