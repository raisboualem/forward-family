app.directive('listenAndRepeatAnswerSet', ['utilityService', function (utility) {
    return {
        restrict: 'E',
        controller: 'answerSetController',
        templateUrl: "templates/directives/listenAndRepeatAnswerSet.html",
        link: function (scope, element, attr) {
            scope.playAudio = function (src) {
                angular.element('#repeat_load_audio').attr("src", scope.getAssetsPath(src));
                audio = angular.element('#repeat_load_audio');
                audio[0].play();
            };

            scope.selectQuestion = function (question) {
                if (question) {
                    if (question.AUDIO) {
                        scope.playAudio(question.AUDIO);
                    }

                    scope.ds.selectedText = question.TEXT;

                    if (question.IMAGE)
                        scope.ds.selectedImage = scope.getImagePath(question.IMAGE);

                    if (!question.attended) {
                        scope.ds.attendedTotalQuestions += 1;
                        scope.ds.answeredQuestions += 1;
                        scope.ds.correctQuestions += 1;
                        scope.ds.totalScore += 1;
                        question.attended = true;
                    }

                    var questionInteractionInfo = {
                        'id': scope.questionNo,
                        'questionText': scope.displayAsText(scope.question),
                        'questionType': scope.controlType,
                        'learnerResponse': question.TEXT,
                        'correctAnswer': question.TEXT,
                        'wasCorrect': true,
                        'showAlert': false,
                        'dragDropAnswerRemoved': false,
                    };

                    scope.$emit('updateData', questionInteractionInfo);
                }
            }

            scope.getImagePath = function (image) {
                return utility.getImagePath(scope.selectedActivity.path, image);
            };
        }
    };
}]);