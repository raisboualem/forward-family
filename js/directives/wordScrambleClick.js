app.directive('wordScrambleClick', ['$rootScope', 'utilityService', 'dataService', function ($rootScope, utilityService, dataService) {
    return {
        restrict: 'E',
        templateUrl: "templates/directives/wordScrambleClick.html",
        controller: "answerSetController",
        link: function (scope, element, attr) {
            scope.audiocontrol = $rootScope.audioCaptionControl;
            scope.ds = dataService.getDataSource();

            if (scope.question['@maxAttempts'] == undefined)
                scope.question['@maxAttempts'] = scope.currentSection['@maxAttempts']?scope.currentSection['@maxAttempts']:5;

            var correctAnswers = 0;
            if (Array.isArray(scope.question.ANSWERSET)) {
                var length = scope.question.ANSWERSET.length;
                for (var i = 0; i < length; i++) {
                    var answerSet = scope.question.ANSWERSET[i];

                    if (!answerSet.ANSWER.order) {
                        var order = utilityService.getRandomNumber(length);
                        answerSet.ANSWER.order = order;
                        answerSet.INDEX = i;
                    }
                }

                scope.$emit('updateActivity');
            }

            var wordScrambleClickQuestionHandler = scope.$watch('question', function () {
                if (scope.question.TEXT)
                    scope.targetPresentInQuesText = scope.question.TEXT.match(/\{\d+}/g) ? true : false;
                scope.wordScramble_texts = scope.get_drop_down_text_array(scope.displayAsText(scope.question));
                scope.wordScrambleTextsCount = scope.get_drop_down_text_array_count(scope.wordScramble_texts);
                scope.questionAnswerSet = scope.question.ANSWERSET;
            });

            scope.wordClicked = function (answer) {
                var answerSet = scope.question.ANSWERSET[correctAnswers];
                var data = answer.TEXT;
                if (scope.question.ANSWERSET.result || scope.question['@maxAttempts'] == 0)
                    return false;

                data = data.replace('\n', '').trim();
                answerSet.showResult = false;
                scope.validate(data, answerSet, answerSet['@maxAttempts'], null, null, scope.question.ANSWERSET);

                if (answerSet.result) {
                    answerSet.value = data;
                    answer.isDisabled = true;
                    correctAnswers += 1;
                } else {
                    scope.question['@maxAttempts'] = parseInt(scope.question['@maxAttempts']) - 1;

                    if (scope.question['@maxAttempts'] == 0) {
                        scope.question.disabled = true;
                        scope.question.result = false;

                        if (scope.ds.communicativeSkill == "quiz")
                            scope.question.showResult = scope.showFeedbackEOM();
                        else
                            scope.question.showResult = true;

                        scope.ds.attendedTotalQuestions = parseInt(scope.ds.attendedTotalQuestions) + 1;
                        scope.$emit('updateActivity');
                    }
                }

                scope.$emit('updateActivity');
            };

            scope.hidetip = function () {
                scope.ds.focuss = false;
            };

            scope.$on('$destroy', function(){
                wordScrambleClickQuestionHandler();
            });
        }
    };
}]);
