app.directive('html5Audio', function () {
    return {
        restrict: 'E',
        scope: {
            audioSrc: '=',
            play: '=playAudio'
        },
        templateUrl: "templates/directives/html5Audio.html",
        link: function (scope, element, attr) {
            scope.play = function () {
                if (audio) {
                    scope.play = function () {
                        element.play();
                    }
                } else {
                    console.log("Your browser does not support the audio element.");
                }
            }
        }
    };
});