app.directive('groupList', ['utilityService', '$timeout', 'dataService', function (utilityService, timeout, dataService) {
    return {
        restrict: 'E',
        templateUrl: "templates/directives/groupList.html",
        controller: "answerSetController",
        link: function (scope, element, attr) {
            scope.ds = dataService.getDataSource();

            scope.ds.isPopoverOpen = false;

            scope.dynamicPopover = {
                templateUrl: 'groupListPopover.html',
            };

            var wrongAnswerHandler = scope.$on('wrongAnswer', function (event, data) {
                scope.ds.showWrongAnswerTooltip = true;
                scope.ds.itemIndex = data;
            });

            scope.setFocus = function (isFocus, index) {
                if (index != undefined) {
                    scope.ds.index = index;
                    scope.ds.lastFocusedTxt = index;
                }

                scope.ds.isPopoverOpen = isFocus;
                scope.ds.showWrongAnswerTooltip = false;
            };

            scope.focusNextHeader = function (index, itemLength, event) {
                var nextAudioItem = document.getElementById('grpItemAudio_' + (index + 1));
                var audioItem = document.getElementById(scope.ds.index + 1);
                var descriptionBtn = document.getElementById('goToDescriptionBtn');
                var itemFound = false;

                for (var i = index; i < itemLength; i++) {
                    var nextAudioItem = document.getElementById('grpItemAudio_' + (index + 1));

                    if (nextAudioItem) {
                        nextAudioItem.focus();
                        itemFound = true;
                        break;
                    }
                }

                if (!itemFound) {
                   var findNextItem = event.target.nextElementSibling;
                    if (findNextItem) {
                        var nextAudioItem = findNextItem.children[0].id;
                        var nextAudioItem = document.getElementById(nextAudioItem);
                        if (nextAudioItem)
                           nextAudioItem.focus();
                    }
                    else {
                        audioItem.focus();
                    }
                }
            };

            var resetFocus = scope.$on('reset-focus', function (event, data) {
                if (scope.ds.index == scope.ds.lastFocusedTxt) {
                    var element = angular.element(document.getElementById(scope.ds.index));
                    element.focus();
                }
            });

            scope.isAudio = function (fileName) {
                var pattern = new RegExp(".mp3");
                var result = pattern.test(fileName);
                return result;
            };

            scope.hideTooltip = function () {
                scope.ds.cancelTimeout = timeout(function () {
                    scope.ds.showGrpTooltip = false;
                    scope.ds.grpTooltipIndex = null;
                }, 2000);
            };

            scope.play_audio = function (src, tooltipIndex, $event) {

                timeout.cancel(scope.ds.cancelTimeout);

                scope.ds.grpTooltipIndex = tooltipIndex;
                scope.ds.showGrpTooltip = true;
                scope.ds.Grpfocuss = true;
                angular.element(".tooltip-audio").css({visibility: 'visible'});
                angular.element('#load_audio').attr("src",
                    utilityService.getAudioPath(scope.selectedActivity.path, src));

                angular.element('#load_audio').attr("alt", scope.getFileName(src));
                audio = angular.element('#load_audio');
                audio[0].play();

                $event.stopPropagation();
            };

            scope.hidetip = function () {
                scope.ds.Grpfocuss = false;
            };

            scope.$on('$destroy', function(){
                resetFocus();
                wrongAnswerHandler();
            });
        }
    };
}]);
