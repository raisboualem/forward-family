app.directive('multiAnswerDragDrop', ['dataService', 'utilityService',
    function (dataService, utilityService) {
        return {
            restrict: 'E',
            templateUrl: "templates/directives/multiAnswerDragDrop.html",
            controller: 'answerSetController',
            link: function (scope, element, attr) {
                scope.ds = dataService.getDataSource();
                scope.cnt = 0;

                var questionHandlerMultiAnswer = scope.$watch('question', function () {
                    scope.drop_down_texts = scope.get_drop_down_text_array(scope.displayAsText(scope.question));
                    scope.dropDownTextsCount = scope.get_drop_down_text_array_count(scope.drop_down_texts);

                    scope.questionAnswerSet =[];
                    for(var i=0;i<scope.question.ANSWERSET.length;i++){
                        if(scope.question.ANSWERSET[i].ANSWER['@CORRECT']){
                            scope.questionAnswerSet.push(scope.question.ANSWERSET[i]);
                        }
                    }
                });

                scope.ds.dragStarted = false;

                var dragMoveHandler = scope.$on('draggable:move', function () {
                    if (scope.ds.dragStarted == false) {
                        scope.$apply(function () {
                            scope.ds.dragStarted = true;
                        });
                    }
                });

                var dragEndHandler = scope.$on('draggable:end', function (e) {
                    if (scope.ds.dragStarted == true) {

                        scope.$apply(function () {
                            scope.ds.dragStarted = false;
                        });
                    }
                });

                scope.getVideoPath = function (videoFile) {
                    return utilityService.getVideoPath(scope.selectedActivity.path, videoFile);
                };

                scope.$on('$destroy', function(){
                    element.remove();
                    questionHandlerMultiAnswer();
                    dragMoveHandler();
                    dragEndHandler();
                });
            }
        };
    }]);
