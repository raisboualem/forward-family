(function() {
    'use strict';
    angular
        .module('quizApp')
        .directive('drDraggable', drDraggable);
    // function for drag and drop
    function drDraggable() {
        return {
            restrict: 'E',
            scope: {
                question: "@"
            },
            require: 'ngModel',
            link: function(scope, elem, attr, ngModel) {
                var e = elem[0];

                e.addEventListener('drop', function(ev) {
                    drop(ev);
                });

                elem.bind('dragover', function(ev) {
                    ev.preventDefault();
                });

                function drop(ev) {
                    ev.preventDefault();

                    if (!angular.element(ev.target).hasClass('btn')) {
                        var data = ev.dataTransfer.getData("text");
                        var ans = angular.element(document.getElementById(data)).text().trim();

                        ngModel.$setViewValue(ans);
                        ev.target.parentNode.replaceChild(document.getElementById(data), ev.target);
                        document.getElementById(data).classname = "";
                    };
                };
            },
            template: '<div class="items"></div>'
        };
    };
})();
