/*
This directive allows us to remove focus from element.
*/
app.directive('ngBlurs', function () {
    return function (scope, element, attrs) {
        element.bind('click', function (event) {
            scope.$apply(function (){
                scope.$eval(attrs.ngBlurs);
            });
        });
    };
});