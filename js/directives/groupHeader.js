app.directive('groupHeader',['dataService',function(dataService){
    return {
        restrict: 'E',
        templateUrl: "templates/directives/groupHeader.html",
        controller:"answerSetController",
        link: function(scope, element, attr) {
            scope.ds = dataService.getDataSource();

            scope.ds.dragStarted = false;

            scope.ds.isPopoverOpen = false;

            scope.dynamicPopover = {
                templateUrl: 'groupHeadingPopover.html',
            };

            scope.setFocus = function (isFocus, index) {
                if (index != undefined) {
                    scope.ds.index = index;
                    scope.ds.lastFocusedTxt = index;
                }

                scope.ds.isPopoverOpen = isFocus;
                scope.ds.showWrongAnswerTooltip = false;
            };

            var wrongAnswerHandler = scope.$on('wrongAnswer', function (event, data) {
                scope.ds.showWrongAnswerTooltip = true;
                scope.ds.itemIndex = data;
            });

            scope.getPopupHeight = function(){
                var element =  angular.element(document.getElementsByClassName('popover'));
                var height = scope.ds.height - element[0].offsetTop;
                return height;
            };

            var resetFocusHandler = scope.$on('reset-focus', function (event, data) {
                if (scope.ds.index == scope.ds.lastFocusedTxt){
                    var element =  angular.element(document.getElementById(scope.ds.index));
                    element.focus();
                }
            });

            var dragMoveHandler = scope.$on('draggable:move', function () {
                if (scope.ds.dragStarted == false) {
                    scope.$apply(function () {
                        scope.ds.dragStarted = true;
                    });
                }
            });

            var dragEndHandler = scope.$on('draggable:end', function (e) {
                if (scope.ds.dragStarted == true) {

                    scope.$apply(function () {
                        scope.ds.dragStarted = false;
                    });
                }
            });

            scope.$on('$destroy', function(){
                element.remove();
                wrongAnswerHandler();
                resetFocusHandler();
                dragMoveHandler();
                dragEndHandler();
            });
        }
    };
}]);
