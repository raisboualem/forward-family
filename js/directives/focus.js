/**
 * Created by swapnil on 15-09-17.
 */
app.directive('focus', function () {
    return {
        restrict: 'A',
        link: function ($scope, elem, attrs) {

            elem.bind('keydown', function (e) {
                var code = e.keyCode || e.which;
                if (code === 13) {
                    var splittedId = elem[0].id.split('_');
                    if (splittedId.length == 3) {
                        var questionId = parseInt(splittedId[1]);
                        var answerId = parseInt(splittedId[2]);
                        var nextElem = document.getElementById(splittedId[0] + '_' + questionId + '_' + (answerId + 1)); //Adjacent text box in same question
                        if (!nextElem) {
                            nextElem = document.getElementById(splittedId[0] + '_' + (questionId + 1) + '_0');//Multianswer next question first textbox

                            if (!nextElem) {
                                nextElem = document.getElementById(splittedId[0] + '_' + (questionId + 1));//Single Answer next textbox
                            }
                        }

                        if (nextElem) {
                            setTimeout(function () {
                                var result = nextElem.focus();
                                if (nextElem.select)
                                    nextElem.select();
                            }, 1);
                        }
                    } else if (elem[0].id.split('_').length == 2) {
                        var id = parseInt(elem[0].id.split('_')[1]);
                        var nextId = id + 1;
                        var nextElem = document.getElementById(splittedId[0] + '_' + nextId);// next question single textbox

                        if (!nextElem) {
                            nextElem = document.getElementById(splittedId[0] + '_' + nextId + '_0');//Multianswer next question first textbox
                        }

                        if (nextElem) {
                            setTimeout(function () {
                                var result = nextElem.focus();
                                nextElem.select();
                            }, 1);

                        }
                    }
                }
            });
        }
    }
});