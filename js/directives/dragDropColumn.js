app.directive('dragDropColumn', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
    return {
        restrict: 'E',
        templateUrl: "templates/directives/dragDropColumn.html",
        controller: "answerSetController",
        link: function (scope, element, attr) {
            scope.setFocus = function (isFocus, index, showTooltip) {
                if (index) {
                    scope.ds.index = index;
                    scope.ds.lastFocusedTxt = index;
                }

                scope.ds.showTooltip  = showTooltip;
                scope.ds.isPopoverOpen = isFocus;
            };

            var dragMoveHandler = scope.$on('draggable:move', function () {
                if (scope.dragStarted == false) {
                    scope.$apply(function () {
                        scope.dragStarted = true;
                    });
                }
            });

            var dragEndHandler = scope.$on('draggable:end', function (e) {
                 $timeout(function () {
                     if (scope.dragStarted == true) {
                         scope.$apply(function () {
                             scope.dragStarted = false;
                         });
                     }
                 }, 100);
             });

             scope.$on('$destroy', function(){
                 element.remove();
               dragMoveHandler();
               dragEndHandler();
            });

            scope.init = function () {
                scope.popoverTpl = "dragDropColumnPopover.html";
                scope.dragStarted = false;

                if (scope.question.TEXT) {
                    var regExGlobal = /<COLUMN>(.*?)<\/COLUMN>/g;
                    var regEx = /<COLUMN>(.*?)<\/COLUMN>/;

                    var columnTxt = scope.question.TEXT.match(regExGlobal);
                    scope.questionTxt = [];

                    for (var i = 0; i < columnTxt.length; i++) {
                        var txt = columnTxt[i].match(regEx)[1];
                        scope.questionTxt[i] = scope.get_drop_down_text_array(txt);
                    }
                }
            }();
        }
    }
}
]);
