app.directive('singleDragDropAnswerSet', ['utilityService', function (utilityService) {
    return {
        restrict: 'E',
        controller: 'answerSetController',
        templateUrl: "templates/directives/singleDragDropAnswerSet.html",
        link: function (scope, element, attr) {
            scope.$watch('question', function () {
                scope.drop_down_texts = scope.get_drop_down_text_array(scope.displayAsText(scope.question));
                scope.dropDownTextsCount = scope.get_drop_down_text_array_count(scope.drop_down_texts);

                if (Array.isArray(scope.question.ANSWERSET)) {
                    var length = scope.question.ANSWERSET.length;
                    for (var i = 0; i < length; i++) {
                        var answerSet = scope.question.ANSWERSET[i];

                        if (!answerSet.ANSWER.order) {
                            var order = utilityService.getRandomNumber(length);
                            answerSet.ANSWER.order = order;
                        }
                    }

                    scope.$emit('updateActivity');
                }
            });

            scope.dynamicPopover = {
                templateUrl: 'singleDragDropPopover.html',
            };

            scope.ds.isPopoverOpen = false;

            scope.setFocus = function (isFocus, index) {
                if (index != undefined) {
                    scope.ds.index = index;
                    scope.ds.lastFocusedTxt = index;
                }

                scope.ds.isPopoverOpen = isFocus;
            };

            scope.dragStarted = false;

            var dragMoveHandler = scope.$on('draggable:move', function () {
                if (scope.dragStarted == false) {
                    scope.$apply(function () {
                        scope.dragStarted = true;
                    });
                }
            });

            var dragEndHandler = scope.$on('draggable:end', function () {
                if (scope.dragStarted == true) {
                    scope.$apply(function () {
                        scope.dragStarted = false;
                    });
                }
            });

            var resetFocusHandler = scope.$on('reset-focus', function (event, data) {
                if (scope.ds.index == scope.ds.lastFocusedTxt){
                    var element =  angular.element(document.getElementById(scope.ds.index));
                    element.focus();
                }
            });

            scope.hidetip = function () {
                scope.ds.focuss = false;

            };

             scope.$on('$destroy', function(){
                dragMoveHandler();
                dragEndHandler();
                resetFocusHandler();
             });
        }
    };
}]);
