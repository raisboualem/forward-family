app.directive('accent', ['dataService', '$parse', '$timeout',function (dataService, $parse, $timeout) {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function ($scope, elem, attrs, ngModel) {
            if (!ngModel) return; // do nothing if no ng-model

            $scope.ds = dataService.getDataSource();

            var isFocused = false;
            elem.bind('focus', function (e) {
                $scope.ds.lastFocusedTxt = attrs.id;
                isFocused = true;
            });
            elem.bind('blur', function (e) {
                isFocused = false;
            });

            $scope.$on('reset-focus', function (event, data) {
                var isDisabled =  elem.hasClass('disabled');
                if (attrs.id == $scope.ds.lastFocusedTxt && attrs.disabled!= true && !isDisabled){
                    elem.focus();
                }else{
                 var unansweredInput = $( "input" ).not( ".disabled" )
                    if (unansweredInput)
                       unansweredInput[0].focus();
                }
            });

            $scope.$on('reset-audio-focus', function (event, data) {
               var isDisabled =  elem.hasClass('disabled');
               var singleInput =  elem.hasClass('fillInput');
               if(singleInput) {
                  if ($scope.question.AUDIO == data && !isDisabled){
                      elem.focus();
                  }
               }
               else {
                  if ($scope.question.AUDIO == data && !isDisabled && $scope.$first){
                      elem.focus();
                  }
               }
            });

            $scope.$on('add-french-accent', function (event, data) {
                $timeout(function(){
                    if (isFocused  && attrs.readonly!= true) {
                        var modelGetter = $parse(attrs['ngModel']);

                        // This returns a function that lets us set the value of the ng-model binding expression:
                        var modelSetter = modelGetter.assign;

                        if (ngModel.$viewValue) {
                            data = ngModel.$viewValue + data;
                        }
                        // This is how you can use it to set the value 'bar' on the given scope.
                        modelSetter($scope, data);
                    }
                },100);
            });
        }
    }
}]);