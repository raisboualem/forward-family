app.directive("answerSet", function ($compile, $http, dataService) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            controlType: '=',
            currentSection: '=',
            selectedActivity: '=',
            question: '=',
            questionNo: '='
        },
        controller: 'answerSetController',
        templateUrl: "templates/directives/answerSet.html",
        link: function (scope, element, attrs) {
        }
    }
});