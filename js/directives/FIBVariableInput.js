app.directive('fillInVariableInput', ['$rootScope', 'utilityService', 'dataService', function ($rootScope, utilityService, dataService) {
    return {
        restrict: 'E',
        templateUrl: "templates/directives/FIBVariableInput.html",
        controller: "answerSetController",
        link: function (scope, element, attr) {
            scope.cnt = 0;

            var questionHandlerVariableInput = scope.$watch('question', function () {
                scope.txtAnswerSets = scope.parseAnswersets(scope.question);
            });

            scope.setModel = function (answerSet, txt) {
                answerSet.value = txt;
            };

            scope.getAnswerSetIndex = function (questionNo, parentIndex, index) {
                var answerSetIndex = 0;

                for (var i = 0; i < parentIndex; i++) {
                    answerSetIndex += scope.txtAnswerSets[i][1] ? scope.txtAnswerSets[i][1].length : 0;
                }

                answerSetIndex += index;

                return answerSetIndex;
            };

            scope.parseAnswersets = function (question) {
                var answerSets = [];
                if (question) {
                    var questionTxt = question.TEXT;
                    var txtArray = questionTxt.split(/\{\d+}/g);

                    for (var i = 0; i < txtArray.length; i++) {
                        if (txtArray[i].trim() != '') {
                            if (question.ANSWERSET[i])
                                answerSets.push([txtArray[i], [question.ANSWERSET[i]]]);
                            else
                                answerSets.push([txtArray[i]]);
                        } else if (question.ANSWERSET[i]) {
                            if (i > 0)
                                answerSets[answerSets.length - 1][1].push(question.ANSWERSET[i]);
                            else
                                answerSets.push(['', [question.ANSWERSET[i]]]);
                        }
                    }
                }

                return answerSets;
            };

            scope.getMaxCharsFromAnswerSet = function (answers, maxCharAnswer) {
                var maxChar = 0;

                if (answers) {
                    if (Array.isArray(answers)) {
                        for (var i = 0; i < answers.length; i++) {
                            var answer = answers[i];
                            maxCharAnswer = answer.TEXT.length > maxChar ? i : maxCharAnswer;
                            maxChar = answers[maxCharAnswer].TEXT.length;
                        }
                    } else {
                        maxCharAnswer = answers.TEXT.length > maxChar ? 0 : maxCharAnswer;
                    }
                }

                return maxCharAnswer;
            };

            scope.checkForLineBreak = function (txt, index) {
                if (txt) {
                    return txt.indexOf('<br/>') != -1 && index == 0;
                }
            };

            scope.$on('$destroy', function(){
                questionHandlerVariableInput();
            });
        }
    };
}]);
