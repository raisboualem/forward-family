app.directive('multipleChoiceAnswerSet', ['utilityService', 'dataService', '$timeout',
    function (utilityService, dataService, $timeout) {
        return {
            restrict: 'E',
            controller: 'answerSetController',
            templateUrl: "templates/directives/multipleChoiceAnswerSet.html",
            link: function (scope, element, attr) {

                scope.ds = dataService.getDataSource();

                var questionHandlerMulti = scope.$watch('question', function () {
                    scope.drop_down_texts = scope.get_drop_down_text_array(scope.displayAsText(scope.question));
                    scope.dropDownTextsCount = scope.get_drop_down_text_array_count(scope.drop_down_texts);

                    if (scope.question)
                        scope.questionAnswerSet = scope.question.ANSWERSET;
                });

                scope.isCorrect = function (answer) {
                    var result = false;
                    if (answer && answer['@CORRECT'] == "TRUE" ||
                        answer['@correct'] == "TRUE")
                        result = true;
						//console.log(answer['@CORRECT']);
                    return result;
                };

                scope.hideTooltip = function () {
                    scope.ds.cancelTimeout = $timeout(function () {
                        scope.ds.showTooltips = false;
                        scope.ds.tooltipIndexs = null;
                        scope.ds.Multifocuss = false;
                    }, 2000);

                };

                scope.play_audio = function (audio, event, tooltipIndex) {
                    $timeout.cancel(scope.ds.cancelTimeout);
                    scope.ds.showTooltips = true;
                    scope.ds.Multifocuss = true;
                    scope.ds.tooltipIndexs = tooltipIndex;
                    angular.element('#mulitpleChoiceAudio').attr("src",
                        utilityService.getAudioPath(scope.selectedActivity.path, audio));

                    angular.element('#mulitpleChoiceAudio').attr("alt", scope.getFileName(audio));
                    audio = angular.element('#mulitpleChoiceAudio');
                    audio[0].play();
                    event.preventDefault();
                };

                scope.hidetip = function () {
                    scope.ds.Multifocuss = false;
                };

                scope.$on('$destroy', function(){
                    element.remove();
                    questionHandlerMulti();
                });
            }
        };
    }]);

