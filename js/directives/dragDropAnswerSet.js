app.directive('dragDropAnswerSet', ['dataService', 'utilityService', '$timeout', '$window',
    function (dataService, utilityService, $timeout, $window) {
        return {
            restrict: 'E',
            templateUrl: "templates/directives/dragDropAnswerSet.html",
            controller: 'answerSetController',
            link: function (scope, element, attr) {
                var questionWatch = scope.$watch('question', function () {
                    scope.drop_down_texts = scope.get_drop_down_text_array(scope.displayAsText(scope.question));
                    scope.dropDownTextsCount = scope.get_drop_down_text_array_count(scope.drop_down_texts);
                    scope.questionAnswerSet = scope.question.ANSWERSET;
                });

                var dragMoveHandler = scope.$on('draggable:move', function () {
                    if (scope.ds.dragStarted == false) {
                        scope.$apply(function () {
                            scope.ds.dragStarted = true;
                        });
                    }
                });

                var dragEndHandler = scope.$on('draggable:end', function (e) {
                    $timeout(function () {
                        if (scope.ds.dragStarted == true) {

                            scope.$apply(function () {
                                scope.ds.dragStarted = false;
                            });
                        }
                    }, 100);
                });

                var resetFocusHandler = scope.$on('reset-focus', function (event, data) {
                    if (scope.ds.index == scope.ds.lastFocusedTxt) {
                        var element = angular.element(document.getElementById(scope.ds.index));
                        element.focus();
                    }
                });

                scope.showTooltip = function(isFocus, index){
                  scope.showTooltip = isFocus;
                };

                scope.setFocus = function (isFocus, index, showTooltip) {
                    if (index) {
                        scope.ds.index = index;
                        scope.ds.lastFocusedTxt = index;
                    }

                    scope.ds.showTooltip  = showTooltip;
                    scope.ds.isPopoverOpen = isFocus;
                };

                scope.getVideoPath = function (videoFile) {
                    return utilityService.getVideoPath(scope.selectedActivity.path, videoFile);
                };

                scope.hidetip = function () {
                    scope.ds.focuss = false;
                };

                scope.getDragDropVerticalAnswersetHeight = function () {
                    var qc = document.getElementById('questionContainer');
                    var sh = document.getElementById('sectionHeader');
                    var shh = sh? sh.offsetHeight:0;
                    var qcHeight = qc ? qc.clientHeight : 0;
                    var answerHeight = qcHeight - shh - 20;

                    return answerHeight;
                }

                var accessCHangeHandler = scope.$on('accessibilityChanged', function () {
                    $timeout(function () {
                        scope.ds.dragDropAnswerHeight = scope.getDragDropVerticalAnswersetHeight();
                    }, 2000);
                });

                 $window.onresize = function () {
                     scope.$apply(function () {
                         scope.ds.dragDropAnswerHeight = scope.getDragDropVerticalAnswersetHeight();
                     });
                 };

                 scope.$on('$destroy', function(){
                    questionWatch();
                    dragMoveHandler();
                    dragEndHandler();
                    resetFocusHandler();
                    accessCHangeHandler();
                    $timeout.cancel( scope.dragDropAnswerTimer );
                 });

                scope.init = function () {
                    scope.ds.isPopoverOpen = false;
                    scope.ds.dragStarted = false;
                    scope.ds = dataService.getDataSource();
                    if(scope.ds.sound){
                       scope.ds.sound.stop();
                    }
                    scope.dynamicPopover = {
                        templateUrl: !Array.isArray(scope.currentSection.QUESTIONS.QUESTION) || scope.selectedActivity.isHorizontal ?
                            'singleDragDropPopover.html' : 'dragDropPopover.html',
                    };

                     scope.dragDropAnswerTimer = $timeout(function () {
                        scope.ds.dragDropAnswerHeight = scope.getDragDropVerticalAnswersetHeight();
                     }, 1000);
                }();
            }
        };
    }]);
