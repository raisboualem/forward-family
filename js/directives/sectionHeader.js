app.directive("sectionHeader", ['$rootScope', 'dataService', 'utilityService', '$compile', '$timeout', 'groupService', '$stateParams',
    function (rootScope, dataService, utilityService, compile, timeout, groupService, stateParams) {
        return {
            restrict: "E",
            scope: {
                activityType: '=',
                activityName: '=',
                currentSection: '=',
                isHorizontal: '=',
                isMultiAnswer: '=',
                inverseSectionAudio: '=',
                largeImg: '=',
                moduleId: '='
            },
            controller: function ($scope, $element, $attrs) {
            },
            templateUrl: "templates/directives/sectionHeader.html",
            link: function (scope, element, attr) {
                scope.sectionAudioState = 'stopped';
                scope.playAudio = function (src, tooltipIndex, event) {
                    var sectionAudio = document.getElementById('sectionAudio');
                    var audioSource = src.PATH ? src.PATH : src;
                    if(audioSource['#text'])
                    var audioSource = src['#text'].PATH ? src['#text'].PATH : src['#text'];

                    if(scope.sectionAudioState=='stopped'){
                        scope.sectionAudioState = 'playing';
                        scope.captionTextWidth = scope.getCaptionWidth(scope.currentSection) + 41;
                        utilityService.playAudio(audioSource, tooltipIndex, event, scope.selectedActivity.path, sectionAudio);
                        scope.secHeadTooltipWidthTimeout = timeout(function () {
                            var tooltip = document.getElementsByClassName('tooltip-inner');
                            if (tooltip[0])
                                tooltip[0].style.width = scope.captionTextWidth + 'px';
                        }, 20);

                    }else if(scope.sectionAudioState=='playing'){
                        utilityService.pauseAudio(sectionAudio);
                        scope.ds.showTooltip = true;
                        scope.sectionAudioState = 'paused';
                    }else if(scope.sectionAudioState=='paused'){
                        utilityService.resumeAudio(sectionAudio);
                        scope.sectionAudioState = 'playing';
                    }
                }


                scope.sectionAudioEnded = function(){
                    scope.sectionAudioState = 'stopped';
                };

                scope.getCaptionWidth = function (section) {
                    var maxWidth;
                    var maxChar = section.AUDIOCAPTION;
                    var element = compile('<span class=\"form-controls sectionHiddenSpan\" ' +
                        'style=\"visibility: hidden;position: absolute;min-width:0;font-weight: bold;font-size: 1.2em;\" id="maxInput" type="text"></span>')(scope);

                    element[0].className += (" fontScaleX" + rootScope.fontSize);
                    var container = document.getElementById('mainContent');

                    if (container)
                        container.appendChild(element[0]);

                    var e = element[0];

                    if (e) {
                        e.innerHTML = maxChar;
                        maxWidth = e.clientWidth;
                    }

                    return maxWidth;
                };

                scope.getImageAssetsPath = function (imageName) {
                    return utilityService.getImagePath(scope.selectedActivity.path, imageName);
                };


                scope.sectionAudioInverseFn = function () {

                    var count = 0;
                    var len = Array.isArray(scope.currentSection.QUESTIONS.QUESTION) ? scope.currentSection.QUESTIONS.QUESTION.length : 1;

                    for (var i = 0; i < len; i++) {

                        var question = scope.currentSection.QUESTIONS.QUESTION[i] ? scope.currentSection.QUESTIONS.QUESTION[i] : scope.currentSection.QUESTIONS.QUESTION;
                        if (question.disabled == true) {
                            count++;
                        }
                    }

                    var sectionAudio = document.getElementById('inverseSectionAudio');

                    if (sectionAudio && (i == count) && !scope.sectionAudioEnabled) {
                        scope.sectionAudioEnabled = true;
                        sectionAudio.focus();
                    }

                    return i == count;
                };
                $('#container').click(function(event) {

                    timeout(function () {
                    var elem = event.target.id;
                        if(elem == 'playBtn0'){
                            pausedBtn0.focus();
                        }
                        else if(elem == 'pausedBtn0'){
                            playBtn0.focus();
                        }
                        else if(elem == 'playBtn1'){
                            pausedBtn1.focus();
                         }
                        else if(elem == 'pausedBtn1'){
                             playBtn1.focus();
                        }
                        else if(elem == 'playBtn2'){
                            pausedBtn2.focus();
                         }
                        else if(elem == 'pausedBtn2'){
                             playBtn2.focus();
                        }
                        else if(elem == 'playBtn' && scope.sectionAudioState == 'playing'){
                             pausedBtn.focus();
                          }
                         else if(elem == 'pausedBtn'){
                              playBtn.focus();
                         }

                    }, 500);

                        if( event.target.className == "audioImg ng-scope"|| event.target.className == "playBtn"
                              || event.target.id == "inverseSectionAudio")
                            {
                                return;
                            }
                          else{
                            timeout(function (){
                            var sectionAudio = document.getElementById('sectionAudio');
                            if(scope.sectionAudioState != 'stopped'){
                                utilityService.stopAudio(sectionAudio);
                                scope.sectionAudioState = 'stopped';
                                }
                            },100);


                          }
                });

                $('html').keydown(function(event) {

                       timeout(function () {
                        var elem = event.target.id;
                          if(elem == 'playBtn0'&& scope.sectionAudioState=='playing'){
                              pausedBtn0.focus();
                          }
                          else if(elem == 'pausedBtn0' && scope.sectionAudioState=='paused'){
                              playBtn0.focus();
                          }
                          else if(elem == 'playBtn1' && scope.sectionAudioState=='playing'){
                              pausedBtn1.focus();
                           }
                          else if(elem == 'pausedBtn1' && scope.sectionAudioState=='paused'){
                               playBtn1.focus();
                          }
                          else if(elem == 'playBtn2' && scope.sectionAudioState=='playing'){
                               pausedBtn2.focus();
                            }
                           else if(elem == 'pausedBtn2' && scope.sectionAudioState=='paused'){
                                playBtn2.focus();
                           }
                           else if(elem == 'playBtn' && scope.sectionAudioState=='playing'){
                               pausedBtn.focus();
                            }
                           else if(elem == 'pausedBtn' && scope.sectionAudioState=='paused'){
                                playBtn.focus();
                           }

                        }, 500);

                        if(event.target.className == "audioImg ng-scope"|| event.target.className == "playBtn" ||
                              event.target.className == "dummyBtnClass"|| event.target.id == "inverseSectionAudio"

                              )
                            {
                                return;
                            }
                          else{
                            timeout(function (){
                            var sectionAudio = document.getElementById('sectionAudio');
                            if(scope.sectionAudioState != 'stopped'){
                                utilityService.stopAudio(sectionAudio);
                                scope.sectionAudioState = 'stopped';
                                }
                            },100);
                          }
                });

                scope.$on('$destroy', function(){
                    timeout.cancel( scope.secHeadTooltipWidthTimeout );
                    accessChange();
                });

                 scope.getImageCaptionsFromLang = function() {
                  var imgC = scope.currentSection.IMAGECAPTIONS.IMAGECAPTION;
                  var resultImgC = '';
                      if(imgC) {
                          resultImgC = imgC.filter(function(elem,indx){
                              return scope.ds.ispeaklanguage === elem['@lang'];
                          })
                          scope.imageCaptionObj = resultImgC[0].TEXT;
                      }
                 };

                var accessChange = scope.$on('accessibilityChanged', function () {
                     if(scope.currentSection.IMAGECAPTIONS){
                        scope.getImageCaptionsFromLang();
                     }
                });

                scope.init = function () {
                    scope.ds = dataService.getDataSource();

                    var activityId = stateParams.activityId;

                    scope.ds.activityId = activityId;

                    scope.utilityService = utilityService;

                    scope.selectedActivity = groupService.getItem(activityId, scope.moduleId);

                    if (scope.currentSection.AUDIOS && !Array.isArray(scope.currentSection.AUDIOS.AUDIO)) {
                        scope.currentSection.AUDIOS.AUDIO = [scope.currentSection.AUDIOS.AUDIO];
                    }
                    if(scope.currentSection && scope.currentSection.AUDIOS && !Array.isArray(scope.currentSection.AUDIOS.AUDIO)){
                       scope.currentSection.AUDIOS.AUDIO = [scope.currentSection.AUDIOS.AUDIO];
                   }
                   if(scope.currentSection.IMAGECAPTIONS){
                    scope.getImageCaptionsFromLang();
                   }
                }();
            }
        }
    }
]);