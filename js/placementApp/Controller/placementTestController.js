(function() {
    'use strict';

    angular
        .module('placementApp')
        .controller('placementTestController', placementTestController)
        .controller('placementeStartController', placementeStartController)
        .controller('placementTestQuizController', placementTestQuizController)
        .controller('placementTestResultController', placementTestResultController);

    // placementTestController
    function placementTestController($scope, $state, PlacementTestService, ResultService, localStorageService) {
        var self = this;

        self.ResultService = ResultService;
        self.PlacementTestService = PlacementTestService;

        onInit();

        function onInit() {
            self.PlacementTestService.getStaticManiFest().then(function(resp) {
                self.PlacementTestService.placementTest = resp.data.placementTest;

                self.jobChoices = self.PlacementTestService.placementTest.jobChoices;

                self.employerChoices = self.PlacementTestService.placementTest.employerChoices;

                self.divisionChoices = self.PlacementTestService.placementTest.divisionChoices;
            });
        };

        // functionality for placement form
        self.formSubmit = function(form) {
            var form = $scope.placementForm;
            if (form.$valid) {
                self.ResultService.userData("Name", form.name['$modelValue']);
                self.ResultService.userData("Email", form.email['$modelValue']);
                self.ResultService.userData("Job", form.job['$modelValue'].label);
                self.ResultService.userData("Employer", form.employer['$modelValue'].label);
                self.ResultService.userData("Division", form.division['$modelValue'].label);
                localStorageService.setItem('userData', JSON.stringify(self.ResultService.getUserData()));
                $state.go('placementTest_start');
            } else {
                angular.forEach(form, function(value, key) {
                    if (typeof value === 'object' && value.hasOwnProperty('$modelValue'))
                        value.$setTouched();
                });
            }
        };

        self.onJobChanged = function(job) {
            self.selectedEmployee = self.PlacementTestService.placementTest.employerChoices[job.id];
        };

        self.onEmployerChanged = function(employer) {
            self.selectedDivision = self.PlacementTestService.placementTest.divisionChoices[employer.id];
        };
    };

    function placementeStartController($scope, $state, XmlDataService, QuestionService, $timeout) {
        var self = this;
        self.QuestionService = QuestionService;
        self.defaultLanguage = 'en';

        self.onStartPlacementTest = function() {
            $state.go('placementTest_quiz')
        };

        onInit();

        function onInit() {
            var fileName = $scope.moduleId;
            XmlDataService.getXMLData(fileName).then(function(data) {
             var json = XmlDataService.parseXmlToJSON(data);
             $scope.placementTestActivity = json.ACTIVITY;
             self.QuestionService.setData(json.ACTIVITY);

            self.sectionDescriptionTextIntro = self.QuestionService.loadDescription(self.defaultLanguage).INTRO;
            self.sectionDescriptionTextRow = self.QuestionService.loadDescription(self.defaultLanguage).ROW;
            self.sectionDescriptionTextOutro = self.QuestionService.loadDescription(self.defaultLanguage).OUTRO;

            });
        };

    };

    //functionality for placement test questionnaire
    function placementTestQuizController($scope, $rootScope, $state, $timeout, $stateParams, QuestionService, ResultService, XmlDataService, UtilityService, ngAudio, localStorageService, shuffleDragDropAnswersService) {
        var self = this;
        self.fillCorrectAns = {
            answer: [],
        };
        self.defaultLanguage = 'en';
        self.QuestionService = QuestionService;
        self.ResultService = ResultService;
        self.shuffleDragDropAnswerSet = shuffleDragDropAnswersService;

        onInit();

        function onInit() {
            var fileName = $scope.moduleId;
            XmlDataService.getXMLData(fileName).then(function(data) {
                var json = XmlDataService.parseXmlToJSON(data);
                $scope.placementTestActivity = json.ACTIVITY;
                self.QuestionService.setData(json.ACTIVITY)

                const qindex = JSON.parse(localStorageService.getItem('currentQuestionIndex'));
                const sindex = JSON.parse(localStorageService.getItem('currentSectionIndex'));
                const qnumber = JSON.parse(localStorageService.getItem('questionNumber'));
                const userAns = JSON.parse(localStorageService.getItem('userAns'));
                const correctAns = JSON.parse(localStorageService.getItem('correctAns'));
                const results = JSON.parse(localStorageService.getItem('results'));
                const array = JSON.parse(localStorageService.getItem('array'));
                const ansData = JSON.parse(localStorageService.getItem('ansData'));
                const userData = JSON.parse(localStorageService.getItem('userData'));

                self.QuestionService.currentQuestionIndex = qindex || 0;
                self.QuestionService.currentSectionIndex = sindex || 0;
                self.QuestionService.questionNumber = qnumber || 1;
                self.QuestionService.isArray = array || [];
                self.ResultService.userAns = userAns || [];
                self.ResultService.correctAns = correctAns || [];
                self.ResultService.results = results || [];
                self.ResultService.storeAns = ansData || {};
                self.ResultService.storeData = userData || {};

                localStorageService.setItem('currentQuestionIndex', JSON.stringify(self.QuestionService.currentQuestionIndex));
                localStorageService.setItem('currentSectionIndex', JSON.stringify(self.QuestionService.currentSectionIndex));
                localStorageService.setItem('results', JSON.stringify(self.ResultService.results));

                self.section = self.QuestionService.loadSection();
                self.question = self.QuestionService.loadQuestions();
                self.sectionInstructionText = self.QuestionService.loadInstructions(self.defaultLanguage);
                self.correctAnswer = self.ResultService.saveCorrectAnswer();
                self.audioFile = self.QuestionService.getAudioPath(self.QuestionService.audioFileLoc()) + _.get(self.question[self.QuestionService.currentQuestionIndex], 'AUDIO');

                var filePath = self.audioFile;
                self.checkFileExist(filePath);

                if(self.section['@TYPE'] === 'DRAGDROP') {
                    self.shuffledAnswers = self.shuffleDragDropAnswerSet.loadShuffleAnswerSet();

					self.quesDragNumber = self.QuestionService.questionNumber + self.question.length - 1;
                }

                $scope.dynamicPopover = {
                    templateUrl: 'dragDropPopover.html',
                };


                if(self.section['@TYPE'] === 'FILLTHEBLANKS' && self.question[self.QuestionService.currentQuestionIndex].ANSWERSET['@controlType'] === 'BUTTON') {
                    var currentAudio = self.audioFile;
                    if (currentAudio) {
                        $scope.audio = ngAudio.load(currentAudio);
                        $scope.audio.play();
                    } else {
                        $scope.audio.pause();
                    };
                };

            });
        };

        self.checkFileExist = function(filePath){
            var xhr = new XMLHttpRequest();
            xhr.open('HEAD', filePath, false);
            xhr.send();

            if (xhr.status == "404") {
                $scope.audioPresent = false;
            } else {
                $scope.audioPresent = true;
            }
        }

        $scope.setFocus = function (isFocus, index, showTooltip) {
            if (index || index == 0) {
                $scope.index = index;
                $scope.lastFocusedTxt = index;
            }

            $scope.isPopoverOpen = isFocus;
        };

        $scope.dropped = function (data, evt, question, fromPopup) {
            $scope.isPopoverOpen = false;
            if (data) {
               $scope.onDropCompleteInput(data, evt);
            }

            question.ANSWERSET.disabled = true;
            question.ANSWERSET.tabIndex = -1;
        };

        self.onAudioPlay = function() {
            $scope.showTooltip = true;
            var currentAudio = self.audioFile;
            if (currentAudio) {
                $scope.audio = ngAudio.load(currentAudio);
                $scope.audio.play();
            } else {
                $scope.audio.pause();
            };
        };

        self.onClickAudioPlay = function(question) {
            $scope.showTooltip = true;
            if (question) {
                var audio = self.QuestionService.getAudioPath(self.QuestionService.audioFileLoc()) + question.AUDIO;
                $scope.audio = ngAudio.load(audio);
                $scope.audio.play();
            };
        };


        $scope.drag = function(ev) {
            ev.dataTransfer.setData("Text", ev.target.id);
        };

//        $scope.keyPressHandler = function(e) {
//            if (e.keyCode === 13) {
//                e.preventDefault();
//                e.stopPropagation();
//            }
//        };

        $scope.onDropCompleteInput = function(data, evt) {
            var ans = _.values(data);
            self.fillCorrectAns.answer.push(ans.pop());
        };

        $scope.onDragComplete = function (data, evt){
            var index = self.fillCorrectAns.answer.indexOf(data);
            if (index > -1) {
                self.fillCorrectAns.answer.splice(index, 1);
            };
        };

        $scope.onDropCompleteInput = function(data, evt) {
           if(data.answer){
                var ans = _.values(data);
                self.fillCorrectAns.answer.push(ans.pop());
           }
           else{
                var ans = data;
                self.fillCorrectAns.answer.push(ans);
           }
           if(self.fillCorrectAns.answer.length ==  self.question.length)
                $scope.showSubmit = true;
        };

        $scope.showSubmit = false;
        $scope.onEnterSubmit = function(e) {
            if(e.keyCode === 13) {
                $scope.showSubmit = true;
            };
        };

        self.showNextQuestion = function(questionForm) {
            $scope.showSubmit = false;
            if (questionForm.$valid) {
                questionForm.$setPristine();

                self.ResultService.saveUserAnswer(self.fillCorrectAns.answer);
                self.ResultService.saveCorrectAnswer();
                self.ResultService.answersData(self.QuestionService.currentSectionIndex, self.ResultService.getUserAnswer());
                self.ResultService.userData('Data', self.ResultService.getAnswersData());
                self.ResultService.calculateScore();
                self.QuestionService.updateQuestionIndex(self.quesDragNumber);
                self.QuestionService.resetQuestionIndex();
                self.shuffleDragDropAnswerSet = shuffleDragDropAnswersService;

                if(self.QuestionService.currentSectionIndex != self.QuestionService.sectionLength()) {
                    self.section = self.QuestionService.loadSection();
                    self.question = self.QuestionService.loadQuestions();
                    self.sectionInstructionText = self.QuestionService.loadInstructions(self.defaultLanguage);
                    self.audioFile = self.QuestionService.getAudioPath(self.QuestionService.audioFileLoc()) + _.get(self.question[self.QuestionService.currentQuestionIndex], 'AUDIO');

                     var filePath = self.audioFile;
                     self.checkFileExist(filePath);

                } else if (self.QuestionService.currentSectionIndex === self.QuestionService.sectionLength()) {
                    $state.go('result-page');
                }

                if(self.section['@TYPE'] === 'DRAGDROP') {
                    self.shuffledAnswers = self.shuffleDragDropAnswerSet.loadShuffleAnswerSet();
                    self.quesDragNumber = self.QuestionService.questionNumber + self.question.length -1;
                };

                localStorageService.setItem('currentQuestionIndex', JSON.stringify(self.QuestionService.currentQuestionIndex));
                localStorageService.setItem('currentSectionIndex', JSON.stringify(self.QuestionService.currentSectionIndex));
                localStorageService.setItem('sectionLength', JSON.stringify(self.QuestionService.sectionLength()));
                localStorageService.setItem('questionNumber', JSON.stringify(self.QuestionService.questionNumber));
                localStorageService.setItem('userAns', JSON.stringify(self.ResultService.userAns));
                localStorageService.setItem('correctAns', JSON.stringify(self.ResultService.correctAns));
                localStorageService.setItem('results', JSON.stringify(self.ResultService.results));
                localStorageService.setItem('ansData', JSON.stringify(self.ResultService.getAnswersData()));
                localStorageService.setItem('userData', JSON.stringify(self.ResultService.getUserData()));

                self.fillCorrectAns.answer = [];


            }; // end of questionForm
        }; // end of showNextQuestion
    }; // end of placementTestQuizController


    function placementTestResultController($rootScope, $scope, $state, $stateParams, $timeout, localStorageService, ResultService, QuestionService, env, xmlDataService, utilityService) {
        var self = this;

        self.QuestionService = QuestionService;
        self.ResultService = ResultService;
        self.defaultLanguage = "en";

        onInit();

        function onInit() {
            const sindex = JSON.parse(localStorageService.getItem('currentSectionIndex'));
            const loading = JSON.parse(localStorageService.getItem('loading'));
            const learnerLevel = JSON.parse(localStorageService.getItem('learnerLevel'));
            const results = JSON.parse(localStorageService.getItem('results'));
            const sectionLength = JSON.parse(localStorageService.getItem('sectionLength'));
            const finalScore = JSON.parse(localStorageService.getItem('finalScore'));

            self.sectionLength = sectionLength;
            self.sectionIndex = sindex || 0;
            self.learnerLevel = learnerLevel || [];
            self.results = results || [];
            self.finalScore = finalScore;
            if (loading != null) {
                self.loading = loading;
            } else {
                self.loading = true;
            }

            $timeout(function() {
                self.loading = false;
                localStorageService.setItem('loading', JSON.stringify(self.loading));
            }, 3000);


            if (self.sectionIndex >= 1 && self.sectionIndex < 9) {
                self.learnerLevel = 'A';
            } else if (self.sectionIndex >= 9 && self.sectionIndex < 14) {
                self.learnerLevel = 'B';
            } else {
                self.learnerLevel = 'C';
            };

            localStorageService.setItem('learnerLevel', JSON.stringify(self.learnerLevel));
        }

        $timeout(function() {
            localStorageService.clearStorage();
            $state.go('placementTest_quiz');
        }, 10000);

    };

})();
