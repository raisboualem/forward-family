(function() {
    'use strict';

    angular
        .module('placementApp')
        .service('xmlDataService', xmlDataService);

    function xmlDataService($http, $rootScope, $stateParams) {

        this.getXMLData = function(filename) {
            var path = 'data/ESL/module/xml/placementTest.xml'
            var defer = $http.get(path);
            return defer;
        };

        //Parse xml data and return JSON object
        this.parseXmlToJSON = function(xml) {
            var json = null;

            if (xml) {
                if (window.DOMParser) {
                    try {
                        var dom = (new DOMParser()).parseFromString(xml.data, "text/xml");
                        var json = xml2json(dom);
                        json = json.replace('undefined', '');
                        json = json.replaceAll('DATASTR', '');
                        json = JSON.parse(json);
                    } catch (e) {
                        json = null;
                    }
                }
            }

            return json;
        };

        String.prototype.replaceAll = function(f, r) {
            return this.split(f).join(r);
        }
    }

})();
