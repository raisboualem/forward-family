(function() {
    'use strict';

    angular
        .module('placementApp')
        .service('PlacementTestService', PlacementTestService)
        .service('QuestionService', QuestionService)
        .service('ResultService', ResultService)
        .service('UtilityService', UtilityService)
        .service('XmlDataService', XmlDataService)
        .service('shuffleDragDropAnswersService', shuffleDragDropAnswersService)
    ;

    //service for placement Test Questionnaire
    function PlacementTestService($http, $state) {

        var service = {
            getStaticManiFest: getStaticManiFest,
            jobChoices: [],
            employerChoices: [],
            divisionChoices: [],
        };
        return service;

        function getStaticManiFest() {
            return $http.get('static_manifest.json')
        };

    }; // end of PlacementTestService


    function QuestionService($http, $stateParams, $rootScope, xmlDataService, localStorageService) {
        var service = {
            isArray: [],
            loadData: {},
            setData: setData,
            getData: getData,
            setIndexLimit: setIndexLimit,
            sectionLength: sectionLength,
            loadSection: loadSection,
            loadDesc: loadDesc,
            loadQuestions: loadQuestions,
            loadAnswers: loadAnswers,
            loadInstructions: loadInstructions,
            loadDescription: loadDescription,
            updateQuestionIndex: updateQuestionIndex,
            resetQuestionIndex: resetQuestionIndex,
            currentSectionIndex: 0,
            currentQuestionIndex: 0,
            questionNumber: 1,
            getAssetsPath: getAssetsPath,
            getAudioPath: getAudioPath,
            audioFileLoc: audioFileLoc
        };

        return service;

        function setData(data) {
            service.loadData = data;
        }

        function getData() {
            return service.loadData;
        }

        function setIndexLimit() {
            service.currentSectionIndex = sectionLength() - 1;
        }

        function updateQuestionIndex(x) {
            if (loadSection()['@TYPE'] != 'DRAGDROP') {
                service.currentQuestionIndex += 1;
                service.questionNumber += 1;
            } else {
                service.currentQuestionIndex = loadQuestions().length;
                if (x) {
                    service.questionNumber = x + 1;
                } else {
                    service.questionNumber += 1;
                }
            }
        }

        function resetQuestionIndex() {
            if (service.currentQuestionIndex === loadQuestions().length) {
                service.currentQuestionIndex = 0;
                service.currentSectionIndex += 1;

                service.isArray = [];
                localStorageService.setItem('array', JSON.stringify(service.isArray));
            }

        }

        function sectionLength() {
            return service.loadData.SECTIONS.SECTION.length;
        }

        function loadSection() {
            return service.loadData.SECTIONS.SECTION[service.currentSectionIndex];
        }

        function loadDesc() {
            return service.loadData;
        }

        function loadQuestions() {
            return loadSection().QUESTIONS.QUESTION;
        }

        function loadAnswers() {
            if (loadSection()['@TYPE'] === 'DRAGDROP') {
                var array = []
                for (var key in loadQuestions()) {
                    array.push(loadQuestions()[key].ANSWERSET.ANSWER.TEXT);
                }
                return array;
            } else if (loadSection()['@TYPE'] === 'FILLTHEBLANKS' && loadQuestions()[service.currentQuestionIndex].ANSWERSET['@controlType'] === 'INPUT') {
                return loadQuestions()[service.currentQuestionIndex].ANSWERSET.ANSWER.TEXT;
            } else {
                for (var key in loadQuestions()[service.currentQuestionIndex].ANSWERSET.ANSWER) {
                    if (loadQuestions()[service.currentQuestionIndex].ANSWERSET.ANSWER[key]['@CORRECT'] === 'TRUE') {
                        if (!service.isArray.includes(loadQuestions()[service.currentQuestionIndex].ANSWERSET.ANSWER[key].TEXT)) {
                            service.isArray.push(loadQuestions()[service.currentQuestionIndex].ANSWERSET.ANSWER[key].TEXT);
                        }
                    }
                }
                localStorageService.setItem('array', JSON.stringify(service.isArray));
                return service.isArray;
            }
        }

        function loadInstructions(language) {

            if (language === 'fr') {
                return loadSection().INSTRUCTIONS.INSTRUCTION[0].TEXT;
            } else if (language === 'en') {
                return loadSection().INSTRUCTIONS.INSTRUCTION[1].TEXT;
            } else if (language === 'es') {
                return loadSection().INSTRUCTIONS.INSTRUCTION[2].TEXT;
            } else if (language === 'zh') {
                return loadSection().INSTRUCTIONS.INSTRUCTION[3].TEXT;
            }
        };

        function loadDescription(language) {

            var desc = loadDesc().DESCRIPTIONS.DESCRIPTION;
               var resultDesc = '';

                   if(Array.isArray(desc)) {
                       resultDesc = desc.filter(function(elem,indx){
                           return language === elem['@lang'];
                       })
                   }
               return resultDesc[0];
        };


        function getAssetsPath(fileName) {
            var module = $rootScope.isFrenchModule ? 'esl' : 'fsl';
            var path = 'data/' + module + '/' + 'modulePlacementTest' + '/xml/' + fileName;
            return path;
        }

        function getAudioPath(fileName, audioPath) {
            return service.getAssetsPath(fileName) + '/audio/';
        };

        function audioFileLoc() {
            var folderName = 'M00-Test';
            return folderName;
        }

    };

    function ResultService($http, $state, QuestionService, localStorageService) {

        var service = {
            score: 0,
            userAns: [],
            correctAns: [],
            results: [],
            storeData: {},
            storeAns: {},
            getUserData: getUserData,
            getAnswersData: getAnswersData,
            getUserAnswer: getUserAnswer,
            getCorrectAnswer: getCorrectAnswer,
            getResults: getResults,
            answersData: answersData,
            userData: userData,
            saveUserAnswer: saveUserAnswer,
            saveCorrectAnswer: saveCorrectAnswer,
            calculateScore: calculateScore,
            setFinalScore: setFinalScore
        }

        return service;

        function getUserData() {
            return service.storeData;
        }

        function getResults() {
            return service.results;
        }

        function getUserAnswer() {
            return service.userAns;
        }

        function getCorrectAnswer() {
            return service.correctAns;
        }

        function getAnswersData() {
            return service.storeAns;
        }

        function answersData(key, value) {
            service.storeAns[key] = value;
        }

        function userData(key, value) {
            service.storeData[key] = value;
        }

        function saveUserAnswer(array) {
            if (!Array.isArray(array)) {
                service.userAns.push(array);
            } else {
                service.userAns = array;
            }
        }

        function saveCorrectAnswer() {
            if ((QuestionService.loadAnswers() != undefined) && (QuestionService.loadSection()['@TYPE'] === 'FILLTHEBLANKS') && (QuestionService.loadQuestions()[QuestionService.currentQuestionIndex].ANSWERSET['@controlType'] === 'INPUT')) {
                if (!service.correctAns.includes(QuestionService.loadAnswers())) {
                    service.correctAns.push(QuestionService.loadAnswers());
                }
            } else {
                service.correctAns = QuestionService.loadAnswers();
            }
        }

        function setFinalScore() {
            var sumScore = 0;
            for (var index = 0; index < service.results.length; index++) {
                sumScore += parseInt(service.results[index]);
            }
            var result = sumScore / service.results.length;
            return result.toFixed();
        }

        function calculateScore() {
            if (service.userAns.length === QuestionService.loadQuestions().length) {
                var index = 0;
                while (index < service.userAns.length) {
                    if (service.userAns[index] === service.correctAns[index]) {
                        service.score += 1;
                    };
                    index += 1;
                };
                var questionsCount = QuestionService.loadQuestions().length;
                var result = (service.score / questionsCount) * 100;
                service.results.push(parseInt(result.toFixed()));

                if (result < 60) {
                    userData('Final Score', parseInt(setFinalScore()));
                    localStorageService.setItem('finalScore', JSON.stringify(parseInt(setFinalScore())));
                    $state.go('result-page');
                };

                if (QuestionService.currentSectionIndex === QuestionService.sectionLength() - 1) {
                    userData('Final Score', parseInt(setFinalScore()));
                    localStorageService.setItem('finalScore', JSON.stringify(parseInt(setFinalScore())));
                };

                service.score = 0;
                service.userAns = [];
                service.correctAns = [];
            };
        };
    };

    function UtilityService($http, $state, $stateParams, $rootScope) {

        var service = {
            getAssetsCommonPath: getAssetsCommonPath,
            getFilePath: getFilePath
        };
        return service;

        function getAssetsCommonPath() {
            var module = $rootScope.isFrenchModule ? 'esl' : 'fsl';
            var path = 'data/' + module + '/' + 'modulePlacementTest' + '/xml/' + 'placementTest';
            return path;
        };

        function getFilePath() {
            return service.getAssetsCommonPath() + '.xml';
        };
    };

    function XmlDataService($http, $state, $stateParams, UtilityService) {

        var service = {
            getXMLData: getXMLData,
            parseXmlToJSON: parseXmlToJSON
        };
        return service;

        function getXMLData(filename) {
            var path = UtilityService.getFilePath(filename);
            var defer = $http.get(path);
            return defer;
        };

        function parseXmlToJSON(xml) {
            var json = null;

            if (xml) {
                if (window.DOMParser) {
                    try {
                        var dom = (new DOMParser()).parseFromString(xml.data, "text/xml");
                        var json = xml2json(dom);
                        json = json.replace('undefined', '');
                        json = json.replaceAll('DATASTR', '');
                        json = JSON.parse(json);
                    }
                    catch (e) {
                        json = null;
                    }
                }
            }

            return json;
        };

        String.prototype.replaceAll = function (f, r) {
            return this.split(f).join(r);
            }
    };

    function shuffleDragDropAnswersService($http, $state, $stateParams, QuestionService, utilityService) {

        var service = {
            loadShuffleAnswerSet: loadShuffleAnswerSet,
            currentQuestionIndex: 0
        };
        return service;

        function loadShuffleAnswerSet(){
            var section = QuestionService.loadSection();
            var questions = section.QUESTIONS.QUESTION;

            var answerCollection = _.map(questions, function(data) {
                    return _.get(data, 'ANSWERSET.ANSWER.TEXT');
            });

            var shuffledAnswerSet = _.shuffle(answerCollection)

            return shuffledAnswerSet;
        };
    }

})();
