(function() {
    'use strict';

    angular
        .module('placementApp')
        .service('utilityService', utilityService);

    function utilityService($http, $rootScope, $stateParams) {

        this.getAssetsCommonPath = function(fileName) {
            console.log(fileName, "fileName")
            var module = $rootScope.isFrenchModule ? 'FSL' : 'ESL';
            var path = 'data/' + module + '/' + 'module' + $stateParams.moduleId + '/xml/' + fileName;
            return path;
        };

        this.getFilePath = function(fileName) {
          console.log(fileName)
            var path = this.getAssetsCommonPath(fileName) + '.xml';
            return path;
        };

        this.getFileName = function(filePath) {
            if (filePath) {
                var temp = filePath.split('/');
                filePath = temp[temp.length - 1];
            }

            return filePath;
        };
    }

})();
