(function() {
    'use strict';

    angular
        .module('placementApp')
        .config(routes);

    function routes($stateProvider, $urlRouterProvider, $locationProvider,
        $httpProvider, $urlMatcherFactoryProvider) {

        $stateProvider
            .state('legacy', {
                abstract: true,
            })
            .state('placementTest', {
                url: '/placementtest/:moduleId',
                templateUrl: 'templates/placement/standalone/placement-test-landing.html',
                controller: 'placementTestController',
                controllerAs: 'ctrl'
            })
            .state('placementTest_start', {
                url: '/placementtest-start',
                templateUrl: 'templates/placement/standalone/placement-test-start.html',
                controller: 'placementeStartController',
                controllerAs: 'ctrl'
            })
            .state('placementTest_quiz', {
                url: '/placementtest-quiz/:moduleId',
                templateUrl: 'templates/placement/standalone/placement-test-quiz.html',
                controller: 'placementTestQuizController',
                controllerAs: 'ctrl'
            })
            .state('result-page', {
                url: '/placementtest-result/:moduleId',
                templateUrl: 'templates/placement/standalone/placement-test-result.html',
                controller: 'placementTestResultController',
                controllerAs: 'ctrl'
            })
            $urlRouterProvider.otherwise('/');
            // $locationProvider.html5Mode(true);
    };

})();
