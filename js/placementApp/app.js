(function() {
    'use strict';
    var env = {};

    if (window) {
        Object.assign(env, window.__env);
    };
    angular
        .module('placementApp', [
            'ui.router',
            'ui.bootstrap',
            'ui.bootstrap-tooltip',
            'ui.bootstrap-popover',
            'ui.bootstrap-dropdown',
            'ngAudio',
            'ngDraggable',
            'ngSanitize',
            'ngDialog',
            'angularUtils.directives.dirPagination'
        ])
        .constant('env', env);;

})();
