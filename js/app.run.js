app.run(['$rootScope', "$window", 'dataService', 'utilityService', '$stateParams', '$timeout',
    function ($rootScope, $window, dataService, utilityService, stateParams, $timeout) {

        $rootScope.inProgress = false;
        $rootScope.fontSize = 2;
        var ua =  $window.navigator.userAgent.toLowerCase();
        $rootScope.isFireFox =ua.indexOf('firefox') > -1;
        $rootScope.isSafari = ua.indexOf('safari/') > -1 &&  ua.indexOf('chrome') == -1;
        $rootScope.isChrome = ua.indexOf('chrome') > -1;
        $rootScope.isInternetExplorer = ua.indexOf('trident/') > -1;
        $rootScope.isWindows = $window.navigator.platform.toUpperCase().indexOf('WIN')>=0;


        $rootScope.isArray = function (obj) {
            return Array.isArray(obj);
        };

        $rootScope.height =  $window.innerHeight;

        $window.onresize = function(){
            $rootScope.$apply(function(){
                $rootScope.height =  $window.innerHeight;
            });
        };

        $rootScope.$on('$locationChangeStart', function (event, next, prev) {

            if (prev.indexOf("main.html") != -1 && next.indexOf("main.html") != -1) {
                prev = prev.substr(prev.indexOf("main.html") + 9, prev.length);
                next = next.substr(next.indexOf("main.html") + 9, next.length);
            }

            var pattern = new RegExp("(.+(activities|dialogues|quiz|whatsNew|theoryVignettes|studySection).+)$");
            var result1 = pattern.test(prev);
            var result2 = pattern.test(next);
            var ds = dataService.getDataSource();

            if ((result1 == false && result2 == true)) {

                $timeout(function () {
                    utilityService.refreshSuspendData(stateParams.moduleId);
                }, 500);

                var pattern = new RegExp("(.+(whatsNew)|(theoryVignettes)|(studySection).+)$");
                var isWhatsNew = pattern.test(next);
                if (isWhatsNew) {
                    ds.isWhatsNew = true;
                }

            }

            if ($rootScope.inProgress && prev && next) {
                if ((result1 == true && result2 == false)) {
                    $rootScope.isLoading = true;
                    utilityService.saveData(prev, ds);
                    $rootScope.isLoading = false;

                    ds.resumeDataLoaded = false;
                    ds.resumeActivity = null;
                    ds.resumeData = null;
                    ds.activity = null;
                    ds.isResumed = false;
                    ds.answeredQuestions = 0;
                    ds.containerHeight = 0;
                    ds.attendedTotalQuestions = 0;
                    ds.isWhatsNew = false;
                    ds.isTheory = false;
                    ds.menuSelectionCompleted = undefined;

                    $rootScope.inProgress = false;
                }
            }
        });
    }])
