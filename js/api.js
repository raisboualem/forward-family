$.urlParam = function (name) {
    var results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
    if (results == null) {
        return 0;
    }
    else {
        return results[1] || 0;
    }
}

window.API = (function(){
    return {
        LMSInitialize: function() {
            var result = "false";
            $.ajax({
                type: "GET",
                url: "/LMSInitialize",
                data: null,
                async: false,
                success: function (t) {//On Successfull service call
                    result =  t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    result = "false";
                }
            });

            return result;
        },
        LMSCommit: function() {
            var result = "false";

            $.ajax({
                type: "GET",
                async: false,
                url: "/LMSCommit",
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    result = "false";
                }
            });

            return result;
        },
        LMSFinish: function() {
            var result = "false";

            $.ajax({
                type: "GET",
                async: false,
                url: "/LMSFinish",
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    result = "false";
                }
            });
        },
        LMSGetValue: function(model) {
            var result = "false";
            var module = $.urlParam('module');
			var lang = $.urlParam('lang');
			var webid = $.urlParam('webid');

            $.ajax({
                type: "GET",
                async: false,
                url: "/LMSGetValue",
                data: {dataModelElement: model, module: module, lang: lang, webid: webid},
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    result =  "false";
                }
            });

            return result;
        },
        LMSSetValue: function(model, value) {
            var result = "false";
            var module = $.urlParam('module');
			var lang = $.urlParam('lang');
			var webid = $.urlParam('webid');

            $.ajax({
                type: "POST",
                url: "/LMSSetValue",
                async: false,
                data: {dataModelElement: model, value: value, module: module, lang: lang, webid: webid},
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    result = "false";
                }
            });

            return result;
        },

        LMSResetTimeout: function(model, value) {
            var result = "false";
            var module = $.urlParam('module');
  		    var lang = $.urlParam('lang');
  		    var webid = $.urlParam('webid');

            $.ajax({
                type: "POST",
                url: "/keepSessionAlivePing",
                async: false,
                data: {dataModelElement: model, value: value, module: module, lang: lang, webid: webid},
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    result = "false";
                }
            });

            return result;
        },

		 LMSSetSuspendValue: function(model, value) {
            var result = "false";

            $.ajax({
                type: "POST",
				async: false,
                url: "/LMSSetValue",
                data: {dataModelElement: model, value: value},
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    result = "false";
                }
            });

            return result;
        },
        LMSGetLastError: function() {
            return "0";
        },
        LMSGetErrorString: function(errorCode) {
            return "No error";
        },
        LMSGetDiagnostic: function(errorCode) {
            return "No error";
        },
	LMSSavePdf:function(data, fileName, wn){
            var result = "false";
            $.ajax({
                type: "POST",
                url: "/SaveWhatsNewPDFInServer",
                data: {pdf: data, fileName:fileName},
                async: true,
                success: function (t) {//On Successfull service call
                    wn.location.href = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    wn.document.body.innerHTML = '';
                    wn.document.writeln('Some error occured.');
                    result = "false";
                }
            });

            return result;
        },
    };
})();













//var addSCORMAPIs = function (window) {
//    if (window) {
//        window.API = {
//            LMSInitialize: LMSInitialize,
//            LMSFinish: LMSFinish,
//            LMSGetValue: LMSGetValue,
//            LMSSetValue: LMSSetValue,
//            LMSCommit: LMSCommit,
//            LMSGetLastError: LMSGetLastError,
//            LMSGetErrorString: LMSGetErrorString,
//            LMSGetDiagnostic: LMSGetDiagnostic
//        }
//    }
//};
//
////API Functions
//function LMSInitialize(initParam) {
//    return true;
//}
//
//function LMSFinish(initParam) {
//    return true;
//}
//
//
//function LMSGetValue(element) {
//var  value;
//    switch (element){
//        case 'cmi.core.student_id':{
//            value = "1";
//            break;
//        }
//        case 'cmi.core.student_name':{
//            value = "Alax";
//            break;
//        }
//    }
//
//    return value;
//}
//
//function LMSSetValue(element, value) {
//    return true;
//}
//
//function LMSCommit(commitParam) {
//    return true;
//}
//
//function LMSGetLastError() {
//    return true;
//}
//
//function LMSGetErrorString(errorCode) {
//    return true;
//}
//
//function LMSGetDiagnostic(errorCode) {
//    return true;
//}
//
//
