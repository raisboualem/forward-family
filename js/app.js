﻿'use strict';

if (typeof Object.assign != 'function') {
  Object.defineProperty(Object, "assign", {
    value: function assign(target, varArgs) {
      'use strict';
      if (target == null) { // TypeError if undefined or null
        throw new TypeError('Cannot convert undefined or null to object');
      }

      var to = Object(target);

      for (var index = 1; index < arguments.length; index++) {
        var nextSource = arguments[index];

        if (nextSource != null) { // Skip over if undefined or null
          for (var nextKey in nextSource) {
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
      return to;
    },
    writable: true,
    configurable: true
  });
}


// Import variables if present (from js/env.js)
var env = {};

if (window) {
  Object.assign(env, window.__env);
};

var app = angular.module('quizApp', [
    'ui.router',
    'ngResource',
    'ui.bootstrap',
    'ui.bootstrap-tooltip',
    'ui.bootstrap-popover',
    'ui.bootstrap-dropdown',
    'ngDialog',
    'ui-rangeSlider',
    'ngAnimate',
    'ngAria',
    'mgcrea.ngStrap.tooltip',
    'ngTouch',
    'ngDraggable',
    'timer',
    'ui.event','ngCookies','ngSanitize','ngIdle','ngAudio','dndLists'])
    .config(['$urlRouterProvider', '$stateProvider', '$locationProvider', 'KeepaliveProvider', 'IdleProvider', function ($urlRouterProvider, $stateProvider, $locationProvider, KeepaliveProvider, IdleProvider) {
        $stateProvider
            .state('instructions', {
                url: '/main/:moduleId',
                templateUrl: 'templates/quiz/instructions.html',
                controller: 'quizController'
            })
            .state('groups', {
                url: '/main/modules/:moduleId/groups/:selectedUnit',
                templateUrl: 'templates/quiz/grouping.html',
                controller: 'groupingController'
            })
            .state('selectedActivity', {
                url: '/main/modules/:moduleId/activities/:activityId',
                templateUrl: 'templates/questions.html',
                controller: 'questionsController'
            })
            .state('activity_showScore', {
                url: '/main/modules/:moduleId/activities/:activityId/score',
                templateUrl: 'templates/questions.html',
                controller: 'questionsController'
            })
            .state('selectedDialogue', {
                url: '/main/modules/:moduleId/dialogues/:dialogueId/tab/:tabNo',
                templateUrl: 'templates/dialogue.html',
                controller: 'dialogueController'
            })
            .state('dialogue_showScore', {
                url: '/main/modules/:moduleId/dialogues/:dialogueId/score',
                templateUrl: 'templates/dialogue.html',
                controller: 'dialogueController'
            })
            .state('selectedQuiz', {
                url: '/main/modules/:moduleId/quiz/:quizId',
                templateUrl: 'templates/finalQuiz.html',
                controller: 'finalQuizController'
            })
            .state('selectedWhatsNew', {
                url: '/main/modules/:moduleId/whatsNew/:whatsNewId/page/:page',
                templateUrl: 'templates/whatsNew.html',
                controller: 'whatsNewController'
            })
            .state('selectedWhatsNew.content', {
                url: '/page/:pageNo#:nav',
                templateUrl: 'templates/whatsNew-content.html',
                controller: 'whatsNewContentController'
            })
            .state('selectedTheoryVignette', {
                url: '/main/modules/:moduleId/theoryVignettes/:tvId',
                templateUrl: 'templates/theoryVignette.html',
                controller: 'theoryVignetteController'
            })
            .state('selectedTheoryVignette.content', {
                url: '/content',
                templateUrl: 'templates/theoryVignette-content.html',
                controller: 'theoryVignetteContentController'
            })
            .state('selectedStudySection', {
                url: '/main/modules/:moduleId/studySection/:studySectionId',
                templateUrl: 'templates/studySection.html',
                controller: 'studySectionController'
            })
            .state('selectedDialogueQuestions', {
                url: '/main/modules/:moduleId/dialogues/:dialogueId/tab/:tabNo/page/:pageNo',
                templateUrl: 'templates/dialogue.html',
                controller: 'dialogueController'
            })
            .state('activityTestStarted', {
                url: '/main/modules/:moduleId/activities/:activityId/page/:pageNo',
                templateUrl: 'templates/questions.html',
                controller: 'questionsController'
            })
            .state('quizStarted', {
                url: '/main/modules/:moduleId/quiz/:quizId/section/:section/question/:question',
                templateUrl: 'templates/finalQuiz.html',
                controller: 'finalQuizController'
            })
            .state('quizStarted_dragDrop', {
                url: '/main/modules/:moduleId/quiz/:quizId/section/:section',
                templateUrl: 'templates/finalQuiz.html',
                controller: 'finalQuizController'
            })
            .state('quiz_showScore', {
                url: '/main/modules/:moduleId/quiz/:quizId/score',
                templateUrl: 'templates/finalQuiz.html',
                controller: 'finalQuizController'
            })
            .state('placementTest_landing', {
                url: '/main/placement-test-landing/:moduleId/',
                templateUrl: 'templates/placement/placement_test_landing.html',
                controller: 'placementTestController',
                controllerAs: 'ctrl'
            })
            .state('placementTest_start', {
                url: '/main/placement-test-start/',
                templateUrl: 'templates/placement/placement_test_start.html',
                controller: 'placementeStartController',
                controllerAs: 'ctrl'
            })
            .state('placementTest_quiz', {
                url: '/main/placement-test-quiz/:moduleId/',
                templateUrl: 'templates/placement/placement_test_quiz.html',
                controller: 'placementTestQuizController',
                controllerAs: 'ctrl'
            })
            .state('result-page', {
                url: '/main/result-page/:moduleId/',
                templateUrl: 'templates/placement/questionnaire_results_page.html',
                controller: 'placementTestResultController',
                controllerAs: 'ctrl'
            })

            function readTextFile(file, callback) {
                var rawFile = new XMLHttpRequest();
                if (rawFile.overrideMimeType)
                    rawFile.overrideMimeType("application/json");
                rawFile.open("GET", file, true);
                rawFile.onreadystatechange = function() {
                    if (rawFile.readyState === 4 && rawFile.status == "200") {
                        callback(rawFile.responseText);
                    }
                }
                rawFile.send(null);
            }

            readTextFile("timeout.txt", function(text){
            var data = JSON.parse(text);

            for (var key in data) {
                if (data.hasOwnProperty(key)) {

                    var objKey = data[key][0];
                    var objValue = removeSpecialChar(objKey);

                    IdleProvider.idle(objValue);

                    objKey = data[key][1];
                    objValue= removeSpecialChar(objKey);

                    IdleProvider.timeout(objValue);
                }
            }

            });

        //$locationProvider.html5Mode(true);

        function removeSpecialChar(objKey)
        {

        var stringObj = JSON.stringify(objKey);

        var lastIndex = stringObj.lastIndexOf("\"");
        var indexOfColon = stringObj.search(":");
        stringObj = stringObj.slice(indexOfColon+2,lastIndex);
        var timeoutValue = parseInt(stringObj);
        return timeoutValue;

        }
    }]);

// register environment in angular app
app.constant('env', env);
