/**
 * Created by sodev on 10/24/16.
 */
app.factory('interactionFactory', [function () {
    function interaction(id, pageNo, type, value, source){
        this.id = id;
        this.pageNo = pageNo;
        this.type = type;
        this.value = value;
        this.source = source;
    };
    return{
        getInteraction:function(id, sectionId, type, value, source){
            var result = new interaction(id,sectionId,type, value, source);
            return result;
        }
    }
}
]);