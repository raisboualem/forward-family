app.service('contentService', ['$rootScope', 'utilityService', 'staticManiFestService', function (rootScope, utilityService, staticManiFestService) {
    var contents = {};

    rootScope.$watch(function (){
        return staticManiFestService.loading;
    }, function(isReady) {
        if(!isReady) {
            contents = staticManiFestService.contentObjectList;
        }
    });

    return {
        getAllContents: function () {
            return contents;
        },

        getContentById: function (id, module) {
            var module = module != undefined ? parseInt(module) : 1;

            var item = utilityService.getGroupItem(contents, id, module);
            return item;
        },

        getUnits: function (module) {
            module = module != undefined ? parseInt(module) : 1;
            var moduleType = rootScope.isFrenchModule ? 'fr' : 'en';
            var result = contents[moduleType][module];
            return result;
        },

        getContentList: function (module) {
            module = module != undefined ? parseInt(module) : 1;
            var moduleType = rootScope.isFrenchModule ? 'fr' : 'en';
            var result = contents[moduleType][module]["units"];
            return result;
        },

        getForcedPath: function (module) {
            if (module != undefined) {
                var moduleType = rootScope.isFrenchModule ? 'fr' : 'en';
                var forcedPath = contents[moduleType][module]['forcedPath'];
                return forcedPath;
            }
        }
    }
}])

