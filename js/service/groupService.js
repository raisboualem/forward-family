/**
 * Created by sodev on 2/23/16.
 */
app.service('groupService', ['$rootScope', 'ActivityService',
    'dialogueService', 'whatsNewService', 'quizService', 'contentService',
    'utilityService', 'localStorageService', 'dataService', '$state', '$filter', '$location',
    function (rootScope, activityService, dialogueService,
              whatsNewService, quizService, contentService,
              utilityService, localStorageService, dataService, state, filter, location) {

        this.getUnits = function (moduleId) {
            var units;

            if (rootScope.isUnits) {
                units = contentService.getUnits(moduleId);
            } else {
                var activities = activityService.getActivityList(moduleId);
                var dialogues = dialogueService.getDialogueList(moduleId);
                var whatsNews = whatsNewService.getwhatsNewList(moduleId);
                var quiz = quizService.getFinalQuizList(moduleId);

                var forcedPath = [];

                var actForcedPath = activities?activities['forcedPath']:[];
                var diaForcedPath = dialogues?dialogues['forcedPath']:[];
                var whatForcedPath = whatsNews?whatsNews['forcedPath']:[];
                var quizForcedPath = quiz?quiz['forcedPath']:[];

                forcedPath = forcedPath.concat(actForcedPath, diaForcedPath, whatForcedPath, quizForcedPath);

                units = {
                    'units': [{'title': 'activities', 'contents': activities?activities['contents']:[]},]
                    , 'forcedPath': forcedPath
                };
            }

            
            //console.log(units.units[1].contents[0].glossary);
            return units;
        };

        this.getUnitContents = function (moduleId) {
            var units = this.getUnits(moduleId);

            if (units)
                return units['units'];
        };

        this.getItem = function (id, moduleId) {
            var grpContents = this.getUnitContents(moduleId);
            var collection = [];

            if (grpContents) {
                for (var i = 0; i < grpContents.length; i++) {
                    collection = collection.concat(grpContents[i].contents);
                }
            }

            for (var i = 0; i < collection.length; i++) {
                if (collection[i].id == id && collection[i].module == moduleId) {
                    return collection[i];
                }
            }
        };

        this.getForcedPath = function (module) {
            if (module != undefined) {
                var units = this.getUnits(module);
                var forcedPath = units['forcedPath'];
                return forcedPath;
            }
        };

        this.checkForStatus = function (item, forcedPath) {
            var isDisabled = false;

            if (item && forcedPath) {
                var fpMileStoneIndex = this.getForcePathMilestoneIndex(forcedPath);
                var itemIndex = this.getItemFPIndex(item.id, forcedPath);

                if (fpMileStoneIndex != undefined) {
                    isDisabled = itemIndex > fpMileStoneIndex;
                } else {
                    isDisabled = itemIndex > 0;
                }
            }

            return isDisabled;
        };


        this.getNextItem = function (id, module) {
            var fp, fpIndex, nextItem, contents = [];
            var grpContents = this.getUnitContents(module);

            if (grpContents) {
                for (var i = 0; i < grpContents.length; i++) {
                    var grp = grpContents[i].contents;
                    var prevUnitLength = 0;

                    for (var k = 0; k < i; k++) {
                        prevUnitLength += grpContents[k].contents.length;
                    }

                    for (var j = 0; j < grp.length; j++) {
                        var item = grp[j];

                        if (item) {
                            item.order = parseInt(item.order) + prevUnitLength;
                            contents.push(item);
                        }
                    }
                }

                contents = filter('orderBy')(contents, 'order');

                fp = this.getForcedPath(module);
                fpIndex = this.getItemFPIndex(id);

                if (!fp || !fpIndex) {
                    for (var i = 0; i < contents.length; i++) {
                        if (contents[i].id == id) {
                            nextItem = contents[i + 1].id;
                        }
                    }
                } else {
                    if (fpIndex < fp.length - 1)
                        nextItem = fp[fpIndex + 1];
                    else {
                        for (var i = 0; i < contents.length; i++) {
                            if (contents[i].id == id) {
                                nextItem = contents[i + 1].id;
                            }
                        }
                    }
                }
            }

            return nextItem;
        };

        this.getNextUnit = function (id, moduleId) {
            var contents = this.getUnitContents(moduleId);

            if (contents) {
                for (var i = 0; i < contents.length; i++) {
                    if (contents[i].contents) {
                        for (var j = 0; j < contents[i].contents.length; j++) {
                            if (contents[i].contents[j].id == id) {
                                return contents[i + 1].title;
                            }
                        }
                    }
                }
            }
        };

        this.goToNextItem = function (nextItemId, moduleId) {
            var nextItem = this.getItem(nextItemId, moduleId);
            var ds = dataService.getDataSource();

            if (nextItem) {

                var url = location.url();
                utilityService.saveData(url, ds);
                ds.isResumed = false;
                ds.answeredQuestions = 0;
                ds.attendedTotalQuestions = 0;
                ds.isWhatsNew = false;
                ds.isTheory = false;
                ds.menuSelectionCompleted = undefined;
                rootScope.inProgress = false;

                var type = nextItem.skillType;

                this.selectedType(type, nextItem, ds);
            }
        };

        this.selectedType = function (type, nextItem, ds) {
            var moduleId = nextItem.module;
            var nextItemId = nextItem.id;

            switch (type) {
                    case   'activities':
                    {
                        ds.isWhatsNew = ds.isTheory = false;
                        ds.activity = null;
                        state.go('selectedActivity',
                            {'moduleId': moduleId, 'activityId': nextItemId});

                        break;
                    }
                    case 'dialogues':
                    {
                        localStorageService.clearStorage();
                        ds.isWhatsNew = ds.isTheory = false;

                        state.go('selectedDialogue', {
                            'moduleId': moduleId,
                            'dialogueId': nextItemId,
                            'tabNo': 1
                        });

                        break;
                    }
                    case 'quiz':
                    {
                        localStorageService.clearStorage();
                        ds.isWhatsNew = ds.isTheory = false;

                        state.go('selectedQuiz',
                            {'moduleId': moduleId, 'quizId': nextItemId});

                        break;
                    }
                    case 'whatsNew':
                    {
                        localStorageService.clearStorage();
                        ds.isWhatsNew = true;
                        ds.isTheory = false;

                        state.go('selectedWhatsNew',
                            {'moduleId': moduleId, 'whatsNewId': nextItemId, 'pageNo': 1});

                        break;
                    }
                    case 'theoryVignettes':
                    {
                        localStorageService.clearStorage();
                        ds.isTheory = true;
                        ds.isWhatsNew = false;

                        ds.communicativeSkill = 'theoryVignettes';

                        state.go('selectedTheoryVignette',
                            {'moduleId': moduleId, 'tvId': nextItemId});

                        break;
                    }
                    case 'studySection':
                    {
                        ds.isWhatsNew = ds.isTheory = false;

                        localStorageService.clearStorage();
                        state.go('selectedStudySection',
                            {'moduleId': moduleId, 'studySectionId': nextItemId});

                        break;
                    }
                }
        }

        this.getItemFPIndex = function (itemId, forcedPath) {
            if (itemId && forcedPath) {
                for (var i = 0; i < forcedPath.length; i++) {
                    if (forcedPath[i] == itemId) {
                        return i;
                    }
                }
            }
        };

        this.getForcePathMilestoneIndex = function (forcedPath) {
            var fpIndex = 0;

            if (forcedPath) {
                for (var i = 0; i < forcedPath.length; i++) {
                    var fpId = forcedPath[i];
                    var status = utilityService.getSuspendDataStatus(fpId);

                    if (status != 'success') {
                        fpIndex = i;
                        break;
                    }
                }
            }

            return fpIndex;
        };
    }]);
