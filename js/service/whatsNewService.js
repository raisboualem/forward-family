app.factory('whatsNewService', ['$rootScope', '$filter', 'utilityService', 'contentService', 'localStorageService', 'staticManiFestService',
    function (rootScope, $filter, utilityService, contentService, localStorageService, staticManiFestService) {
        var wnList = {};

        rootScope.$watch(function (){
            return staticManiFestService.loading;
        }, function(isReady) {
            if(!isReady) {
                wnList = staticManiFestService.wnList;
            }
        });		
		
		wnList = {
					"en": {
					  "1": {
						"contents": [
						  {
							"id": "100",
							"module": "1",
							"filename": "m1-esl-whatsnew-what-s-new-nouveautes-lesson2-100",
							"title": "GLOSSARY",
							"type": "lesson2",
							"lang": "en",
							"meta": "lesson2",
							"skillType": "whatsNew",
							"feedback": "true",
							"path": "M01-L01",
							"disableAudio": "true"
						  }
						]
					  }
					}
				  };
		
		
        return {
            getwhatsNewList: function (module) {
                module = module ? module : 1;
                var moduleType = rootScope.isFrenchModule ? 'fr' : 'en';

                return wnList[moduleType][module];
            },
        }
    }]);