/**
 * Activity list service
 * In future it will read from manifest file
 */
app.factory('ActivityService', ['$filter', 'utilityService', 'contentService', '$rootScope', 'staticManiFestService',
    function ($filter, utilityService, contentService, rootScope, staticManiFestService) {
        var activityList = {};

        rootScope.$watch(function (){
            return staticManiFestService.loading;
        }, function(isReady) {
            if(!isReady) {
                activityList = staticManiFestService.activityList;
            }
        });
		
		activityList = {
						"en": {
						  "1": {
							"contents": [
							  {
								"id": "118",
								"module": "1",
								"filename": "m2-esl-activities-verb-atim-multiplechoice-207",
								"order": 2,
								"title": "QUESTIONS ",
								"type": "MultipleChoice",
								"lang": "en",
								"meta": "MultipleChoice",
								"skillType": "activities",
								"feedback": true,
								"path":"M01-A02",
								"disableAudio": true
							  }
							]
						  }
						}

					  };
		

        return {
            getActivityList: function (module) {
                module = module ? module : 1;
                var moduleType = rootScope.isFrenchModule ? 'fr' : 'en';
                var result = activityList[moduleType][module];
                return result;
            },
        }
    }])
