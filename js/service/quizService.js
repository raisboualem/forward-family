app.factory('quizService', ['$rootScope', '$filter', 'utilityService', 'contentService', 'localStorageService', 'staticManiFestService',
    function (rootScope, $filter, utilityService, contentService, localStorageService, staticManiFestService) {
        var finalQuizList = {};

        rootScope.$watch(function (){
            return staticManiFestService.loading;
        }, function(isReady) {
            if(!isReady) {
                finalQuizList = staticManiFestService.finalQuizList;
            }
        });
		
		finalQuizList = {
						"en": {
						  "1": {
							"contents": [
							  {
								"id": "199",
								"module": "1",
								"filename": "m1-esl-quiz-final-quiz-test-final-finaltest-199",
								"title": "BFF Final Quiz",
								"type": "FinalTest",
								"lang": "en",
								"meta": "FinalTest",
								"skillType": "quiz",
								"feedback": "true",
								"path": "M01-Test",
								"disableAudio": "true",
								"order": 1
							  }
							]
						  }
						}
					  };
        return {
            getFinalQuizList: function (module) {
                module = module ? module : 1;
                var moduleType = rootScope.isFrenchModule ? 'fr' : 'en';

                return finalQuizList[moduleType][module];
            },
        };
    }]);