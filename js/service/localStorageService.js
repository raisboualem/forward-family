app.service('localStorageService', function () {
    this.setItem = function (key, value, persistAcrossSessions) {
        if (typeof(Storage) != "undefined") {
            if (persistAcrossSessions)
                localStorage.setItem(key, value);
            else
                sessionStorage.setItem(key, value);
        }
    };

    this.removeItem = function (key,persistAcrossSessions) {
        if (typeof(Storage) != "undefined") {
            if (persistAcrossSessions) {
                localStorage.removeItem(key);
            }else{
                sessionStorage.removeItem(key);
            }
        }
    };

    this.getItem = function (key, persistAcrossSessions) {
        if (typeof(Storage) != "undefined") {
            if (persistAcrossSessions) {
                var item = localStorage.getItem(key);
                return item;
            } else {
                var item = sessionStorage.getItem(key);
                return item;
            }
        }
    };

    this.clearStorage = function (persistAcrossSessions) {
        if (typeof(Storage) != "undefined") {
            if (persistAcrossSessions)
                localStorage.clear();
            else
                sessionStorage.clear();
        }
    }
});