app.factory('dialogueService', ['$rootScope', '$filter', 'utilityService', 'contentService', 'localStorageService', 'staticManiFestService',
    function (rootScope, $filter, utilityService, contentService, localStorageService, staticManiFestService) {
        var dialogueList = {};

        rootScope.$watch(function (){
            return staticManiFestService.loading;
        }, function(isReady) {
            if(!isReady) {
                dialogueList = staticManiFestService.dialogueList;
            }
        });
		
		dialogueList = {
						"en": {
						  "1": {
							"contents": [
							  {
								"id": "151",
								"module": "1",
								"filename": "m1-esl-dialogues-business-scene-1-scene-d-affaires-1-dialogue-151",
								"order": 1,
								"title": "BFF Business Scene 1",
								"type": "Dialogue",
								"lang": "en",
								"meta": "Dialogue",
								"skillType": "dialogues",
								"feedback": "true",
								"path":"M01-S01",
								"disableAudio": "true"
							  },
							  {
								"id": "152",
								"module": "1",
								"filename": "m1-esl-dialogues-business-scene-2-scene-d-affaires-2-dialogue-152",
								"order": 2,
								"title": "BFF Business Scene 2",
								"type": "Dialogue",
								"lang": "en",
								"meta": "Dialogue",
								"skillType": "dialogues",
								"feedback": "true",
								"path":"M01-S02",
								"disableAudio": "true"
							  }
							]
						  }
						}
					  };
		
		
        return {
            getDialogueList: function (module) {
                module = module ? module : 1;
                var moduleType = rootScope.isFrenchModule ? 'fr' : 'en';

                var result = dialogueList[moduleType][module];
                return result;
            },
        };
    }]);
