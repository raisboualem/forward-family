app.service('utilityService', ['$rootScope', '$location', '$anchorScroll', 'dataService', '$stateParams', '$timeout', '$compile', '$filter','$cookies','ngAudio','ngDialog',
    function (rootScope, location, anchorScroll, dataService, stateParams, $timeout, compile, $filter, cookies,  ngAudio, ngDialog) {
        var mainContent = 'mainContent';
        var ds = dataService.getDataSource();
        var audioVar = '';

        this.setMainContentId = function (mainContentId) {
            if (mainContentId && mainContentId != '') {
                mainContent = mainContentId;
            }
        };

        this.goToMainContent = function () {
            location.hash(mainContent);
            anchorScroll();
            location.hash('');
        };

        this.getRandomNumber = function (length) {
            var random = Math.floor((Math.random() * length * 1000000) + 1);
            return random;
        };

        this.getSuspendData = function () {
            var suspendData = dataService.getDataSource().suspendData;

            if (suspendData && suspendData != "null")
                return suspendData;
        };

        this.setSuspendData = function (suspendData) {
            if (suspendData)
                dataService.getDataSource().suspendData = suspendData;

        };

        this.checkForSessionAlive= function () {
          var statusCode = cookies.get('statusValue');
             if (statusCode == '-40'){
                 var model = ngDialog.open({
                     template: 'templates/modal/sessionAlive.html',
                     controller: 'sessionAliveController',
                     showClose: false,
                     ariaAuto: true
                 });

             }
        };

        this.getSuspendDataStatus = function (id) {
            var suspendData = this.getSuspendData();
            var status;

            if (suspendData) {
                var activityData = suspendData[id];
                if (activityData) {
                    status = activityData.status;
                }
            }

            return status;
        };

        this.setSuspendDataStatus = function (id, status) {
            var suspendData = this.getSuspendData();
            if (suspendData) {
                var activityData = suspendData[id];
                if (activityData) {
                    activityData.status = status;
                } else {
                    suspendData[id] = {status: status};
                }
            } else {
                suspendData = {};
                suspendData[id] = {status: status};
                this.setSuspendData(suspendData);
            }
        };

        this.setActivityResumeData = function (id, location, answeredQuestions, attendedTotalQuestions, totalScore,shuffle_acivity,results) {

            var suspendData = this.getSuspendData();
            if (suspendData) {
                var activityData = suspendData[id];
                if (activityData) {
                    if (!activityData.resumeData) {
                        activityData.resumeData = {
                            location: location,
                            answeredQuestions: answeredQuestions,
                            attendedTotalQuestions: attendedTotalQuestions,
                            totalScore: totalScore,
                            shuffle_acivity: shuffle_acivity,
                            results: results,
                        }
                    }
                    else if (activityData.resumeData) {
                        activityData.resumeData.answeredQuestions = answeredQuestions;
                        activityData.resumeData.attendedTotalQuestions = attendedTotalQuestions;
                        activityData.resumeData.totalScore = totalScore;
                        activityData.resumeData.location = location;
                        activityData.resumeData.shuffle_acivity = shuffle_acivity;
                        activityData.resumeData.results = results;
                    }
                } else {
                    suspendData[id] = {resumeData: {location: location}};
                }
            } else {
                suspendData = {};
                suspendData[id] = {resumeData: {location: location}};
                this.setSuspendData(suspendData);
            }
        };

        this.getItemFPIndex = function (itemId, forcedPath) {
            if (itemId && forcedPath) {
                for (var i = 0; i < forcedPath.length; i++) {
                    if (forcedPath[i] == itemId) {
                        return i;
                    }
                }
            }
        };

        this.mergeSuspendData = function (newData, moduleId) {
            for (var id in newData) {
                if (newData.hasOwnProperty(id) && newData[id] && id != 'lastAccessLocation') {
                    this.removeSuspendData(id);
                    this.addSuspendData(id, newData[id]);
                }
            }
        };

        this.setSuspendDataEOMAttempts = function (id, attempts, isPassed) {
            var suspendData = this.getSuspendData();
            if (suspendData) {
                var activityData = suspendData[id];
                if (activityData) {
                    activityData.isEOM = true;
                    activityData.attempts = attempts;
                    activityData.isPassed = isPassed;
                } else {
                    suspendData[id] = {attempts: attempts, isEOM: true, isPassed: isPassed};
                }
            } else {
                suspendData = {};
                suspendData[id] = {attempts: attempts, isEOM: true, isPassed: isPassed};
                this.setSuspendData(suspendData);
            }
        }

        this.getSuspendDataEOMAttempts = function (id) {
            var suspendData = this.getSuspendData();
            var attempts;

            if (suspendData) {
                var activityData = suspendData[id];
                if (activityData) {
                    attempts = activityData.attempts;
                }
            }

            return attempts;
        }

        this.setAnsweredQuestions = function (id, answeredQuestions) {

            var suspendData = this.getSuspendData();
            if (suspendData) {
                var activityData = suspendData[id];
                if (activityData && activityData.resumeData) {
                    activityData.resumeData.answeredQuestions = answeredQuestions;
                    
                }
            }
        };

        this.saveData = function (prev, ds) {
            //Check if its from activity
            var pattern = new RegExp("(.+(activities).+)$");
            var isActivity = pattern.test(prev);
            if (isActivity) {
                var activityId = ds.activityId;
                var score = ds.activityScore;

                //Set location to resume from launch page
                var destination = prev.substr(prev.indexOf('#') + 1, prev.length).trim();
                this.setLastAccessLocation(destination);

                if (activityId && score)
                    ScormProcessSetScore(activityId, score);

                ds.activityId = ds.activityScormStatus = ds.activityScore = null;
                ScormProcessCommit();
                this.checkForSessionAlive();
            } else {
                //Check if its from EOM
                var quizPattern = new RegExp("(.+(quiz).+)$");
                var isQuiz = quizPattern.test(prev);
                if (isQuiz) {
                    var quizId = ds.quizId;
                    var score = ds.quizScore;

                    if (quizId && score)
                        ScormProcessSetScore(quizId, score);

                    ds.quizId = ds.quizScormStatus = ds.quizScore = null;
                }

                this.removeLastAccessLocation();
            }

            var suspendData = JSON.stringify(ds.suspendData);
            ScormProcessSetSuspendState(suspendData);
            
            if (ds.suspendData) {
              cookies.put('font', rootScope.fontSize, {'path': '/'});
              cookies.put('audioCaption', rootScope.audioCaptionControl == false ? 0 : rootScope.audioCaptionControl , {'path': '/'});
              cookies.put('videoCaption', rootScope.videoCaptionControl == false ? 0 : rootScope.videoCaptionControl, {'path': '/'});
              cookies.put('imageCaption', rootScope.imageCaptionControl == false ? 0 : rootScope.imageCaptionControl, {'path': '/'});
              cookies.put('accessibilityMode', rootScope.accessibilityModeControl == false ? 0 :  rootScope.accessibilityModeControl, {'path': '/'});
              cookies.put('systemLanguage', rootScope.sysLang == false? 0 : rootScope.sysLang  , {'path': '/'});
              cookies.remove('statusValue');
            }

            cookies.put('iSpeakLanguage', rootScope.translateLang, {'path': '/'});

            if (!isActivity)
                ScormProcessCommit();
                this.checkForSessionAlive();
        };

        this.setAttendedTotalQuestions = function (id, attendedTotalQuestions) {
            var suspendData = this.getSuspendData();
            if (suspendData && attendedTotalQuestions) {
                var activityData = suspendData[id];
                if (activityData && activityData.resumeData) {
                    activityData.resumeData.attendedTotalQuestions = attendedTotalQuestions;
                }
            }
        };

        this.setTotalScore = function (id, totalScore) {
            var suspendData = this.getSuspendData();
            if (suspendData) {
                var activityData = suspendData[id];
                if (activityData && activityData.resumeData) {
                    activityData.resumeData.totalScore = totalScore;
                }
            }
        };

        this.addInteraction = function (activityId, interaction) {
            var activityResumeData = this.getResumeData(activityId);

            if (!activityResumeData) {
                var suspendData = this.getSuspendData();
                var activityData = suspendData[activityId];
                activityData.resumeData = {data: {interactions: []}};
                activityResumeData = activityData.resumeData;
            }

            if (!activityResumeData.data)
                activityResumeData.data = {};
            if (!activityResumeData.data.interactions)
                activityResumeData.data.interactions = [];

            activityResumeData.data.interactions.push(interaction);
        };

        this.checkForResume = function (id) {
            var suspendData = this.getSuspendData();
            var isResumable = false;

            if (suspendData) {
                var activityData = suspendData[id];
                if (activityData) {
                    var resumeData = activityData.resumeData;
                    isResumable = resumeData ? true : false;
                }
            }

            return isResumable;
        };

        this.getResumeData = function (id) {
            var suspendData = this.getSuspendData();
            var resumeData;

            if (suspendData) {
                var activityData = suspendData[id];
                if (activityData) {
                    resumeData = activityData.resumeData;
                }
            }

            return resumeData;
        };

        this.setLastAccessLocation = function (location) {
            var suspendData = this.getSuspendData();

            if (suspendData) {
                suspendData.lastAccessLocation = location;
            }
        };

        this.removeResumeData = function (id) {
            var suspendData = this.getSuspendData();

            if (suspendData) {
                var activityData = suspendData[id];
                if (activityData && activityData.resumeData) {
                    delete activityData['resumeData'];
                }
            }
        };

        this.removeSuspendData = function (id) {
            var suspendData = this.getSuspendData();

            if (suspendData) {
                if (suspendData[id] && !suspendData[id].isEOM) {
                    delete suspendData[id];
                }
            }
        };

        this.addSuspendData = function (id, data) {
            var suspendData = this.getSuspendData();

            if (suspendData) {
                if (data && !data.isEOM) {
                    suspendData[id] = data;
                } else if (data && data.isEOM) {
                    if (data.attempts) {
                        if ((suspendData[id].attempts && !suspendData[id].isPassed &&
                            parseInt(data.attempts) > parseInt(suspendData[id].attempts) ) ||
                            (suspendData[id].attempts == undefined)) {
                            suspendData[id].attempts = data.attempts;
                        }
                    }

                    if (data.status)
                        suspendData[id].status = data.status;
                }
            }
        };

        this.removeLastAccessLocation = function () {
            var suspendData = this.getSuspendData();

            if (suspendData) {
                delete suspendData['lastAccessLocation'];
            }
        };

        this.getNextItem = function (collection, id, fp) {
            var nextItem;
            var fpIndex = this.getItemFPIndex(id);

            if (!fp || !fpIndex) {
                for (var i = 0; i < collection.length; i++) {
                    if (collection[i].id == id) {
                        var order = collection[i].order;

                        if (order) {
                            var nextItemOrder = parseInt(order) + 1;
                            for (var j = 0; j < collection.length; j++) {
                                if (collection[j].order == nextItemOrder) {
                                    nextItem = collection[j].id;
                                    break;
                                }
                            }
                        } else {
                            nextItem = collection[i + 1].id;
                        }
                    }
                }
            } else {
                var itemFPIndex = this.getItemFPIndex(id, fp);

                if (itemFPIndex < fp.length - 1)
                    nextItem = fp[itemFPIndex + 1];
            }

            return nextItem;
        };

        this.refreshSuspendData = function (moduleId) {
            var newSuspendState = ScormProcessGetSuspendState();
            if(newSuspendState)
            var jsonSuspendData = JSON.parse(newSuspendState);
            this.mergeSuspendData(jsonSuspendData, moduleId);
        };

        this.getAssetsCommonPath = function (fileName) {
            var module = rootScope.isFrenchModule ? 'fsl' : 'esl';
            var path = 'data/' + module + '/' + 'module' + stateParams.moduleId + '/xml/' + fileName;
            return path;
        };

        this.getFilePath = function (fileName) {
            var path = this.getAssetsCommonPath(fileName) + '.xml';
            return path;
        };

        this.getImagePath = function (fileName, imagePath) {
            return this.getAssetsCommonPath(fileName) + '/image/' + imagePath;
        };

        this.getAudioPath = function (fileName, audioPath) {
            return this.getAssetsCommonPath(fileName) + '/audio/' + audioPath;
        };

        this.getVideoPath = function (fileName, videoPath) {
            return this.getAssetsCommonPath(fileName) + '/video/' + videoPath;
        };

        this.setAccessibilityData = function () {
            var suspendAccessData = this.getSuspendData();
            cookies.put('systemLanguage', rootScope.sysLang, {'path': '/'});

            if (suspendAccessData) {
                // suspendAccessData.theme = rootScope.selectedBodyClass;
                suspendAccessData.font = rootScope.fontSize;
                suspendAccessData.videoCaption = rootScope.videoCaptionControl;
                suspendAccessData.audioCaption = rootScope.audioCaptionControl;
                suspendAccessData.imageCaption = rootScope.imageCaptionControl;
                suspendAccessData.accessibilityMode = rootScope.accessibilityModeControl;
                suspendAccessData.systemLanguage = rootScope.sysLang;
            }
        };

        this.getAccessibilityData = function () {
            var suspendAccessData = this.getSuspendData();
            var theme, accessibilityMode, font,
                videoCaption,audioCaption,imageCaption;

            if (suspendAccessData) {
                theme = suspendAccessData.theme;
                font = suspendAccessData.font;
                videoCaption = suspendAccessData.videoCaption;
                audioCaption = suspendAccessData.audioCaption;
                imageCaption = suspendAccessData.imageCaption;
                accessibilityMode = suspendAccessData.accessibilityMode;

            }
            return {
                "theme": theme,
                "font": font,
                "videoCaption": videoCaption,
                "audioCaption": audioCaption,
                "imageCaption": imageCaption,
                "accessibilityMode": accessibilityMode
            };
        };

        this.getFileName = function (filePath) {
            if (filePath) {
                var temp = filePath.split('/');
                filePath = temp[temp.length - 1];
            }

            return filePath;
        };

        this.hideTooltip = function () {
            ds.showTooltip = false;
        };

        this.pauseAudio = function (audioElement) {
            if(audioVar) {
              ds.showTooltip = false;
              audioVar.pause();
            }
        };

        this.resumeAudio = function (audioElement) {
            audioVar.play();
            ds.showTooltip = true;
        };

        this.stopAudio = function (audioElement) {
             if(audioVar){
                audioVar.stop();
             }
        };

        this.setPageTitle = function (title) {
            var parentWindow = window.parent;
            var originLocation = window.location.origin;
            var module = stateParams.moduleId;

            var langTitle = rootScope.isFrenchModule ? 'Pour l’amour du français' : 'For the Love of English';
            var title = langTitle + ' - ' + 'Module ' + module + ' - ' + title;

            if (parentWindow && originLocation)
                parentWindow.postMessage(title, originLocation);
        };

        this.playFeedbackAudio = function (result, audioId) {
            var musicSrc = result ? 'correct.mp3' : 'incorrect.mp3';
            musicSrc = 'assets/sounds/' + musicSrc;
            var audioElement;
            if (audioId)
                audioElement = document.getElementById(audioId);
            else
                audioElement = document.getElementById('load_audio');

            var audio = angular.element(audioElement);
            audio.attr("src", musicSrc);
            audio[0].play();
        };

        this.playAudio = function (src, tooltipIndex, event, filename, audioElement) {
            $timeout.cancel(ds.cancelTimeout);

            if (event) {
                var element = angular.element(event.currentTarget);
                element.focus();
            }

            return function (utilityService) {
                $timeout(function () {
                    ds.showTooltip = true;
                    ds.focus = true;
                    ds.toolTipIndex = tooltipIndex;

                    if (!audioElement) {
                        audioElement = document.getElementById('#load_audio');
                    }
                    var audio = angular.element(audioElement);
                    var path = utilityService.getAudioPath(filename, src);
                    var name = utilityService.getFileName(path);
                    audioVar = ngAudio.load(path);
                    setTimeout(function () {
                      audioVar.play();
                    }, 700);
                     rootScope.$broadcast('reset-audio-focus', src);
                }, 10);
            }(this);

        };

        this.setButtonInteraction = function (id) {
            var elem = document.getElementById(id);
            var button = angular.element(elem);
            if(button)
                button.click();
        };

        this.setInputInteraction = function (id, value) {
            var input = document.getElementById(id);
            var angularInput = angular.element(input);
            var ngModelCtrl = angularInput.controller('ngModel');
            if(input){
                angularInput.value = value;
                ngModelCtrl.$setViewValue(value);
                ngModelCtrl.$viewValue = value;
                ngModelCtrl.$render();

                var keydown = document.createEvent('HTMLEvents');
                keydown.initEvent("keydown", false, true);
                keydown.which = 13;
                input.dispatchEvent(keydown);
            }
        }

        this.loadSection = function (interactions, scope, jsonActivity, selectedActivity, container, pageIndex, pages) {
            var pageNo = pages[pageIndex];
            var section = Array.isArray(jsonActivity.SECTIONS.SECTION) ? jsonActivity.SECTIONS.SECTION[pageNo - 1] : jsonActivity.SECTIONS.SECTION;
            var sectionInteractions = $filter('filter')(interactions, {'pageNo': pageNo});
            scope.pageNo1 = pageNo;
            scope.section1 = section;
            scope.selectedActivity1 = selectedActivity;

            childScope = scope.$new();
            var contentTemplate = "<question current-page-No='pageNo1' current-section='section1' activity='jsonActivity' selected-activity='selectedActivity1'></question>";
            var question = compile(contentTemplate)(childScope)[0];
            question.style.display = 'none';
            container.appendChild(question);

            var cnt = 0;

            for (var i = 0; i < sectionInteractions.length; i++) {
                var interaction = sectionInteractions[i];

                var closure = function (interaction) {
                    var utilityService = this;
                    $timeout(function () {
                        if (interaction.type == 'BUTTON' ||
                            interaction.type == 'MULTICHOICE' ||
                            interaction.type == 'DROPDOWN' ||
                            interaction.type == 'CHECKBOX') {
                            utilityService.setButtonInteraction(interaction.id);
                            cnt++;

                            if (cnt == sectionInteractions.length) {
                                if (pageIndex == pages.length - 1) {
                                    utilityService.resumeStateLoaded(scope, container, jsonActivity);
                                }
                                else {
                                    container.removeChild(question);
                                    utilityService.loadSection(interactions, scope, jsonActivity, selectedActivity, container, pageIndex + 1, pages);
                                }
                            }
                        } else if (interaction.type == 'INPUT') {
                            utilityService.setInputInteraction(interaction.id, interaction.value);
                            cnt++;

                            if (cnt == sectionInteractions.length) {
                                if (pageIndex == pages.length - 1) {
                                    utilityService.resumeStateLoaded(scope, container, jsonActivity);
                                }
                                else {
                                    container.removeChild(question);
                                    utilityService.loadSection(interactions, scope, jsonActivity, selectedActivity, container, pageIndex + 1, pages);
                                }
                            }
                        } else if (interaction.type == 'DRAGDROP') {
                            var id = 'popover_' + interaction.id + '_' + interaction.source;
                            var dropItem = document.getElementById(id);
                            if(dropItem)
                            dropItem.click();
                            cnt++;

                            if (cnt == sectionInteractions.length) {
                                if (pageIndex == pages.length - 1) {
                                    utilityService.resumeStateLoaded(scope, container, jsonActivity);
                                }
                                else {
                                    container.removeChild(question);
                                    utilityService.loadSection(interactions, scope, jsonActivity, selectedActivity, container, pageIndex + 1, pages);
                                }
                            }
                        }
                    }, 1000);
                };

                closure.call(this, interaction);
            }
        };

        this.resumeStateLoaded = function (scope, container, jsonActivity) {
            scope.ds.isResuming = false;
            childScope.$destroy();
            document.body.removeChild(container);
            scope.$emit('resumeStateLoaded', jsonActivity);
        };

        this.modifyActivityWithResumeData = function (scope, selectedActivity, jsonActivity, resumeData) {
            if (jsonActivity && resumeData) {
                if (resumeData.data && resumeData.data.interactions) {
                    var interactions = resumeData.data.interactions;
                    var container = document.createElement('div');
                    container.setAttribute('id', 'resumeContainer');
                    document.body.appendChild(container);
                    var pages = [];
                    scope.ds.isResuming = true;

                    for (var i = 0; i < interactions.length; i++) {
                        var interaction = interactions[i];
                        var pageNo = interaction.pageNo;

                        if (pages.indexOf(parseInt(pageNo)) == -1) {
                            pages.push(pageNo);
                        }
                    }
                    var pages = pages.filter(function(item, i, ar){ return ar.indexOf(item) === i; });

                    this.loadSection(interactions, scope, jsonActivity, selectedActivity, container, 0, pages);
                }
            }
        };

        // enable video caption by default
        var videoCaptionControl = 1;
      
        rootScope.videoCaptionControl = videoCaptionControl;
        rootScope.imageCaptionControl = 1;

    }]);