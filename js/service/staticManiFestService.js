(function () {
    'use strict';

    angular
        .module('quizApp')
        .service('staticManiFestService',  staticManiFestService)
    ;

    function staticManiFestService ($http) {

        var service = {
            loading           : false,
            activityList      : {},
            contentObjectList : {},
            dialogueList      : {},
            finalQuizList     : {},
            wnList            : {}
        };

        getStaticManiFest();
        return service;

        function getStaticManiFest () {
            if (service.loading) return;
            service.loading = true;
            return $http.get('static_manifest.json').then(function(resp) {
                service.loading = false;
                service.activityList = resp.data.activityList;
                service.contentObjectList = resp.data.contentObjectList;
                service.dialogueList = resp.data.dialogueList;
                service.finalQuizList = resp.data.finalQuizList;
                service.wnList = resp.data.wnList;
            });
        };
    };
})();