app.service('localizationService', function () {
    var localizationResources = {
        'en': {
            'THANK_YOU':'Congratulations, your assessment is complete. High scores show areas of strength. Lower scores indicate where you and your family can make improvements',
            'INSTRUCTION_TRIAGE':'Before accessing your results, please complete the following five questions. Your details are anonymous.',
            'OBJECTIVES': 'OBJECTIVES',
            'MAIN_TITLE_OBJECTIVE': 'Objectives',
            'ACCESSIBILITY': 'SETTINGS',
            'ACCESSIBILITY_CAPITALIZE': 'Accessibility',
            'COMMUNICATION_SKILLS': 'Communication Skills',
            'GRAMMATICAL_SKILLS': 'Grammatical Skills',
            'MODULE': 'Module',
            'WHATS_NEW_PANEL_TITLE':'MODULE',
            'START': 'START',
            'WELCOME': 'Welcome',
            'SKIP_MAIN': 'Skip to main content',
            'VERSION': 'Version',
            'SUCCESS': 'Completed',
            'IN_PROGRESS': 'In Progress',
            'TRY_IT': 'Not Started',
            'LEGEND': 'LEGEND',
            'DIALOGUE': 'Dialogue',
            'DIALOGUES': 'TRIAGE',
            'ACTIVITY': 'Activity',
            'ACTIVITIES': 'QUESTION',
            'QUIZ': 'FEEDBACK',
            'WHATS_NEW': 'GLOSSARY',
            'WHATSNEW': 'What\'s New',
            'START_FROM_BEGINNING': 'START FROM BEGINNING',
            'START_ASSESSMENT': 'START ASSESSMENT',
            'START_FROM_WHERE_I_LEFT': 'RESUME',
            'SHOW_IMAGE': 'Show Image',
            'DESCRIPTION': 'DESCRIPTION',
            'DASHBOARD': 'Dashboard',
            'DESCRIPTION_BUTTON':'Dashboard',
            'YOUR_SCORE': 'Results',
            'YOUR_TRIAGE': 'Triage',
            'PREVIOUS': '<< Previous',
            'NEXT': 'Next >>',
            'NEXT_QUESTION': 'Next Question',
            'TRANSLATION': 'Translation',
            'COLOUR_CODED': 'Colour-Coded',
            'START_TEST': 'Start Test',
            'CANCEL': 'Cancel',
            'LAUNCH_RESUME': 'Do you want to continue where you left off?',
            'REMAINING_ATTEMPTS': 'Attempts Remaining',
            'UNIT1': 'Unit 1',
            'UNIT2': 'Unit 2',
            'UNIT3': 'Unit 3',
            'UNIT4': 'Unit 4',
            'UNIT5': 'Unit 5',
            'UNIT6': 'Unit 6',
            'UNIT7': 'Unit 7',
            'UNIT8': 'Unit 8',
            'UNIT9': 'Unit 9',
            'UNIT10': 'Unit 10',
            'UNIT11': 'Unit 11',
            'SIMULATION': 'Simulation',
            'REVIEW': 'Review',
            'OK': 'Yes',
            'NOMENU': 'No, go to main menu',
            'MAIN_MENU': 'MAIN MENU',
            'NEXT_ITEM': 'Go to next dialogue',
            'NEXT_ACTIVITY': 'Go to next activity',
            'DISPLAY_THEME': 'Display Theme',
            'ACCESSIBILITY_MENU': 'Accessibility Menu',
            'FONT_RATIO': 'Font Size',
            'CAPTIONS': 'Text Captions',
            'AUDIO_CAPTION_LABEL': 'Audio',
            'VIDEO_CAPTION_LABEL': 'Video',
            'IMAGE_CAPTION_LABEL': 'Image',
            'APPLY': 'Apply',
            'NEXT_UNIT': 'Next Unit',
            'SELECT': 'Select',
            'CORRECT_ANSWER': '{0} is correct answer',
            'WRONG_ANSWER': '{0} is wrong answer.',
            'WRONG_ANSWER_WITH_FEEDBACK': '{0} is wrong answer.Correct answer is {1}.',
            'WRONG_ANSWER_ZERO_ATTEMPT': 'All attempts are used. Correct answer is {0}',
            'INSTRUCTIONS': 'INSTRUCTIONS',
            'DIALOGUE_INSTRUCTIONS':'Instructions',
            'CLOSE_TEST': 'Close the test',
            'PRINT': 'Save and download the content to a non-accessible PDF document',
            'CONTENT_TOP': 'Navigate to the CONTENTS AT A GLANCE page',
            'MENU_LEGEND': 'Navigate to the LEGEND page',
            'GO_BACK_TO_STUDY_SECTION': 'Navigate back to Study Section',
            'WHATS_NEW_FRENCH': 'Simultaneously display both English and French',
            'WHATS_NEW_ENGLISH': 'Display only English',
            'CANCEL_MODAL': 'Cancel modal instruction',
            'CLICK_HERE': 'Click Here',
            'DIALOGUE_LISTEN_REPEAT_INST': 'Dialogue listen and repeat consist of three buttons : Previous, Play, Forward.'
            + 'to play audio learner should navigate to play button using tab and hit '
            + 'enter. to play next audio learner should navigate to forward arrow and '
            + 'hit enter and then play audio.to go to previous audio learner should navigate'
            + 'to backward arrow, hit enter and play audio. to navigate to any item use'
            + 'tab key and to navigate backward use shift + tab key.',
            'DIALOGUE_AUDIO_QUESTION': 'Read the text of the question',
            'STUDY_SECTION': 'Study section',
            'STUDYSECTION': 'Study section',
            'THEORY_VIGNETTE': 'Theory Vignette',
            'THEORYVIGNETTES': 'Theory Vignette',
            'TRYIT_STATUS': 'has not been started',
            'INPROGRESS_STATUS': 'is still in progress',
            'SUCCESS_STATUS': 'has been completed',
            'MAIN_MENU_TITLE': 'Main Menu',
            'PRINT_LABEL_WN' : 'Print',
            'CONTENT_LABEL_WN' : 'Content At Glance',
            'LEGEND_LABEL_WN' : 'Legend',
            'STUDY_LABEL_WN' : 'Study Section',
            'LANGUAGE_LABEL_WN' : 'Language Translation',
            'SMALLEST_FONTSIZE': 'Click here for Font Size to be Smallest',
            'SMALL_FONTSIZE':'Click here for Font Size to be Small',
            'MEDIUM_FONTSIZE':'Click here for Font Size to be Medium',
            'LARGE_FONTSIZE':'Click here for Font Size to be Large',
            'LARGEST_FONTSIZE':'Click here for Font Size to be Largest',
            'INACTIVE_AUDIO' : 'Click here to inactivate the caption in Audio section',
             'ACTIVE_AUDIO' : 'Click here to activate the caption in Audio section',
              'INACTIVE_VIDEO' :  'Click here to inactivate the caption in Video section',
              'ACTIVE_VIDEO' : 'Click here to activate the caption in Video section',
              'INACTIVE_IMAGE':'Click here to inactivate the caption in Image section',
              'ACTIVE_IMAGE':'Click here to activate the caption in Image section',
              'DISABLED_BUTTON' : 'button is disabled',
            'RESUMING_WAIT':'Resuming please wait..',
            'CLOSE_INSTRUCTION_POPUP':'Close dialogue instruction popover',
            'ACCESSIBILITY_MODE_CAPTION_LABEL':'Accessibility mode',
            'PLAY_AUDIO':'Play Audio',
            'PLAY_AUDIO_LAB':'Press to play Audio',
            'IMAGE_QUESTION':'See the image to answer the question',
            'WARNING_POPUP':'Warning!',
            'WARNING_POPUP_TEXT':'You have been inactive for 15 minutes. If you do not resume activity immediately, your session will be ended.',
            'TIMEDOUT_POPUP':'Goodbye!',
            'TIMEDOUT_POPUP_TEXT':'Your session has been ended and your progress has been saved. You may close the browser tab.',
            'THIS':'This',
            'DIALOGUES_TITLE':'"Dialogue"',
            'WHATSNEW_TITLE':'"What\'s New"',
            'ACTIVITIES_TITLE':'"Activity"',
            'QUIZ_TITLE':'"Final Quiz"',
            'INNER_QUIZ_TITLE':'Final Quiz',
            'SUPPORT':'SUPPORT',
            'SUPPORT_LABEL':'By clicking on Support you will be redirected to a site that may not be accessible',
            'QUESTION':'Question',
            'I_SPEAK': 'I Speak',
            'SHOW_TRANSLATION':'Show Translation',
            'SHOW_INSTRUCTIONS':'Show Instructions',
            'HIDE_INSTRUCTIONS':'Hide Instructions',
            'SYSTEM_LANGUAGE':'System Language',
            'LANGUAGE':'Language',
            'SETTINGS':'Settings',
            'FINAL_QUIZ_SECTION_LABEL':'Section',
            'MINUTE_TAKEN':'Minute',
            'SESSION_ALIVE':'Your connection to the server has been lost. We apologize for the inconvenience. Please click the button below to close the browser tab.',
            'CLOSE_TAB':'Close browser tab',
            'YOUR_SCORE':'Results',
            'YOUR_TRIAGE': 'Triage',
            'RESUME_WAIT':'We apologize for the wait, your activity is loading.',
            'DEFINITION':'Definition',
            'MULTIPLE_TAB':'Please note that only one Module session may be active at any given time. Please close the browser tab containing your active Module session first before attempting to begin a new Module session.',
            'PLACEMENT_TEST':'PLACEMENT TEST',
            'SUBMIT':'SUBMIT',
			'FRENCH':'FRENCH',
			'AUDIO_PLAY':'AUDIO IS NOT AVAILABLE',
			'IE_POPUP_TEXT':'Please note that Internet Explorer is a legacy browser that has been discontinued by Microsoft. As a result, you may experience some issues while using the LRDG Modules using Internet Explorer. For the best possible user experience, we recommend switching browsers to Google Chrome, Mozilla Firefox or Apple Safari (MacOS only).',
			'I_UNDERSTAND':'I understand',
        },
        'fr': {
            'OBJECTIVES': 'OBJECTIFS',
            'MAIN_TITLE_OBJECTIVE': 'Objectives',
            'ACCESSIBILITY': 'PARAMÈTRES',
            'COMMUNICATION_SKILLS': 'Compétences en communication',
            'GRAMMATICAL_SKILLS': 'Compétences grammaticales',
            'MODULE': 'Module',
            'WHATS_NEW_PANEL_TITLE':'MODULE',
            'START': 'COMMENCER',
            'WELCOME': 'Bienvenue',
            'SKIP_MAIN': 'Passer au contenu principal',
            'VERSION': 'Version',
            'SUCCESS': 'Accompli',
            'IN_PROGRESS': 'En cours',
            'TRY_IT': 'Pas commencé',
            'LEGEND': 'LÉGENDE',
            'DIALOGUES': 'Dialogues',
            'DIALOGUE': 'Dialogue',
            'ACTIVITY': 'Activité',
            'ACTIVITIES': 'Activités',
            'QUIZ': 'Test final',
            'WHATS_NEW': 'Nouveautés',
            'WHATSNEW': 'Nouveautés',
            'START_FROM_BEGINNING': 'COMMENCER AU DÉBUT',
            'START_FROM_WHERE_I_LEFT': 'REPRENDRE',
            'SHOW_IMAGE': "Afficher l'image",
            'DESCRIPTION': 'DESCRIPTION',
            'DESCRIPTION_BUTTON':'Description',
            'YOUR_SCORE': 'Votre résultat',
            'YOUR_TRIAGE': 'Triage',
            'PREVIOUS': '<< Précédent',
            'NEXT': 'Suivant >>',
            'NEXT_QUESTION': 'Prochaine question',
            'TRANSLATION': 'Traduction',
            'COLOUR_CODED': 'Code couleur',
            'START_TEST': 'Commencer le test',
            'CANCEL': 'Annuler',
            'LAUNCH_RESUME': 'Voulez-vous continuer là où vous avez quitté?',
            'REMAINING_ATTEMPTS': 'Tentatives Restantes',
            'UNIT1': 'Cours 1',
            'UNIT2': 'Cours 2',
            'UNIT3': 'Cours 3',
            'UNIT4': 'Cours 4',
            'UNIT5': 'Cours 5',
            'UNIT6': 'Cours 6',
            'UNIT7': 'Cours 7',
            'UNIT8': 'Cours 8',
            'UNIT9': 'Cours 9',
            'UNIT10': 'Cours 10',
            'UNIT11': 'Cours 11',
            'SIMULATION': 'Simulation',
            'REVIEW': 'Révision',
            'OK': 'Oui',
            'NOMENU': 'Non, aller au menu principal',
            'MAIN_MENU': 'MENU PRINCIPAL',
            'NEXT_ITEM': 'Passer au prochain dialogue',
            'NEXT_ACTIVITY': 'Passer à l\'activité suivante',
            'DISPLAY_THEME': 'Affichage Thème',
            'ACCESSIBILITY_MENU': 'Menu d’accessibilité',
            'FONT_RATIO': 'Grosseur de la police',
            'CAPTIONS': 'Sous-titres',
            'AUDIO_CAPTION_LABEL': 'Audio',
            'VIDEO_CAPTION_LABEL': 'Vidéo',
            'IMAGE_CAPTION_LABEL': 'Image',
            'APPLY': 'Appliquer',
            'ACCESSIBILITY_CAPITALIZE': 'Accessibilité',
            'NEXT_UNIT': 'Cours suivant',
            'SELECT': 'Sélectionner',
            'CORRECT_ANSWER': '{0} est la bonne réponse.',
            'WRONG_ANSWER': 'La réponse {0} est erronée.',
            'WRONG_ANSWER_WITH_FEEDBACK': '{0} est une réponse erronée.La réponse correcte est {1}.',
            'WRONG_ANSWER_ZERO_ATTEMPT': 'Toutes les tentatives sont utilisées. La réponse correcte est {0}',
            'INSTRUCTIONS': 'INSTRUCTIONS',
            'DIALOGUE_INSTRUCTIONS':'Instructions',
            'CLOSE_TEST': 'Fermer le test',
            'PRINT': 'Imprimer et télécharger le contenu en document PDF non accessible',
            'CONTENT_TOP': 'Naviguer à la page CONTENU: EN UN COUP D’OEIL',
            'MENU_LEGEND': 'Naviguer à la page LÉGENDE',
            'GO_BACK_TO_STUDY_SECTION': 'Naviguer au Section d\'etude',
            'WHATS_NEW_FRENCH': 'Afficher simultanément l\'anglais et en francais',
            'WHATS_NEW_ENGLISH': 'Afficher seulement en anglais',
            'CANCEL_MODAL': 'Annuler l\'instruction modale',
            'CLICK_HERE': 'Cliquez ici',
            'DIALOGUE_LISTEN_REPEAT_INST': 'Dialogue écouter et répéter se composent de trois boutons: Précédent, Play, Forward.'
            + 'Pour jouer l\'apprenant audio doit naviguer pour jouer bouton enutilisant '
            + 'l\'onglet et appuyez sur Entrée. Pour jouer l\'apprenant audio suivant'
            + 'doit naviguer vers la flèche vers l\'avant et appuyez sur Entrée et '
            + 'ensuite jouer audio.Pour aller à l\'apprenant audio précédent doit '
            + 'naviguer vers la flèche en arrière, appuyez sur Entrée et de jouer audio.'
            + 'Pour naviguer vers n\'importe quel élément, utilisez la touche de '
            + 'tabulation et pour naviguer vers l\'arrière, utilisez la touche shift + tab.',
            'DIALOGUE_AUDIO_QUESTION': 'Lire le texte de la question',
            'CLICK_HERE': 'Cliquez ici',
            'STUDY_SECTION': 'Section d\'étude',
            'STUDYSECTION': 'Section d\'étude',
            'THEORY_VIGNETTE': 'Théorie Vignette',
            'THEORYVIGNETTES': 'Théorie Vignette',
            'TRYIT_STATUS': ' n\'est pas encore commencée',
            'INPROGRESS_STATUS': 'est toujours en cours',
            'SUCCESS_STATUS': 'a été accompli',
            'MAIN_MENU_TITLE': 'Menu principal',
            'PRINT_LABEL_WN' : 'Impression',
            'CONTENT_LABEL_WN' : 'Contenu en bref',
            'LEGEND_LABEL_WN' : 'Légende',
            'STUDY_LABEL_WN' : 'Section d\'étude',
            'LANGUAGE_LABEL_WN' : 'La traduction de la langue',
            'SMALLEST_FONTSIZE': 'Cliquez ici pour que la taille de la police soit la plus petite',
             'SMALL_FONTSIZE': 'Cliquez ici pour que la taille de la police soit petite',
             'MEDIUM_FONTSIZE': 'Cliquez ici pour que la taille de police soit moyenne' ,
             'LARGE_FONTSIZE': 'Cliquez ici pour que la taille de la police soit grande',
             'LARGEST_FONTSIZE':'Cliquez ici pour que la taille de police soit la plus grande',
             'INACTIVE_AUDIO' : 'Cliquez ici pour désactiver la légende dans la section Audio',
             'ACTIVE_AUDIO' : 'Cliquez ici pour activer la légende dans la section Audio',
             'INACTIVE_VIDEO' : 'Cliquez ici pour désactiver la légende dans la section Vidéo',
             'ACTIVE_VIDEO' : 'Cliquez ici pour activer la légende dans la section Vidéo',
             'INACTIVE_IMAGE' : 'Cliquez ici pour désactiver la légende dans la section Image',
             'ACTIVE_IMAGE' : 'Cliquez ici pour activer la légende dans la section Image',
            'DISABLED_BUTTON' : 'bouton est désactivé',
            'RESUMING_WAIT':'Reprise patientez s\'il vous plaît ..',
            'CLOSE_INSTRUCTION_POPUP':'Fermer instruction de dialogue popover',
            'ACCESSIBILITY_MODE_CAPTION_LABEL':'Mode d\'accessibilité',
            'PLAY_AUDIO':'Jouer de l\'audio',
            'PLAY_AUDIO_LAB':'Appuyez sur pour lire l\'audio',
            'IMAGE_QUESTION':'Voir l\'image pour répondre à la question',
            'WARNING_POPUP':'Attention!',
            'WARNING_POPUP_TEXT':'Vous avez été inactif pendant 15 minutes. Si vous ne reprenez pas l\'activité immédiatement, votre session sera terminée.',
            'TIMEDOUT_POPUP':'Au revoir!',
            'TIMEDOUT_POPUP_TEXT':'Votre session est terminée et vos progrès ont été enregistrés. Vous pouvez fermer l\'onglet du navigateur.',
            'THIS':'Cette',
            'DIALOGUES_TITLE':'« Dialogue »',
            'WHATSNEW_TITLE':'« Nouveauté »',
            'ACTIVITIES_TITLE':'« Activité »',
            'QUIZ_TITLE':'« Test final »',
            'INNER_QUIZ_TITLE':'Test final',
            'SUPPORT':'SOUTIEN',
            'SUPPORT_LABEL':'En cliquant sur Soutien, vous serez redirigé vers un site qui n\'est peut-être pas accessible',
            'QUESTION':'Question',
            'I_SPEAK': 'Je parle',
            'SHOW_TRANSLATION':'Afficher la traduction',
            'SHOW_INSTRUCTIONS':'Afficher les instructions',
            'HIDE_INSTRUCTIONS':'Masquer les instructions',
            'SYSTEM_LANGUAGE':'Langue du système',
            'LANGUAGE':'Langue',
            'SETTINGS':'Paramètres',
            'FINAL_QUIZ_SECTION_LABEL':'Section',
            'MINUTE_TAKEN':'Minute',
            'SESSION_ALIVE':'Votre connexion au serveur a été perdue. Nous nous excusons pour le dérangement. Veuillez cliquer sur le bouton ci-dessous pour fermer l\'onglet du navigateur.',
            'CLOSE_TAB':'Fermer l\'onglet du navigateur',
            'YOUR_SCORE':'Ton Score',
            'YOUR_TRIAGE': 'Ton Triage',
            'RESUME_WAIT':'Nous nous excusons pour l\'attente, votre activité est en cours de chargement.',
            'DEFINITION':'Définition',
            'MULTIPLE_TAB':'Veuillez noter qu\'une seule session de module peut être active à un moment donné. Veuillez fermer l\'onglet du navigateur contenant votre session de module active avant d\'essayer de démarrer une nouvelle session Module.',
            'PLACEMENT_TEST':'TEST DE PLACEMENT',
            'SUBMIT':'SOUMETTRE',
			'FRENCH':'FRANÇAIS',
			'AUDIO_PLAY':'L\'AUDIO N\'EST PAS DISPONIBLE',
			'I_UNDERSTAND':'Je comprends',
			'IE_POPUP_TEXT':'Veuillez noter que Internet Explorer est un ancien navigateur qui a été discontinué par Microsoft. Il est possible que vous rencontriez des problèmes en utilisant les modules LRDG sur Internet Explorer. Pour la meilleure expérience d’utilisateur possible, nous vous recommandons d’utiliser les navigateurs Google Chrome, Mozilla Firefox ou Apple Safari (seulement sur MacOs).'
	    },

        'es': {
        'OBJECTIVES':'OBJETIVOS',
        'MAIN_TITLE_OBJECTIVE':'Objetivos',
        'ACCESSIBILITY':'CONFIGURACIÓN',
        'COMMUNICATION_SKILLS':'Habilidades de comunicación',
        'GRAMMATICAL_SKILLS':'Habilidades gramaticales',
        'MODULE': 'Módulo',
        'WHATS_NEW_PANEL_TITLE':'MÓDULO',
        'START': 'COMIENZO',
        'WELCOME':'Bienvenido',
        'SKIP_MAIN':'Skip Main',
        'VERSION':'Versión',
        'SUCCESS':'Cumplido',
        'IN_PROGRESS':'En curso ',
        'TRY_IT': 'No se ha iniciado',
        'LEGEND': 'Leyenda',
        'DIALOGUES':'Diálogos',
        'DIALOGUE': 'Diálogo',
        'ACTIVITY':'Actividades',
        'ACTIVITIES':'Actividades',
        'QUIZ': 'Prueba Final',
        'WHATS_NEW': 'Novedades',
        'WHATSNEW':'Novedades',
        'START_FROM_BEGINNING': 'COMIENZA DESDE EL PRINCIPIO',
        'START_FROM_WHERE_I_LEFT':'RETOMAR',
        'SHOW_IMAGE':'Mostrar imagen',
        'DESCRIPTION':'DESCRIPCIÓN',
        'DESCRIPTION_BUTTON':'Descripción',
        'YOUR_SCORE':'Sus resultados',
        'YOUR_TRIAGE': 'Sus Triage',
        'PREVIOUS':'Anterior',
        'NEXT': 'Siguiente',
        'NEXT_QUESTION':'Próxima pregunta',
        'TRANSLATION':'Traducción',
        'COLOUR_CODED':'Codificado por color',
        'START_TEST':'Iniciar Prueba',
        'CANCEL': 'Cancelar',
        'LAUNCH_RESUME':'Reanudar el lanzamiento',
        'REMAINING_ATTEMPTS':'Otros intentos',
        'UNIT1': 'Unidad 1',
        'UNIT2': 'Unidad 2',
        'UNIT3': 'Unidad 3',
        'UNIT4': 'Unidad 4',
        'UNIT5': 'Unidad 5',
        'UNIT6': 'Unidad 6',
        'UNIT7': 'Unidad 7',
        'UNIT8': 'Unidad 8',
        'UNIT9': 'Unidad 9',
        'UNIT10': 'Unidad 10',
        'UNIT11': 'Unidad 11',
        'SIMULATION': 'Simulación',
        'REVIEW': 'Revisión',
        'OK': 'De acuerdo',
        'NOMENU': 'No hay menú',
        'MAIN_MENU':'MENÚ PRINCIPAL',
        'NEXT_ITEM':'Pasar al siguiente diálogo',
        'NEXT_ACTIVITY': 'Saltar a la siguiente actividad',
        'DISPLAY_THEME':'Tema de visualización',
        'ACCESSIBILITY_MENU':'Menú de accesibilidad',
        'FONT_RATIO':'Tamaño de fuente',
        'CAPTIONS':'Subtítulos',
        'AUDIO_CAPTION_LABEL':'Etiqueta de subtítulos de audio',
        'VIDEO_CAPTION_LABEL':'Etiqueta de subtítulos de vídeo',
        'IMAGE_CAPTION_LABEL':'Etiqueta de subtítulos de imagen',
        'APPLY': 'Aplicar',
        'ACCESSIBILITY_CAPITALIZE':'Accesibilidad',
        'NEXT_UNIT':'Próxima Unidad',
        'SELECT': 'Seleccionar',
        'CORRECT_ANSWER':'{0} es la respuesta correcta',
        'WRONG_ANSWER':'{0} es una respuesta equivocada',
        'WRONG_ANSWER_WITH_FEEDBACK':'{0} es respuesta incorrecta. La respuesta correcta es {1}',
        'WRONG_ANSWER_ZERO_ATTEMPT': 'Se utilizan todos los intentos. La respuesta correcta es {0}',
        'INSTRUCTIONS': 'INSTRUCCIONES',
        'DIALOGUE_INSTRUCTIONS':'Instrucciones',
        'CLOSE_TEST':'Cerrar la prueba',
        'PRINT': 'Guardar y descargar el contenido en un documento PDF no accesible',
        'CONTENT_TOP':'Navegue hasta la página CONTENTS AT A GLANCE',
        'MENU_LEGEND':'Navegue a la página LEGEND',
        'GO_BACK_TO_STUDY_SECTION':'Navegar de nuevo a la sección de estudio',
        'WHATS_NEW_FRENCH':'Mostrar simultáneamente tanto el inglés como el francés',
        'WHATS_NEW_ENGLISH':'Mostrar sólo Español',
        'CANCEL_MODAL':'Cancelar instrucción modal',
        'CLICK_HERE':'Haga clic aquí',
        'DIALOGUE_LISTEN_REPEAT_INST':'El diálogo de escucha y la repetición constan de tres botones: Anterior, Reproducir, Reenviar. '
         + 'Para reproducir audio principiante debe navegar para reproducir el botón mediante la pestaña y pulse'
         + 'Entrar. Para reproducir el siguiente alumno de audio debe navegar hacia adelante flecha y '
         + 'Pulse entrar y luego reproducir audio.para ir al audio anterior el principiante debe navegar'
         + 'A la flecha hacia atrás, pulse Intro y reproduzca audio. Para navegar a cualquier artículo use'
         + 'Y para navegar hacia atrás use la tecla shift + tab.',
        'DIALOGUE_AUDIO_QUESTION':'Lea el texto de la pregunta',
        'STUDY_SECTION':'Sección de estudio',
        'STUDYSECTION':'Sección de estudio',
        'THEORY_VIGNETTE':'Vignette de la teoría',
        'THEORYVIGNETTES':'Vignette de la teoría',
        'TRYIT_STATUS':'aún no ha comenzado',
        'INPROGRESS_STATUS':'está en curso',
        'SUCCESS_STATUS':'se llevó a cabo',
        'MAIN_MENU_TITLE':'Menú principal',
        'PRINT_LABEL_WN':'Impresión',
        'CONTENT_LABEL_WN':'Contenido a la vista',
        'LEGEND_LABEL_WN':'Leyenda',
        'STUDY_LABEL_WN':'Sección de Estudio',
        'LANGUAGE_LABEL_WN':'Traducción de idiomas',
        'SMALLEST_FONTSIZE':'Haga clic aquí para que el tamaño de fuente sea más pequeño',
         'SMALL_FONTSIZE':'Haga clic aquí para que el tamaño de fuente sea pequeño',
         'MEDIUM_FONTSIZE':'Haga clic aquí para que el tamaño de la fuente sea medio',
         'LARGE_FONTSIZE':'Haga clic aquí para que el tamaño de la fuente sea grande',
         'LARGEST_FONTSIZE':'Haga clic aquí para que el tamaño de la fuente sea mayor',
         'INACTIVE_AUDIO':'Haga clic aquí para desactivar el título en la sección Audio',
         'ACTIVE_AUDIO':'Haga clic aquí para activar el subtítulo en la sección Audio',
         'INACTIVE_VIDEO':'Haga clic aquí para desactivar el título en la sección Vídeo',
         'ACTIVE_VIDEO':'Haga clic aquí para activar el título en la sección Video',
         'INACTIVE_IMAGE':'Haga clic aquí para desactivar el título en la sección de imagen',
         'ACTIVE_IMAGE':'Haga clic aquí para activar el título en la sección de imagen',
        'DISABLED_BUTTON':'El botón está deshabilitado',
        'RESUMING_WAIT':'Reanudar por favor espere ..',
        'CLOSE_INSTRUCTION_POPUP':'Cerrar instrucción de diálogo popover',
        'ACCESSIBILITY_MODE_CAPTION_LABEL':'Modo de accesibilidad',
        'PLAY_AUDIO':'Reproducción de audio',
        'PLAY_AUDIO_LAB':'Presione para reproducir audio',
        'IMAGE_QUESTION':'Vea la imagen para contestar la pregunta',
        'WARNING_POPUP':'Advertencia!',
        'WARNING_POPUP_TEXT':'Has estado inactivo durante 15 minutos. Si no reanuda la actividad inmediatamente, la sesión finalizará.',
        'TIMEDOUT_POPUP':'Adiós!',
        'TIMEDOUT_POPUP_TEXT':'Tu sesión ha finalizado y tu progreso se ha guardado. Puede cerrar la pestaña del navegador.',
        'THIS':'Este',
        'DIALOGUES_TITLE':'"Diálogos"',
        'WHATSNEW_TITLE':'"Novedades"',
        'ACTIVITIES_TITLE':'"Actividades"',
        'QUIZ_TITLE':'"Prueba Final"',
        'INNER_QUIZ_TITLE':'Prueba Final',
        'SUPPORT':'SOPORTE',
        'SUPPORT_LABEL':'Al hacer clic en Soporte, se le redirigirá a un sitio que puede no ser accesible',
        'QUESTION':'Pregunta',
        'I_SPEAK': 'Yo hablo',
        'SHOW_TRANSLATION':'Mostrar traducción',
        'SHOW_INSTRUCTIONS':'Mostrar instrucciones',
        'HIDE_INSTRUCTIONS':'Ocultar instrucciones',
         'SYSTEM_LANGUAGE':'Idioma del sistema',
         'LANGUAGE':'Idioma',
         'SETTINGS':'Configuración',
         'FINAL_QUIZ_SECTION_LABEL':'Sección',
         'MINUTE_TAKEN':'Minuto',
         'SESSION_ALIVE':'Su conexión con el servidor se ha perdido. Nos disculpamos por las molestias. Haga clic en el botón a continuación para cerrar la pestaña del navegador.',
         'CLOSE_TAB':'Cerrar la pestaña del navegador',
         'YOUR_SCORE':'Sus Resultados',
         'YOUR_TRIAGE': 'Sus Triage',
         'RESUME_WAIT':'Nos disculpamos por la espera, su actividad se está cargando.',
         'DEFINITION':'Definición',
         'PLACEMENT_TEST':'PRUEBA DE NIVEL',
         'SUBMIT':'ENVIAR',
		 'FRENCH':'FRANCÉS',
		 'AUDIO_PLAY':'EL AUDIO NO ESTÁ DISPONIBLE',
		 'I_UNDERSTAND':'Entiendo',
         'MULTIPLE_TAB':'Tenga en cuenta que solo una sesión del Módulo puede estar activa en cualquier momento. Cierre la pestaña del navegador que contiene su sesión activa del Módulo primero antes de intentar comenzar una nueva sesión del Módulo.',
         'IE_POPUP_TEXT':'Tenga en cuenta que Internet Explorer es un navegador antiguo que ha sido descontinuado por Microsoft. Es posible que tenga problemas al utilizar los módulos LRDG en Internet Explorer. Para la mejor experiencia de usuario posible, recomendamos utilizar los navegadores Google Chrome, Mozilla Firefox o Apple Safari (solo en MacOs).',
        },
        'zh': {
          'OBJECTIVES':'目标',
          'MAIN_TITLE_OBJECTIVE':'目标',
          'ACCESSIBILITY':'设置',
          'SUPPORT':'帮助',
          'MAIN_MENU':'主要菜单',
          'MAIN_MENU_TITLE': '主要菜单',
          'WHATS_NEW': '新知识要点',
          'WHATSNEW': '新知识要点',
          'DIALOGUES': '对话',
          'DIALOGUE': '对话',
          'ACTIVITY': '练习',
          'ACTIVITIES': '练习',
          'QUIZ': '最终测试',
          'SUCCESS': '完成',
          'IN_PROGRESS': '进行中',
          'TRY_IT': '还未开始',
          'LEGEND': 'LEGEND',
          'TRYIT_STATUS': '还没有开始',
          'INPROGRESS_STATUS':'还在进行中',
          'SUCCESS_STATUS':'已经完成了',
          'ACCESSIBILITY_MENU': '辅助功能菜单',
          'FONT_RATIO': '字体大小',
          'CAPTIONS': '文字字幕',
          'AUDIO_CAPTION_LABEL': '音频',
          'VIDEO_CAPTION_LABEL': '视频',
          'IMAGE_CAPTION_LABEL': '图像',
          'APPLY': '运用',
          'CANCEL': '取消',
          'I_SPEAK': '讲解',
          'SYSTEM_LANGUAGE':'系统语言',
          'MODULE': '模块',
          'WHATS_NEW_PANEL_TITLE':'模块',
          'SETTINGS':'设置',
          'LANGUAGE':'语言',
          'INNER_QUIZ_TITLE':'最终测试',
          'SHOW_TRANSLATION':'显示翻译',
          'SHOW_INSTRUCTIONS':'显示说明',
          'HIDE_INSTRUCTIONS':'隐藏说明',
          'START_TEST': '开始测试',
          'FINAL_QUIZ_SECTION_LABEL':'节',
          'QUESTION':'问题',
          'NEXT_QUESTION': '下一个问题',
          'YOUR_SCORE': '你的分数',
          'YOUR_TRIAGE': 'chiness Triage',
          'CLOSE_TEST': '关闭测试',
          'DESCRIPTION': '描述',
          'DESCRIPTION_BUTTON':'描述',
          'START_FROM_BEGINNING': '开始开始',
          'START_FROM_WHERE_I_LEFT': '恢复',
          'SHOW_IMAGE':'显示图像',
          'INSTRUCTIONS': '说明',
          'DIALOGUE_INSTRUCTIONS':'说明',
          'NEXT_ACTIVITY': '转到下一个活动',
          'WARNING_POPUP':'警告!',
          'WARNING_POPUP_TEXT':'你已经不活动了15分钟。 如果您不立即恢复活动，您的会话将会结束。',
          'TIMEDOUT_POPUP':'再见!',
          'TIMEDOUT_POPUP_TEXT':'您的会话已结束，您的进度已被保存。 您可以关闭浏览器标签。',
          'THIS':'这个',
          'DIALOGUES_TITLE':'"对话"',
          'WHATSNEW_TITLE':'"新知识要点"',
          'ACTIVITIES_TITLE':'"练习"',
          'QUIZ_TITLE':'"最终测试"',
          'UNIT1': '单元 1',
          'UNIT2': '单元 2',
          'UNIT3': '单元 3',
          'UNIT4': '单元 4',
          'UNIT5': '单元 5',
          'UNIT6': '单元 6',
          'UNIT7': '单元 7',
          'UNIT8': '单元 8',
          'UNIT9': '单元 9',
          'UNIT10': '单元 10',
          'UNIT11': '单元 11',
          'SIMULATION': '模拟',
          'REVIEW': '评论',
          'DIALOGUE_INSTRUCTIONS':'说明',
          'PLAY_AUDIO':'播放音频',
          'PLAY_AUDIO_LAB':'按播放音频',
          'NEXT_ITEM': '转到下一个对话',
          'START': '开始',
          'LEGEND': '图例',
          'NEXT_UNIT': '下一个单位',
          'STUDY_SECTION': '学习部分',
          'STUDYSECTION': '学习部分',
          'THEORY_VIGNETTE': '理论',
          'THEORYVIGNETTES': '理论',
          'LAUNCH_RESUME': '你想继续你离开的地方吗？',
          'REMAINING_ATTEMPTS': '其餘的嘗試',
          'OK': '是',
          'SESSION_ALIVE':' 您与服务器的连接已丢失。 我们对不便表示抱歉。 请点击下面的按钮关闭浏览器标签。',
          'CLOSE_TAB':'关闭浏览器标签',
          'NOMENU': '不，进入主菜单',
          'PRINT_LABEL_WN' : '打印',
          'CONTENT_LABEL_WN' : '目录内容',
          'LEGEND_LABEL_WN' : '说明',
          'RESUME_WAIT':'我们为等待而感到抱歉，您的活动正在加载。',
          'DEFINITION':'定義',
          'PLACEMENT_TEST':'分班測驗水平測試',
          'SUBMIT':'提交',
          'FRENCH':'法国',
		  'AUDIO_PLAY':'音频不可用',
		  'I_UNDERSTAND':'我明白',
		  'MULTIPLE_TAB':'请注意，在任何给定时间只有一个模块会话可能处于活动状态。 请先关闭包含活动模块会话的浏览器选项卡，然后再尝试开始新的模块会话。',
		  'IE_POPUP_TEXT':'Internet Explorer是Microsoft已停止使用的旧版浏览器。 因此，在使用Internet Explorer的LRDG模块时可能会遇到一些问题。 为了获得最佳用户体验，我们建议您将浏览器切换到Google Chrome，Mozilla Firefox或Apple Safari（仅适用于MacOS 。',
        }
    };


    this.getResources = function (lang) {
        return localizationResources[lang];
    }


});
