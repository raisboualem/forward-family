app.service('xmlDataService', ['$http', 'utilityService', function ($http, utilityService) {
    //Reads xml data file asynchronously and return xml data promise
    this.getXMLData = function (filename) {
        var path = utilityService.getFilePath(filename);
        var defer = $http.get(path);
        return defer;
    };

    //Parse xml data and return JSON object
    this.parseXmlToJSON = function (xml) {
        var json = null;
        if (xml) {
            if (window.DOMParser) {
                try {
                    var dom = (new DOMParser()).parseFromString(xml.data, "text/xml");
                    var json = xml2json(dom);
                    json = json.replace('undefined', '');
                    json = json.replaceAll('DATASTR', '');
                    json = JSON.parse(json);
                }
                catch (e) {
                    json = null;
                }
            }
        }
		
		//SHUFFLE QUESTIONS
		shuffle(json.ACTIVITY.SECTIONS.SECTION);
		
		//SHUFFLE the answer choices
		for (i = 0; i < json.ACTIVITY.SECTIONS.SECTION.length; i++) { 
		  shuffle(json.ACTIVITY.SECTIONS.SECTION[i].QUESTIONS.QUESTION.ANSWERSET.ANSWER);
		 
		}
		shuffle(json.ACTIVITY.SECTIONS.SECTION);
        //console.log(scope.ds.resumeActivity);
        return json;
    };

    String.prototype.replaceAll = function (f, r) {
        return this.split(f).join(r);
    }
	
	function shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }
}]);