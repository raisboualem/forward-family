app.service('templateService',['$http', function($http) {
    this.getTemplateData = function(templateType){
        var path = 'templates/directives/' + templateType + '.html?ts=' + new Date().getTime();
        var defer = $http.get(path);
        return defer;
    }
}]);