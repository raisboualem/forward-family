<!-- Start of lrdgportal Zendesk Widget script -->
/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],
r=document.createElement("iframe");
window.zEmbed=function(){a.push(arguments)},
window.zE=window.zE||window.zEmbed,r.src="javascript:false",
r.title="",
r.role="presentation",
(r.frameElement||r).style.cssText="display: none",
d=document.getElementsByTagName("script"),
d=d[d.length-1],
d.parentNode.insertBefore(r,d),
i=r.contentWindow,
s=i.document;
try{o=s}
catch(e)
{n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',
o=s}o.open()._l=function(){var o=this.createElement("script");
n&&(this.domain=n),
o.id="js-iframe-async",
o.src=e,
this.t=+new Date,
this.zendeskHost=t,
this.zEQueue=a,
this.body.appendChild(o)},
o.write('<body onload="document._l();">'),
o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","lrdgportal.zendesk.com");
/*]]>*/
<!-- End of lrdgportal Zendesk Widget script -->

window.zESettings = {
  webWidget: {
    contactForm: {subject: true },
     offset: { horizontal: '600px' }
     }
};

var url = window.location.search;
var params = url.split('=')[1];
zE(function() {
         zE.setLocale(params);
});

window.onload = function() {
    var textId = document.getElementById('supportText');

    if(textId) {
        if(params == 'en') {
            textId.innerHTML = 'This support form is provided by an external third-party and unfortunately does not meet W3C\'s Web Content Accessibility Guidelines (WCAG). As such, it may not be usable for those with certain disabilities. To those who are unable to use this form, we do apologize and urge you to contact your account manager with your bugs, problems or issues.<br><br> Have you taken a screenshot? If not, please do so before completing this form. A screenshot is necessary in order for us to better resolve any issues.<br><br> Click on the "Support" button to view the form.';
        }
        if(params == 'fr'){
            textId.innerHTML = 'Ce formulaire de support est fourni par un tiers externe et ne respecte malheureusement pas les « Web Content Accessibility Guidelines (WCAG) » du W3C. En tant que tel, il pourrait donc s\'avérer inutilisable pour les personnes ayant certaines incapacités. Pour ceux et celles qui ne sont pas en mesure d\'utiliser ce formulaire, nous nous en excusons et nous vous incitons à contacter votre gestionnaire de compte pour tout problème ou bug. <br><br> Avez-vous pris une capture d\'écran? Sinon, veuillez s\'il-vous-plaît le faire avant de compléter ce formulaire. Une capture d\'écran est nécessaire afin que nous puissions résoudre tout type de problème.<br><br> Cliquez sur le bouton « Assistance » pour afficher le formulaire.';
        }
        if(params == 'es'){
            textId.innerHTML = 'Este formulario de soporte es provisto por un tercero externo y desafortunadamente no cumple con las Pautas de Accesibilidad al Contenido Web (WCAG) del W3C. Como tal, puede no ser útil para personas con ciertas discapacidades. Para aquellos que no pueden usar este formulario, nos disculpamos y les pedimos que se comuniquen con su administrador de cuenta con sus errores, problemas o problemas. <br><br> ¿Has tomado una captura de pantalla? Si no, hágalo antes de completar este formulario. Una captura de pantalla es necesaria para que podamos resolver mejor cualquier problema.<br><br> Haga clic en el botón "Soporte" para ver el formulario.';
        }
        if(params ==  'zh'){
           textId.innerHTML = '这种支持形式由外部第三方提供，不幸的是不符合W3C的网页内容无障碍指南（WCAG）。 因此，对于某些残疾人士可能无法使用。 对于那些无法使用此表单的人，我们表示歉意，并敦促您与您的客户经理联系，解决您的错误，问题或问题。<br><br> 你有没有截图？ 如果没有，请在填写此表格之前填写。 截图是必要的，以便我们更好地解决任何问题。<br><br> 点击“支持”按钮查看表格。';
        }
    }
}



