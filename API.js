window.API = (function(){
    return {
        LMSSavePdf:function(data, fileName, wn){
            var result = "false";
            $.ajax({
                type: "POST",
                url: "http://dev1.lrdgtech.ca/SaveWhatsNewPDFInServer",
                data: {pdf: data, fileName:fileName},
                async: true,
                success: function (t) {//On Successfull service call
                    wn.location.href = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    wn.document.body.innerHTML = '';
                    wn.document.writeln('Some error occured.');
                    result = "false";
                }
            });

            return result;
        },
        LMSInitialize: function() {
            var result = "false";
            $.ajax({
                type: "GET",
                url: "https://www.lrdgtech.ca/LMSInitialize",
                data: null,
                async: false,
                success: function (t) {//On Successfull service call
                    result =  t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    result = "false";
                }
            });

            return result;
        },
        LMSCommit: function() {
            var result = "false";

            $.ajax({
                type: "GET",
                async: false,
                url: "https://www.lrdgtech.ca/LMSCommit",
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    result = "false";
                }
            });

            return result;
        },
        LMSFinish: function() {
            var result = "false";

            $.ajax({
                type: "GET",
                async: false,
                url: "https://www.lrdgtech.ca/LMSFinish",
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    result = "false";
                }
            });
        },
        LMSGetValue: function(model) {
            var result = "false";

            $.ajax({
                type: "GET",
                async: false,
                url: "https://www.lrdgtech.ca/LMSGetValue",
                data: {dataModelElement: model},
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    result =  "false";
                }
            });

            return result;
        },
        LMSSetValue: function(model, value) {
            var result = "false";

            $.ajax({
                type: "GET",
                url: "https://www.lrdgtech.ca/LMSSetValue",
                data: {dataModelElement: model, value: value},
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    result = "false";
                }
            });

            return result;
        },


        LMSSetSuspendValue: function(model, value) {
            var result = "false";

            $.ajax({
                type: "GET",
                async: false,
                url: "https://www.lrdgtech.ca/LMSSetValue",
                data: {dataModelElement: model, value: value},
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    result = "false";
                }
            });

            return result;
        },
        LMSSetValue: function(model, value) {
            var result = "false";

            $.ajax({
                type: "GET",
                url: "https://www.lrdgtech.ca/LMSSetValue",
                data: {dataModelElement: model, value: value},
                success: function (t) {//On Successfull service call
                    result = t;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                    result = "false";
                }
            });

            return result;
        },
        LMSGetLastError: function() {
            return "0";
        },
        LMSGetErrorString: function(errorCode) {
            return "No error";
        },
        LMSGetDiagnostic: function(errorCode) {
            return "No error";
        }
    };
})();













//var addSCORMAPIs = function (window) {
//    if (window) {
//        window.API = {
//            LMSInitialize: LMSInitialize,
//            LMSFinish: LMSFinish,
//            LMSGetValue: LMSGetValue,
//            LMSSetValue: LMSSetValue,
//            LMSCommit: LMSCommit,
//            LMSGetLastError: LMSGetLastError,
//            LMSGetErrorString: LMSGetErrorString,
//            LMSGetDiagnostic: LMSGetDiagnostic
//        }
//    }
//};
//
////API Functions
//function LMSInitialize(initParam) {
//    return true;
//}
//
//function LMSFinish(initParam) {
//    return true;
//}
//
//
//function LMSGetValue(element) {
//var  value;
//    switch (element){
//        case 'cmi.core.student_id':{
//            value = "1";
//            break;
//        }
//        case 'cmi.core.student_name':{
//            value = "Alax";
//            break;
//        }
//    }
//
//    return value;
//}
//
//function LMSSetValue(element, value) {
//    return true;
//}
//
//function LMSCommit(commitParam) {
//    return true;
//}
//
//function LMSGetLastError() {
//    return true;
//}
//
//function LMSGetErrorString(errorCode) {
//    return true;
//}
//
//function LMSGetDiagnostic(errorCode) {
//    return true;
//}
//
//
